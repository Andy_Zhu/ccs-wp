#
# Cookbook Name:: phpapp
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe "apache2"
include_recipe "mysql::client"
include_recipe "mysql::server"
include_recipe "php"
include_recipe "php::module_mysql"
include_recipe "php::module_curl"
include_recipe "apache2::mod_php5"
include_recipe "mysql::ruby"

%w{openssh-server avahi-daemon avahi-discover libnss-mdns git vim ruby-dev tmux curl unzip}.each do |pkg|
  apt_package pkg do
    action :install
  end
end

apache_site "default" do
  enable false
end

mysql_database node['phpapp']['database'] do
  connection (
    {
      :host => 'localhost',
      :username => 'root',
      :password => node['mysql']['server_root_password']
    }
  )
  action :create
end

mysql_database_user node['phpapp']['db_username'] do
  connection (
    {
      :host => 'localhost',
      :username => 'root',
      :password => node['mysql']['server_root_password']
    }
  )
  password node['phpapp']['db_password']
  database_name node['phpapp']['database']
  privileges [:all]
  host node['phpapp']['db_user_host']
  action :grant
end

mysql_database_user node['phpapp']['db_username'] do
  connection (
    {
      :host => 'localhost',
      :username => 'root',
      :password => node['mysql']['server_root_password']
    }
  )
  password node['phpapp']['db_password']
  database_name node['phpapp']['database']
  privileges [:all]
  host '%'
  action :grant
end

directory node["phpapp"]["path"] do
  owner "root"
  group "root"
  mode "0777"
  action :create
  recursive true
end

web_app 'phpapp' do
  template 'site.conf.erb'
  docroot node['phpapp']['path']
  server_name node['phpapp']['server_name']
end

# use the WP latest example to get the file SQL 
# file below (# http://gettingstartedwithchef.com/first-steps-with-chef.html
# http://testcms.chinacarservice.com/latestcms.sql

testcms_latest = Chef::Config[:file_cache_path] + "/latestcms.sql"

remote_file testcms_latest do
  source "http://testcms.chinacarservice.com/latestcms.sql"
  mode "0644"
end

execute "mysql-install-privileges" do
  command "mysql -u root -e \"GRANT ALL ON *.* to 'carmageddon'@'%' IDENTIFIED BY 'ccsTESTcms'\""      
  command "mysql -u root -e \"GRANT ALL ON *.* to 'carmageddon'@'localhost' IDENTIFIED BY 'ccsTESTcms'\""      
end

execute "install-latest-sql" do
  #command "mysql -u carmageddon -pccsTESTcms carmageddon < " + testcms_latest
  command "mysql -u root carmageddon < " + testcms_latest
end

execute "deploy db config" do
  command "cp /var/www/carmageddon/database-sample.php /var/www/carmageddon/application/config/database.php"
end

# convert above script to use more modular style below:
# then import it below
#mysql_database "DB_NAME" do
#  connection mysql_connection_info
#  sql "source /tmp/chef-solo/site-cookbooks/main/path/to/sql_dump.sql;"
#end
