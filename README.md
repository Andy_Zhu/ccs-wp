What is Carmageddon?

An introduction to China Car Service’s CMS

Carmageddon is the codename for China Car Service’s Content Management System, which powers almost 100 of our websites throughout Asia.

Bitbucket:  [https://bitbucket.org/leonstafford/gaeccsbookings](https://bitbucket.org/leonstafford/gaeccsbookings)

Trello:     [https://trello.com/b/FsHzFuHs/carmageddon-ccs-cms](https://trello.com/b/FsHzFuHs/carmageddon-ccs-cms)

Production: [http://carmageddon.chinacarservice.com](http://carmageddon.chinacarservice.com)

Staging:    [http://testcms.chinacarservice.com](http://testcms.chinacarservice.com)

Technologies used are:

* CodeIgniter

* MySQL

* jQuery

On top of a MediaTemple VPS running:

* CentOS 6 x86_64

* Apache 2.2

* MySQL 5.5

* PHP 5.4

* Perl 5.10

* Python 2.6

* Ruby 1.8

* Postfix 2.8

* Plesk Panel 11

* YUM package management

Originally developed around 2011, it allows CCS staff to manage the text content, vehicle inventory and pricing for all our various client-facing websites, including our "parent market" sites like chinacarservice.com and asiacarservice.com, along with our “sub market” sites such as beijingcarservice.com and seoulcarservice.com.

Sites are "published" via FTP as static sites, similar to the MovableType platform. This is done for performance, security and ease of management.

As a developer, you will have access to your local carmageddon development  environment, along with a fully featured staging environment at [http://testcms.chinacarservice.com](http://testcms.chinacarservice.com/)

Both will allow administrator access with the following credentials:

**Username** 

test@test.com

**Password**

ccsTESTcms

* * *


Setting up your local development environment

There is a mostly automated process to get your Carmageddon environment setup here: (requires Google Drive access to CCS Developer Documentation)

https://docs.google.com/document/d/1sIakdmXay-C138hoculiaqkjEyS_5m1GAABRFLKLVCU/edit?usp=sharing


Development Procedure (Carmageddon)

For Developers

Pull/fetch from origin religiously, before any commits, merges, etc.


(on local)

git pull origin master

**Do some work**

sed -i 's/retarded_code/awesome_ode/g' ./app/main_controller.php

**If there are any db migrations required**, you should create migration file in {project_root}/migrations. Migration file name should only valid raw-sql code. It is good idea to create migration and apply it on your development environment instead of manually changing db. Migration can be applied from web-view at url /index.php/migrate/visual Check if your migration applies successfully before commit.

**Commit and push your changes**

(on local)

git pull origin master

git status

*sanity check*

git add .

git commit -m "fixed issue with conflicting jQuery version sitewide"

git push origin master

* * *


Deployment Procedure (Carmageddon)

**1. Dev deploys master to staging and performs smoke test**

**2. Upon verification, TL moves to "On Staging" list in Trello**

**3. Leon Stafford currently responsible for live deployments

**4. Leon moves deployed cards to "Deployed to LIVE" in Trello**

*** if any bugs found in LIVE deployment, Team Lead/PO creates cards for any bugs found and contacts dev responsible* Bugs raised in Trello have Red label applied.


