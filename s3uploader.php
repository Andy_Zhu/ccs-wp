<?php
/*
 * CCS S3 Uploader
 * ===============
 * This expects a site id (int) and site name (string)
 * as command line arguments.
 * 
 * ie, s3uploader.php 2 'Beijing Car Service
 */

require 'vendor/autoload.php';
use Aws\S3\S3Client;

/* User account with read/write access to CCS S3 and CF accounts only for use in development
 * TODO: separate this out into a config file and use different one with production 
 * for easy revokation/investigate best practice
 */

$aws_credentials = array(
    'key'    => $argv[3],
    'secret' => $argv[4]
);

$client = S3Client::factory($aws_credentials);

$objects_to_push_to_s3 = array();
$paths_to_invalidate = array();
$site_id = $argv[1];
$bucket = $argv[5];
$di = new RecursiveDirectoryIterator('/tmp/data/' . $site_id . '/fors3/');

foreach (new RecursiveIteratorIterator($di) as $filename => $file) {

    $filename = str_replace('/tmp/data/' . $site_id . '/fors3/', '', $filename);
    $full_filepath = '/tmp/data/' . $site_id . '/fors3/' . $filename;
    
    if (!preg_match('/(\/|\.)$/i', $full_filepath)){

        $client->putObject(array(
            'Bucket'     => $bucket,
            'Key'        => $filename,
            'SourceFile' => $full_filepath
        ));

        $paths_to_invalidate[] = $filename;
    }
}



// TODO: Store CF Distribution ID in CMS
// or grab from the known origin naming
$cloudfront_distribution_id = $argv[6];
$access_key = $aws_credentials['key'];
$secret_key = $aws_credentials['secret'];
$epoch = date('U');
$paths_to_invalidate_nodes = '';

foreach ($paths_to_invalidate as $path){
    $path = str_replace("'", '', $path);
    $paths_to_invalidate_nodes .=  "<Path>/$path</Path>";
}

$paths_to_invalidate_nodes = str_replace('"', '', $paths_to_invalidate_nodes);

// TODO: Store CF Distribution ID in CMS
// or grab from the known origin naming
$cloudfront_distribution_id = $cloudfront_distribution_id;
$epoch = date('U');

$xml = <<<EOD
<InvalidationBatch>
    {$paths_to_invalidate_nodes}
    <CallerReference>{$cloudfront_distribution_id}{$epoch}</CallerReference>
</InvalidationBatch>
EOD;

$len = strlen($xml);
$date = gmdate('D, d M Y G:i:s T');
$sig = base64_encode(
    hash_hmac('sha1', $date, $secret_key, true)
);
 
$msg = "POST /2010-11-01/distribution/{$cloudfront_distribution_id}/invalidation HTTP/1.0\r\n";
$msg .= "Host: cloudfront.amazonaws.com\r\n";
$msg .= "Date: {$date}\r\n";
$msg .= "Content-Type: text/xml; charset=UTF-8\r\n";
$msg .= "Authorization: AWS {$access_key}:{$sig}\r\n";
$msg .= "Content-Length: {$len}\r\n\r\n";
$msg .= $xml;
 
$fp = fsockopen('ssl://cloudfront.amazonaws.com', 443, 
    $errno, $errstr, 30
);
if (!$fp) {
    die("Connection failed: {$errno} {$errstr}\n");
}


fwrite($fp, $msg);
$resp = '';
while(! feof($fp)) {
    $resp .= fgets($fp, 1024);
}
fclose($fp);
echo $resp;
