<?php
$lang_ci =& get_instance();
$lang_ci->load->model('Translations_Model');

$translations = $lang_ci->Translations_Model->get_translations(array('language'=>'chinese'));

foreach ($translations as $language_data) {
    $lang[$language_data->label] = $language_data->translation;
}