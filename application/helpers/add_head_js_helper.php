<?php
/**
 * Helper to append javascript to head from different views
 *@author Omar TCHOKHANI <omatcho02@gmail.com> <omatcho@odesk.com>
 *
 */
function add_head_js($path = FALSE) {
	
	static $js;
	
	if ($path) {
		$js .= '<script src="/javascript/' . $path .'" type="text/javascript"></script>';
	}else {
		return $js;
	}
}

function get_head_js() {
	return add_head_js();
}

?>