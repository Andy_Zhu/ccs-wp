<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('lang')) {
	function lang($line, $default = '', $id = '') {

		$CI =& get_instance();
		$CI->load->model('Translations_Model');

		$english_translation = $CI->Translations_Model->get_english_translation($line);

		$df = $default ? $default : $english_translation;
		$label = $line;
		$line = $CI->lang->line($line) ? $CI->lang->line($line) : $df;
		$line = $line ? $line : $label;


		if ($id != '') {
			$line = '<label for="' . $id . '">' . $line . "</label>";
		}
		return $line;
	}
}

if (!function_exists('lang_small')) {
	function lang_small($id) {
		$CI =& get_instance();
		$CI->load->model('Languages_Model');
		return $CI->Languages_Model->get_small_flag_path($id);
	}
}