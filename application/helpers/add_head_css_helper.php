<?php
/**
 * Helper to append javascript to head from different views
 *@author Omar TCHOKHANI <omatcho02@gmail.com> <omatcho@odesk.com>
 *
 */
function add_head_css($path = FALSE) {
	
	static $css;
	
	if ($path) {
		$css .= '<link rel="stylesheet" href="/css/' . $path .'" type="text/css" />';
	}else {
		return $css;
	}
}

function get_head_css() {
	return add_head_css();
}

?>