<?php
/*
 * creates a sliding pagination markup
 * @author Omar TCHOKHANI <omatcho02@gmail.com> <omatcho@odesk.com>
 *
 */
function pagination($paginator, $inUrl, $appendPage= true, $query=false) {
		
		$numofpages = $paginator['pages'];
		$page_num = $paginator['page'];
		
		$inUrl = rtrim($inUrl, '/');
		if($appendPage){
			if(!$query) {
				$inUrl .= '/page/';
			}else{
				if(strpos($inUrl, '?') !== FALSE) {
					$inUrl = rtrim($inUrl, '&') . '&page=';
				}else{
					$inUrl .= "?page=";
				}
			}
		}else{
			$inUrl .='/';
		}
		
		$page_pagination = '';
		if ($numofpages > '1' ) {


            $range =8;
            $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
            $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
            $page_min = $page_num- $range_min;
            $page_max = $page_num+ $range_max;

            $page_min = ($page_min < 1) ? 1 : $page_min;
            $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
            if ($page_max > $numofpages) {
                $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                $page_max = $numofpages;
            }

            $page_min = ($page_min < 1) ? 1 : $page_min;

            //$page_content .= '<p class="menuPage">';

            if ( ($page_num > ($range - $range_min)) && ($numofpages > $range) ) {
                $page_pagination .= '<a   title="First" href="'. $inUrl . '1">|&lt;</a> ';
            }

            if ($page_num != 1) {
                $page_pagination .= '<a title="Previous" href="'. $inUrl . ($page_num-1).'">&lt;</a> ';
            }

            for ($i = $page_min;$i <= $page_max;$i++) {
                if ($i == $page_num)
                $page_pagination .= '<span class="current">' . $i . '</span> ';
                else
                $page_pagination .= '<a class="num" href="'. $inUrl . $i . '">'.$i.'</a> ';
            }

            if ($page_num < $numofpages) {
                $page_pagination.= ' <a title="Next" class="num" href="'.  $inUrl . ($page_num+1) . '">&gt;</a>';
            }


            if (($page_num< ($numofpages - $range_max)) && ($numofpages > $range)) {
                $page_pagination .= ' <a class="num" title="Last" href="'. $inUrl . $numofpages. '">&gt;|</a> ';
            }

            return $page_pagination;
        }//end if more than 1 page 
        return '';
}
?>