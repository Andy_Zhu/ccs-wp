<?php
function site_url($uri = ''){

	$uri = trim($uri, '/');
	$CI =& get_instance();

	$uri = 'index.php/' . $uri;

	return $CI->config->site_url($uri);
}

function seo_name($string) {

	//Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	$string = strtolower($string);
	//Strip any unwanted characters
	$string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	//Clean multiple dashes or whitespaces
	$string = preg_replace('/[\s-]+/', " ", $string);
	//Convert whitespaces and underscore to dash
	$string = preg_replace('/[\s_]/', "-", $string);

	return trim($string, ' -');

}