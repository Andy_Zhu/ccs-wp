<?php
function vehicle_image_path($filename)
{
	/*static $SLIDE_SHOWS_DIR = './images/slideshows';
	static $vehicleS_THUMB_DIR = './images/vehicles/thumbs';
	static $vehicleS_IMAGES_DIR = './images/vehicles/images';
	*/

	static $dir = './images/vehicles/images';

	return $dir . '/' . $filename;
}

function vehicle_image_thumb_path($filename)
{
	static $thumbs_dir = './images/vehicles/images';

	$name_parts = explode('.', $filename);
	$ext = array_pop($name_parts);
	$thumb_name = implode('.', $name_parts) . '_thumb.' . $ext;

	return $thumbs_dir . '/' . $thumb_name;
}

function slideshow_image_path($filename)
{

	static $dir = './images/slideshows';

	return $dir . '/' . $filename;
}

function vehicle_image_src($filename, $preview = true)
{
	$filepath = 'images/vehicles/images/' . $filename;
	return IMG_PATH . 'vehicles/images/' . $filename . '?cb=' . (file_exists($filepath) ? filemtime($filepath) : '');
	/*if($preview) {
		return IMG_PATH . 'vehicles/images/' . $filename;
	}

	return 'images/' . $filename;*/
}

function textbox_image_path($filename)
{
	static $dir = './images/textboxes';
	return $dir . '/' . $filename;
}

function textbox_image_src($filename, $preview = true)
{
	$filepath = 'images/textboxes/' . $filename;
	$cb = (file_exists($filepath) ? filemtime($filepath) : '');
	if ($preview) {
		return IMG_PATH . 'textboxes/' . $filename . '?cb=' . $cb;
	}
	return '/images/' . $filename . '?cb=' . $cb;
}

function banner_image_src($filename, $preview = true)
{
  $filepath = 'images/banners/' . $filename;
  $cb = (file_exists($filepath) ? filemtime($filepath) : '');
  if ($preview) {
    return IMG_PATH . 'banners/' . $filename . '?cb=' . $cb;
  }
  return '/images/' . $filename . '?cb=' . $cb;
}

function vehicle_image_thumb_src($filename, $preview = true)
{
	$name_parts = explode('.', $filename);
	$ext = array_pop($name_parts);
	$thumb_name = implode('.', $name_parts) . '_thumb.' . $ext;

	$filepath = 'images/vehicles/images/' . $thumb_name;
	return IMG_PATH . 'vehicles/images/' . $thumb_name . '?cb=' . (file_exists($filepath) ? filemtime($filepath) : '');

	/*if($preview) {
		return IMG_PATH . 'vehicles/images/' . $thumb_name;
	}

	return 'images/' . $thumb_name;*/
}

function slideshow_image_src($filename, $preview = true)
{
	$filepath = 'images/slideshows/' . $filename;
	$cb = file_exists($filepath) ? filemtime($filepath) : '';
	if (in_array($filename, array('default-slide-1.jpg', 'default-slide-2.jpg', 'default-slide-3.jpg')))

		return IMG_PATH . 'slideshows/' . $filename . '?cb=' . $cb;
	if ($preview) {
		return IMG_PATH . 'slideshows/' . $filename . '?cb=' . $cb;
	}

	return '/images/' . $filename . '?cb=' . (file_exists($filepath) ? filemtime($filepath) : '');
}

function vehicle_image_single_thumb_src($filename, $preview = true)
{
	$filepath = 'images/vehicles/thumbs/' . $filename;
	$cb = (file_exists($filepath) ? filemtime($filepath) : '');
	if ($preview) {
		return IMG_PATH . 'vehicles/thumbs/' . $filename . '?cb=' . $cb;
	}

	return '/images/' . $filename . '?cb=' . $cb;
}

function vehicle_image_single_thumb_path($filename)
{
	return './images/vehicles/thumbs/' . $filename;
}


function booking_background_path($filename)
{
  return '/images/background/' . $filename;
}


function site_logo_path($filename)
{
	return './images/logos/' . $filename;
}

function site_logo_src($filename, $preview = true)
{
	if ($preview) {
		return IMG_PATH . 'logos/' . $filename . '?cb=' . filemtime('images/logos/' . $filename);
	}

	return '/images/' . $filename . '?cb=' . filemtime('images/logos/' . $filename);
}

function tour_image_path($filename)
{
	return './images/tours/' . $filename;
}

function tour_image_src($filename, $preview = true)
{
	if ($preview) {
		return IMG_PATH . 'tours/' . $filename . '?cb=' . filemtime('images/tours/' . $filename);
	}

	return '/images/' . $filename . '?cb=' . filemtime('images/tours/' . $filename);
}