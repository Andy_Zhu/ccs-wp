<?php
function clone_get_sections($site_id, $page_uri) {

	$ci =& get_instance();

	$ci->load->model('Websites_Model');
	$ci->load->model('Text_Boxes_Model');
  $ci->load->model('Text_Boxes_Categories_Model');
	$ci->load->model('Website_Vehicles_Model');
	$ci->load->model('Vehicles_Model');
	$ci->load->model('Static_Pages_Model');
	$ci->load->model('Websites_Tours_Model');
	$ci->load->model('Tours_Images_Model');
	$ci->load->model('Slideshow_Images_Model');
	$ci->load->model('Website_Banners_Model');
  $ci->load->model('Booking_Services_Model');

	$site = $ci->Websites_Model->find($site_id);

	$data = array(
		'section'=>$page_uri,
		'site'=>$site
	);


	if(in_array($page_uri, array('contact-us', 'about-us', 'rates','fleet', 'services', 'online-booking', 'home'))) {
		$boxes = $ci->Text_Boxes_Model->get_boxes(array('website_id'=>$site_id, 'page_uri'=>$page_uri));
		$data['boxes'] = $boxes;
	}

  if($page_uri == 'locations'){
    $data['boxes'] = $ci->Text_Boxes_Categories_Model->get_cats(array('website_id' => $site_id, 'section' => 'locations'));
  }

  if($page_uri == 'booking-services'){
    $data['boxes'] = $ci->Booking_Services_Model->get_booking_services(array('website_id' => $site_id));
  }

	if($page_uri == 'fleet') {
		$website_vehicles = $ci->Website_Vehicles_Model->get_vehicles(array('website_id'=>$site_id));
		$v_ids = array();
		foreach ($website_vehicles as $vehicle) {
			$v_ids[$vehicle->vehicle_id] = $vehicle->vehicle_id;
		}
		$vehicles = empty($v_ids) ? array() : $ci->Vehicles_Model->get_vehicles(array('website_id'=>$site_id, 'vehicle_id'=>$v_ids));
		$data['website_vehicles'] = $website_vehicles;
		$data['vehicles'] = $vehicles;
	}

	if(in_array($page_uri, array('static-pages'))) {
		$static_page = $ci->Static_Pages_Model->get_pages(array('website_id'=>$site_id));
		$data['static_pages'] = $static_page;
	}

	if($page_uri == 'tours') {
		$data['tours'] = $ci->Websites_Tours_Model->get_tours(array('website_id'=>$site_id));
	}

	if($page_uri == 'home') {
		$data['slideshow_images'] = $ci->Slideshow_Images_Model->get_images(array('website_id'=>$site->id));
	}

	if($page_uri == 'banners') {
		$data['banners'] = $ci->Website_Banners_Model->get_banners(array('website_id'=>$site_id));
	}

	return $ci->load->view('admin/partials/ajax_clone_sections', $data, TRUE);
}


function clone_do_save($data) {
	$ci =& get_instance();

	$ci->load->model('Websites_Model');
	$ci->load->model('Text_Boxes_Model');
  $ci->load->model('Text_Boxes_Categories_Model');
	$ci->load->model('Website_Vehicles_Model');
	$ci->load->model('Websites_Tours_Model');
	$ci->load->model('Tours_Images_Model');
	$ci->load->model('Slideshow_Images_Model');
	$ci->load->model('Static_Pages_Model');
	$ci->load->model('Logs_Model');
	$ci->load->model('Users');
	$ci->load->model('Website_Banners_Model');
  $ci->load->model('Booking_Services_Model');

	$site = $ci->Websites_Model->find($data['site_id']);
	$section = $data['page'];

	//what to be cloned
	if($section != 'locations' && $section != 'booking-services'){
	  $boxes = isset($data['boxes_ids']) ? $ci->Text_Boxes_Model->get_boxes(array('box_id'=>$data['boxes_ids'])) : array();
  }elseif($section == 'booking-services'){
    $booking_boxes = isset($data['boxes_ids']) ? $ci->Booking_Services_Model->get_booking_services(array('box_id'=>$data['boxes_ids'])) : array();
	}elseif($section == 'locations'){
	  $cat_boxes = isset($data['boxes_ids']) ? $ci->Text_Boxes_Categories_Model->get_cats(array('cat_id'=>$data['boxes_ids'], 'section'=>'locations')) : array();
    $cat_ids = array_map(function($o) { return $o->id; }, $cat_boxes);
    $boxes = isset($cat_boxes) ? $ci->Text_Boxes_Model->get_boxes(array('cat_id'=>$cat_ids, 'page_uri'=>'locations', 'website_id'=>$site->id)) : array();
  }

	//fleet
	$site_vehicles = empty($data['vehicle_ids']) ? array() : $ci->Website_Vehicles_Model->get_vehicles(array('website_id'=>$site->id, 'vehicles_id'=>$data['vehicles_ids']));
	//tours
	$tours = empty($data['tours_ids']) ? array() : $ci->Websites_Tours_Model->get_tours(array('website_id'=>$site->id, 'tour_id'=>$data['tours_ids']));
	$tours_images = empty($tours) ? array() : $ci->Tours_Images_Model->get_tours_images(array('tour_id'=>array_keys($tours), 'group'=>'tour_id'));
	//static pages
	$static_pages = isset($data['static_pages_ids']) ? $ci->Static_Pages_Model->get_pages(array('page_id'=>$data['static_pages_ids'])) : array();
	//home page
	$slideshow_images = isset($data['slideshow_images_ids']) ? $ci->Slideshow_Images_Model->get_images(array('website_id'=>$site->id, 'image_id'=>$data['slideshow_images_ids'])) : array();
	//banners
	$banners = isset($data['banners_uris']) ? $ci->Website_Banners_Model->get_banners(array('website_id'=>$site->id, 'page_uri'=>$data['banners_uris'])) : array();

	//end of what to be cloned!

		//begin cloning
	foreach ($data['targets'] as $target_site_id) {

		$target_vehicles = empty($site_vehicles) ? array() : $ci->Website_Vehicles_Model->get_vehicles(array('website_id'=>$target_site_id));
		$target_vehicle_ids = array();
		foreach ($target_vehicles as $tv) {
			$target_vehicle_ids[] = $tv->vehicle_id;
		}

    $target_cats = empty($cat_boxes) ? array() : $ci->Text_Boxes_Categories_Model->get_cats(array('website_id'=>$target_site_id, 'section'=>'locations' ));
    $target_cat_titles = array();
    foreach ($target_cats as $cat) {
      $target_cat_titles[] = $cat->title;
    }
    if($section != 'booking-services'){
		//clone text boxes
  		foreach ($boxes as $box) {

  			if(!$box->title) {
  				$exists = $ci->Text_Boxes_Model->find(array('content'=>$box->content, 'image'=>$box->image, 'page_uri'=>$section, 'website_id'=>$target_site_id));
  			}else{
  				$exists = $ci->Text_Boxes_Model->find(array('title'=>$box->title, 'image'=>$box->image, 'page_uri'=>$section, 'website_id'=>$target_site_id));
  			}

  			if($exists) {
  				//echo $target_site_id, '<br />';
  				//echo '<pre>', print_r($exists), '</pre>';
  			}else{

  				$ci->Text_Boxes_Model->create(array(
  					'website_id'=>$target_site_id,
  					'page_uri'=>$section,
  					'content'=>$box->content,
  					'title'=>$box->title,
  					'type'=>$box->type,
  					'ranking'=>$box->ranking,
  					'cat_id'=>$box->cat_id,
  					'extra'=>$box->extra,
  					'image'=>$box->image
  				));
  			}
  		}
    }else{
      foreach($booking_boxes as $box){
        $exists = $ci->Booking_Services_Model->find(array('website_id'=>$target_site_id, 'service_name' => $box->service_name));
        if($exists) {
          //echo $target_site_id, '<br />';
          //echo '<pre>', print_r($exists), '</pre>';
        }else{

          $ci->Booking_Services_Model->create(array(
            'website_id'=>$target_site_id,
            'service_name'=>$box->service_name,
            'active'=>$box->active,
            'airport'=>$box->airport,
            'pickup'=>$box->pickup,
            'dayhire'=>$box->dayhire,
            'tour'=>$box->tour,
            'ranking'=>$box->ranking,
            'form_name'=>$box->form_name,
            'description'=>$box->description
          ));
        }
      }
    }
    //clone text_box_categories for locations

    foreach($cat_boxes as $cat){
      if(in_array($cat->title, $target_cat_titles)) continue; //already there
      $new_cat_id = $ci->Text_Boxes_Categories_Model->create(array(
        'website_id'=>$target_site_id,
        'section'=>$section,
        'title'=> $cat->title,
        'ranking'=>$cat->ranking,
      ));
      $new_text_boxes = $ci -> Text_Boxes_Model -> get_boxes(array('cat_id' => $cat->id, 'website_id'=>$target_site_id, 'page_uri'=>'locations'));
      foreach($new_text_boxes as $new_box){
        $ci -> Text_Boxes_Model -> update(array('cat_id'=> $new_cat_id), $new_box -> id);
      }
    }

		//clone vehicles for fleet page

		foreach ($site_vehicles as $vehicle) {
			if(in_array($vehicle->vehicle_id, $target_vehicle_ids)) continue; //already there
			$ci->Website_Vehicles_Model->create(array(
				'website_id'=>$target_site_id,
				'vehicle_id'=>$vehicle->vehicle_id,
				'half_day_price'=>$vehicle->half_day_price,
				'full_day_price'=>$vehicle->full_day_price,
				'extra_hours_price'=>$vehicle->extra_hours_price,
				'extra_km_price'=>$vehicle->extra_km_price,
				'other_info'=>$vehicle->other_info,
				'ranking'=>$vehicle->ranking
			));
		}

		//clone tours

		foreach ($tours as $tour) {

			$tour_exists = $ci->Websites_Tours_Model->find(array('title'=>$tour->title, 'website_id'=>$target_site_id));

			if($tour_exists) {

			}else{

				$new_tour_id = $ci->Websites_Tours_Model->create(array(
					'website_id'=>$target_site_id,
					'title'=>$tour->title,
					'price'=>$tour->price,
					'duration'=>$tour->duration,
					'notes'=>$tour->notes,
					'ranking'=>$tour->ranking
				));
				//clone tour images if any
				if(array_key_exists($tour->id, $tours_images) && $new_tour_id) {

					foreach ($tours_images[$tour->id] as $image) {
						$ci->Tours_Images_Model->create(array(
							'tour_id'=>$new_tour_id,
							'caption'=>$image->caption,
							'filename'=>$image->filename
						));
					}
				}
				//end of images clone
			}
		}

		//clone static pages

		foreach ($static_pages as $page) {

			$exists = $ci->Static_Pages_Model->find($target_site_id, $page->uri);

			if($exists) {
				$ci->Static_Pages_Model->update(array(
					'content'=>$page->content,
					'name'=>$page->name
				), $page->uri, $target_site_id);
			}else{
				$ci->Static_Pages_Model->create(array(
					'website_id'=>$target_site_id,
					'uri'=>$page->uri,
					'content'=>$page->content,
					'name'=>$page->name
				));
			}
		}

		//clone home page sections
		foreach ($slideshow_images as $slide) {

			$exists = $ci->Slideshow_Images_Model->is_set($target_site_id, $slide->slide_id);
			if($exists) {

			}else{
				$ci->Slideshow_Images_Model->create(array(
					'website_id'=>$target_site_id,
					'slide_id'=>$slide->slide_id,
					'filename'=>$slide->filename,
					'orig_name'=>$slide->orig_name,
					'date_created'=>date('Y-m-d')
				));
			}
		}
		//clone home page about
		if(isset($data['home_about'])) {
			$target_site = $ci->Websites_Model->find($target_site_id);
			if($target_site->short_about) {

			}else{
				$ci->Websites_Model->update(array('short_about'=>$site->short_about), $target_site->id);
			}
		}

		//clone banners
		foreach ($banners as $banner) {

			$new_banner = array(
				'website_id'=>$target_site_id,
				'content'=>$banner->content,
				'enabled'=>$banner->enabled,
				'styles'=>$banner->styles,
				'user_id'=>$ci->Users->get_identity()->id,
				'page_uri'=>$banner->page_uri
			);

			if($ci->Website_Banners_Model->find($target_site_id, $banner->page_uri)) {
				$ci->Website_Banners_Model->update($target_site_id, $banner->page_uri, $new_banner);
			}else{
				$ci->Website_Banners_Model->create($new_banner);
			}
		}

		//log the clone action

		$ci->Logs_Model->create(array(
			'website_id'=>$target_site_id,
			'user_id'=> $ci->Users->get_identity()->id,
			'page_uri'=>$section,
			'content'=> 'Section(s) cloned from: ' . $site->name
		));

	}

}