<?php

function template_name_to_seo_link($string) {
	//Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	$string = strtolower($string);
	//Strip any unwanted characters
	$string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	//Clean multiple dashes or whitespaces
	$string = preg_replace('/[\s-]+/', " ", $string);
	//Convert whitespaces and underscore to dash
	$string = preg_replace('/[\s_]/', "-", $string);

	return trim($string, ' -');
}

function template_css_src($filename, $preview = true, $template = "default") {

	if ($preview) {
		return SITE_ROOT . 'application/views/templates/' . $template . '/css/' . $filename . '?cb=' . filemtime('application/views/templates/' . $template . '/css/' . $filename);
	}

	return "/css/$filename" . '?cb=' . filemtime('application/views/templates/' . $template . '/css/' . $filename);
}

function template_js_src($filename, $preview = true, $template = "default") {

	if ($preview) {
		return SITE_ROOT . 'application/views/templates/' . $template . '/js/' . $filename . '?cb=' . filemtime('application/views/templates/' . $template . '/js/' . $filename);
	}

	return "/js/$filename" . '?cb=' . filemtime('application/views/templates/' . $template . '/js/' . $filename);
}


function template_image_src($filename, $preview = true, $template = "default") {

	if ($preview) {
		return SITE_ROOT . 'application/views/templates/' . $template . '/images/' . $filename . '?cb=' . filemtime('application/views/templates/' . $template . '/images/' . $filename);
	}

	return "/images/$filename" . '?cb=' . filemtime('application/views/templates/' . $template . '/images/' . $filename);
}


function template_link($site_id, $section, $preview=true) {
	if($preview && $section != 'contact-us' && $section != 'booking-form' && $section != 'enquire-form' && $section != 'about-contact-form') {
		return site_url('admin/preview') . "?id=$site_id&section=$section";
	}
  if($section == 'booking-form' || $section == 'enquire-form'){
    $quote = "";
    if( $section == 'enquire-form'){
        $quote = '/quote';
      }
    if($preview){
      return "http://testbookingforms.chinacarservice.com/booking/beijing{$quote}";
    }else{
      if (is_object($site_id)) {
        $site = $site_id;
        $market = isset($site->shortname) ? $site->shortname : '';
        $country = $site->country_id;
        if ($country == "9" || $country == "1") {
          $parent_market = "asia";
        } else {
          $parent_market = "china";
        }
        if($site -> lang_id == 2){
          $contactus_url = "https://royal-asia-limo.com/booking/{$market}{$quote}";
        }else{
          $contactus_url = "https://{$parent_market}carservice.com/booking/{$market}{$quote}";
        }
        return $contactus_url;
      }
    }
  }

  if($section == 'about-contact-form'){
    // if($preview){
    //   return "http://testbookingforms.chinacarservice.com/booking/mailer/contact/";
    // }else{
    if (is_object($site_id)) {
      $site = $site_id;
      $market = isset($site->shortname) ? $site->shortname : '';
      $country = $site->country_id;
      if ($country == "9" || $country == "1") {
        $parent_market = "asia";
      } else {
        $parent_market = "china";
      }
      if($site -> lang_id == 2){
        $contactus_url = "https://royal-asia-limo.com/booking/mailer/contact/";
      }else{
        $contactus_url = "https://{$parent_market}carservice.com/booking/mailer/contact/";
      }
      return $contactus_url;
    }
    // }
  }

	if($section == 'home') return 'index.html';
	if($section == 'contact-us') {
		if (is_object($site_id)) {
			$site = $site_id;
			$market = isset($site->shortname) ? $site->shortname : '';
			//strtolower(trim(str_ireplace('Car Service', '', $site->name)));
			$country = $site->country_id;
			if ($country == "9" || $country == "1") {
				$parent_market = "asia";
			} else {
				$parent_market = "china";
			}
      if($site -> lang_id == 2){
        $contactus_url = "https://royal-asia-limo.com/booking/{$market}/contact";
      }else{
        $contactus_url = "https://{$parent_market}carservice.com/booking/{$market}/contact";
      }


			return $contactus_url;
		}
	}

	return $section . '.html';
}

function template_car_page_link($site_id, $section, $carpage = null, $preview = true) {

	if ($preview) {
		return site_url('admin/preview') . "?id=$site_id&section=$section&carpage=$carpage";
	}

	if ($carpage) {
		return $carpage . '.html';
	}

	if ($section == 'home') return 'index.html';

	return $section . '.html';
}

function template_tour_category_page_link($site_id, $section, $category = null, $preview = true) {

	if ($preview) {
		return site_url('admin/preview') . "?id=$site_id&section=$section&catpage={$category->id}";
	}

	if ($category) {
		return template_name_to_seo_link($category->title) . '.html';
	}

	if ($section == 'home') return 'index.html';

	return $section . '.html';
}

function template_fetch_available() {

	$templates = array();
	$path = APPPATH . 'views/templates';

	foreach (scandir($path) as $dirname) {
		if ($dirname == '.' || $dirname == '..' || !is_dir($path . DIRECTORY_SEPARATOR . $dirname))
			continue;

		$config = $path . DIRECTORY_SEPARATOR . $dirname . DIRECTORY_SEPARATOR . 'config.ini';
		if (file_exists($config)) {
			$lines = file($config);
			$tmp = array();
			foreach ($lines as $line) {
				$parts = explode('=', $line);
				if (count($parts) != 2) continue;
				$tmp[trim($parts[0])] = trim($parts[1]);
			}
			$config = $tmp;
		} else {
			$config = array();
		}

		$templates[$dirname] = isset($config['name']) ? $config['name'] : ucwords($dirname);
	}
	return $templates;
}

function template_editor_view($section, $template) {

	$editor = str_replace('-', '_', $section);
	//static page editor
	if (in_array($section, array('faqs', 'policies', 'why-service'))) {
		if ($section == 'faqs' && $template == 'qualifire') {
			$editor = 'faqs';
		} else {
			$editor = 'static_page';
		}
	}

	if (file_exists(APPPATH . 'views/templates/' . $template . '/editor/' . $editor . '.php')) {

		return 'templates/' . $template . '/editor/' . $editor . '.php';
	}

	return 'admin/editor/' . $editor . '.php';
}

function template_render_page($site_id, $section, $carpage = null, $is_preview = true, $tourpage = null) {

	$ci = get_instance();

	$ci->load->model('Websites_Model');
	$ci->load->model('Website_Vehicles_Model');
	$ci->load->model('Vehicles_Model');
	$ci->load->model('Vehicle_Images_Model');
	$ci->load->model('Vehicle_Classes_Model');
	$ci->load->model('Text_Boxes_Model');
	$ci->load->model('Text_Boxes_Categories_Model');
	$ci->load->model('Static_Pages_Model');
	$ci->load->model('Slideshow_Images_Model');
	$ci->load->model('Pages_Model');
	$ci->load->model('Websites_Tours_Model');
	$ci->load->model('Websites_Tours_Categories_Model');
	$ci->load->model('Website_Banners_Model');
	$ci->load->model('Countries_Model');
	$ci->load->model('Languages_Model');

	$data = array('is_preview' => $is_preview);

	$website = $ci->Websites_Model->find($site_id);
	$country = $ci->Countries_Model->find($website->country_id);

    $childrenWebsiteNames = $ci->Websites_Model->findChildrenName($site_id);

	$language = $ci->Languages_Model->find($website->lang_id);
	$web_site_language = $ci->Languages_Model->find($website->lang_id);

	$alt_website = null;

	if ($website->alt_website_id != '' && $website->alt_website_id != 0) {
		$alt_website = $ci->Websites_Model->find($website->alt_website_id);
	}

	if (!$website) {
		$ci->session->set_flashdata('info', 'Site could not be found');
		redirect(site_url('admin/websites'));
	}

	$site_vehicles = $ci->Website_Vehicles_Model->get_vehicles(array('website_id' => $site_id, 'order' => 'ranking asc'));
    
    //add by marc
    $data['vehicle_names'] = $ci->Website_Vehicles_Model->get_vehicles_fields($site_id);

	if ($language->lang_code == 'eng') {
		$vehicles = $ci->Vehicles_Model->get_vehicles(array('vehicle_id' => array_keys($site_vehicles), 'order' => 'short_name asc'));
	} else {
		$vehicles = $ci->Vehicles_Model->get_vehicles(array('vehicle_id' => array_keys($site_vehicles), 'order' => 'short_name asc', 'lang_code' => $language->lang_code));
	}

	$tours_categories = $ci->Websites_Tours_Categories_Model->get_tours_cats(array('website_id' => $site_id));
	$tours_count = empty($tours_categories) ? 0 : $ci->Websites_Tours_Model->count(array('tour_cat_id' => array_keys($tours_categories)));

	$other_sites = $ci->Websites_Model->get_avail_langs($site_id);
	$data['other_sites'] = $other_sites;
	$data['meta_description'] = null;
	$data['meta_keywords'] = null;
	$data['meta_title'] = null;
	$data['section'] = $section;
	$data['site_id'] = $site_id;
	$data['site'] = $website;
	$data['site_vehicles'] = $site_vehicles;
	$data['vehicles'] = $vehicles;
	$data['page'] = $ci->Pages_Model->find($site_id, $section);
	$data['has_tours'] = $tours_count > 0 ? TRUE : FALSE;
	$data['tours_categories'] = $tours_categories;
	$banner = $ci->Website_Banners_Model->find($site_id, $section);
	$data['banner'] = $banner;
	$data['hasBanner'] = ($banner && $banner->enabled == 'YES');
	$data['website_country'] = $country;
	$data['alt_website'] = $alt_website;
    $data['arrivals'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => 'locations', 'order' => 'ranking asc'));
    $data['service_boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => 'services', 'order' => 'ranking asc'));
    $boxes = array();
    $top_boxes = array();
    foreach ($data['service_boxes'] as $box) {
      $box->title = htmlentities($box->title);
      if ($box->type == 'top') {
        $top_boxes[$box->id] = $box;
      }
    }
    $data['top_boxes'] = $top_boxes;

    $data['about_boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => 'about-us', 'order' => 'ranking asc'));
    $testimonial_object;
    foreach ($data['about_boxes'] as $testi_box) {
    if($testi_box->type == 'testimonial' && $testi_box->extra) {
      $testimonials_box = $testi_box;
      break;
    }
  }

  if($website -> template != "qualifire"){
    if(isset($testimonials_box)){
      $data['testimonial'] = $testimonials_box;
    }
  }

	if ($carpage) {
		if (isset($language) && $language->lang_code != '') {
			$page_vehicle = $ci->Vehicles_Model->find_by_page_name($carpage, $language->lang_code);
		} else {
			$page_vehicle = $ci->Vehicles_Model->find_by_page_name($carpage);
		}
		$data['page_vehicle'] = $page_vehicle;
		$data['page_site_vehicle'] = $ci->Website_Vehicles_Model->find($page_vehicle->id, $site_id);
		$data['page_vehicle_images'] = $ci->Vehicle_Images_Model->get_images(array('vehicle_id' => $page_vehicle->id));
		$data['page_view'] = 'templates/' . $website->template . '/vehicle_page';
		$data['section'] = str_replace(' ', '-', trim($page_vehicle->short_name));
		$data['carpage'] = $carpage;

	}

	if ($tourpage) {

		$tourCategory = $ci->Websites_Tours_Categories_Model->find($tourpage);
		$tours = $ci->Websites_Tours_Model->get_tours(array('tour_cat_id' => $tourCategory->id));
		$tmp_tours_boxes = empty($tours) ? array() : $ci->Text_Boxes_Model->get_boxes(array(
			'website_id' => $site_id,
			'order' => 'ranking',
			'type' => 'tour',
			'page_uri' => 'tours',
			'extra_id' => array_keys($tours)
		));
		$tours_boxes = array();
		foreach ($tmp_tours_boxes as $tmp_box) {
			$tours_boxes[$tmp_box->extra_id][] = $tmp_box;
		}
		unset($tmp_tours_boxes);

		$data['tour_category'] = $tourCategory;
		$data['tours_categories'] = $tours_categories;
		$data['tours'] = $tours;
		$data['tours_boxes'] = $tours_boxes;
		$data['page_view'] = 'templates/' . $website->template . '/tourscategory_page';
		$data['section'] = $section;
		$data['meta_title'] = $tourCategory->title;
		$data['meta_keywords'] = $tourCategory->meta_keywords;
		$data['meta_description'] = $tourCategory->meta_desc;
		//$data['carpage'] = $carpage;

	}

	if (!$tourpage && $section == 'tours') {
		$data['page_view'] = 'templates/' . $website->template . '/tours_page';
	}

	if (!$carpage && $section == 'home') {
		$data['slideshow_images'] = $ci->Slideshow_Images_Model->get_images(array('website_id' => $site_id));
		if (empty($data['slideshow_images'])) {
			for ($i = 1; $i < 4; $i++) {
				$default = new stdClass();
				$default->filename = "default-slide-$i.jpg";
				$data['slideshow_images'][] = $default;
			}
		}
		$data['boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => $section, 'order' => 'ranking asc'));
    $home_about_boxes = array();
    foreach ($data['boxes'] as $home_about_box) {
      if($home_about_box->type == 'about') {
        $home_about_boxes[] = $home_about_box;
      }
    }
    if(count($home_about_boxes)){
      $data['home_about_boxes'] = $home_about_boxes;
    }
		$data['home_boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => $section));
		$data['page_view'] = 'templates/' . $website->template . '/home';

	}

	if (!$carpage && $section == 'contact-us') {
		$data['contact_box'] = $ci->Text_Boxes_Model->find(array('website_id' => $site_id, 'page_uri' => $section));
		//	if(!empty($data['contact_box'])) $data['contact_box'] = array_shift($data['contact_box']);
		$data['website'] = $ci->Websites_Model->find($site_id);
		$data['page_view'] = 'templates/' . $website->template . '/contact_us';
	}

	if (!$carpage && $section == 'fleet') {
		$boxes = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => $section));
		$tmp = $boxes;
		$data['fleet_box'] = empty($tmp) ? null : array_shift($tmp);
	  $data['boxes'] = $boxes;
    $data['childBoxes'] = array();
    $data['child_vehicles'] = array();
    $data['child_site_vehicles'] = array();
    $data['child_site'] = array();
    if(count($childrenWebsiteNames)){
      $childBoxes = array();
      $child_site_vehicles = array();
      foreach($childrenWebsiteNames as $key => $v){
        $child_fleet = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $key, 'page_uri' => $section));
        $childBoxes[$v] = empty($child_fleet) ? null : array_shift($child_fleet);
        $child_site_vehicles[$v] = $ci->Website_Vehicles_Model->get_vehicles(array('website_id' => $key, 'order' => 'ranking asc'));
        $child_sites[$v] =  $ci->Websites_Model->find($key);
        if ($language->lang_code == 'eng') {
          $child_vehicles[$v] = $ci->Vehicles_Model->get_vehicles(array('vehicle_id' => array_keys($child_site_vehicles[$v]), 'order' => 'short_name asc'));
        } else {
          $child_vehicles[$v] = $ci->Vehicles_Model->get_vehicles(array('vehicle_id' => array_keys($child_site_vehicles[$v]), 'order' => 'short_name asc', 'lang_code' => $language->lang_code));
        }
      }
      $data['childBoxes'] = $childBoxes;
      $data['child_site_vehicles']= $child_site_vehicles;
      $data['child_sites'] = $child_sites;
      $data['child_vehicles'] = $child_vehicles;
    }
		$data['page_view'] = 'templates/' . $website->template . '/fleet';

	}


	if (!$carpage && in_array($section, array('about-us', 'online-booking', 'services', 'rates', 'testimonials'))) {
		$data['page_view'] = 'templates/' . $website->template . '/box_page';
    if($section == 'testimonials'){
      $data['boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => 'about-us', 'order' => 'ranking asc, id desc'));
    }else{
      $data['boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => $section, 'order' => 'ranking asc'));
    }

    $data['childBoxes'] = array();
    if(count($childrenWebsiteNames) && $section == 'rates'){
        $childBoxes = array();
        foreach($childrenWebsiteNames as $key => $v){
          $childBoxes[$v] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $key, 'page_uri' => $section, 'order' => 'ranking asc'));
        }
      $data['childBoxes'] = $childBoxes;
    }
		if ($section == 'online-booking') {

			foreach ($data['boxes'] as $key => $box) {
				$box->content = str_replace('http://www.beijingcarservice.com/wp-content/themes/ccsnew/images/pay-icons.png', template_image_src('pay-icons.png', $is_preview), $box->content);
				if (!$is_preview) {
					$box->content = str_replace(SITE_ROOT . 'application/views/templates/' . $website->template . '/images/pay-icons.png', template_image_src('pay-icons.png', $is_preview), $box->content);
				}
				$data['boxes'][$key] = $box;
			}

		}

		if ($section == 'services') {
			$boxes = array();
			foreach ($data['boxes'] as $box) {
				$box->title = htmlentities($box->title);
				if ($box->type != 'top') {
					$boxes[$box->id] = $box;
				}
			}
			$data['boxes'] = $boxes;
			$data['services_intro'] = $ci->Text_Boxes_Model->find(array('website_id' => $site_id, 'page_uri' => $section, 'type' => 'intro'));
		}

	}

	if (!$carpage && in_array($section, array('faqs', 'policies', 'why-service'))) {
		$data['page_view'] = 'templates/' . $website->template . '/static_page';
		$data['static_page'] = $ci->Static_Pages_Model->find_by_uri($section, $site_id);
		if ($section == 'faqs') {
			$data['boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => $section, 'type' => 'faq'));
		}
	}

	if (!$carpage && $section == 'tours') {
		$ci->load->model('Websites_Tours_Categories_Model');
		$data['page_view'] = 'templates/' . $website->template . '/tours_page';
		$data['tours'] = $tours_categories;

	}


	if (!$carpage && $section == 'locations' && $website->template == 'qualifire') {
		//$ci->load->model('Websites_Tours_Categories_Model');
		$data['boxes'] = $ci->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'page_uri' => $section, 'order' => 'ranking asc'));
		$data['boxes_categories'] = $ci->Text_Boxes_Categories_Model->get_cats(array('website_id' => $site_id, 'section' => $section));

		$data['page_view'] = 'templates/' . $website->template . '/locations_page';
		$data['is_full_page'] = TRUE;
		//$data['tours'] = $tours_categories;

	}


	$ci->load->helper('language');
	$ci->lang->language = array();
	$ci->lang->is_loaded = array();
	$ci->lang->load("message", $language->language);
	$data["language"] = $language;
	$data["web_site_language"] = $web_site_language;


	if ($is_preview) {
		$ci->load->view('templates/' . $website->template . '/page', $data);
	} else {
		return $ci->load->view('templates/' . $website->template . '/page', $data, TRUE);
	}
}
