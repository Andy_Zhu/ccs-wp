<?php

class FTP_Client {

	var $_connection_id;
	var $_is_logged_in = false;
	var $_messages = array();
	
	function __construct() {
		
		
	}
	
	function connect($server, $ftp_user, $ftp_password, $is_passive = false) {
		
		$this->_connection_id = ftp_connect($server);
		
		$login_result = ftp_login($this->_connection_id	, $ftp_user, $ftp_password);
		
		ftp_pasv($this->_connection_id, $is_passive);
		
		if((!$this->_connection_id) || (!$login_result)) {
			
			$this->log_message('FTP Connection has failed!');
			$this->log_message("Attempted to connect to $server for user $ftp_user");
			return false;
		}
		
		$this->log_message("Connected to $server , for user: $ftp_user");
		return true;
	}
	
	function make_dir($dir) {
		
		if(ftp_mkdir($this->_connection_id, $dir)) {
			
			$this->log_message("Directory: $dir Created successfully");
			return TRUE;
		}
		
		$this->log_message("Failed creating directory: $dir");
		return FALSE;
	}
	
	function upload_file($file_from, $file_to) {
		
		static $ascii = array('csv', 'txt');
		
		$parts = explode('.', $file_from);
		
		$extention = end($parts);
		
		$mode = in_array($extention, $ascii) ? FTP_ASCII : FTP_BINARY;
		
		$upload = ftp_put($this->_connection_id, $file_to, $file_from, $mode);
		
		if(!$upload) {
			$this->log_message("FTP Upload filed $file_from ==> $file_to");
			return false;
		}
		
		$this->log_message("FTP Upload successfull $file_from ==> $file_to");
		return true;
	}
	
	function get_file($file_from, $file_to) {
		
		static $ascii = array('csv', 'txt');
		
		$parts = explode('.', $file_from);
		
		$extention = end($parts);
		
		$mode = in_array($extention, $ascii) ? FTP_ASCII : FTP_BINARY;
		
		$download = ftp_get($this->_connection_id, $file_to, $file_from, $mode);
		
		if(!$download) {
			$this->log_message("FTP Download filed $file_from ==> $file_to");
			return false;
		}
		
		$this->log_message("FTP Download successfull $file_from ==> $file_to");
		return true;
	}
	
	function get_dir($from_dir, $to_dir) {
		
		$from_dir = rtrim($from_dir, '/');
		$to_dir = rtrim($to_dir, '/');
		
		$dirs = $this->list_dir($from_dir);
			
		foreach($dirs as $entry) {
			
			$entry  = basename($entry);
			
			if($entry == '.' || $entry == '..') continue;
			
			/*if($this->is_dir($entry)) {
				echo "DIR $entry \n";
				mkdir($to_dir . '/' . $entry);
				die("DIR");
				$this->get_dir($from_dir . "/$entry", $to_dir . '/' . $entry);
				continue;
			}*/
			
			
			$from_file = $from_dir . '/' . $entry;
			$to_file = $to_dir . '/' .$entry;
			$rs = $this->get_file($from_file, $to_file);
			echo "$from_file TO $to_file == $rs\n";
			
		}
		
	}
	
	function is_dir($dir){
	    if(ftp_chdir($this->_connection_id,$dir)) {
	        ftp_cdup($this->_connection_id);
	        return true;
	    } else {
	        return false;
	    }
	} 
	
	function upload_dir($from_dir, $to_dir) {
		$form_file = rtrim($from_dir, '/');
		$to_dir = rtrim($to_dir, '/');
		
		$dir = opendir($from_dir);
			
		while(($entry = readdir($dir)) != null) {
			if($entry == '.' || $entry == '..') continue;
				$form_file = $from_dir . '/' . $entry;
				$to_file = $to_dir . '/' . $entry;
				$this->upload_file($form_file, $to_file);
		}
		
		closedir($dir);
	}
	
	public function change_dir($dir) {

		if(ftp_chdir($this->_connection_id, $dir)) {
			$this->log_message('Current directory is now ' . ftp_pwd($this->_connection_id));
			return true;
		}else{
			$this->log_message('Change Directory failed');
			return false;
		}
	}
	
	public function list_dir($dir, $parameters = '-al') {
		
		$contentArray = ftp_nlist($this->_connection_id, $parameters . ' ' . $dir);
		
		return $contentArray;
	}
	
	
	function pwd() {
		
		return ftp_pwd($this->_connection_id);
	}
	
	
	function log_message($message) {
		$this->_messages[] = $message;
	}
	
	function get_messages() {
		return $this->_messages;
	}
	
	
	public function __destruct() {
		if($this->_connection_id) {
			ftp_close($this->_connection_id);
		}
	}
	
}

?>