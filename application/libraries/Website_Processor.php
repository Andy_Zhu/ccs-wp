<?php
class Website_Processor {
	
	private $_params = array();
	
	public function __construct($params = array()) {
		
		$this->_params = array_merge($this->_params, $params);
	}
	
	public function process($section, $website, &$layout_data, $redirect) {
		
		//load default editor form precossor class
		require_once APPPATH  . 'views/admin/editor/post_processor.php';
		$template_processor = new TemplateProcessor();
		//load any overrided function
		if(file_exists(APPPATH . 'views/templates/' . $website->template . '/editor/post_processor.php')) {
			require_once APPPATH . 'views/templates/' . $website->template . '/editor/post_processor.php';
		}
		//process any form
		$func_name = 'do_' . str_replace('-', '_', $section);
		if(function_exists($func_name)){
			call_user_func_array($func_name, array($section, $website, $layout_data, $redirect));
		}else if(method_exists($template_processor, $func_name)){
			call_user_func_array(array($template_processor, $func_name), array($section, $website, $layout_data, $redirect));
		}
		//call extra processor
		if(function_exists('do_post_extra')){
			call_user_func_array('do_post_extra', array($section, $website, $layout_data, $redirect));
		}else if(method_exists($template_processor, 'do_post_extra')){
			call_user_func_array(array($template_processor, 'do_post_extra'), array($section, $website, $layout_data, $redirect));
		}
	}
}
