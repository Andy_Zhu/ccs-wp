<?php

/**
 * @property CI_DB_active_record|CI_DB_mysql_driver db
 * @property CI_Loader load
 * @property string lang_code
 * @property string language
 * @property CI_Image_lib image_lib
 */
class Languages_Model extends CI_Model {

	/**
	 *
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'languages';
	private $small_flag_size = array('x' => 15, 'y' => 15);
	private $big_flag_size = array('x' => 200, 'y' => 200);


	public function __construct() {

		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->library('image_lib');
	}

	public function get_languages($options = array()) {

		$defaultOptions = array(
			'limit' => NULL,
			'offset' => NULL,
			'order' => 'id desc'
		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		$this->db->from($this->_table_name);


		if (isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}

		if (isset($options['order'])) {
			$this->db->order_by($options['order']);
		}

		$query = $this->db->get();

		$languages = array();

		foreach ($query->result() as $row) {
			$languages[$row->id] = $row;
		}

		return $languages;
	}

	public function get_flag_path($id) {
		$lang = $this->find($id);
		if (!$lang){
			return '';
		}
		return "images/flags/lang_" . $lang->lang_code . ".jpg";
	}

	public function get_small_flag_path($id) {
		$lang = $this->find($id);
		if (!$lang){
			return '';
		}
		return "images/flags/lang_" . $lang->lang_code . "_small.jpg";
	}

	public function get_big_flag_path($id) {
		$lang = $this->find($id);
		if (!$lang){
			return '';
		}
		return "images/flags/lang_" . $lang->lang_code . "_big.jpg";
	}

	private function delete_flag_file($id) {
		$flag_image_path = array(
			$this->get_flag_path($id),
			$this->get_big_flag_path($id),
			$this->get_small_flag_path($id)
		);

		foreach ($flag_image_path as $flag) {
			if (file_exists($flag)) {
				unlink($flag);
			}
		}
	}

	private function create_small_flag($id) {
		$lang = $this->find($id);
		$path = "images/flags/lang_" . $lang->lang_code . "_small.jpg";
		$original_path = $this->get_flag_path($lang->id);
		$this->resize_flag($original_path, $path, $this->small_flag_size);
	}

	private function create_big_flag($id) {
		$lang = $this->find($id);
		$path = "images/flags/lang_" . $lang->lang_code . "_big.jpg";
		$original_path = $this->get_flag_path($lang->id);
		$this->resize_flag($original_path, $path, $this->big_flag_size);
	}

	private function resize_flag($src, $dst, $size) {
		if (file_exists($dst)) {
			unlink($dst);
		}
		$img_cfg_thumb['image_library'] = 'gd2';
		$img_cfg_thumb['source_image'] = $src;
		$img_cfg_thumb['maintain_ratio'] = TRUE;
		$img_cfg_thumb['new_image'] = basename($dst);
		$img_cfg_thumb['width'] = $size['x'];
		$img_cfg_thumb['height'] = $size['y'];
		$img_cfg_thumb['maintain_ratio'] = true;
		$img_cfg_thumb['master_dim'] = 'width';

		$this->image_lib->initialize($img_cfg_thumb);
		$this->image_lib->resize();
		$this->image_lib->clear();
	}

	/**
	 * @param $id
	 * @return Languages_Model
	 */
	public function find($id) {
		if (!$id) return NULL;
		$result = $this->db->get_where($this->_table_name, array('id' => $id));
		if (!$result->num_rows()) return NULL;
		return $result->row();

	}

	public function create($data) {
		if ($data['id'] == '') {
			$this->db->where('lang_code', $data['lang_code'])->or_where('language', $data['language']);
			$lang = $this->db->get_where($this->_table_name);
			if ($lang->num_rows()) {
				return false;
			}
			$this->db->insert($this->_table_name, $data);
			$id = $this->db->insert_id();
		} else {
			$id = $this->update($data);
		}
		$this->db->cache_on();
		$this->create_big_flag($id);
		$this->create_small_flag($id);
		$this->db->cache_off();

		return $id;
	}

	public function delete($id) {
		$this->delete_flag_file($id);
		return $this->db->delete($this->_table_name, array('id' => $id));
	}

	public function update($data) {
		$this->db->update(
			$this->_table_name,
			array('lang_code' => $data['lang_code'], 'language' => $data['language']),
			array('id' => $data['id']));
		return $data['id'];
	}
}
