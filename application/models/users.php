<?php
/**
 * 
 * Users Model, used by admin panel
 * @property CI_Email email
 * @property string firstname
 * @property string lastname
 * @property string status
 * @property integer id
 * @author Omar TCHOKHANI <omatcho02@gmail.com>
 *
 */
class Users extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'users';
	
	/**
	 * 
	 * password encryption salt
	 * @var string
	 */
	var $_salt = 'ds@-_hjhd4225àài';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_users($options=array()){
		
		$defaultOptions = array(
			'role'=>NULL,
			'status'=>NULL,
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'id desc'
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->from($this->_table_name);
		if(isset($options['role'])) {
			$this->db->where('role', $options['role']);
		}
		if(isset($options['status'])) {
			$this->db->where('status', $options['status']);
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$users = array();
		
		foreach ($query->result() as $row) {
			$users[$row->id] = $row;
		}
		
		return $users;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			'role' => NULL,
			'status'=>NULL,
			'user_id'=>NULL
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(id) as usersCount');
		$this->db->from($this->_table_name);
		
		if(isset($options['user_id']) && !is_array($options['user_id'])) {
			$options['user_id'] = array($options['user_id']);
		}
		
		if(isset($options['role'])) {
			$this->db->where('role', $options['role']);
		}
		
		if(isset($options['status'])) {
			$this->db->where('status', $options['status']);
		}
		
		if(!empty($options['user_id'])) {
			$this->db->where('user_id in (' . implode(',', $options['user_id']) . ')');
		}
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->usersCount;
	}
	
	
	public function create_user($data) {
		$data['ts_created'] = date('Y-m-d H:i:s');
		$data['password'] = $this->encrypt_password($data['password']);
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function random_password($length=8) {
		$pass = '';
		
		while(strlen($pass) <= $length) {
			
			$chr = chr(rand(0, 255));
			if(!preg_match('/[a-zA-Z]/', $chr)) continue;
			
			$pass .=$chr;
		}
		
		return strtoupper($pass);
	}

	/**
	 * @param $id
	 * @return Users
	 */
	public function find($id) {
		
		if(!$id) return NULL;

		$result = $this->db->get_where($this->_table_name, array('id'=>$id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function find_by_email($email) {
		
		if(!$email) return NULL;
		
		$result = $this->db->get_where($this->_table_name, array('email'=>$email));
		
		if(!$result->num_rows()) return null;
		
		return $result->row();
	}
	
	public function encrypt_password($raw_password) {
		
		return md5($this->_salt . $raw_password);
	}

	/**
	 * @param $email
	 * @param $raw_password
	 * @return Users
	 */
	public function find_by_credential($email, $raw_password) {
		
		$where = array('email'=>$email, 'password'=>$this->encrypt_password($raw_password));
		
		$result = $this->db->get_where($this->_table_name, $where);
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
	}
	
	public function create_identity($row) {
		
		$this->session->set_userdata('admin_identity', $row);
		//update last login
		$this->db->where('id', $row->id);
		$this->db->update($this->_table_name, array('last_login'=>date('Y-m-d H:i:s')));
	}
	
	public function clear_identity(){
		
		$this->session->unset_userdata('admin_identity');
	}
	
	public function get_identity() {
		
		return $this->session->userdata('admin_identity');
	}
	
	public function has_identity() {
		
		return ($this->get_identity());
	}
	
	public function is_admin() {
		
		return ($this->has_identity() && $this->get_identity()->role == 'admin');
	}
	
	public function is_editor() {
		
		return ($this->has_identity() && $this->get_identity()->role == 'editor');
	}
	
	public function is_author() {
		return ($this->has_identity() && $this->get_identity()->role == 'author');
	}
	
	public function delete($id) {
		
		return $this->db->delete($this->_table_name, array('id'=>$id));
		
	}
	
	public function update($data, $id) {
		
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	public function activate($id) {
		
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, array('status'=>'A'));
	}
	
	public function block($id) {
		
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, array('status'=>'B'));
	}
	
	function get_users_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1, 'status'=>null, 'role'=>null);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_users($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}

}

?>