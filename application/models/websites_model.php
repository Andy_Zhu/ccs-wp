<?php


ini_set('mysql.connect_timeout', 300);
ini_set('default_socket_timeout', 300);

/**
 * @property string template
 * @property string name
 * @property CI_Loader load
 * @property CI_DB_active_record|CI_DB_mysql_driver db
 * @property integer lang_id
 * @property string test_ftp_target_dir
 * @property string test_ftp_host
 * @property string test_ftp_user
 * @property string test_ftp_password
 * @property string ftp_target_dir
 * @property string ftp_host
 * @property string ftp_user
 * @property string ftp_password
 * @property string test_url
 * @property string url
 * @property string logo
 * @property Languages_Model Languages_Model
 * @property string alt_website_id
 */
class Websites_Model extends CI_Model {

	/**
	 *
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'websites';


	public function __construct(){

		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	public function get_websites($options=array()){

		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'name asc',
			'website_id'=>null,
			'country_id'=>null
		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		$this->db->from($this->_table_name);

		if(isset($options['website_id']) && !is_array($options['website_id'])) {
			$options['website_id'] = array($options['website_id']);
		}

		if(isset($options['country_id']) && !is_array($options['country_id'])) {
			$options['country_id'] = array($options['country_id']);
		}

		if(!empty($options['website_id'])) {
			$this->db->where('id in (' . implode(',', $options['website_id']) . ')');
		}

		if(!empty($options['country_id'])) {
			$this->db->where('country_id in (' . implode(',', $options['country_id']) . ')');
		}

		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}

		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}

		$query = $this->db->get();

		$websites = array();

		foreach ($query->result() as $row) {
			$websites[$row->id] = $row;




		}

		return $websites;

	}

	public function all(){
		$res = $this->db->get($this->_table_name);
		$rows = Array();
		foreach ($res->result() as $row){
			$rows[] = $row;
		}
		return $rows;
	}

	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(

		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		if(isset($options['country_id']) && !is_array($options['country_id'])) {
			$options['country_id'] = array($options['country_id']);
		}
		$this->db->select('count(id) as websitesCount');
		$this->db->from($this->_table_name);
		if(isset($options['country_id'])) {
			$this->db->where('country_id in (' . implode(',', $options['country_id']) . ')');
		}

		$query = $this->db->get();

		$row = $query->row();

		return $row->websitesCount;
	}


	public function create($data) {

		if(!isset($data['ftp_target_dir'])) $data['ftp_target_dir'] = '/httpdocs';

		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	/**
	 * @param $id
	 * @return Websites_Model
	 */
	public function find($id) {

		if(!$id) return NULL;

		$result = $this->db->get_where($this->_table_name, array('id'=>$id));

		if(!$result->num_rows()) return NULL;

		return $result->row();

	}

	public function findByUrl($url) {

		if(!$url) return NULL;

		$result = $this->db->get_where($this->_table_name, array('url'=>$url));

		if(!$result->num_rows()) return null;

		return $result->row();
	}

  public function findChildrenName($id) {

    if(!$id) return NULL;
    $this->db->from($this->_table_name);
    $this->db->where('parent_site_id', $id);
    $query = $this->db->get();

    $childenWebsites = array();

    foreach ($query->result() as $row) {
      $childenWebsites[$row->id] = $row -> shortname;
    }
    return $childenWebsites;

  }

	public function delete($id) {

		return $this->db->delete($this->_table_name, array('id'=>$id));

	}

	public function update($data, $where) {
		if(is_array($where)) {
			return $this->db->update($this->_table_name, $data, $where);
		}else{
			$this->db->where('id', $where);
			return $this->db->update($this->_table_name, $data);
		}
	}


	function get_websites_paginator($options=array()) {

		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);

		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}

		unset($options['limit']);
		unset($options['offset']);

		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];

		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}

		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}

		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}

		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;

		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;

		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_websites($options);

		$paginator['data'] = $users;

		return $paginator;
	}

	function seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);

	    if($unique) {

	    	$temp = $string;
	    	$counter = 1;
	    	while($this->findByUrl($string)) {

	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }

	    return trim($string, ' -');
	}

	/**
	 * Get all other availible languages
	 *
	 * Returns array('lang_id'=>array('lang'=>Language_Model, 'website'=>Website_Model))
	 *
	 * @param integer $id website id
	 * @return array Array of availible languages and websites
	 */
	function get_avail_langs($id){
		$site = $this->find($id);
		if (!$site->alt_website_id){
			return array();
		}
		$other_sites = explode(',', $site->alt_website_id);
		$langs = array();
		$this->load->model('Languages_Model');
		foreach ($other_sites as $other_site){
			$osite = $this->find($other_site);
			$lang = $this->Languages_Model->find($osite->lang_id);
			$langs[] = array('lang'=>$lang, 'website'=>$osite);
		}
		return $langs;
	}

	public function alt_selected($site, $alt){
		$site = $this->find($site);
		$other_sites = explode(',', $site->alt_website_id);
		return in_array($alt, $other_sites);
	}
}
