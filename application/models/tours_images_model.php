<?php

class Tours_Images_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'tours_images';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_tours_images($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'tour_id'=>null,
			'group'=>null,
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->from($this->_table_name);
		
		if(isset($options['tour_id']) && !is_array($options['tour_id'])) {
			$options['tour_id'] = array($options['tour_id']);
		}
		
		if(!empty($options['tour_id'])) {
			$this->db->where('tour_id in (' . implode(',', $options['tour_id']) . ')');
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$tours= array();
		
		if($options['group'] == 'tour_id') {
			foreach ($query->result() as $row) {
				$tours[$row->tour_id][$row->id] = $row;
			}
		}else{
			foreach ($query->result() as $row) {
				$tours[$row->id] = $row;
			}
		}
		
		
		return $tours;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(id) as imagesCount');
		$this->db->from($this->_table_name);
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->imagesCount;
	}
	
	
	public function create($data) {
	
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function find($id) {
		
		if(!$id) return NULL;

		$result = $this->db->get_where($this->_table_name, array('id'=>$id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function find_by_filename($name){
		
		if(!$name) return NULL;
		
		$result = $this->db->get_where($this->_table_name, array('filename'=>$name));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
	}
	
	public function delete($id) {
		
		$image  = $this->find($id);
		
		if(!$image) return FALSE;
		
	 	$rs = $this->db->delete($this->_table_name, array('id'=>$id));
	 	
	 	//if the image is not used somewhere else than delete the file
	 	if($rs && !$this->find_by_filename($image->filename)) {
	 		$this->load->helper('images');
	 		@unlink(tour_image_path($image->filename));
	 	}
	 	
	 	return $rs;	
	}
	
	public function update($data, $id) {
		
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_tours_images_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$tours = $this->get_tours_images($options);
		
		$paginator['data'] = $tours;
		
		return $paginator;
	}
	
	public function seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);
	    
	    if($unique) {
	    	
	    	$temp = $string;
	    	$counter = 1;
	    	while($this->findByUrl($string)) {
	    		
	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }
	    
	    return trim($string, ' -');
	}
	

}

?>