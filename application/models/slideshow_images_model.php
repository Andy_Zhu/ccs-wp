<?php

class Slideshow_Images_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'slideshow_images';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_images($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'id desc',
			'website_id'=>null,
			'image_id'=>null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['image_id']) && !is_array($options['image_id'])) {
			$options['image_id'] = array((int)$options['image_id']);	
		}
		
		$this->db->from($this->_table_name);
		
	
		if(isset($options['website_id'])) {
			$this->db->where('website_id', $options['website_id']);
		}
		
		if(!empty($options['image_id'])) {
			$this->db->where('id in (' . implode(',', $options['image_id']) . ')');	
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$images = array();
		
		foreach ($query->result() as $row) {
			if(isset($options['website_id'])) {
				$images[$row->slide_id] = $row;
			}else{
				$images[$row->id] = $row;
			}
			
		}
		
		return $images;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(id) as imgsCount');
		$this->db->from($this->_table_name);
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->imgsCount;
	}
	
	
	public function create($data) {
		$data['date_created'] = date('Y-m-d');
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function is_set($website_id, $slide_id) {
		
		$result = $this->db->get_where($this->_table_name, array('website_id'=>$website_id, 'slide_id'=>$slide_id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}

	
	public function delete($id) {
		
		return $this->db->delete($this->_table_name, array('id'=>$id));
		
	}
	
	public function update($data, $website_id, $slide_id) {
		$data['date_created'] = date('Y-m-d');
		$this->db->where('website_id', $website_id);
		$this->db->where('slide_id', $slide_id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_images_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_images($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}

}

?>