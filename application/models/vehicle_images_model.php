<?php

class Vehicle_Images_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'vehicule_images';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_images($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'image_id asc',
			'website_id'=>null,
			'vehicle_id'=>null,
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->from($this->_table_name);
		
	/*
		if(isset($options['website_id'])) {
			$this->db->where('website_id', $options['website_id']);
		}*/
		
		if(isset($options['vehicle_id'])) {
			
			if(!is_array($options['vehicle_id'])) $options['vehicle_id'] = array($options['vehicle_id']);
			
			$this->db->where('vehicle_id in (' . implode(',',  $options['vehicle_id']) . ')');
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$images = array();
		
		foreach ($query->result() as $row) {
			$images[$row->image_id] = $row;
		}
		
		return $images;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		
		
		$this->db->select('count(*) as imgsCount');
		$this->db->from($this->_table_name);
		
		if(isset($options['vehicle_id'])) {
			
			if(!is_array($options['vehicle_id'])) $options['vehicle_id'] = array($options['vehicle_id']);
			
			$this->db->where('vehicle_id in (' . implode(',',  $options['vehicle_id']) . ')');
		}
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->imgsCount;
	}
	
	
	public function create($data) {
		$data['date_created'] = date('Y-m-d');
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function is_set($vehicle_id, $image_id) {
		
		$result = $this->db->get_where($this->_table_name, array('vehicle_id'=>$vehicle_id, 'image_id'=>$image_id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}

	
	public function delete($vehicle_id, $image_id) {
		
		return $this->db->delete($this->_table_name, array('vehicle_id'=>$vehicle_id, 'image_id'=>$image_id));
		
	}
	
	public function delete_for_vehicle($vehicle_id) {
		return $this->db->delete($this->_table_name, array('vehicle_id'=>$vehicle_id));
	}
	
	public function update($data, $vehicle_id, $image_id) {
		$data['date_created'] = date('Y-m-d');
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->where('image_id', $image_id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_images_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_images($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}

}

?>