<?php

/**
 * @property CI_DB_active_record|CI_DB_mssql_driver db
 * @property string name
 */
class Countries_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'countries';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_countries($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'name asc',
			'country_id'=>null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->from($this->_table_name);
		
		if(isset($options['country_id']) && !is_array($options['country_id'])) {
			$options['country_id'] = array($options['country_id']);	
		}
		
		if(!empty($options['country_id'])) {
			$this->db->where('id in (' . implode(',', $options['country_id']) . ')');
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$countries = array();
		
		foreach ($query->result() as $row) {
			$countries[$row->id] = $row;
		}
		
		return $countries;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(id) as countriesCount');
		$this->db->from($this->_table_name);
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->countriesCount;
	}
	
	
	public function create($data) {
		
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	/**
	 * @param integer $id
	 * @return Countries_Model
	 */
	public function find($id) {
		
		if(!$id) return NULL;

		$result = $this->db->get_where($this->_table_name, array('id'=>$id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function find_by_name($name) {
		
		if(!$name) return NULL;
		
		$result = $this->db->get_where($this->_table_name, array('name'=>$name));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
	}
	
	public function delete($id) {
		
		return $this->db->delete($this->_table_name, array('id'=>$id));
		
	}
	
	public function update($data, $id) {
		
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_countries_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$countries = $this->get_countries($options);
		
		$paginator['data'] = $countries;
		
		return $paginator;
	}
	
	function seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);
	    
	    if($unique) {
	    	
	    	$temp = $string;
	    	$counter = 1;
	    	while($this->findByUrl($string)) {
	    		
	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }
	    
	    return trim($string, ' -');
	}
	

}

?>