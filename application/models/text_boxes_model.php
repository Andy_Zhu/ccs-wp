<?php

/**
 * @property CI_DB_active_record|CI_DB_mysql_driver db
 * @property integer id
 * @property CI_Loader load
 */
class Text_Boxes_Model extends CI_Model {

	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'text_boxes';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_boxes($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'id asc',
			'website_id'=>null,
			'page_uri' => null,
			'type'=>null,
			'box_id'=>null,
			'extra_id' => null,
			'cat_id' => null,
			'select' => '*',
			'has_image'=> null,
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['box_id']) && !is_array($options['box_id'])) {
			$options['box_id'] = array($options['box_id']);
		}
		
		if(isset($options['extra_id']) && !is_array($options['extra_id'])) {
			$options['extra_id'] = array($options['extra_id']);
		}
		
		if(isset($options['cat_id']) && !is_array($options['cat_id'])) {
			$options['cat_id'] = array($options['cat_id']);
		}
		
		$this->db->select($options['select']);
		$this->db->from($this->_table_name);
		
		if(!empty($options['extra_id'])) {
			$this->db->where_in('extra_id', $options['extra_id']);
		}
		
		if(!empty($options['cat_id'])) {
			$this->db->where_in('cat_id', $options['cat_id']);
		}
		
		if(isset($options['has_image'])) {
			
			if($options['has_image'] == true) {
				$this->db->where('image !=', 'NULL');
			}else{
				$this->db->where('image', 'NULL');
			}
		}
		if(isset($options['website_id'])) {
			
			$this->db->where('website_id', $options['website_id']);
		}
		
		if(!empty($options['box_id'])) {
			$this->db->where('id in (' . implode(',', $options['box_id']) . ')');
		}
		
		if(isset($options['page_uri'])) {
			
			$this->db->where('page_uri', $options['page_uri']);
		}
		
		if(isset($options['type'])) {
			
			$this->db->where('type', $options['type']);
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$websites = array();
		
		foreach ($query->result() as $row) {
			$websites[$row->id] = $row;
		}

		
		return $websites;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			'website_id' => null,
			'page_uri' => null,
			'type' => null,
			'extra_id'=> null,
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['extra_id']) && !is_array($options['extra_id'])) {
			$options['extra_id'] = array($options['extra_id']);
		}
	
		$this->db->select('count(id) as boxesCount');
		$this->db->from($this->_table_name);
		
		if(!empty($options['extra_id'])) {
			$this->db->where('extra_id in (' . implode(',', $options['extra_id']) . ')');
		}
		
		if(isset($options['website_id'])) {
			
			$this->db->where('website_id', $options['website_id']);
		}
		
		if(isset($options['page_uri'])) {
			
			$this->db->where('page_uri', $options['page_uri']);
		}
		
		if(isset($options['type'])) {
			$this->db->where('type', $options['type']);
		}
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->boxesCount;
	}
	
	
	public function create($data) {
		
		if(!isset($data['type'])) $data['type'] = 'normal';
		$data['content'] = str_replace('<ul', '<ul class="default" ', $data['content']);
		$data['content'] = str_replace('<ol', '<ol class="default" ', $data['content']);
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	/**
	 * @param $id
	 * @return Text_Boxes_Model
	 */
	public function find($id) {
		
		if(!$id) return NULL;
		
		if(is_array($id)) {
			$result = $this->db->get_where($this->_table_name, $id);
		}else{
			$result = $this->db->get_where($this->_table_name, array('id'=>$id));
		}
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	public function find_all_by_uri($uri) {
    
    $result = $this->db->get_where($this->_table_name, array('page_uri'=>$uri));
    
    if(!$result->num_rows()) return NULL;
    
    $textboxes = array();
    
    foreach ($result->result() as $row) {
       $textboxes[$row->id] = $row;
    }

    
    return $textboxes;
   // return $result->row();
    
  }
	public function find_by_uri($uri, $website_id) {
		
		$result = $this->db->get_where($this->_table_name, array('uri'=>$uri, 'website_id'=>$website_id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function has_box($uri, $website_id, $extra_id=NULL) {
	
		$result = $this->db->get_where($this->_table_name, array('uri'=>$uri, 'website_id'=>$website_id, 'extra_id'=>$extra_id));
		
		if(!$result->num_rows()) return false;
		
		return $result->row();
		
	}
	
	
	public function delete($id, $website_id=null, $extra_id=NULL) {
		
		$row = $this->find($id);
		if(!$row) return false;
		
		$option =  array('id'=>$id);
		if(!is_null($website_id)) $option['website_id'] = $website_id;
		if(!is_null('extra_id')) $options['extra_id'] = $extra_id;
		
		$rs = $this->db->delete($this->_table_name,$option);
		
		if($rs && !empty($row->image)) {
			$this->load->helper('images');
			@unlink(textbox_image_path($row->image));
			
		}
		return $rs;
	}
	
	public function clear_for_page($page_uri, $website_id, $type='normal', $extra_id=NULL) {
		
		$options = array('page_uri'=>$page_uri, 'website_id'=>$website_id, 'type'=>$type);
		if(isset($extra_id)) {
			$options['extra_id'] = $extra_id;
		}
		
		return $this->db->delete($this->_table_name, $options);
		
	}
	
	public function update($data, $id) {
		
		$this->db->where('id', $id);
		
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_boxes_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1, 'website_id'=>null);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_boxes($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}

}

?>