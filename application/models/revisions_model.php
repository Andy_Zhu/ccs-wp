<?php
/**
 * Class Revisions_Model
 *
 * @property CI_DB_active_record|CI_DB_mysql_driver db
 * @property CI_Loader load
 * @property Users Users
 * @property string $_table_name
 * @property string content
 * @property integer id
 * @property integer author
 * @property string created
 */
class Revisions_Model extends CI_Model {
	var $_table_name = 'revisions';


	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('Users');
	}

	/**
	 * @param string $ident
	 * @param array $content
	 * @return int
	 */
	public function add_version($ident, $content) {
		$author = $this->Users->get_identity()->id;
		$datetime = date('Y-m-d H:i:s');
		$content = json_encode($content, JSON_UNESCAPED_UNICODE);

		$data = array(
			'ident' => $ident, 'author' => $author,
			'created' => $datetime, 'content' => $content
		);

		if ($this->db->insert($this->_table_name, $data)) {
			return $this->db->insert_id();
		} else {
			return 0;
		}
	}

	/**
	 * @param integer $id
	 * @return Revisions_Model
	 */
	public function get_version($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->_table_name);
		if ($query->num_rows() == 0) {
			return null;
		} else {
			return $query->row();
		}
	}

	/**
	 * @param integer $ident
	 * @return array
	 */
	public function list_versions_for_ident($ident) {
		$this->db->where('ident', $ident);
		$this->db->order_by('created', 'desc');
		$query = $this->db->get($this->_table_name);
		return $query->result();
	}

	/**
	 * @return array
	 */
	public function list_all_versions(){
		$this->db->where(true);
		$query = $this->db->get($this->_table_name);
		return $query->result();
	}

	/**
	 * Generate ident string from site id, section id and, if exists, cat id
	 *
	 * @param integer $site_id
	 * @param integer $section_id
	 * @param integer|bool $cat_id
	 * @return string
	 */
	public function gen_ident($site_id, $section_id, $cat_id=false){
		return "" . $site_id . ":" . $section_id . ($cat_id ? ":" . $cat_id : "");
	}
}