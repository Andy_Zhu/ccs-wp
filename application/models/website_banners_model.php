<?php

class Website_Banners_Model extends CI_Model {

	/**
	 *
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'banners';


	public function __construct(){

		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	public function get_banners($options=array()){

		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'website_id'=>null,
			'page_uri'=>null,
			'group'=>null,
			'order'=>'ts_created asc'
		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		if(isset($options['website_id']) && !is_array($options['website_id'])) {
			$options['website_id'] = array($options['website_id']);
		}

		if(isset($options['page_uri']) && !is_array($options['page_uri'])) {
			$options['page_uri'] = array($options['page_uri']);
		}

		$this->db->from($this->_table_name);


		if(!empty($options['website_id'])) {
			$this->db->where('website_id in (' . implode(',', $options['website_id']) . ')');
		}

		if(isset($options['page_uri'])) {
			$quote = '';
			foreach ($options['page_uri'] as $puri) {
				$quote .= sprintf("'%s', ", $puri);
			}
			$quote = trim($quote, ' ,');
			$this->db->where('page_uri in ('. $quote . ')');
		}

		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}

		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}

		$query = $this->db->get();

		$banners= array();

		if(isset($options['group'])) {
			$group = $options['group'];
			foreach ($query->result() as $row) {
				$banners[$row->$group] = $row;
			}

		}else{

			foreach ($query->result() as $row) {
				$banners[] = $row;
			}
		}


		return $banners;

	}

	public function count($options=array()) {

		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(

		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		if(isset($options['website_id']) && !is_array($options['website_id'])) {
			$options['website_id'] = array($options['website_id']);
		}



		$this->db->select('count(*) as bannersCount');
		$this->db->from($this->_table_name);

		if(!empty($options['website_id'])) {
			$this->db->where('website_id in (' . implode(',', $options['website_id']) . ')');
		}

		if(isset($options['page_uri'])) {
			$this->db->where('page_uri', $options['page_uri']);
		}

		$query = $this->db->get();

		$row = $query->row();

		return $row->bannersCount;
	}


	public function create($data) {

		$data['ts_created'] = date('Y-m-d H:i:s');
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	public function find($website_id, $page_uri) {

		if(!$website_id || !$page_uri) return NULL;

		$result = $this->db->get_where($this->_table_name, array('page_uri'=>$page_uri, 'website_id'=>$website_id));

		if(!$result->num_rows()) return NULL;

		return $result->row();

	}

	public function delete($website_id, $page_uri) {

		$rs = $this->db->delete($this->_table_name, array('website_id'=>$website_id, 'page_uri'=>$page_uri));

		return $rs;
	}

	public function clear_all($website_id) {
		if(!$website_id) return NULL;
		return $this->db->delete($this->_table_name, array('website_id'=>$website_id));
	}

	public function update($website_id, $page_uri, $data) {
		var_dump($data);
		$this->db->where('website_id', $website_id);
		$this->db->where('page_uri', $page_uri);
		return $this->db->update($this->_table_name, $data);
	}

	function get_banners_paginator($options=array()) {

		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);

		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}

		unset($options['limit']);
		unset($options['offset']);

		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];

		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}

		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}

		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}

		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;

		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;

		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$tours = $this->get_banners($options);

		$paginator['data'] = $tours;

		return $paginator;
	}

	public function seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);

	    if($unique) {

	    	$temp = $string;
	    	$counter = 1;
	    	while($this->findByUrl($string)) {

	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }

	    return trim($string, ' -');
	}


}

?>