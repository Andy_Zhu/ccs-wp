<?php

/**
 * @property integer tour_cat_id
 * @property integer id
 */
class Websites_Tours_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'tours';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_tours($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'ranking asc',
			'website_id'=>null,
			'tour_id'=>null,
			'tour_cat_id' => null,
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['tour_id']) && !is_array($options['tour_id'])) {
			$options['tour_id'] = array((int)$options['tour_id']);	
		}
		
		if(isset($options['tour_cat_id']) && !is_array($options['tour_cat_id'])) {
			$options['tour_cat_id'] = array((int)$options['tour_cat_id']);	
		}
		
		if(isset($options['website_id']) && !is_array($options['website_id'])) {
			$options['website_id'] = array($options['website_id']);
		}
		
		$this->db->from($this->_table_name);
		
		
		if(!empty($options['website_id'])) {
			$this->db->where('website_id in (' . implode(',', $options['website_id']) . ')');
		}
		if(!empty($options['tour_cat_id'])) {
			$this->db->where('tour_cat_id in (' . implode(',', $options['tour_cat_id']) . ')');
		}
		
		if(!empty($options['tour_id'])) {
			$this->db->where('id in (' . implode(',', $options['tour_id']) . ')');	
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$tours= array();
		
		foreach ($query->result() as $row) {
			$tours[$row->id] = $row;
		}
		
		return $tours;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['website_id']) && !is_array($options['website_id'])) {
			$options['website_id'] = array($options['website_id']);
		}
		
		if(isset($options['tour_cat_id']) && !is_array($options['tour_cat_id'])) {
			$options['tour_cat_id'] = array($options['tour_cat_id']);
		}
		
		
		
		$this->db->select('count(id) as toursCount');
		$this->db->from($this->_table_name);
		
		if(!empty($options['website_id'])) {
			$this->db->where('website_id in (' . implode(',', $options['website_id']) . ')');
		}
		
		if(!empty($options['tour_cat_id'])) {
			$this->db->where('tour_cat_id in (' . implode(',', $options['tour_cat_id']) . ')');
		}
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->toursCount;
	}
	
	
	public function create($data) {
	
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	/**
	 * @param $id
	 * @return Websites_Tours_Model
	 */
	public function find($id) {
		
		if(!$id) return NULL;
		
		if(is_array($id)) {
			$result = $this->db->get_where($this->_table_name, $id);
		}else {
			$result = $this->db->get_where($this->_table_name, array('id'=>$id));
		}
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function delete($id) {
		
		$rs = $this->db->delete($this->_table_name, array('id'=>$id));
		
		if(!$rs) return $rs;
		
		$this->load->model('Text_Boxes_Model');
		
		$boxes = $this->Text_Boxes_Model->get_boxes(array('extra_id'=>$id, 'type'=>'tour', 'page_uri'=>'tours'));

		foreach ($boxes as $box) {
			$this->Text_Boxes_Model->delete($box->id);
		}
		
		return $rs;
	}
	
	public function update($data, $id) {
		
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_tours_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$tours = $this->get_tours($options);
		
		$paginator['data'] = $tours;
		
		return $paginator;
	}
	
	public function seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);
	    
	    if($unique) {
	    	
	    	$temp = $string;
	    	$counter = 1;
	    	while($this->findByUrl($string)) {
	    		
	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }
	    
	    return trim($string, ' -');
	}
	

}

?>