<?php

/**
 * @property string thumb
 * @property string page_name
 * @property string icon
 * @property integer id
 * @property string filename
 */
class Vehicles_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'vehicles';
	

	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_vehicles($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'id desc',
			'vehicle_id'=>null,
            'lang_code' => null,
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->from($this->_table_name);
		
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		if(!empty($options['vehicle_id'])) {
			$this->db->where('id in (' . implode(',', $options['vehicle_id']) . ')');
		}
		
		$query = $this->db->get();
		
		$vehicles = array();
		
		foreach ($query->result() as $row) {


            if(!empty($options['lang_code'])) {

                if($row->{'long_name_'.$options['lang_code']} != '') {
                    $row->long_name = $row->{'long_name_'.$options['lang_code']};
                }
                if($row->{'short_name_'.$options['lang_code']} != '') {
                    $row->short_name = $row->{'short_name_'.$options['lang_code']};
                }
                if($row->{'internal_name_'.$options['lang_code']} != '') {
                    $row->internal_name = $row->{'internal_name_'.$options['lang_code']};
                }
                if($row->{'description_'.$options['lang_code']} != '') {
                    $row->description = $row->{'description_'.$options['lang_code']};
                }
            }

            $vehicles[$row->id] = $row;
		}

		return $vehicles;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(id) as vCount');
		$this->db->from($this->_table_name);
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->vCount;
	}
	
	
	public function create($data) {
		$data['page_name'] = $this->_seo_name($data['short_name']);
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	/**
	 * @param $id
	 * @return Vehicles_Model
	 */
	public function find($id) {

		if(!$id) return NULL;

		$result = $this->db->get_where($this->_table_name, array('id'=>$id));
		
		if(!$result->num_rows()) return NULL;

		return $result->row();
		
	}
	
	public function find_by_page_name($name, $lang_code = 'eng') {
		
		if(!$name) return NULL;

		$result = $this->db->get_where($this->_table_name, array('page_name'=>$name));
		
		if(!$result->num_rows()) return NULL;

        $row = $result->row();

        if($lang_code != 'eng') {
            if($row->{'long_name_'.$lang_code} != '') {
                $row->long_name = $row->{'long_name_'.$lang_code};
            }
            if($row->{'short_name_'.$lang_code} != '') {
                $row->short_name = $row->{'short_name_'.$lang_code};
            }
            if($row->{'internal_name_'.$lang_code} != '') {
                $row->internal_name = $row->{'internal_name_'.$lang_code};
            }
            if($row->{'description_'.$lang_code} != '') {
                $row->description = $row->{'description_'.$lang_code};
            }
        }
		
		return $row;
		
	}
	
	public function delete($id) {
		
		return $this->db->delete($this->_table_name, array('id'=>$id));
		
	}
	
	
	public function update($data, $id) {
		$data['page_name'] = $this->_seo_name($data['short_name']);
		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_vehicles_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_vehicles($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}
	
	
	
	protected function _seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);
	    
	    if($unique) {
	    	
	    	$temp = $string;
	    	$counter = 1;
	    	while($this->find_by_page_name($string)) {
	    		
	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }
	    
	    return strtolower(trim($string, ' -'));
	}
	

}

?>
