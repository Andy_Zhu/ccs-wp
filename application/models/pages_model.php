<?php

class Pages_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'pages';
	

	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_pages($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'id desc',
			'website_id'=>null,
			'page_uri'=>null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->from($this->_table_name);
		
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		if(!empty($options['website_id'])) {
			$this->db->where('website_id in (' . implode(',', $options['website_id']) . ')');
		}
		
		if(!empty($options['page_uri'])) {
			$this->db->where('page_uri in (' . implode(',', $options['page_uri']) . ')');
		}
		
		$query = $this->db->get();
		
		$pages = array();
		
		foreach ($query->result() as $row) {
			$pages[] = $row;
		}
		
		return $pages;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(*) as vCount');
		$this->db->from($this->_table_name);
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->vCount;
	}
	
	
	public function create($data) {
		
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function find($id, $uri) {
		
		if(!$id) return NULL;

		$result = $this->db->get_where($this->_table_name, array('website_id'=>$id, 'page_uri'=>$uri));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function delete($id, $uri) {
		
		return $this->db->delete($this->_table_name, array('website_id'=>$id, 'page_uri'=>$uri));
		
	}
	
	
	public function update($data, $id, $uri) {
		
		$this->db->where('website_id', $id);
		$this->db->where('page_uri', $uri);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_pages_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$pages = $this->get_pages($options);
		
		$paginator['data'] = $pages;
		
		return $paginator;
	}
	
	
	
	protected function _seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);
	    
	    if($unique) {
	    	
	    	$temp = $string;
	    	$counter = 1;
	    	while($this->find_by_page_name($string)) {
	    		
	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }
	    
	    return strtolower(trim($string, ' -'));
	}
	

}

?>