<?php

class Static_Pages_Model extends CI_Model {

	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'static_pages';
	
	
	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_pages($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'id desc',
			'website_id'=>null,
			'page_id'=>null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['page_id']) && !is_array($options['page_id'])) {
			$options['page_id'] = array((int)$options['page_id']);
		}
		
		$this->db->from($this->_table_name);
		
		if(isset($options['website_id'])) {
			
			$this->db->where('website_id', $options['website_id']);
		}
		
		if(!empty($options['page_id'])) {
			$this->db->where('id in (' . implode(',', $options['page_id']) . ')');	
		}
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$websites = array();
		
		foreach ($query->result() as $row) {
			$websites[$row->id] = $row;
		}
		
		return $websites;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			'website_id' => null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		$this->db->select('count(id) as websitesCount');
		$this->db->from($this->_table_name);
		
		if(isset($options['website_id'])) {
			
			$this->db->where('website_id', $options['website_id']);
		}
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->websitesCount;
	}
	
	
	public function create($data) {
		
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function create_about_us($data) {
		$data['uri'] = 'about-us';
		$data['name'] = 'About us';
	}
	
	public function create_faqs($data) {
		$data['uri'] = 'faqs';
		$data['name'] = 'Faqs';
	}
	
	public function create_policies($data) {
		$data['uri'] = 'policies';
		$data['name'] = 'Policies';
	}
	
	public function find($site_id, $page_uri) {
		
		
		$result = $this->db->get_where($this->_table_name, array('website_id'=>$site_id, 'uri'=>$page_uri));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function find_by_uri($uri, $website_id) {
		
		$result = $this->db->get_where($this->_table_name, array('uri'=>$uri, 'website_id'=>$website_id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	public function has_page($uri, $website_id) {
	
		$result = $this->db->get_where($this->_table_name, array('uri'=>$uri, 'website_id'=>$website_id));
		
		if(!$result->num_rows()) return false;
		
		return $result->row();
		
	}
	
	
	public function delete($id, $website_id) {
		
		return $this->db->delete($this->_table_name, array('id'=>$id, 'website_id'=>$website_id));
		
	}
	
	public function update($data, $uri, $website_id) {
		
		$this->db->where('uri', $uri);
		$this->db->where('website_id', $website_id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_websites_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1, 'website_id'=>null);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_pages($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}

}

?>