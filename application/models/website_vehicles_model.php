<?php

class Website_Vehicles_Model extends CI_Model {
	
	/**
	 * 
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'website_vehicules';
	

	public function __construct(){
		
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}
	
	public function get_vehicles($options=array()){
		
		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>null,
			'website_id'=>null,
			'vehicle_id'=>null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['vehicle_id']) && !is_array($options['vehicle_id'])) {
			$options['vehicle_id'] = array($options['vehicle_id']);
		}
		
		$this->db->from($this->_table_name);
		
		
		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}
		
		if(isset($options['website_id'])) {
			$this->db->where('website_id', $options['website_id']);
		}
		
		if(!empty($options['vehicle_id'])) {
			$this->db->where('vehicle_id in (' . implode(',', $options['vehicle_id']). ')');
		}
		
		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}
		
		$query = $this->db->get();
		
		$vehicules = array();
		
		foreach ($query->result() as $row) {
			$vehicules[$row->vehicle_id] = $row;
		}
		
		return $vehicules;
		
	}
	
	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(
			'website_id'=>null,
			'vehicle_id'=>null
		);
		
		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}
		
		if(isset($options['vehicle_id']) && !is_array($options['vehicle_id'])) {
			$options['vehicle_id'] = array((int)$options['vehicle_id']);
		}
		
		$this->db->select('count(*) as vCount');
		$this->db->from($this->_table_name);
		
		if(isset($options['website_id'])) {
			$this->db->where('website_id', $options['website_id']);
		}
		
		if(!empty($options['vehicle_id'])) {
			$this->db->where('vehicle_id in (' . implode(',', $options['vehicle_id']) . ')');
		}
		
		$query = $this->db->get();
		
		$row = $query->row();
		
		return $row->vCount;
	}
	
	
	public function create($data) {
		
		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}
	
	public function find($vehicle_id, $website_id) {
		
		$result = $this->db->get_where($this->_table_name, array('vehicle_id'=>$vehicle_id, 'website_id'=>$website_id));
		
		if(!$result->num_rows()) return NULL;
		
		return $result->row();
		
	}
	
	
	public function delete($vehicle_id, $website_id) {
		
		return $this->db->delete($this->_table_name, array('vehicle_id'=>$vehicle_id, 'website_id'=>$website_id));
		
	}
	
	public function clear_for_website($id) {
		
		return $this->db->delete($this->_table_name, array('website_id'=>$id));
	}
	
	public function clear_for_vehicle($id) {
		
		return $this->db->delete($this->_table_name, array('vehicle_id'=>$id));
	}
	
	public function update($data, $vehicle_id, $website_id) {
		
		$this->db->where('vehicle_id', $vehicle_id);
		$this->db->where('website_id', $website_id);
		return $this->db->update($this->_table_name, $data);	
	}
	
	function get_vehicles_paginator($options=array()) {
		
		$defaultOptions = array('itemCountPerPage'=>10,'page'=>1);
		
		foreach ($defaultOptions as $key =>$value) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
		}
		
		unset($options['limit']);
		unset($options['offset']);
		
		$totalCount = $this->count($options);
		$itemCountPerPage = $options['itemCountPerPage'];
		$page = $options['page'];
		
		//set up the paginator array
		$paginator = array();
		$paginator['itemCountPerPage'] = $itemCountPerPage;
		$paginator['totalCount'] = $totalCount;
		$paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
		$paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
		$paginator['pagesInRange'] = array();
		for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
			array_push($paginator['pagesInRange'], $pageNum);
		}
		
		$paginator['next'] = $paginator['page'] + 1;
		if($paginator['next'] > $paginator['pages']) {
			$paginator['next'] = 0;
		}
		
		$paginator['previous'] = $paginator['page'] - 1;
		if($paginator['previous'] <= 0) {
			$paginator['previous'] = 0;
		}
		
		$passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
		$showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
		$paginator['from'] = $passed;
		$paginator['to'] = $showing;
		
		//fetch restaurant for the current page
		$limit = $itemCountPerPage;
		$offset = $passed - 1;
		
		$options['limit'] = $itemCountPerPage;
		$options['offset'] = $offset;
		$users = $this->get_vehicles($options);
		
		$paginator['data'] = $users;
		
		return $paginator;
	}

    private function get_vehicle_class($id)
    {
        if($id == '') return false;
        $this->load->model('Vehicle_Classes_Model');
        $row = $this->Vehicle_Classes_Model->find($id);
        return $row->name;
    }

    public function get_vehicles_fields($website_id)
    {
        $sql = 'SELECT b.id, b.class_id as cid, b.thumb as b_img, b.passengers as pax, b.luggage as lug, b.icon as s_img, a.half_day_price as hd_rate, a.full_day_price as fd_rate, a.half_day_hour as hd_time, a.full_day_hour as fd_time, a.extra_hours_price as hourly_rate, a.extra_km_price as km_rate, min(a.airport_transfer_price) as airport_rate, a.border_crossing_price as cross_border_rate FROM website_vehicules a join vehicles b on b.id=a.vehicle_id where a.website_id=? group by cid order by a.ranking;';
        $query = $this->db->query($sql, array($website_id));
        $row = $query -> result();

        foreach($row as $val)
        {
            $val->class = $this->get_vehicle_class($val->cid);
        }
        return $row; 
    }
}
?>
