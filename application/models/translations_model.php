<?php

/**
 * @property string label
 * @property string language
 * @property CI_Loader load
 */
class Translations_Model extends CI_Model {

    /**
     *
     * Database table name
     * @var string
     */
    var $_table_name = 'translations';


    public function __construct(){

        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    public function get_translations($options=array()){


        $defaultOptions = array(
            'language'=>NULL,
            'label'=>NULL,
            'limit'=>NULL,
            'offset'=>NULL,
            'order'=>'translations.id desc'
        );

        foreach ($defaultOptions as $key => $v) {
            $options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
        }

        $this->db->from($this->_table_name);
        $this->db->join('languages', 'languages.id = lang_id');
        $this->db->select('translations.id as id, translations.label as label, translations.translation as translation, languages.id as lang_id, languages.lang_code as lang_code, languages.language as language');

        if(!empty($options['language'])) {
            $this->db->where("languages.language ='".$options['language']."'");
        }

        if(!empty($options['label'])) {
            $this->db->where("translations.label ='".$options['label']."'");
        }

        if(isset($options['limit'])) {
            $this->db->limit($options['limit'], $options['offset']);
        }

        if(isset($options['order'])) {
            $this->db->order_by($options['order']);
        }

        $query = $this->db->get();

        $translations = array();

        foreach ($query->result() as $row) {
            $translations[] = $row;
        }

        return $translations;

    }

    public function count($options=array()) {
        unset($options['limit']);
        unset($options['offset']);
        $defaultOptions = array(
            'language'=>NULL,
            'order'=>'translations.id desc'
        );

        foreach ($defaultOptions as $key => $v) {
            $options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
        }

        $this->db->select('count(translations.id) as translationsCount');
        $this->db->from($this->_table_name);
        $this->db->join('languages', 'languages.id = lang_id');

        if(!empty($options['language'])) {
            $this->db->where("languages.language ='".$options['language']."'");
        }

        if(isset($options['order'])) {
            $this->db->order_by($options['order']);
        }

        $query = $this->db->get();

        $row = $query->row();

        return $row->translationsCount;
    }

    function get_translations_paginator($options=array()) {

        $defaultOptions = array('itemCountPerPage'=>10,'page'=>1);

        foreach ($defaultOptions as $key =>$value) {
            $options[$key] = array_key_exists($key, $options) ? $options[$key] : $value;
        }

        unset($options['limit']);
        unset($options['offset']);

        $totalCount = $this->count($options);
        $itemCountPerPage = $options['itemCountPerPage'];
        $page = $options['page'];

        //set up the paginator array
        $paginator = array();
        $paginator['itemCountPerPage'] = $itemCountPerPage;
        $paginator['totalCount'] = $totalCount;
        $paginator['pages'] = ceil($paginator['totalCount']/$paginator['itemCountPerPage']);
        $paginator['page'] = max(1, min($paginator['pages'], $page)); //make sure the page is within the range
        $paginator['pagesInRange'] = array();
        for($pageNum=1; $pageNum <= $paginator['pages']; $pageNum++) {
            array_push($paginator['pagesInRange'], $pageNum);
        }

        $paginator['next'] = $paginator['page'] + 1;
        if($paginator['next'] > $paginator['pages']) {
            $paginator['next'] = 0;
        }

        $paginator['previous'] = $paginator['page'] - 1;
        if($paginator['previous'] <= 0) {
            $paginator['previous'] = 0;
        }

        $passed = ($itemCountPerPage * ($paginator['page']-1)) + 1;
        $showing = min(($itemCountPerPage * $paginator['page']), $totalCount);
        $paginator['from'] = $passed;
        $paginator['to'] = $showing;

        //fetch restaurant for the current page
        $limit = $itemCountPerPage;
        $offset = $passed - 1;

        $options['limit'] = $itemCountPerPage;
        $options['offset'] = $offset;
        $translations = $this->get_translations($options);

        $paginator['data'] = $translations;

        return $paginator;
    }

    public function create_translation($data) {
        $this->db->insert($this->_table_name, $data);
        return $this->db->insert_id();
    }

    public function delete($id) {
        return $this->db->delete($this->_table_name, array('id'=>$id));
    }

	/**
	 * @param $id
	 * @return Translations_Model
	 */
	public function find($id) {
        if(!$id) return NULL;
        $this->db->join('languages', 'languages.id = lang_id');
        $this->db->select('translations.id as id, translations.label as label, translations.translation as translation, languages.id as lang_id, languages.lang_code as lang_code, languages.language as language');
        $result = $this->db->get_where($this->_table_name, array('translations.id'=>$id));
        if(!$result->num_rows()) return NULL;
        return $result->row();
    }

    public function get_english_translation($label) {
        if(!$label) return NULL;
        $this->db->join('languages', 'languages.id = lang_id');
        $this->db->select('translations.id as id, translations.label as label, translations.translation as translation, languages.id as lang_id, languages.lang_code as lang_code, languages.language as language');
        $this->db->where("languages.language = 'english'");
        $result = $this->db->get_where($this->_table_name, array('translations.label'=>$label));
        if(!$result->num_rows()) return NULL;
        return $result->row()->translation;
    }

    public function update($data, $id) {

        $this->db->where('id', $id);
        return $this->db->update($this->_table_name, $data);
    }

	public function cleanup_translations($labels) {
		$this->db->select('label');
		$this->db->where_not_in('label', array_unique($labels));
		$this->db->delete($this->_table_name);
//		echo $this->db->last_query();
		$this->db->get($this->_table_name);
	}
}
