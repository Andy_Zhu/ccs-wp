<?php

/**
 * @property CI_DB_active_record|CI_DB_mysql_driver db
 * @property CI_Loader load
 */
class Email_Templates_Model extends CI_Model {

	/**
	 *
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'email_templates';


	public function __construct() {

		parent::__construct();
		$this->load->database();
	}

	public function get_template($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->_table_name);
		if ($query->num_rows() == 0) {
			return null;
		} else {
			return $query->row();
		}
	}

	public function save_template($data, $id=false) {
		if ($id){
			$this->db->where('id', $id);
			if ($this->db->update($this->_table_name, $data)){
				return $id;
			}else{
				return 0;
			}
		}else{
			if ($this->db->insert($this->_table_name, $data)){
				return $this->db->insert_id();
			}else{
				return 0;
			}
		}
	}

	public function find_by_name_and_website($name, $website) {
		$this->db->select('email_templates.email as email');
		$this->db->join('email_templates_names', "email_templates_names.id=email_templates.template_name_id");
		$this->db->join('websites', "websites.id=email_templates.website_id");
		$this->db->where("websites.name", $website);
		$this->db->where("email_templates_names.name", $name);
		$query = $this->db->get($this->_table_name);

		return $query->row();
	}

	public function delete_template($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->_table_name);
	}

	/**
	 * @param $name mixed
	 * @param $site mixed
	 * @return array
	 */
	public function get_all($name=false, $site=false) {
		if ($name || $site){
			if ($name)
				$this->db->where('template_name_id', $name);
			if ($site)
				$this->db->where('website_id', $site);
		}else{

		}
		$query = $this->db->get($this->_table_name);

		$out = array();
		foreach ($query->result() as $result){
			$out[$result->id] = $result;
		}
		return $out;
	}
}