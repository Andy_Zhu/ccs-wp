<?php

/**
 * @property CI_DB_active_record|CI_DB_mssql_driver db
 * @property string name
 */
class Booking_Services_Model extends CI_Model {

	/**
	 *
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'booking_services';


	public function __construct(){

		parent::__construct();
		$this->load->database();
		$this->load->library('session');
	}

	public function get_booking_services($options=array()){

		$defaultOptions = array(
			'limit'=>NULL,
			'offset'=>NULL,
			'order'=>'service_name asc',
			'website_id'=>null,
			'box_id'=>null
		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		$this->db->from($this->_table_name);

    if(isset($options['box_id']) && !is_array($options['box_id'])) {
      $options['box_id'] = array($options['box_id']);
    }

		if(isset($options['website_id']) && !is_array($options['website_id'])) {
			$options['website_id'] = array($options['website_id']);
		}

		if(!empty($options['website_id'])) {
			$this->db->where('website_id in (' . implode(',', $options['website_id']) . ')');
		}

    if(!empty($options['box_id'])) {
      $this->db->where('id in (' . implode(',', $options['box_id']) . ')');
    }

		if(isset($options['limit'])) {
			$this->db->limit($options['limit'], $options['offset']);
		}

		if(isset($options['order'])) {
			$this->db->order_by($options['order']);
		}

		$query = $this->db->get();
  $dbgquery = $this->db->last_query();
		$services = array();

		foreach ($query->result() as $row) {
			$services[$row->id] = $row;
		}
		return $services;

	}

	public function count($options=array()) {
		unset($options['limit']);
		unset($options['offset']);
		$defaultOptions = array(

		);

		foreach ($defaultOptions as $key => $v) {
			$options[$key] = array_key_exists($key, $options) ? $options[$key] : $v;
		}

		$this->db->select('count(id) as countriesCount');
		$this->db->from($this->_table_name);

		$query = $this->db->get();

		$row = $query->row();

		return $row->countriesCount;
	}


	public function create($data) {

		$this->db->insert($this->_table_name, $data);
		return $this->db->insert_id();
	}

	/**
	 * @param integer $id
	 * @return Services_Model
	 */
	public function find($id) {

		if(!$id) return NULL;

    if(is_array($id)) {
      $result = $this->db->get_where($this->_table_name, $id);
    }else{
      $result = $this->db->get_where($this->_table_name, array('id'=>$id));
    }

		if(!$result->num_rows()) return NULL;

		return $result->row();

	}

	public function find_by_name($name) {

		if(!$name) return NULL;

		$result = $this->db->get_where($this->_table_name, array('service_name'=>$name));

		if(!$result->num_rows()) return NULL;

		return $result->row();
	}

	public function delete($id) {

		return $this->db->delete($this->_table_name, array('id'=>$id));

	}

	public function update($data, $id) {

		$this->db->where('id', $id);
		return $this->db->update($this->_table_name, $data);
	}

	public function clear_for_page($website_id) {

    $options = array('website_id'=>$website_id);

    return $this->db->delete($this->_table_name, $options);

  }
	function seo_name($string, $unique= FALSE) {
	    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
	    $string = strtolower($string);
	    //Strip any unwanted characters
	    $string = preg_replace('/[^a-z0-9_\s-]/', "", $string);
	    //Clean multiple dashes or whitespaces
	    $string = preg_replace('/[\s-]+/', " ", $string);
	    //Convert whitespaces and underscore to dash
	    $string = preg_replace('/[\s_]/', "-", $string);

	    if($unique) {

	    	$temp = $string;
	    	$counter = 1;
	    	while($this->findByUrl($string)) {

	    		$string = $temp . '-' . $counter;
	    		$counter++;
	    	}
	    }

	    return trim($string, ' -');
	}


}

?>