<?php

/**
 * @property CI_DB_active_record|CI_DB_mysql_driver db
 * @property CI_Loader load
 */
class Email_Templates_Names_Model extends CI_Model {

	/**
	 *
	 * Database table name
	 * @var string
	 */
	var $_table_name = 'email_templates_names';


	public function __construct() {

		parent::__construct();
		$this->load->database();
	}

	/**
	 * @param integer $id Template name id
	 * @return Email_Templates_Names_Model
	 */
	public function get_name($id) {
		$this->db->where('id', $id);
		$query = $this->db->get($this->_table_name);
		if ($query->num_rows() == 0) {
			return null;
		} else {
			return $query->row();
		}
	}

	public function save_name($data, $id=false) {
		if ($id){
			$this->db->where('id', $id);
			if ($this->db->update($this->_table_name, $data)){
				return $id;
			}else{
				return 0;
			}
		}else{
			if ($this->db->insert($this->_table_name, $data)){
				return $this->db->insert_id();
			}else{
				return 0;
			}
		}
	}

	public function delete_name($id) {
		$this->db->where('id', $id);
		$this->db->delete($this->_table_name);
	}

	/**
	 * @return array
	 */
	public function get_all() {
		$query = $this->db->get($this->_table_name);
		$out = array();
		foreach ($query->result() as $result){
			$out[$result->id] = $result;
		}
		return $out;
	}
}