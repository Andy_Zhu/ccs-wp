<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @property CI_Loader load
 * @property Languages_Model Languages_Model
 * @property Users Users
 * @property CI_Email email
 * @property CI_Session session
 * @property CI_Form_validation form_validation
 * @property CI_Input input
 * @property Websites_Model Websites_Model
 * @property Countries_Model Countries_Model
 * @property Slideshow_Images_Model Slideshow_Images_Model
 * @property Text_Boxes_Model Text_Boxes_Model
 * @property Websites_Tours_Categories_Model Websites_Tours_Categories_Model
 * @property Websites_Tours_Model Websites_Tours_Model
 * @property Static_Pages_Model Static_Pages_Model
 * @property Vehicles_Model Vehicles_Model
 * @property Website_Vehicles_Model Website_Vehicles_Model
 * @property Website_Banners_Model Website_Banners_Model
 * @property CI_Config config
 * @property CI_Uri uri
 * @property Website_Processor website_processor
 * @property Text_Boxes_Categories_Model Text_Boxes_Categories_Model
 * @property CI_Image_lib image_lib
 * @property Pages_Model Pages_Model
 * @property CI_Upload upload
 * @property Vehicle_Classes_Model Vehicle_Classes_Model
 * @property Vehicle_Images_Model Vehicle_Images_Model
 * @property CI_FTP ftp
 * @property Logs_Model Logs_Model
 * @property Translations_Model Translations_Model
 * @property CI_DB_active_record|CI_DB_mssql_driver db
 * @property Revisions_Model Revisions_Model
 * @property Email_Templates_Model Email_Templates_Model
 * @property Email_Templates_Names_Model Email_Templates_Names_Model
 * @author Omar TCHOKHANI <omatcho02@gmail.com>
 *
 */
class Admin extends CI_Controller {

	const TRANSLATIONS_PER_PAGE = 100;

	public function __construct() {

		parent::__construct();

		$this->load->database();
		$this->load->model('Users');
		$this->load->helper(array('url', 'form', 'add_head_js', 'add_head_css', 'pagination', 'form', 'images', 'templates'));
		$this->load->library('form_validation', 'session');
		if (!$this->Users->has_identity() && $this->uri->segment(2) != 'login') {
			redirect(site_url('admin/login'));
		}

	}

	function _checkAdminPrivileges() {
		if (!$this->Users->has_identity() || !$this->Users->is_admin()) {
			redirect(site_url('admin'));
		}
	}

	/**
	 * Login Page
	 * Authenticate an admin user
	 */
	public function login() {

		if ($this->Users->has_identity()) redirect(site_url('admin/'));

		$data = array('tab' => NULL);

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
			//show message if the form was submited
			if ($this->input->post('admin_login_btn'))
				$data['login_error'] = "Invalid login information.";

		} else {
			//begin login proccess
			$email = $this->input->post('email');
			$raw_password = $this->input->post('password');

			$row = $this->Users->find_by_credential($email, $raw_password);

			if ($row) {
				if ($row->status == 'A') { //if the account is enabled
					$this->Users->create_identity($row); //login the user
					redirect(site_url('admin/'));
				} else {
					$data['login_error'] = "Your account has been disabled.";
				}
			} else {

				$data['login_error'] = "Invalid login information.";
			}
		}

		$data['isPopup'] = true;
		$data['view_page'] = 'admin/login';
		$this->load->view('admin/layout', $data);
	}

	/**
	 * Logout page
	 *clear admin session
	 */
	public function logout() {

		$this->Users->clear_identity();
		redirect(site_url('admin/login'));
	}

	public function notauth() {
		$data = array('tab' => NULL);
		$data['isPopup'] = true;
		$data['view_page'] = 'admin/notauth';
		$this->load->view('admin/layout', $data);
	}

	/**
	 * admin home page
	 *
	 */
	public function index() {

		if (!$this->Users->has_identity()) {

			redirect(site_url('admin/login'));

		} else {
			//for now just redirect to restaurants page
			redirect(site_url('admin/websites'));
		}

	}


	/**
	 *
	 * Account page
	 * Update account info
	 * Change Password
	 */
	public function account() {

		$data = array('tab' => 'account');

		//process form submitions
		if ($this->input->post('profile')) {

			$this->form_validation->set_rules('firstname', 'Firstname', 'required|xss_clean');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');

			if ($this->form_validation->run() == FALSE) {
				$data['form_error_message'] = 'Failed to update info. Please try again.';
			} else {

				$update = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email')
				);

				$rs = $this->Users->update($update, $this->Users->get_identity()->id);
				if ($rs) {
					//update session
					$this->Users->create_identity($this->Users->find($this->Users->get_identity()->id));
				}
				$data['flashMessages'] = array('Information updated successfully.');
			}

		} else if ($this->input->post('change_password_btn')) {

			$this->form_validation->set_rules('current_password', 'Current Password', 'required|xss_clean');
			$this->form_validation->set_rules('new_password', 'New Password', 'required|xss_clean');
			$this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|xss_clean');

			if ($this->form_validation->run() == FALSE) {

				$data['change_password_error'] = 'Failed to change password. please try again.';

			} else if ($this->Users->get_identity()->password != $this->Users->encrypt_password($this->input->post('current_password'))) {

				$data['change_password_error'] = 'Current Password Invalid. please try again.';

			} else if ($this->input->post('new_password') != $this->input->post('new_password2')) {

				$data['change_password_error'] = 'New Password Confim did not match. please try again.';

			} else {

				$update = array('password' => $this->Users->encrypt_password($this->input->post('new_password')));
				$rs = $this->Users->update($update, $this->Users->get_identity()->id);
				if ($rs) {
					//update session
					$this->Users->create_identity($this->Users->find($this->Users->get_identity()->id));
				}
				$data['flashMessages'] = array('Password changed successfully.');
			}
		}


		$data['identity'] = $this->Users->get_identity();
		$data['view_page'] = 'admin/account';
		$this->load->view('admin/layout', $data);

	}

	public function users($filter = "any-all", $page = 1) {

		$data = array('tab' => 'users');

		if (!$this->Users->is_admin()) { //anyone can manage restaurants;
			$this->notauth();
			return;
		}

		if ($this->input->post('filter_btn')) {

			$page = $this->input->post('page');
			$role = $this->input->post('role');
			$status = $this->input->post('status');

			$url = 'admin/users/' . $role . '-' . $status . '/' . $page;
			redirect(site_url($url));
		}

		if ($this->input->post('add_user_btn')) {

			$this->form_validation->set_rules('firstname', 'Firstname', 'required|xss_clean');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email|callback_unique_email');
			$this->form_validation->set_rules('role', 'Role', 'required|xss_clean');
			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');
			if ($this->form_validation->run() == false) {
				$data['create_user_message'] = 'Faild to create user, all fields are required';
			} else {

				$user = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'role' => $this->input->post('role'),
					'password' => $this->Users->random_password()
				);
				//add to db

				if ($this->Users->create_user($user)) {

					//send the password.
					//config for smtp
					/* $config = Array(
					  'protocol' => 'smtp',
					  'smtp_host' => 'ssl://smtp.googlemail.com',
					  'smtp_port' => 465,
					  'smtp_user' => 'your  account@gmail.com', // change it to yours
					  'smtp_pass' => 'your_password', // change it to yours
					  'mailtype' => 'html',
					  'charset' => 'utf-8',
					  'wordwrap' => TRUE
					);*/
					//config for sendmail
					$config = array(
						'mailtype' => 'html',
						'charset' => 'utf-8',
						'wordwrap' => TRUE
					);

					$this->load->library('email', $config);
					//$this->load->library('email');
					$this->email->set_newline("\r\n");

					$subject = 'Your CCS ' . $user['role'] . ' Account';
					$from = 'admin@beijingcarservice.com';
					$message = $this->load->view('admin/templates/email_login_info.php', $user, TRUE);

					$this->email->from($from, 'CCS');
					$this->email->to($user['email']);
					$this->email->subject($subject);
					$this->email->message($message);
					$this->email->send();

					//echo $this->email->print_debugger();
					$this->session->set_flashdata('info', array('User ' . $user['firstname'] . ' ' . $user['lastname'] . ' Added successfully'));
					redirect(site_url('admin/users'));
				} else {
					$data['create_user_message'] = 'Faild to create user, database error, please try again';
				}
			}
		}

		$filters = explode("-", $filter);
		$role = $filters[0];
		$status = $filters[1];
		$page = max(1, (int)$page);

		$options = array('itemCountPerPage' => 10, 'page' => $page);
		if ($role != 'any') $options['role'] = $role;
		if ($status != 'all') $options['status'] = $status;

		$paginator = $this->Users->get_users_paginator($options);


		$data['identity'] = $this->Users->get_identity();
		$data['role'] = $role;
		$data['status'] = $status;
		$data['paginator'] = $paginator;
		$data['view_page'] = 'admin/users';
		$this->load->view('admin/layout', $data);

	}

	function unique_email($email) {

		if ($this->Users->find_by_email($email)) {
			$this->form_validation->set_message('unique_email', 'The %s already in the database');
			return FALSE;
		}

		return TRUE;
	}

	function unique_url($url) {
		$this->load->model('Websites_Model');
		//if ($this->Websites_Model->find_by_url(trim($url))) {
		//	$this->form_validation->set_message('unique_url', 'The %s already in the database');
		//	return FALSE;
		//}

		return TRUE;
	}

	public function edit_user($id = null) {

		if (!$id) {
			$this->session->set_flashdata('info', 'User not found!');
			redirect(site_url('admin/users'));
		}

		$user = $this->Users->find($id);

		if ($this->input->post()) {

			$this->load->library('form_validation');
			$this->form_validation->set_rules('firstname', 'Firstname', 'required|xss_clean');
			$this->form_validation->set_rules('lastname', 'Lastname', 'required|xss_clean');
			if ($user->email != trim($this->input->post('email'))) {
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email|callback_unique_email');
			} else {
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email');
			}

			$this->form_validation->set_rules('role', 'Role', 'required|xss_clean');
			$this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');

			if ($this->form_validation->run() !== FALSE) {

				$update = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'email' => $this->input->post('email'),
					'role' => $this->input->post('role'),
					'status' => $this->input->post('status'),
				);

				if (strlen($this->input->post('password'))) {
					$update['password'] = $this->Users->encrypt_password($this->input->post('password'));
				}
				$this->Users->update($update, $id);

				$this->session->set_flashdata('info', $user->firstname . ' ' . $user->lastname . ' Account info Updated!');
				redirect(site_url('admin/users'));
			}
		}

		$data = array('tab' => 'users', 'page_title' => "Updating user #$id");
		$data['user'] = $user;
		$data['id'] = $id;
		$data['view_page'] = 'admin/edit_user';
		$this->load->view('admin/layout', $data);
	}

	public function websites_list() {
		$this->load->model('Websites_Model');

		if ($this->_is_ajax()) {
			$web_sites = $this->Websites_Model->get_websites();
			$list = array();
			foreach ($web_sites as $key => $value) {
				$website = new StdClass();
				$website->value = $key;
				$website->label = $value->name;
				$list[] = $website;
			}
			header("Content-Type: application/json");
			echo json_encode($list);
			return;
		}
	}

	public function websites() {

		$this->load->model('Websites_Model');
		$this->load->model('Countries_Model');

		if ($this->input->post('add_website_btn')) { //form submitted

			if (!$this->Users->is_admin()) { //only admins can add sites
				$this->session->set_flashdata('info', 'You are not authorized to add a new website, Please contact the admin');
				redirect(site_url('admin/websites'));
			}

			$this->form_validation->set_rules('url', 'URL', 'required|xss_clean|callback_unique_url');
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
			$this->form_validation->set_rules('ftp_host', 'FTP Host', 'required|xss_clean');
			$this->form_validation->set_rules('ftp_user', 'FTP User', 'required|xss_clean');
			$this->form_validation->set_rules('ftp_password', 'FTP Password', 'required|xss_clean');
			$this->form_validation->set_rules('meta_keywords', 'Meta keywords', 'xss_clean');
			$this->form_validation->set_rules('meta_desc', 'Meta description', 'xss_clean');
			$this->form_validation->set_rules('template', 'Template', 'required|xss_clean');

			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');
			if ($this->form_validation->run() == false) {
				$data['create_website_message'] = 'Faild to create website, all fields are required';
			} else {

        $shortname = str_replace("Car Service", "", $this->input->post('name'));
        $shortname = str_replace(" ", "", $shortname);
        $shortname = strtolower($shortname);
				$website = array(
					'url' => $this->input->post('url'),
					'name' => $this->input->post('name'),
					'ftp_host' => $this->input->post('ftp_host'),
					'ftp_user' => $this->input->post('ftp_user'),
					'ftp_password' => $this->input->post('ftp_password'),
					'meta_keywords' => $this->input->post('meta_keywords'),
					'meta_desc' => $this->input->post('meta_desc'),
					'template' => $this->input->post('template'),
					'fax' => 'NA',
					'shortcode' => 'NA',
					'meet_n_greet' => 0,
					'shortname' => $shortname
				);

				$id = $this->Websites_Model->create($website);

				if ($id) {
					$uri = 'admin/edit_website?id=' . $id . "&section=basic";
					redirect(site_url($uri));
				} else {
					$data['create_website_message'] = 'Database error, please try again later';
				}
			}
		}
		$data = array('tab' => 'websites', 'page_title' => 'Websites');
		$country_id = $this->input->get('cid', null);

		if (!$country_id) {
			$data['countries'] = $this->Countries_Model->get_countries();
			$data['view_page'] = 'admin/websites_countries';
			$this->load->view('admin/layout', $data);

			return;
		}

		$order_by_fields = array(
			'name' => 'Name',
			'last_update_date' => 'Last Update',
			'last_publish_date' => 'Last Publish',
			'id' => 'ID'
		);

		$data['order_by_fields'] = $order_by_fields;

		$page = max(1, (int)$this->input->get('page'));
		$ord_field = $this->input->get('ord_field') ? $this->input->get('ord_field') : 'name';
		$ord_dir = $this->input->get('ord_dir') ? $this->input->get('ord_dir') : 'asc';


		$data['ord_field'] = $ord_field;
		$data['ord_dir'] = $ord_dir;
		$order_by_clause = "$ord_field $ord_dir";


		$paginator = $this->Websites_Model->get_websites_paginator(array('itemCountPerPage' => 20, 'page' => $page, 'order' => $order_by_clause, 'country_id' => $country_id));

		$data['country_id'] = $country_id;
		$data['countries'] = $this->Countries_Model->get_countries();
		$data['country'] = $data['countries'][$country_id];
		$data['paginator'] = $paginator;
		$data['view_page'] = 'admin/websites';
		$this->load->view('admin/layout', $data);

	}


	public function edit_website() {

		$this->load->model('Websites_Model');
		$this->load->model('Static_Pages_Model');
		$this->load->model('Text_Boxes_Model');
		$this->load->model('Text_Boxes_Categories_Model');
		$this->load->model('Slideshow_Images_Model');
		$this->load->model('Vehicles_Model');
		$this->load->model('Website_Vehicles_Model');
		$this->load->model('Pages_Model');
		$this->load->model('Logs_Model');
		$this->load->model('Websites_Tours_Model');
		$this->load->model('Websites_Tours_Categories_Model');
		$this->load->model('Tours_Images_Model');
		$this->load->model('Website_Banners_Model');
		$this->load->model('Countries_Model');
		$this->load->model('Languages_Model');
        $this->load->model('Booking_Services_Model');


		$layout_data = array('tab' => 'websites', 'page_title' => 'Updating website');
		$layout_data['countries'] = $this->Countries_Model->get_countries();
		$layout_data['country_id'] = $this->input->get('cid', null);

		$layout_data['languages'] = $this->Languages_Model->get_languages();

		$layout_data['all_websites'] = $this->Websites_Model->get_websites();

		$id = $this->input->get('id');
		$section = $this->input->get('section');

		$website = $this->Websites_Model->find($id);
		$editor_view = template_editor_view($section, $website->template);

		if (is_null($id) || !is_numeric($id)) {
			$uri = 'admin/websites';
			$this->session->set_flashdata('info', "No website with ID $id found");
			redirect(site_url($uri));
		}

		switch ($section) {
			case 'home':
				$layout_data['images'] = $this->Slideshow_Images_Model->get_images(array('website_id' => $id));
				$layout_data['boxes'] = $this->Text_Boxes_Model->get_boxes(array('page_uri' => $section, 'website_id' => $id));
                $about_boxes = array();
                foreach ($layout_data['boxes'] as $key => $box) {
                  if ($box->type == 'about') {
                        $about_boxes[$box->id] = $box;
                        unset($layout_data['boxes'][$key]);
                    }
                }
                $layout_data['home_about_boxes'] = $about_boxes;
				$layout_data['home_service_boxes'] = $this->Text_Boxes_Model->get_boxes(array('page_uri' => $section, 'type' => 'shbox', 'website_id' => $id));
				break;

			case 'tours':
				$layout_data['tours_categories'] = $this->Websites_Tours_Categories_Model->get_tours_cats(array('website_id' => $id));
				break;
			case 'subtours':
				$tour_cat_id = $this->input->get('tourcat');
				$tour_category = $this->Websites_Tours_Categories_Model->find($tour_cat_id);
				$layout_data['tours'] = $this->Websites_Tours_Model->get_tours(array('tour_cat_id' => $tour_cat_id));
				$layout_data['tour_cat_id'] = $tour_cat_id;
				$layout_data['tour_category'] = $tour_category;
				break;
			case 'edittour':
				$tour_id = $this->input->get('tourid');
				$tour = $this->Websites_Tours_Model->find($tour_id);
				$tour_category = $this->Websites_Tours_Categories_Model->find($tour->tour_cat_id);
				$layout_data['tour_text_boxes'] = $this->Text_Boxes_Model->get_boxes(array('extra_id' => $tour->id, 'website_id' => $id, 'type' => 'tour', 'page_uri' => 'tours'));
				$layout_data['tour'] = $tour;
				//echo print_r($tour); die();
				$layout_data['tour_category'] = $tour_category;
				break;
			case 'faqs':
			case 'policies':
			case 'why-service':
				$layout_data['static_page'] = $this->Static_Pages_Model->find_by_uri($section, $id);
				if (!$layout_data['static_page']) {
					$data_file = APPPATH . 'views/templates/' . $website->template . '/data/default_' . $section . '.php';
					$name = ucwords(str_replace('-', ' ', $section));
					if ($section == 'why-service') $name = 'Why ' . $website->name;
					$default_content = !file_exists($data_file) ? "[$section]" : $this->load->view('templates/' . $website->template . '/data/default_' . $section, null, TRUE);

					$data = array('website_id' => $id, 'name' => $name, 'uri' => $section, 'content' => $default_content);
					$this->Static_Pages_Model->create($data);

					$layout_data['static_page'] = $this->Static_Pages_Model->find_by_uri($section, $id);
				}

				if ($section == 'faqs') $layout_data['boxes'] = $this->Text_Boxes_Model->get_boxes(array('page_uri' => $section, 'website_id' => $id, 'type' => 'faq'));

				break;
			case 'fleet':

				$layout_data['vehicles'] = $this->Vehicles_Model->get_vehicles(array('order' => 'short_name asc'));
				$layout_data['site_vehicles'] = $this->Website_Vehicles_Model->get_vehicles(array('website_id' => $id));
				$boxes = $this->Text_Boxes_Model->get_boxes(array('website_id' => $id, 'page_uri' => $section));
				$tmp = $boxes;
				$intro = empty($tmp) ? null : array_shift($tmp);
				$layout_data['intro'] = $intro;
				$layout_data['boxes'] = $boxes;
				break;
			case 'online-booking':
			case 'services':
			case 'rates':
			case 'about-us':
			case 'contact-us': //customer service box only

				$layout_data['boxes'] = $this->Text_Boxes_Model->get_boxes(array('page_uri' => $section, 'website_id' => $id));

				if ($section == 'contact-us' && empty($layout_data['boxes'])) {

					$default_content = $this->load->view('templates/default/data/default_contact_us_box', null, TRUE);
					$data = array('website_id' => $id, 'page_uri' => $section, 'title' => 'Customer Service', 'content' => $default_content);
					$this->Text_Boxes_Model->create($data);
					$layout_data['boxes'] = $this->Text_Boxes_Model->get_boxes(array('page_uri' => $section, 'website_id' => $id));
				}

				if ($section == 'services') {

					$top_boxes = array();
					foreach ($layout_data['boxes'] as $key => $box) {

						if ($box->type == 'top') {
							$top_boxes[$box->id] = $box;
							unset($layout_data['boxes'][$key]);
						}
					}
					$layout_data['services_top'] = $top_boxes;
					$layout_data['services_intro'] = $this->Text_Boxes_Model->find(array('page_uri' => $section, 'website_id' => $id, 'type' => 'intro'));

				}

				if ($section == 'online-booking') {
					foreach ($layout_data['boxes'] as $key => $box) {
						$box->content = str_replace('http://www.beijingcarservice.com/wp-content/themes/ccsnew/images/pay-icons.png', template_image_src('pay-icons.png', true), $box->content);
						$data['boxes'][$key] = $box;
					}
				}
				break;
			case 'locations':
				$layout_data['boxes_cats'] = $this->Text_Boxes_Categories_Model->get_cats(array('section' => $section, 'website_id' => $id));
				$layout_data['selected_box_cat'] = $this->input->get('cat_id') && array_key_exists($this->input->get('cat_id'), $layout_data['boxes_cats']) ? $layout_data['boxes_cats'][$this->input->get('cat_id')] : null;
				$boxes_options = array('page_uri' => $section, 'website_id' => $id);
				if ($website->template == 'qualifire') {
					$cat_id = isset($layout_data['selected_box_cat']) ? $layout_data['selected_box_cat']->id : -1;
					$boxes_options['cat_id'] = $cat_id; //return only boxes in the current selected category/group or nothing (cat_id = -1)
					$layout_data['boxes'] = $this->Text_Boxes_Model->get_boxes($boxes_options);
					//intro
					$boxes_options['cat_id'] = null;
					$boxes_options['type'] = 'intro';
					$intro = $this->Text_Boxes_Model->find($boxes_options);
					if ($intro) $layout_data['boxes'][$intro->id] = $intro;
				} else {
					$layout_data['boxes'] = $this->Text_Boxes_Model->get_boxes($boxes_options);
				}
				break;
			case 'banners':
				$this->load->config('pages');
				$layout_data['banners'] = $this->Website_Banners_Model->get_banners(array('website_id' => $id));
				$layout_data['page_uris'] = $this->config->item('page_uris');
				break;

            case 'booking-services':
                $bookingServices = $this->Booking_Services_Model->get_booking_services(array('website_id' => $id, 'order'=>'ranking asc',));
                $layout_data['booking_services'] = $bookingServices;
                break;

			case 'basic':
				break;
			default:
		}

		$uri = "admin/edit_website?id=$id&section=$section";
		if ($section == 'locations' && isset($layout_data['selected_box_cat'])) {
			$uri .= '&cat_id=' . $layout_data['selected_box_cat']->id;
		}
		$redirect = site_url($uri);

		// Revision processing prepare
		$this->load->model('Revisions_Model');
		$cat_id = isset($layout_data['selected_box_cat']) ? $layout_data['selected_box_cat']->id : false;
		$revision_ident = $this->Revisions_Model->gen_ident($id, $section, $cat_id);

		// Load revision
		if ($this->input->get('load_revision')){
			$revision_id = $this->input->get('load_revision');
			$revision = $this->Revisions_Model->get_version($revision_id);
			$_POST = json_decode($revision->content, true);

			$revision_owner = $this->Users->find($revision->author);
			$message = 'Revision by ' . $revision_owner->firstname .
				' ' . $revision_owner->lastname . ' from ' . $revision->created . ' successfully loaded';

			$this->load->model("Logs_Model");
			$this->Logs_Model->create(array(
				'website_id'=>$id,
				'user_id'=>$this->Users->get_identity()->id,
				'page_uri'=>$section,
				'content'=>$message
			));
			$this->session->set_flashdata('revisions', $message);
		}

		//Save version
		if (!empty($_POST) && !isset($revision_id)){
			$this->Revisions_Model->add_version($revision_ident, $_POST);
		}

		//process any submited form
		$this->load->library('Website_Processor');
		$this->website_processor->process($section, $website, $layout_data, $redirect);

        //add s3 bucket name to object
        if ($this->isProduction()) {
        	$website->s3_bucket_name = 'ccs-origin-' . strtolower(preg_replace(array('/\s+/', '/\-/', '/CarService/'), '', $website->name));
        } else {
        	$website->s3_bucket_name = 'ccs-origin-test';
        }

		$layout_data['revisions'] = $this->Revisions_Model->list_versions_for_ident($revision_ident);
		$layout_data['page'] = $this->Pages_Model->find($id, $section);
		$layout_data['id'] = $id;
		$layout_data['section'] = $section;
		$layout_data['website'] = $website;
		$layout_data['editor_view'] = $editor_view;
		$layout_data['view_page'] = 'admin/edit_website';
		$this->load->view('admin/layout', $layout_data);
	}

	public function vehicles($page = 1) {

		$data = array('tab' => 'vehicles', 'page_title' => 'Vehicles');

		$order_by_fields = array(
			'short_name' => 'Short name',
			'long_name' => 'Long Name',
			'id' => 'ID'
		);

		$data['order_by_fields'] = $order_by_fields;

		$page = max(1, (int)$this->input->get('page'));
		$ord_field = $this->input->get('ord_field') ? $this->input->get('ord_field') : 'short_name';
		$ord_dir = $this->input->get('ord_dir') ? $this->input->get('ord_dir') : 'asc';

		$data['ord_field'] = $ord_field;
		$data['ord_dir'] = $ord_dir;
		$order_by_clause = "$ord_field $ord_dir";

		$this->load->model('Vehicles_Model');
		$this->load->model('Vehicle_Classes_Model');

		$options = array('itemCountPerPage' => 20, 'page' => $page, 'order' => $order_by_clause);

		if ($this->input->post('add_vehicle_btn')) { //form submitted

			$this->form_validation->set_rules('class_id', 'Class', 'required|xss_clean');
			$this->form_validation->set_rules('short_name', 'Short Name', 'required|xss_clean');
			$this->form_validation->set_rules('long_name', 'Long Name', 'required|xss_clean');
			$this->form_validation->set_rules('passengers', 'Passengers', 'required|xss_clean');
			$this->form_validation->set_rules('luggage', 'Luggage', 'required|xss_clean');
			$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
			//$this->form_validation->set_rules('thumb', 'Thumbnail', 'required');


			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');
			if ($this->form_validation->run() == false) {
				$data['form_msg'] = 'Failed to add vehicle, Please fix the errors and try again';
			} else {

				$config['upload_path'] = VEHICLES_THUMB_DIR; /* NB! create this dir! */
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config['max_size'] = '';
				$config['max_width'] = '';
				$config['max_height'] = '';
				$config['overwrite'] = FALSE;

				$this->load->library('upload', $config);

				/* Create the config for image library */
				$configThumb = array();
				$configThumb['image_library'] = 'gd2';
				$configThumb['source_image'] = '';
				$configThumb['create_thumb'] = FALSE;
				$configThumb['maintain_ratio'] = FALSE;

				$configThumb['width'] = 300;
				$configThumb['height'] = 215;
				/* Load the image library */
				$this->load->library('image_lib');

				//thumbnail
				$upload = $this->upload->do_upload('thumb');
				$inDdata = $this->upload->data();
				if ($upload === FALSE || $inDdata['is_image'] != 1) {
					$this->session->set_flashdata('info', 'Thumbnail upload failed, try again');
					$uri = 'admin/vehicles';
					redirect(site_url($uri));
				}

				if ($inDdata['is_image'] == 1) {
					$configThumb['source_image'] = $inDdata['full_path'];
					$this->image_lib->initialize($configThumb);
					$this->image_lib->resize();

				}

				//sidebar menu icon
				$upload = $this->upload->do_upload('icon');
				$iconData = $this->upload->data();
				if ($upload !== FALSE && $iconData['is_image'] == 1) {
					$configThumb['width'] = 70;
					$configThumb['height'] = 45;
					$configThumb['source_image'] = $iconData['full_path'];
					$this->image_lib->initialize($configThumb);
					$this->image_lib->resize();
				}


				$vehicle = $this->input->post();
				unset($vehicle['add_vehicle_btn']);
				$vehicle['thumb'] = $inDdata['file_name'];
				$vehicle['icon'] = isset($iconData['filename']) ? $iconData['filename'] : null;
				$id = $this->Vehicles_Model->create($vehicle);

				if ($id) {
					$this->session->set_flashdata('info', 'Vehicle added successfully');
					$uri = 'admin/vehicles';
					redirect(site_url($uri));
				} else {
					$data['form_msg'] = 'Database error, please try again later';
				}
			}
		}

		$paginator = $this->Vehicles_Model->get_vehicles_paginator($options);

		$data['paginator'] = $paginator;
		$data['classes'] = $this->Vehicle_Classes_Model->get_classes();
		$data['view_page'] = 'admin/vehicles';

		$this->load->view('admin/layout', $data);
	}

	public function edit_vehicle() {

		$layout_data = array('tab' => 'vehicles', 'page_title' => 'vehicle Update');
		$this->load->model('Vehicles_Model');
		$this->load->model('Vehicle_Images_Model');
		$this->load->model('Vehicle_Classes_Model');

		$v_id = $this->input->get('id'); //vehicle id
		$vehicle = $this->Vehicles_Model->find($v_id);

		$classes = $this->Vehicle_Classes_Model->get_classes();
		$images = $this->Vehicle_Images_Model->get_images(array('vehicle_id' => $v_id));

		if ($this->input->post('edit_vehicle_btn')) {

			$this->form_validation->set_rules('class_id', 'Class', 'required|xss_clean');
			$this->form_validation->set_rules('short_name', 'Short Name', 'required|xss_clean');
			$this->form_validation->set_rules('long_name', 'Long Name', 'required|xss_clean');
			$this->form_validation->set_rules('passengers', 'Passengers', 'required|xss_clean');
			$this->form_validation->set_rules('luggage', 'Luggage', 'required|xss_clean');
			$this->form_validation->set_rules('description', 'Description', 'required|xss_clean');

			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');
			if ($this->form_validation->run() == false) {
				$layout_data['form_msg'] = 'Faild to add vehicle, all fields are required';
			} else {

				$config['upload_path'] = VEHICLES_THUMB_DIR; /* NB! create this dir! */
				$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
				$config['max_size'] = '';
				$config['max_width'] = '';
				$config['max_height'] = '';
				$config['overwrite'] = FALSE;

				$this->load->library('upload', $config);

				/* Create the config for image library */
				$configThumb = array();
				$configThumb['image_library'] = 'gd2';
				$configThumb['source_image'] = '';
				$configThumb['create_thumb'] = FALSE;
				$configThumb['maintain_ratio'] = FALSE;

				$configThumb['width'] = 300;
				$configThumb['height'] = 215;
				/* Load the image library */
				$this->load->library('image_lib');

				$old_thumb_path = VEHICLES_THUMB_DIR . '/' . $vehicle->thumb;
				$vehicle_update = $this->input->post();
				unset($vehicle_update['tabs']);
				unset($vehicle_update['edit_vehicle_btn']);
				//upload thumbnail
				$upload = $this->upload->do_upload('thumb');
				$data = $this->upload->data();
				$remove_old_thumb = FALSE;
				if ($upload && $data['is_image'] == 1) {
					$configThumb['source_image'] = $data['full_path'];
					$this->image_lib->initialize($configThumb);
					$this->image_lib->resize();
					$vehicle_update['thumb'] = $data['file_name'];
					$remove_old_thumb = true;
				}

				//sidebar menu icon
				$remove_old_icon = FALSE;
				$old_icon_path = VEHICLES_THUMB_DIR . '/' . $vehicle->icon;
				$upload = $this->upload->do_upload('icon');
				$iconData = $this->upload->data();
				if ($upload !== FALSE && $iconData['is_image'] == 1) {
					$configThumb['width'] = 70;
					$configThumb['height'] = 45;
					$configThumb['source_image'] = $iconData['full_path'];
					$this->image_lib->initialize($configThumb);
					$this->image_lib->resize();
					$vehicle_update['icon'] = $iconData['file_name'];
					$remove_old_icon = TRUE;

				}


				//update the vehicle data
				$id = $this->Vehicles_Model->update($vehicle_update, $v_id);
				//remove old thumb
				if ($remove_old_thumb && ($old_thumb_path != VEHICLES_THUMB_DIR . '/' . $vehicle_update['thumb']))
					@unlink($old_thumb_path);
				if ($remove_old_icon && ($old_icon_path != VEHICLES_THUMB_DIR . '/' . $vehicle_update['icon']))
					@unlink($old_icon_path);

				//upload images begin:
				$config['upload_path'] = VEHICLES_IMAGES_DIR;
				$config['file_name'] = $vehicle->page_name;
				$config['overwrite'] = TRUE;
				$configThumb['create_thumb'] = TRUE;
				$configThumb['width'] = 175;
				$configThumb['height'] = 125;
				$configThumb['overwrite'] = TRUE;
				//upload 4 images
				for ($i = 1; $i < 5; $i++) {
					$config['file_name'] = $vehicle->page_name . '-' . $vehicle->id . '-' . $i;
					$this->upload->initialize($config);
					$upload = $this->upload->do_upload('image' . $i);
					/* File failed to upload - continue */
					if ($upload === FALSE) continue;

					$data = $this->upload->data();
					$uploadedFiles[$i] = $data;

					if ($data['is_image'] == 1) {
						$configThumb['source_image'] = $data['full_path'];
						$this->image_lib->initialize($configThumb);
						$this->image_lib->resize();

						$image = $this->Vehicle_Images_Model->is_set($vehicle->id, $i);
						if ($image) {
							$this->Vehicle_Images_Model->update(array('filename' => $data['file_name']), $vehicle->id, $i);
							//@unlink(vehicle_image_path($image->filename));
							//@unlink(vehicle_image_thumb_path($image->filename));
						} else {
							$this->Vehicle_Images_Model->create(array('filename' => $data['file_name'], 'vehicle_id' => $vehicle->id, 'image_id' => $i));
						}
					}
				}


				if ($id) {
					$this->session->set_flashdata('info', 'Vehicle Updated successfully');
					$uri = 'admin/edit_vehicle?id=' . $vehicle->id;
					redirect(site_url($uri));
				} else {
					$data['form_msg'] = 'Database error, please try again later';
				}
			}
		}

		$layout_data['vehicle'] = $vehicle;
		$layout_data['classes'] = $classes;
		$layout_data['images'] = $images;
		$layout_data['view_page'] = 'admin/edit_vehicle';
		$this->load->view('admin/layout', $layout_data);
	}

	public function preview() {

		$site_id = $this->input->get('id');
		$section = $this->input->get('section');
		$carpage = $this->input->get('carpage');
		$tourcat_id = $this->input->get('catpage');

		if (in_array($section, array('subtours', 'edittour')) && !$tourcat_id) $section = 'tours';
		template_render_page($site_id, $section, $carpage, TRUE, $tourcat_id);
	}

	public function clone_sections() {

		$this->load->model('Websites_Model');
		$this->load->helper('clone');
		$this->load->helper('images');

		$layout_data = array('tab' => 'clone', 'page_title' => 'Clone Sections');
		$sites = $this->Websites_Model->get_websites(array('order' => 'name asc'));
		$pages = array(
			'home' => 'Home',
			'about-us' => 'About us',
			'fleet' => 'Fleet',
			'services' => 'Services',
			'rates' => 'Rates',
			'tours' => 'Tours',
			'online-booking' => 'Online booking',
			'contact-us' => 'Contact us',
			'static-pages' => 'Faqs & Policies',
			'banners' => 'Banners',
			'locations' => 'Locations',
			'booking-services' => 'Booking Services'
		);

		if ($this->input->post('clone')) {
			clone_do_save($this->input->post());
			$this->session->set_flashdata('info', 'Section clone completed successfully');
			redirect(site_url('admin/clone_sections'));
		}

		$layout_data['pages'] = $pages;
		$layout_data['sites'] = $sites;
		$layout_data['view_page'] = 'admin/clone_sections';
		$this->load->view('admin/layout', $layout_data);
	}

	public function publish() {
		/**
		 * Basic steps to publishing should be:
		 * 1. Generate static pages
		 * 2. Create instance of publishing model
		 * 3. Publish through the model
		 */
		set_time_limit(0);
		$start = microtime();
		$this->load->model('Website_Vehicles_Model');
		$this->load->model('Websites_Model');
		$this->load->model('Vehicles_Model');
		$this->load->model('Vehicle_Images_Model');
		$this->load->model('Slideshow_Images_Model');
		$this->load->model('Websites_Tours_Model');
		$this->load->model('Tours_Images_Model');
		$this->load->model('Websites_Tours_Categories_Model');
		$this->load->model('Languages_Model');

		//require_once './application/libraries/FTP_Client.php';
		//$this->load->library('FTP_Client');//not working!!!!
		$this->load->library('ftp');

		$site_id = $this->input->get('id');

		$site = $this->Websites_Model->find($site_id);
		$language = $this->Languages_Model->find($site->lang_id);

		if (!$site) {
			$this->session->set_flashdata('info', 'Site not found!');
			redirect(site_url('/admin/websites'));
		}

		//$client = new FTP_Client();

		if ($this->input->get('test') == TRUE) {

			$target_dir = '/' . trim($site->test_ftp_target_dir, '/');
			$target_dir_arr = explode('/', $site->test_ftp_target_dir);
			$server = $site->test_ftp_host;
			$ftp_user = $site->test_ftp_user;
			$ftp_password = $site->test_ftp_password;
		} else {
			$target_dir = '/' . trim($site->ftp_target_dir, '/');
			$target_dir_arr = explode('/', $site->ftp_target_dir);
			$server = $site->ftp_host;
			$ftp_user = $site->ftp_user;
			$ftp_password = $site->ftp_password;
		}

		/*$images_dir = $target_dir . '/images';
		$js_dir = $target_dir . '/js';
		$css_dir = $target_dir . '/css';
		*/
		$images_dir = '/images';
		$js_dir = '/js';
		$css_dir = '/css';

		$ftp_params = array(
			'hostname' => $server,
			'username' => $ftp_user,
			'password' => $ftp_password,
			'passive' => TRUE
		);
		//connect to ftp server
		if(!$this->ftp->connect($ftp_params)) {
		  if($site -> template == "qualifire"){
		    var_dump("connection to ftp failed");
        die();
		  }
			//$this->session->set_flashdata('info', 'Failed to connect to FTP host, please try again');
			//redirect(site_url('/admin/edit_website') . "?id=$site_id&section=basic");
		}

		$template_images_path = './application/views/templates/' . $site->template . '/images';
		$template_js_path = './application/views/templates/' . $site->template . '/js';
		$template_css_path = './application/views/templates/' . $site->template . '/css';
		$template_contact_us = './application/views/templates/' . $site->template . '/contact_us.php';
		$lib_folder_path = './lib';
    if($site->template == "responsive"){
      $template_fonts_path = './application/views/templates/' . $site->template . '/fonts';
    }

		//make a temp directory
		$dir_path = '/tmp/data/' . $site_id;
		$zip_path = $dir_path . '/install.zip';
		$install_script_path = $dir_path . '/install.php';
		$s3_tmp_dir = $dir_path . '/fors3/';

		$zip = new ZipArchive();

		if (!file_exists($dir_path)) {
			$rs = mkdir($dir_path, 0777, TRUE);
			if (!$rs) {
				$this->session->set_flashdata('info', 'Failed create tmp directory @ ' . $dir_path);
				redirect(site_url('/admin/edit_website') . "?id=$site_id&section=basic");
			}
		}

		exec ("rm -Rf $s3_tmp_dir");
		@unlink($install_script_path);
		@unlink($zip_path);
		if (TRUE !== $zip->open($zip_path, ZipArchive::CREATE)) {
			$this->session->set_flashdata('info', 'Failed create tmp install zip archive @ ' . $zip_path);
			redirect(site_url('/admin/edit_website') . "?id=$site_id&section=basic");
		}

		//add common tmplate files to intallation archive
		$this->_add_dir_to_zip($zip, $template_css_path, basename($template_css_path));
		$this->_add_dir_to_zip($zip, $template_images_path, basename($template_images_path));
		$this->_add_dir_to_zip($zip, $template_js_path, basename($template_js_path));
		$this->_add_dir_to_zip($zip, $lib_folder_path, basename($lib_folder_path));
    if($site->template == "responsive"){
      $this->_add_dir_to_zip($zip, $template_fonts_path, basename($template_fonts_path));
    }

		$zip->addFile('./robots.txtTEMPLATE', 'robots.txt');

		static $pages = array('home', 'about-us', 'why-service', 'locations', 'fleet', 'services', 'rates', 'online-booking', 'contact-us', 'policies', 'faqs', 'testimonials');

		foreach ($pages as $section) {

			if ($section == 'home') {
				$file_path = $dir_path . '/index.html';
			} else if ($section == 'contact-us') {
				$file_path = $dir_path . '/' . $section . '.php'; //includes contact form processor
			} else {
				$file_path = $dir_path . '/' . $section . '.html';
			}

			$page_content = template_render_page($site_id, $section, null, false);
			$zip->addFromString(basename($file_path), $page_content);
		}

		//gernerate tours page:
		$tours_categories = $this->Websites_Tours_Categories_Model->get_tours_cats(array('website_id' => $site_id));
		$tours_count = empty($tours_categories) ? 0 : $this->Websites_Tours_Model->count(array('tour_cat_id' => array_keys($tours_categories)));
		$has_tours = $tours_count > 0;


		$is_asia = 3;

		if ($has_tours) {
			//tours main page
			$file_path = $dir_path . '/tours.html';
			$page_content = template_render_page($site_id, 'tours', null, false);
			$zip->addFromString(basename($file_path), $page_content);
			foreach ($tours_categories as $tour_cat) {
				$file_path = $dir_path . '/' . template_name_to_seo_link($tour_cat->title) . '.html';
				$page_content = template_render_page($site_id, 'subtours', null, false, $tour_cat->id);
				$zip->addFromString(basename($file_path), $page_content);
			}
		}

		//generating vehicles pages
		$site_vehicles = $this->Website_Vehicles_Model->get_vehicles(array('website_id' => $site_id));
		$vehicles = empty($site_vehicles) ? array() : $this->Vehicles_Model->get_vehicles(array('vehicle_id' => array_keys($site_vehicles)));
		foreach ($vehicles as $vehicle) {
			$file_path = $dir_path . '/' . $vehicle->page_name . '.html';
			$page_content = template_render_page($site_id, $section, $vehicle->page_name, false);
			$zip->addFromString(basename($file_path), $page_content);
		}

		//upload specific site images:
		$slideshow_images = $this->Slideshow_Images_Model->get_images(array('website_id' => $site_id));
		//$site_vehicles = $this->Website_Vehicles_Model->get_vehicles(array('website_id'=>$site_id));
		//$vehicles = $this->Vehicles_Model->get_vehicles(array('vehicle_id'=>array_keys($site_vehicles)));
		$site_vehicle_images = empty($site_vehicles) ? array() : $this->Vehicle_Images_Model->get_images(array('vehicle_id' => array_keys($site_vehicles)));
		//upload vehicle thumbs
		foreach ($vehicles as $vehicle) {
			//die(var_dump($vehicle));
			$file_from = vehicle_image_single_thumb_path($vehicle->thumb);
			if (!file_exists($file_from)) continue;
			$file_to = $images_dir . '/' . basename($file_from);
			//$client->upload_file($file_from, $file_to);
			$zip->addFile($file_from, $file_to);
			//upload menu icon if any
			if (strlen(trim($vehicle->icon))) {

				$file_from = vehicle_image_single_thumb_path($vehicle->icon);
				if (!file_exists($file_from)) continue;
				$file_to = $images_dir . '/' . basename($file_from);
				//$client->upload_file($file_from, $file_to);
				$zip->addFile($file_from, $file_to);
			}
		}

		//upload slideshow images;
		foreach ($slideshow_images as $image) {
			$file_from = slideshow_image_path($image->filename);
			if (!file_exists($file_from)) continue;
			$file_to = $images_dir . '/' . basename($file_from);
			//$client->upload_file($file_from, $file_to);
			$zip->addFile($file_from, $file_to);
		}

		//upload vehicle single page images;
		foreach ($site_vehicle_images as $image) {

			$file_from = vehicle_image_path($image->filename);
			$file_to = $images_dir . '/' . basename($file_from);
			if (!file_exists($file_from)) continue;
			//$client->upload_file($file_from, $file_to);
			$zip->addFile($file_from, $file_to);
			//thumb
			$file_from = vehicle_image_thumb_path($image->filename);
			if (!file_exists($file_from)) continue;
			$file_to = $images_dir . '/' . basename($file_from);
			//$client->upload_file($file_from, $file_to);
			$zip->addFile($file_from, $file_to);
		}
		//$zip->close();
		//die();
		//upload text box images
		$image_boxes = $this->Text_Boxes_Model->get_boxes(array('website_id' => $site_id, 'has_image' => true, 'select' => 'id, image'));

		foreach ($image_boxes as $box) {
			if (strlen($box->image) == 0) continue;
			$file_from = textbox_image_path($box->image);
			if (!file_exists($file_from)) continue;
			$file_to = $images_dir . '/' . basename($file_from);
			//$client->upload_file($file_from, $file_to);
			$zip->addFile($file_from, $file_to);
		}

		//upload tour category images
		foreach ($tours_categories as $cat) {
			if (empty($cat->image)) continue;
			$file_from = textbox_image_path($cat->image);
			if (!file_exists($file_from)) continue;
			$file_to = $images_dir . '/' . basename($file_from);
			//$client->upload_file($file_from, $file_to);
			$zip->addFile($file_from, $file_to);
		}

		//upload the logo
		$file_from = site_logo_path($site->logo);
		$file_to = $images_dir . '/' . basename($file_from);
		//$client->upload_file($file_from, $file_to);
		if (file_exists($file_from)) $zip->addFile($file_from, $file_to);
		//close archive
		$zip->close();
		//generate extractor file
		$install_file = $dir_path . '/install.php';
		$install_str = '<?php chmod(dirname(__FILE__) ."/", 0777); if(!file_exists(dirname(__FILE__) . "/install.zip")) die("No Zip"); $zip = new ZipArchive();$zip->open(dirname(__FILE__) . "/install.zip"); echo $zip->extractTo(dirname(__FILE__)) ? "Extracted" : "Failed"; unlink(dirname(__FILE__) . "/install.zip"); @unlink(__FILE__); ?>';
		file_put_contents($install_file, $install_str);
		//upload the archive and extract it!
		//$this->ftp->mkdir($target_dir .'/', 0777);
		$complete_target_dir = "";
		foreach ($target_dir_arr as $each_dir) {
			if (strlen($each_dir) > 0) {
				$complete_target_dir .= $each_dir . '/';
				$this->ftp->mkdir($complete_target_dir, 0777);
			}
		}

    $this->ftp->chmod($complete_target_dir . '/', DIR_WRITE_MODE);
  		//make sure we have write permissions in case we didn't create the directory.

    if(!($this->isProduction()) || $site -> template == "qualifire"){
      // $install_dir = str_replace('http://', '', $site -> test_url );
      // $absolute_install_dir = "/var/www/vhosts/".$install_dir;
      // exec('cd ' . $absolute_install_dir . ';php install.php');
      //mkdir to extract zip to
      exec('test -d ' .  $dir_path. '/forQualifire || mkdir ' .  $dir_path. '/forQualifire');
      //increase permissions to allow exec control
      chmod($dir_path . '/forQualifire', 0755);
      //extract the zip and remove un-needed files
      exec('cd ' . $dir_path . ';unzip -oq install.zip -d ./forQualifire/;'); //overwrite, quiet flags

      $upload_target_dir = "/";
      if($complete_target_dir != ""){
        $upload_target_dir .= "".$complete_target_dir."/";
      }
      $result = $this->ftp->mirror($dir_path."/forQualifire/", $upload_target_dir);
      if(!$result){
        var_dump($upload_target_dir); die();
      }
    }

		// $this->ftp->upload($install_file, $complete_target_dir . '/install.php');
		// $this->ftp->upload($zip_path, $complete_target_dir . '/install.zip');
		$this->ftp->close();
		//isntall
		$uri = explode('/', trim($complete_target_dir, '/'));
		array_shift($uri);
		$uri = implode('/', $uri);
		if ($this->input->get('test') == TRUE) {
			$install_uri = $site->test_url . '/install.php';
		} else {
			$install_uri = $site->url . '/install.php';
		}

        //hijack zip, extract it and publish to FTP (then pass this back to Erick or Sergey for their magic cleaning skillz :P )

        //increase permissions to allow exec control
        chmod($dir_path, 0755);
        //mkdir to extract zip to
        exec('test -d ' .  $dir_path. '/fors3 || mkdir ' .  $dir_path. '/fors3');
        //increase permissions to allow exec control
        chmod($dir_path . '/fors3', 0755);
        //extract the zip and remove un-needed files
        exec('cd ' . $dir_path . ';unzip -oq install.zip -d ./fors3/;cd fors3;rm -Rf lib contact-us.php'); //overwrite, quiet flags
        //call the s3uploader via command line rather than learn how to integrate AWS SDK with CodeIgniter namespaces!


        $site->name = strtolower(preg_replace(array('/\s+/', '/\-/', '/CarService/'), '', $site->name));

        if ($this->isProduction()) {
            $site->s3_bucket_name = 'ccs-origin-' . $site->name;
            $site->aws_key = 'AKIAJBYA5P4NSRQBWPKA';
            $site->aws_secret = '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';
        } else {
        	$site->s3_bucket_name = 'ccs-origin-test';
            $site->aws_key = 'AKIAJBYA5P4NSRQBWPKA';
            $site->aws_secret = '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';
            $site->cloudfront_distribution = 'E3VX46OX93BRVR';

        }

        $execution_string = 'php ' . BASEPATH . '../s3uploader.php ' . $site_id . ' ' . $site->name . ' ' . $site->aws_key . ' ' . $site->aws_secret . ' ' . $site->s3_bucket_name . ' ' . $site->cloudfront_distribution;

      if($site -> template != "qualifire"){
        exec($execution_string);
      }
        /*
            //generate extractor file
	        $install_file = $dir_path . '/install.php';
	        $install_str = '<?php chmod(dirname(__FILE__) ."/", 0777); if(!file_exists(dirname(__FILE__) . "/install.zip")) die("No Zip"); $zip = new ZipArchive();$zip->open(dirname(__FILE__) . "/install.zip"); echo $zip->extractTo(dirname(__FILE__)) ? "Extracted" : "Failed"; unlink(dirname(__FILE__) . "/install.zip"); @unlink(__FILE__); ?>';
	        file_put_contents($install_file, $install_str);
	        //upload the archive and extract it!
	        //$this->ftp->mkdir($target_dir .'/', 0777);
            $complete_target_dir = "";
            foreach($target_dir_arr as $each_dir) {
                if(strlen($each_dir)>0){
                    $complete_target_dir .= $each_dir.'/';
                    $this->ftp->mkdir($complete_target_dir,0777);
                }
            }
	        //make sure we have write permissions in case we didn't create the directory.
	        $this->ftp->chmod($complete_target_dir .'/', DIR_WRITE_MODE);
	        $this->ftp->upload($install_file, $complete_target_dir . '/install.php');
	        $this->ftp->upload($zip_path, $complete_target_dir . '/install.zip');
            $this->ftp->close();
	        //isntall
	        $uri = explode('/', trim($complete_target_dir, '/')); array_shift($uri);
	        $uri = implode('/', $uri);
            if($this->input->get('test') == TRUE) {
                $install_uri =  $site->test_url.'/install.php';
            } else {
                $install_uri =  $site->url.'/install.php';
            }

	    	$rs = file_get_contents($install_uri);

		    //remove the temp dir
		    @rmdir('.data/' . $site_id);
        */

        $end = microtime();
		$total = $end - $start;

		//update publish date
		if ($this->input->get('test') != true){
		  echo "updating published date";
			$this->Websites_Model->update(array('last_publish_date' => date('Y-m-d H:i:s')), $site_id);
      echo "finished updating published date";
	  }
		//log the publish action
		$this->load->model('Logs_Model');
		$this->Logs_Model->create(array(
			'website_id' => $site_id,
			'user_id' => $this->Users->get_identity()->id,
			'page_uri' => 'publish',
			'content' => ($this->input->get('test') == TRUE ? 'Site Published to Test server' : 'Site Published to Live server')
		));


		$message = 'Site Published successfully';
	   	//if(strpos($rs, "Failed") !== FALSE)	$message .= '. but failed to install click <a target="_blank" href="' . $install_uri .'">Here</a> to manually install the archive.';

		$this->session->set_flashdata('info', $message);
		redirect(site_url('/admin/edit_website') . "?id=$site_id&section=basic");

	}

	public function logs() {

		if (!$this->Users->is_admin()) {
			$this->set_flashdata('info', 'Not Authorized');
			redirect(site_url('admin/websites'));
		}

		$this->load->model('Logs_Model');
		$this->load->model('Websites_Model');

		$page = max(1, (int)$this->input->get('page'));
		$section = $this->input->get('section') ? $this->input->get('section') : null;
		$user_id = $this->input->get('uid') ? $this->input->get('uid') : null;
		$site_id = $this->input->get('site_id') ? $this->input->get('site_id') : null;

		$sections = array(
			'basic' => 'Settings',
			'publish' => 'Site Publish',
			'home' => 'Home Page',
			'about-us' => 'About us',
			'contact-us' => 'Contact us',
			'fleet' => 'Fleet',
			'services' => 'Services',
			'rates' => 'Rates',
			'tours' => 'Tours',
			'online-booking' => 'Online booking',
			'policies' => 'Policies',
			'faqs' => 'Faqs'
		);

		$options = array(
			'itemCountPerPage' => 30,
			'page' => $page,
			'user_id' => $user_id,
			'website_id' => $site_id,
			'page_uri' => $section,
			'order' => 'ts_date_created desc'
		);

		$paginator = $this->Logs_Model->get_logs_paginator($options);
		$user_ids = array();
		$website_ids = array();
		foreach ($paginator['data'] as $log) {
			$user_ids[$log->user_id] = $log->user_id;
			$website_ids[$log->website_id] = $log->website_id;
		}

		/*$websites = empty($website_ids) ? array() : $this->Websites_Model->get_websites(array('website_id'=>$website_ids));
		$users = empty($user_ids) ? array() : $this->Users->get_users(array('user_id'=>$user_ids));
		*/

		$websites = $this->Websites_Model->get_websites(array('order' => 'name asc'));
		$users = $this->Users->get_users(array('order' => 'firstname asc, lastname asc'));

		$data = array('tab' => 'logs', 'page_title' => 'Logs');
		$data['site_id'] = $site_id;
		$data['user_id'] = $user_id;
		$data['section'] = $section;
		$data['sections'] = $sections;
		$data['paginator'] = $paginator;
		$data['users'] = $users;
		$data['websites'] = $websites;
		$data['view_page'] = 'admin/logs';
		$this->load->view('admin/layout', $data);


	}


	function countries() {

		$this->load->model('Countries_Model');

		if (!$this->input->post()) {
			redirect(site_url('admin/websites'));
		}

		$name = trim($this->input->post('name'));
		$id = $this->input->post('cid');

		//valid required values for add and edit
		if (empty($name)) {

			if ($this->_is_ajax() && ($this->input->post('add_country') || $this->input->post('edit_country'))) {
				$this->_send_json(array(
					'success' => false,
					'error' => true,
					'message' => 'Country name can not be empty.'
				));
			} else {
				$this->session->set_flashdata('info', 'Country name is required and can not be empty');
				redirect(site_url('admin/websites'));
			}

			return;

		} else if ($this->Countries_Model->find_by_name($name)) {

			if ($this->_is_ajax() && ($this->input->post('add_country') || $this->input->post('edit_country'))) {
				$this->_send_json(array(
					'success' => false,
					'error' => true,
					'message' => 'Country name already exists'
				));
			} else {
				$this->session->set_flashdata('info', 'Country name already exists');
				redirect(site_url('admin/websites'));
			}

			return;

		}
		//add country handler
		if ($this->input->post('add_country')) {


			$id = $this->Countries_Model->create(array('name' => $name));

			if (!$id) {
				if ($this->_is_ajax()) {
					$this->_send_json(array(
						'success' => false,
						'error' => true,
						'message' => 'failed to add country'
					));
				} else {
					$this->session->set_flashdata('info', 'Failed to add country, please try again');
					redirect(site_url('admin/websites'));
				}

				return;
			}

			if ($this->_is_ajax()) {

				$html = $this->load->view('admin/partials/country_row', array('country' => $this->Countries_Model->find($id)), TRUE);

				$this->_send_json(array(
					'success' => true,
					'message' => 'country added successfully',
					'id' => $id,
					'html' => $html
				));

			} else {

				$this->session->set_flashdata('info', 'Country Added successfully');
				redirect(site_url('admin/websites'));
			}
		}

		//edit country handler
		if ($this->input->post('edit_country')) {

			$this->Countries_Model->update(array('name' => $name), $id);

			if ($this->_is_ajax()) {

				$this->_send_json(array(
					'name' => $name,
					'id' => $id,
					'message' => 'Country updated successfully',
					'success' => true,
					'error' => false
				));

			} else {
				$this->session->set_flashdata('info', 'Country updated successfully');
				redirect(site_url('admin/websites'));
			}

		}
	}

	/**
	 * Ajax /admin/ajax/$target/$action/$id
	 *
	 * @param string $target
	 * @param string $action
	 * @param int $id
	 * @return null
	 */
	public function ajax($target = NULL, $action = NULL, $id = NULL) {

		if (is_null($target) || is_null($action) || is_null($id)) {

			echo 'MISSING ARGUMENTS';
			return;
		}

		if (!$this->Users->has_identity()) {
			echo "DENIED";
			return;
		}

		$target = strtolower($target);
		$action = strtolower($action);


		if ($target == 'users') {

			$this->load->model('Users');
			$user = $this->Users->find($id);

			if ($action == 'enable') {
				$status = $this->Users->activate($id);
				$message = $status ? $user->id . ' Enabled successfully' : 'Failed to enable user: ' . $user->id;
				$ajaxData = $status ? array('status' => 'ok', 'msg' => 'Enabled', 'update' => 'A', 'id' => $id, 'url' => site_url('admin/ajax/users/disable/' . $id)) : array('status' => 'failed', 'status' => $status, 'msg' => 'Disabled', 'update' => 'B', 'id' => $id);
			} else if ($action == 'disable') {
				$status = $this->Users->block($id);
				$message = $status ? $user->id . ' Disabled successfully' : 'Failed to disbale user ' . $user->id;
				$ajaxData = $status ? array('status' => 'ok', 'msg' => 'Disabled', 'update' => 'B', 'id' => $id, 'url' => site_url('admin/ajax/users/enable/' . $id)) : array('status' => 'failed', 'status' => $status, 'msg' => 'Enabled', 'update' => 'A', 'id' => $id);
			} else if ($action == 'delete') {
				$status = $this->Users->delete($id);
				$message = $status ? 'User Deleted successfully: ' . $user->id : 'Failed to delete User: ' . $user->id;
				$ajaxData = $status ? array('status' => 'ok', 'msg' => 'Deleted', 'update' => 'Deleted') : array('status' => 'failed', 'status' => 'noaction', 'msg' => 'Failed', 'update' => 'Failed');
			} else {
				$message = 'Unkown action ' . $action;
				$ajaxData = array('status' => 'failed', 'msg' => 'Unsupported action: ' . $action);
			}

			header('Content-Type: application/json');
			echo json_encode($ajaxData);
		}

		if ($target == 'website') {

			$this->load->model('Users');
			$this->load->model('Websites_Model');
			$this->load->model('Website_Vehicles_Model');

			if ($action == 'delete') {
				if ($this->Users->is_admin()) { //only admin can delete
					$status = true;
					$status = $this->Websites_Model->delete($id);
					if ($status)
						$status = $this->Website_Vehicles_Model->clear_for_website($id);

					$message = $status ? 'Website Deleted successfully: ' . $id : 'Failed to delete website: ' . $id;
					$ajaxData = $status ? array('status' => 'ok', 'msg' => 'Deleted', 'update' => 'Deleted') : array('status' => 'failed', 'status' => 'noaction', 'msg' => 'Failed', 'update' => 'Failed');
				} else {
					$ajaxData = array('status' => 'failed', 'status' => 'noaction', 'msg' => 'Your are not authorized', 'update' => 'Failed');
				}
			}

			header('Content-Type: application/json');
			echo json_encode($ajaxData);
		}

		if ($target == 'vehicle') {

			$this->load->model('Vehicles_Model');
			$this->load->model('Vehicle_Images_Model');
			$this->load->model('Website_Vehicles_Model');

			if ($action == 'delete') {

				if (!$this->Users->is_admin()) {
					$ajaxData = array('status' => 'failed', 'status' => 'noaction', 'msg' => 'You are not authorized to delete a vehicle', 'update' => 'Failed');
				} else {

					$vehicle = $this->Vehicles_Model->find($id);
					$status = false;

					$status = $this->Vehicles_Model->delete($id);

					if ($status)
						$status = $this->Vehicle_Images_Model->delete_for_vehicle($id);

					if ($status)
						$status = $this->Website_Vehicles_Model->clear_for_vehicle($id);

					if ($status) {
						$pattern = sprintf('%s/.*%d.*', './images/vehicles/images', $id);
						foreach (glob($pattern) as $image) {
							@unlink($image);
						}
						if ($vehicle) {
							@unlink(vehicle_image_single_thumb_path($vehicle->filename));
						}
					}
					$message = $status ? 'Vehicle Deleted successfully: ' . $id : 'Failed to delete Vehicle: ' . $id;
					$ajaxData = $status ? array('status' => 'ok', 'msg' => 'Deleted', 'update' => 'Deleted') : array('status' => 'failed', 'status' => 'noaction', 'msg' => 'Failed', 'update' => 'Failed');

				}
			}

			header('Content-Type: application/json');
			echo json_encode($ajaxData);
		}

		if ($target == 'rates') {

			$ajaxData = array('success' => false, 'error' => true, 'message' => 'Unknown error');
			$this->load->model('Vehicles_Model');
			$this->load->model('Website_Vehicles_Model');
			$this->load->model('Vehicle_Classes_Model');

			if ($action == 'generate') { //en speaking driver table


				$fleet_vehicles = $this->Website_Vehicles_Model->get_vehicles(array('website_id' => $id, 'order' => 'ranking asc'));
				$vehicle_ids = array();
				foreach ($fleet_vehicles as $fvehicle) {
					$vehicle_ids[$fvehicle->vehicle_id] = $fvehicle->vehicle_id;
				}
				$vehicles = empty($vehicle_ids) ? array() : $this->Vehicles_Model->get_vehicles(array('vehicle_id' => $vehicle_ids));
				$classes = $this->Vehicle_Classes_Model->get_classes();
				$viewData = array(
					'vehicles' => $vehicles,
					'fleet_vehicles' => $fleet_vehicles,
					'classes' => $classes
				);

				$html = $this->load->view('admin/partials/ajax_rates_driver', $viewData, TRUE);
				$html = preg_replace('/\n\t/', '', $html);
				$ajaxData = array('success' => true, 'message' => 'success', 'content' => $html);
			}

			header('Content-Type: application/json');
			echo json_encode($ajaxData);
		}

		if ($target == 'tours') {

			$ajaxData = array('success' => false, 'message' => 'No action');

			if ($action == 'deletebox') {
				$this->load->model('Text_Boxes_Model');
				$rs = $this->Text_Boxes_Model->delete($id);
				$message = $rs ? 'Text box delete successfully.' : 'Failed to delete text box, please try again.';
				$ajaxData = array('success' => $rs, 'message' => $message, 'id' => $id);

			}

			if ($action == 'delete') {
				$this->load->model('Websites_Tours_Model');
				$rs = $this->Websites_Tours_Model->delete($id);
				$message = $rs ? 'Tour deleted successfully' : 'Failed to delete tour, please try again.';
				$ajaxData = array('success' => $rs, 'message' => $message, 'id' => $id);

			}

			header('Content-Type: application/json');
			echo json_encode($ajaxData);
		}

		if ($target == 'tourscats') {

			$ajaxData = array('success' => false, 'message' => 'No action');

			if ($action == 'delete') {
				$this->load->model('Websites_Tours_Categories_Model');
				$rs = $this->Websites_Tours_Categories_Model->delete($id);
				$message = $rs ? 'Tour category deleted successfully' : 'Failed to delete tour category, please try again.';
				$ajaxData = array('success' => $rs, 'message' => $message, 'id' => $id);

			}

			header('Content-Type: application/json');
			echo json_encode($ajaxData);
		}

		if ($target == 'clone') {
			$ajaxData = array('success' => false, 'message' => 'No Action');
			$site_id = $this->input->get('site_id');
			$this->load->helper('clone');
			if ($action == 'load-sections') {

				$html = clone_get_sections($site_id, $id);
				$ajaxData = array('success' => true, 'html' => $html);

			}

			header('Content-type: application/json');
			echo json_encode($ajaxData);

		}

		if ($target == 'translations') {

			$this->load->model('Translations_Model');

			$translationId = (int)$id;

			if ($action == 'delete') {

				if (!$this->Users->is_admin()) {
					$ajaxData = array('success' => false, 'message' => 'Not Authorized!', 'id' => $translationId);
				}
				if ($translationId > 1) { // make sure default country "Other" is not deleted
					$res = $this->Translations_Model->delete($translationId);
					$ajaxData = array('success' => true, 'message' => 'Translation deleted', 'id' => $translationId);
				} else {
					$ajaxData = array('success' => false, 'message' => 'Invalid translation', 'id' => $translationId);
				}
			}

			$ajaxData = array('success' => true, 'message' => 'Country deleted', 'id' => '2');
			$this->_send_json($ajaxData);
		}

		if ($target == 'countries') {

			$this->load->model('Countries_Model');
			$this->load->model('Websites_Model');

			$ajaxData = array('success' => false, 'message' => 'Invalid action');

			$countryId = (int)$id;

			if ($action == 'delete') {

				if (!$this->Users->is_admin()) {
					$ajaxData = array('success' => false, 'message' => 'Not Authorized!', 'id' => $countryId);
					$this->_send_json($ajaxData);
					return null;
				}

				if ($countryId > 1) { // make sure default country "Other" is not deleted
					$res = $this->Countries_Model->delete($countryId);
					//upadate websites
					if ($res) {
						$this->Websites_Model->update(array('country_id' => 1), array('country_id' => $countryId));
					}

					$ajaxData = array('success' => true, 'message' => 'Country deleted', 'id' => $countryId);
				} else {
					$ajaxData = array('success' => false, 'message' => 'Invalid country', 'id' => $countryId);
				}
			}

			$this->_send_json($ajaxData);
		}

		if ($target == 'textbox') {

			$this->load->model('Text_Boxes_Categories_Model');

			$ajaxData = array('success' => FALSE, 'message' => 'Unkown action');

			if ($action == 'createcat') {

				$success = $this->Text_Boxes_Categories_Model->create(array(
					'website_id' => $this->input->post('website_id'),
					'section' => $this->input->post('section'),
					'title' => $this->input->post('title')
				));

				$cat = $success ? $this->Text_Boxes_Categories_Model->find($success) : null;
				if ($cat) {
					$cat->edit_url = site_url('admin/edit_website/?id=' . $cat->website_id . '&section=' . $cat->section . '&cat_id=' . $cat->id);
					$cat->delete_url = site_url('admin/ajax/textbox/deletecat/' . $cat->id);
				}
				$ajaxData = array('success' => $success ? TRUE : FALSE, 'message' => $success ? 'Category created' : 'Failed to create category', 'id' => $success, 'category' => $cat);

			} else if ($action == 'deletecat') {
				$this->load->model('Text_Boxes_Model');
				$success = $this->Text_Boxes_Categories_Model->delete($id);
				//delete boxes in the category only if the category itself has been successfully deleted
				if ($success) $this->Text_Boxes_Model->delete(array('cat_id' => $id));

				$ajaxData = array('success' => $success ? TRUE : FALSE, 'message' => $success ? 'Category deleted' : 'Failed to delete category', 'id' => $success);
			}

			$this->_send_json($ajaxData);
		}
		if ($target == 'languages') {
			$this->load->model('Languages_Model');
			$ajaxData = array('success' => FALSE, 'message' => 'Unkown action');
			if ($action == 'add') {
				$errors = array();
				$post_data = $this->input->post(null, true);

				if (!array_key_exists('lang_code', $_POST) or $_POST['lang_code'] == false) {
					$errors[] = 'Language code must be specified!';
				}
				if (!array_key_exists('language', $_POST) or $_POST['language'] == false) {
					$errors[] = 'Language name must be specified!';
				}
				if ((!array_key_exists('userfile', $_FILES) or $_FILES['userfile'] == false) && !array_key_exists('id', $post_data)) {
					$errors[] = 'Flag image must be specified!';
				} else {
					// Upload
					$config['upload_path'] = './images/flags/';
					$config['overwrite'] = true;
					$config['file_name'] = "lang_$post_data[lang_code].jpg";
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size'] = '2000';
					$config['max_width'] = '5024';
					$config['max_height'] = '4768';
					$this->load->library('upload', $config);
					$this->upload->do_upload('userfile');
					$error = $this->upload->display_errors('', '');
					if ($error && !array_key_exists('id', $post_data)) {
						$errors[] = $error;
					}
				}


				if (array_key_exists('userfile', $post_data)) {
					unset($post_data['userfile']);
				}
				if (count($errors) == 0) {
					$lang_id = $this->Languages_Model->create($post_data);
					if (!$lang_id) {
						$errors[] = 'Language code or name already exists!';
					}
				}

				$isinsert = $post_data['id'] == '' ? true : false;

				if (count($errors) > 0) {
					$ajaxData = array(
						'success' => FALSE, 'message' => implode("\n", $errors)
					);
					$this->_send_json($ajaxData);
					return;
				} else {
					$ajaxData = array(
						'success' => true, 'id' => $lang_id,
						'lang_code' => $post_data['lang_code'],
						'language' => $post_data['language'],
						'insert' => $isinsert
					);
				}

				if ($isinsert) {
					$this->generate_translations();
				}
			} elseif ($action == 'delete') {
				$this->Languages_Model->delete($id);
				$ajaxData = array('success' => true, 'id' => $id);
				$this->generate_translations();
			} elseif ($action == 'update') {
				$language = $this->Languages_Model->find($id);
				$language->update($id, $this->input->post(null, true));
			}

			$this->_send_json($ajaxData);
		}
	}

	public function _send_json($ajaxData) {

		header('Content-Type: application/json');
		echo json_encode($ajaxData);
		//echo stripslashes();
	}

	public function _is_ajax() {

		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			return TRUE;
		}
		return FALSE;
	}

	function _is_post() {
		return (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST');
	}

	function _is_get() {
		return (strtoupper($_SERVER['REQUEST_METHOD']) == 'GET');
	}

	function _add_dir_to_zip($zip, $source, $destination) {


		if (is_dir($source) === true) {
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

			foreach ($files as $file) {
				$file = str_replace('\\', '/', $file);

				// Ignore "." and ".." folders
				if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
					continue;

				$file = realpath($file);

				$dest_file = substr($file, strpos($file, $destination));
				$dest_file = str_replace('\\', '/', $dest_file);

				if (is_dir($file) === true) {
					$zip->addEmptyDir($dest_file);
				} else if (is_file($file) === true) {
					$zip->addFromString($dest_file, file_get_contents($file));
				}
			}
		} else if (is_file($source) === true) {
			$zip->addFromString(str_replace('\\', '/', $destination), file_get_contents($source));
		}
		//die();
	}

	public function translations($filter = "all", $page = 1) {

		$this->load->model('Translations_Model');
		$this->load->model('Languages_Model');

		$data = array('tab' => 'translations');

		if (!$this->Users->is_admin()) { //anyone can manage restaurants;
			$this->notauth();
			return;
		}

		if ($this->input->post('filter_btn')) {

			$language = $this->input->post('language');

			$url = 'admin/translations/' . $language . '/' . $page;
			redirect(site_url($url));
		}

		if ($this->input->post('add_translation_btn')) {

			$this->form_validation->set_rules('label', 'Label', 'required|xss_clean');
			$this->form_validation->set_rules('translation', 'Translation', 'required|xss_clean');
			$this->form_validation->set_rules('language', 'Language', 'required|xss_clean');
			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');
			if ($this->form_validation->run() == false) {
				$data['create_translation_message'] = 'Failed to create user, all fields are required';
			} else {

				$translation = array(
					'label' => $this->input->post('label'),
					'translation' => $this->input->post('translation'),
					'lang_id' => $this->input->post('language'),
				);

				if ($this->Translations_Model->create_translation($translation)) {

					$this->session->set_flashdata('info', array('Translation ' . $translation['label'] . ' ' . $translation['translation'] . ' Added successfully'));
					redirect(site_url('admin/translations'));
				} else {
					$data['create_translation_message'] = 'Failed to create user, database error, please try again';
				}
			}
		}

		if ($this->input->get('generate_translations', TRUE)) {
			$this->load->model('Languages_Model');
			$this->load->model('Translations_Model');
			$languages = $this->Languages_Model->get_languages();
			$di = new RecursiveDirectoryIterator('./application/views/templates');
			$count = 0;
			$labels = array();
			foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				if ($ext == 'php') {
					$homephp_file = file_get_contents($filename);
					preg_match_all(
						'/lang\([\"\']([ a-zA-Z0-9_-]*)[\"\'][, ]*[\"\']?([^"\']*)?[\"\']?\)/',
						$homephp_file,
						$matches
					);
					if (!empty($matches[0])) {
						foreach ($languages as $lang) {
							foreach ($matches[1] as $k => $label) {
								$labels[] = $label;
								$translation = strlen($matches[2][$k]) > 0 ? $matches[2][$k] : $label;
								$opt = array(
									'language' => $lang->language,
									'label' => $label
								);
								$translations = $this->Translations_Model->get_translations($opt);
								if (empty($translations)) {
									$translation_to_add = array(
										'label' => $label,
										'translation' => $translation,
										'lang_id' => $lang->id,
									);

									$this->Translations_Model->create_translation($translation_to_add);
									$count++;
								}
							}
						}
					}
				}
			}
			if (count($labels) > 0)
				$this->Translations_Model->cleanup_translations($labels);
			$this->session->set_flashdata('info', array("Translations created: " . $count));
			redirect(site_url('admin/translations'));
		}

		$filters = explode("-", $filter);
		$language = $filters[0];
		$page = max(1, (int)$page);

		$options = array('itemCountPerPage' => self::TRANSLATIONS_PER_PAGE, 'page' => $page);
		if ($language != 'all') $options['language'] = $language;

		$paginator = $this->Translations_Model->get_translations_paginator($options);

		$this->load->helper('language');

		$data['identity'] = $this->Users->get_identity();
		$data['language'] = $language;
		$data['paginator'] = $paginator;
		$data['view_page'] = 'admin/translations';
		$data['languages'] = $this->Languages_Model->get_languages();
		$this->load->view('admin/layout', $data);
	}

	public function edit_translation($id = null) {

		$this->load->model('Translations_Model');
		$this->load->model('Languages_Model');

		if (!$id) {
			$this->session->set_flashdata('info', 'Translation not found!');
			redirect(site_url('admin/translations'));
		}

		$translation = $this->Translations_Model->find($id);

		if ($this->input->post()) {

			$this->load->library('form_validation');
			$this->form_validation->set_rules('label', 'Label', 'required|xss_clean');
			$this->form_validation->set_rules('translation', 'Translation', 'required|xss_clean');
			$this->form_validation->set_rules('language', 'Language', 'required|xss_clean');
			$this->form_validation->set_error_delimiters('<span class="input_error">', '</span>');

			if ($this->form_validation->run() !== FALSE) {

				$translation = array(
					'label' => $this->input->post('label'),
					'translation' => $this->input->post('translation'),
					'lang_id' => $this->input->post('language'),
				);

				$this->Translations_Model->update($translation, $id);

				$this->session->set_flashdata(
					'info', $translation['label'] . ' ' . $translation['language'] . ' Translation info Updated!'
				);
				redirect(site_url('admin/translations'));
			}
		}

		$data = array('tab' => 'translations', 'page_title' => "Updating translation #$id");
		$data['translation'] = $translation;
		$data['id'] = $id;
		$data['view_page'] = 'admin/edit_translation';
		$data['languages'] = $this->Languages_Model->get_languages();
		$this->load->view('admin/layout', $data);
	}

	public function email_templates($action=false, $id=false) {
		if (!$action){
			$action = 'list_templates';
		}

		$this->load->model('Email_Templates_Model');
		$this->load->model('Email_Templates_Names_Model');
		$this->load->model('Websites_Model');
		$data = array('edit'=>$id ? 1 : 0, 'object'=>0);
		$post = $this->input->post();

		if ($post){
			$this->load->library('form_validation');
		}

		$forsite = $this->input->get('forsite');
		$forname = $this->input->get('forname');
		$data['forsite'] = $forsite;
		$data['forname'] = $forname;

		$names = $this->Email_Templates_Names_Model->get_all();
		$data['names'] = $names;
		$templates = $this->Email_Templates_Model->get_all($forname, $forsite);
		$data['templates'] = $templates;
		$websites = $this->Websites_Model->get_websites();
		$data['websites'] = $websites;

		$active_tab = 'templates';

		function encode_email(){
			$fields = array(
				'from', 'to', 'cc', 'bcc', 'title', 'body'
			);
			$res = array();
			foreach ($fields as $field){
				$res[$field] = str_replace("&#10;", "\n", $_POST[$field]);
			}
			return json_encode($res);
		}

		if ($action == 'new_template'){
			$view_template = 'admin/email_template_form';
			if ($post){
				$this->form_validation->set_rules('website_id', 'Website ID', 'required');
				$this->form_validation->set_rules('template_name_id', 'Template Name ID', 'required');
				if ($this->form_validation->run() !== FALSE) {
					$data = array(
						'template_name_id'=>$this->input->post('template_name_id'),
						'website_id'=>$this->input->post('website_id'),
						'email'=>encode_email($_POST)
					);
					$this->Email_Templates_Model->save_template($data);
					$this->session->set_flashdata('info', "Template added");
					redirect(site_url('admin/email_templates'));
					return;
				}
				echo $this->form_validation->error_string;
			}else{

			}
		}elseif ($action == 'edit_template'){
			$view_template = 'admin/email_template_form';
			if (!$id){
				header("HTTP/1.1 404 Not Found");
				return;
			}
			$data['object'] = $this->Email_Templates_Model->get_template($id);
			$data['email'] = json_decode($data['object']->email, true);
			if ($post){
				$data = array(
					'email'=>encode_email($_POST)
				);
				if ($this->input->post('template_name_id')) {
					$data['template_name_id'] = $this->input->post('template_name_id');
				}
				if ($this->input->post('website_id')) {
					$data['website_id'] = $this->input->post('website_id');
				}
				$this->Email_Templates_Model->save_template($data, $id);
				$this->session->set_flashdata('info', "Template saved");
				redirect(site_url('admin/email_templates'));
				return;
			}else{

			}
		}elseif ($action == 'delete_template'){
			if (!$post || !$this->input->post('id')){
				header("HTTP/1.1 404 Not Found");
				return;
			}
			$this->Email_Templates_Model->delete_template($this->input->post('id'));
			$this->session->set_flashdata('info', "Template deleted");
			return;
		}elseif ($action == 'list_templates'){
			$view_template = 'admin/email_templates_list';
		}elseif ($action == 'preview_template'){

		}elseif ($action == 'new_name'){
			$view_template = 'admin/email_template_name_form';
			$active_tab = 'names';
			if ($post){
				$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
				if ($this->form_validation->run() !== FALSE) {

					$data= array(
						'name'=>$this->input->post('name')
					);
					$this->Email_Templates_Names_Model->save_name($data);
					$this->session->set_flashdata('info', "Template name added: " . $data['name']);
					redirect(site_url('admin/email_templates/list_names/'));
					return;
				}
			}else{

			}
		}elseif ($action == 'edit_name'){
			$view_template = 'admin/email_template_name_form';
			$active_tab = 'names';
			if (!$id){
				header("HTTP/1.1 404 Not Found");
				return;
			}
			$data['object'] = $this->Email_Templates_Names_Model->get_name($id);
			if ($post){
				$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
				if ($this->form_validation->run() !== FALSE) {
					$data= array(
						'name'=>$this->input->post('name')
					);
					$this->Email_Templates_Names_Model->save_name($data, $id);
					$this->session->set_flashdata('info', "Template name updated: " . $data['name']);
					redirect(site_url('admin/email_templates/list_names/'));
					return;
				}
			}else{

			}
		}elseif ($action == 'delete_name'){
			if (!$post || !$this->input->post('id')){
				header("HTTP/1.1 404 Not Found");
				return;
			}
			$this->Email_Templates_Names_Model->delete_name($this->input->post('id'));
			$this->session->set_flashdata('info', "Template name deleted");
			return;
		}elseif ($action == 'list_names') {
			$view_template = 'admin/email_templates_names_list';
			$active_tab = 'names';
		}elseif ($action == 'request') {
			$website = $this->input->get('website');
			$name = $this->input->get('name');
			$res = $this->Email_Templates_Model->find_by_name_and_website($name, $website);
			if (count($res)>0){
				$email = $res->email;
			}else{
				header('HTTP/1.0 404 NOT FOUND');
				return;
			}

			header('Content-Type: application/json');
			echo $email;
			return;
		}

		if (!isset($view_template)){
			header("HTTP/1.1 404 Not Found");
			return;
		}
		$data['view_page'] = $view_template;
		$data['active_tab'] = $active_tab;
		$data['tab'] = 'emailtemplates';
		$data['page_title'] = 'Email templates';
		$this->load->view('admin/layout', $data);
	}

	public function get_csv(){
		$this->load->model('Websites_Model');
		$this->load->model('Countries_Model');
		$websites = $this->Websites_Model->all();
		$csv = Array();

		$this->Countries_Model->db->cache_on();
		foreach ($websites as $v){
			$csv[] = str_replace(' Car Service', '', $v->name) . ',' .
				$this->Countries_Model->find($v->country_id)->name . ',' .  $v->url;
		}
		$this->db->cache_off();
		$csv = implode("\n", $csv);

		$this->load->helper('download');
		force_download('markets.csv', $csv);
	}

	protected function isProduction() {
		return (base_url() == 'http://carmageddon.chinacarservice.com/' || base_url() == 'https://carmageddon.chinacarservice.com');
	}

  public function edit_booking_forms(){
    $this->load->model('Websites_Model');

    $this->load->model('Booking_Services_Model');
    $this->load->model('Text_Boxes_Model');
    $this->load->model('Text_Boxes_Categories_Model');
    $websites = $this->Websites_Model->get_websites();
    if ($this->input->post('edit_booking_form_general_btn')) {
      $content = $this->input->post('box_content');
      $ids = $this->input->post('identifier');
      for($i=0; $i < count($content); $i++) {
        $this->Text_Boxes_Model->update(array('content' => $content[$i]), $ids[$i]);
      }
      $this->session->set_flashdata('info', 'Booking forms updated!');
      redirect(site_url('admin/edit_booking_forms?section=general'));
    }
    if ($this->input->post('edit_booking_form_service_btn')) {
      $service_name = $this->input->post('box_service_name');
      $order = $this->input->post('box_order');
      $active = $this->input->post('active');
      $airport =  $this->input->post('airport_pickup');
      $hotels =  $this->input->post('hotel_pickup');
      $dayhire = $this->input->post('dayhire');
      $tour  =  $this->input->post('tour');
      $other =  $this->input->post('other');
      (int)$order = $order;

        if($active[0] == "on"){
          $active[0] = 1;
          unset($active[1]);
          $active = array_values($active);
        }else{
          $active[0] = 0;
        }
        if($airport[0] == "on"){
          $airport[0] = 1;
          unset($airport[1]);
          $airport = array_values($airport);
        }else{
          $airport[0] = 0;
        }
        if($hotels[0] == "on"){
          $hotels[0] = 1;
          unset($hotels[1]);
          $hotels = array_values($hotels);
        }else{
          $hotels[0] = 0;
        }
        if($dayhire[0] == "on"){
          $dayhire[0] = 1;
          unset($dayhire[1]);
          $dayhire = array_values($dayhire);
        }else{
          $dayhire[0] = 0;
        }
        if($tour[0] == "on"){
          $tour[0] = 1;
          unset($tour[1]);
          $tour = array_values($tour);
        }else{
          $tour[0] = 0;
        }
        if($other[0] == "on"){
          $other[0] = 1;
          unset($other[1]);
          $other = array_values($other);
        }else{
           $other[0] = 0;
        }
        if($service_name == ''){
          $this->session->set_flashdata('info', 'Please enter a service name');
          redirect(site_url('admin/edit_booking_forms?section=service'));
        }
        foreach($websites as $w){
          $indata = array('ranking' => $order, 'service_name'=>$service_name, 'website_id'=>($w -> id), 'active'=>$active[0], 'airport_pickup'=>$airport[0], 'hotel_pickup'=>$hotels[0], 'dayhire' => $dayhire[0], 'tour' => $tour[0]
          , 'other' => $other[0]);
          $this->Booking_Services_Model->create($indata);
        }
        $this->session->set_flashdata('info', 'Service added to every website');
        redirect(site_url('admin/edit_booking_forms?section=service'));
    }

    $textbox = $this -> Text_Boxes_Model ->find_all_by_uri('booking');
    $data = array('tab' => 'bookingtemplates');
    $data['textbox'] = $textbox;
    $data['websites'] = $websites;
    $data['view_page'] = 'admin/edit_booking_forms';
    $section = $this->input->get('section');
    $data['section'] = $section;
    $editor_view = $this->load->view('admin/partials/booking_forms_'.$section.'.php', $data, true);
    $data['editor_view'] = $editor_view;
    $this->load->view('admin/layout', $data);
  }
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
