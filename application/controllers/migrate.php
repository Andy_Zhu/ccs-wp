<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends CI_Controller
{

	public function __construct() {

		parent::__construct();

		$this->load->database();
		$this->load->model('Users');
		$this->load->library('input');
		$this->load->helper('url');
		if (!$this->Users->has_identity() && $this->uri->segment(2) != 'login') {
			redirect(site_url('admin/login'));
		}

	}

	public function index() {
		if (!$this->Users->has_identity()) {
			redirect(site_url('admin/login'));
		} else {
			$this->db->query("ALTER TABLE `vehicles` CHARACTER SET = utf8 ;");
			$this->db->query("ALTER TABLE `vehicles` CHANGE COLUMN `short_name` `short_name` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL  , CHANGE COLUMN `long_name` `long_name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `internal_name` `internal_name` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `page_name` `page_name` VARCHAR(100) CHARACTER SET 'utf8' NOT NULL  , CHANGE COLUMN `description` `description` TEXT CHARACTER SET 'utf8' NOT NULL  , CHANGE COLUMN `passengers` `passengers` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `luggage` `luggage` VARCHAR(50) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `thumb` `thumb` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `icon` `icon` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `short_name_chn` `short_name_chn` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `long_name_chn` `long_name_chn` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `internal_name_chn` `internal_name_chn` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `description_chn` `description_chn` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `short_name_por` `short_name_por` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `long_name_por` `long_name_por` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `internal_name_por` `internal_name_por` VARCHAR(100) CHARACTER SET 'utf8' NULL DEFAULT NULL  , CHANGE COLUMN `description_por` `description_por` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL  ;");
			$this->db->query('ALTER TABLE `websites` MODIFY `alt_website_id` CHAR(100) COMMENT \'Comma separated list of file ids\';');
			$this->db->query('UPDATE `websites` SET `alt_website_id` = NULL WHERE `alt_website_id` = 0');
		}
	}


	private function get_migrations_list() {
		$dir = 'migrations/';
		$h = opendir($dir);
		$items = array();
		while (false !== ($entry = readdir($h))) {
			if ($entry != '.' and $entry != '..') {
				$fh = fopen($dir . $entry, 'r');
				$sql = fread($fh, filesize($dir . $entry));
				fclose($fh);
				$items[$entry] = array();
				$items[$entry]['sql'] = $sql;
				$items[$entry]['created'] = filectime($dir . $entry);
			}
		}
		closedir($h);
		$items = array_reverse($items);
		return $items;
	}

	private function _send_json($ajaxData) {
		header('Content-Type: application/json');
		echo json_encode($ajaxData);
		//echo stripslashes();
	}

	public function visual() {
		if (!$this->Users->has_identity()) {
			redirect(site_url('admin/login'));
		} else {
			$items = $this->get_migrations_list();
			$data = array(
				'items' => $items
			);
			$this->load->view('admin/visual_migrations', $data);
		}
	}

	public function apply($name) {
		if (!$this->Users->has_identity()) {
			header("HTTP/1.1 403 Forbidden");
		} else {
			$items = $this->get_migrations_list();
			if (!array_key_exists($name, $items)) {
				$status = false;
				$message = 'Migration not found';
			} else {
				ob_start();
				$this->db->db_debug = false;
				if ($this->db->query($items[$name]['sql'])) {
					$status = true;
					$message = "Migration " . $name . " successfully aplied!";
				} else {
					$status = false;
					$message = $this->db->_error_message();
				}
				ob_clean();
				ob_flush();
			}
			$this->_send_json(array(
				'message' => $message,
				'status' => $status
			));
		}
	}
}