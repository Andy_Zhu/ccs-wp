<?php

class Api extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	/**
	 * [parse_template description]
	 * @param  string $template String with template contents
	 * @return string           Rendered template contents
	 */
	protected function parse_template($template)
	{
		// ER. ugly hack in order to avoid nbsp to be inserted within h2o tags
		$template = str_replace('&nbsp;', ' ', $template);
		$h2o = h2o($template);
		return $h2o->render($_POST);
	}

	/**
	 * [email_templates description]
	 * @param  boolean $action [description]
	 * @param  boolean $id     [description]
	 * @return [type]          [description]
	 */
	public function email_templates($action=false, $id=false)
	{
		include_once('lib/h2o.php');
		$this->load->model('Email_Templates_Model');
		$this->load->model('Email_Templates_Names_Model');
		$this->load->model('Websites_Model');

		if (!$action) {
			$action = 'request';
		}

		switch($action) {
			case 'request':
				$website = $this->input->get('website');
				$name = $this->input->get('name');

				if ($website != '' && $name != '') {
					$email_template = $this->Email_Templates_Model->find_by_name_and_website($name, $website);

					if (count($email_template)>0) {
						$email_template->email = json_decode($email_template->email);
						$email_template->email->to = $this->parse_template($email_template->email->to);
						$email_template->email->from = $this->parse_template($email_template->email->from);
						$email_template->email->cc = $this->parse_template($email_template->email->cc);
						$email_template->email->bcc = $this->parse_template($email_template->email->bcc);
						$email_template->email->title = $this->parse_template($email_template->email->title);
						$email_template->email->body = $this->parse_template($email_template->email->body);

						$response['success'] = true;
						$response['message'] = '';
						$response['data'] = $email_template->email;
					} else {
						$response['success'] = false;
						$response['message'] = 'Template '.$name.' not found for website '.$website;
						$response['data'] = null;
					}
				} else {
					$response['success'] = false;
					$response['message'] = '*website* and *name* params not specified';
					$response['data'] = null;					
				}
				break;
			default:
				$response['success'] = false;
				$response['message'] = '*action* parameter was not specified';
				$response['data'] = null;
				break;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
		return;
	}
}