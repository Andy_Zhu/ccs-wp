<?php
/**
 * Combine and minify js and css files and html
 *
 * @author        derfenix<derfenix@gmail.com>
 * @var
 * TODO: minify js
 */
class Compressor {
	private $CI;
	private $content;
	private $head;
	private $orig_head;
	private $css_list = array();
	private $js_list = array();
	private $base_path;
	private $info;
	public $info_file_path = 'carservicecms_combiner.txt';
	private $combined_path;

	function __construct() {
		$this->CI =& get_instance();
		$this->content = $this->CI->output->get_output();
		$this->base_path = preg_replace('#system/#', '', BASEPATH);
		$this->info = $this->get_info();

        $this->info_file_path = $this->base_path . 'carservicecms_combiner.txt';

        //make folder if doesn't exist with full permissions
        if (!file_exists($this->base_path . 'combined/')) {
            $oldmask = umask(0);
            mkdir($this->base_path . 'combined/', 0777);
            umask($oldmask);
        }

        $this->combined_path = $this->base_path . 'combined/';
	}

	function compress() {
		$matches = array();
		if (!is_string($this->content)){
			return;
		}
		preg_match('/<head>(.*)<\/head>/ims', $this->content, $matches);
		if (count($matches) > 1) {
			$this->head = $this->orig_head = $matches[1];
		} else {
			return;
		}

		$need_save_info = false;
		$this->combine_js();
		$this->combine_css();

		$js_hash = $this->calc_hash($this->js_list);
		if ($js_hash != $this->info['js_hash']) {
			$this->write_combined('js', $js_hash);
			$this->info['js_hash'] = $js_hash;
			$need_save_info = true;
		}

		$css_hash = $this->calc_hash($this->css_list);
		if ($css_hash != $this->info['css_hash']) {
			$this->write_combined('css', $css_hash);
			$this->info['css_hash'] = $css_hash;
			$need_save_info = true;
		}

		if ($need_save_info) $this->save_info();

		$this->put_new_links();
		$this->content = preg_replace("#" . preg_quote($this->orig_head, '#') . "#", $this->head, $this->content);
		$this->minify_html();

		echo $this->content;
	}

	private function combine_css() {
		$matches = array();
		preg_match_all('#<link [^>]*\.css[\'"][^>]*>#i', $this->head, $matches);
		foreach ($matches[0] as $m) {
			$path_match = array();
			preg_match('/href=[\'"]([^\'"]*)[\'"]/i', $m, $path_match);
			$url = $path_match[1];

			if (!preg_match('#(http|//)#', $url)) {
				$this->css_list[] = $this->base_path . $url;
				$this->head = preg_replace("#$m#", '', $this->head);
			}
		}
	}

	private function combine_js() {
		$matches = array();
		preg_match_all('/(<script[^>]*\.js[^>]*><\/script>)/im', $this->head, $matches);
		foreach ($matches[0] as $m) {
			$path_match = array();
			preg_match('/src=[\'"]([^\'"]*)[\'"]/i', $m, $path_match);
			$url = $path_match[1];

			if (!preg_match('#(http|//)#', $url)) {
				$this->js_list[] = $this->base_path . $url;
				$this->head = preg_replace("#$m#", '', $this->head);
			}
		}
	}

	private function put_new_links() {
		$css = '<link rel="stylesheet" href="'.RELATIVE_PATH.'combined/' . $this->info['css_hash'] . '.css">';
		$js = '<script type="text/javascript" src="'.RELATIVE_PATH.'combined/' . $this->info['js_hash'] . '.js"></script>';
		$this->head = $css . $js . $this->head;
	}

	private function minify_html() {
		$this->content = preg_replace('/<!--.*-->/', "", $this->content);
		$this->content = preg_replace('#(^|[ \t])//.*$#im', "", $this->content);
		$this->content = preg_replace("#\n+#", "\n", $this->content);
		$this->content = preg_replace("#\s{3,}#", " ", $this->content);
		$this->content = preg_replace("#>\s+<#", "><", $this->content);
	}

	private function minify_js($s) {
//		$s = preg_replace('#^[ \t]*/\*.*?\*/[ \t]*[\r\n]#s', "", $s);
//		$s = preg_replace('#[ \t]*//.*[ \t]*[\r\n]#', "", $s);
//		$s = preg_replace('#\n+#', "", $s);
//		$s = preg_replace('#\s+#', " ", $s);
		return $s;
	}

	private function minify_css($s) {
		$s = preg_replace('#[ \t]*/\*.*?\*/[ \t]*[\r\n]#s', "", $s);
		$s = preg_replace('#\n+#', "", $s);
		$s = preg_replace('#\s+#', " ", $s);
		return $s;
	}

	private function get_content_from_list($lst) {
		$out = '';
		foreach ($lst as $l) {
			$h = fopen($l, 'r');
			$out .= fread($h, filesize($l));
			fclose($h);
		}
		return $out;
	}

	private function calc_hash($lst) {
		$str = '';
		foreach ($lst as $l) {
			$str .= $l . (string)filectime($l);
		}
		return md5($str);
	}

	private function write_combined($type, $hash) {
		if ($type == 'css') {
			$content = $this->minify_css($this->get_content_from_list($this->css_list));
		} elseif ($type == 'js') {
			$content = $this->minify_js($this->get_content_from_list($this->js_list));;
		} else {
			throw new Exception("Bad combiner type");
		}

		$filename = $this->combined_path . $hash . "." . $type;
		$h = fopen($filename, 'w+');
		fwrite($h, $content);
		fclose($h);
	}

	private function get_info() {
//		if (file_exists($this->info_file_path))
//			unlink($this->info_file_path);

		if (!file_exists($this->info_file_path)) {
			return array(
				'js_hash' => '',
				'css_hash' => ''
			);
		}

		$h = fopen($this->info_file_path, 'r');
		$info = fread($h, filesize($this->info_file_path));
		fclose($h);
		$info = explode("\n", $info);
		return array(
			"js_hash" => $info[0],
			"css_hash" => $info[1]
		);
	}

	private function save_info() {
		$info = implode(
			"\n",
			array(
				$this->info['js_hash'],
				$this->info['css_hash']
			)
		);
		$h = fopen($this->info_file_path, 'w+');
		fwrite($h, $info);
		fclose($h);
	}
}