
<div class="cell">
<h4>Phone And Fax:</h4>
<p><strong>Office Hours:</strong><br>
08:00 to 24:00 (China is GMT+8 hrs)</p>
<p><strong>Phone Numbers:</strong><br>
0755 2595 1800 (Intl: 86 755 2595 1800)</p>
<p><strong>After Hours:</strong><br>
(86) 1363 266 7585</p>
<p>Fax: 0755 2594 1385 (Intl: 86 755 2594 1385)</p>
<p><strong>Enquires Email:</strong><br>
<a href="mailto:bookings@beijingcarservice.com" style="color: black;">bookings@beijingcarservice.com</a></p>

<i>For changes to booking orders or cancellations please email us at <a
	href="mailto:bookings@beijingcarservice.com" style="color: black;">bookings@beijingcarservice.com</a></i>
</div>

<div class="address-block">
<h4>ADDRESS:</h4>
<address style="color: black;"><span>Beijing Car Service</span> <span>Suite
704, Wande Building</span> <span>1019 Shennan Middle Road</span> <span>Futian
District, Shenzhen, China, 518031</span></address>
</div>