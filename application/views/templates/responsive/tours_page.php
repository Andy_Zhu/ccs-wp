	<?php if(empty($tours_categories)):?>
		<h1><?php echo lang("comming-soon", 'Comming soon.'); ?></h1>
	<?php else:?>

		<?php $counter=1; foreach ($tours_categories as $tour_id => $tour_cat):?>
   <div class="row">
     <div class="col-lg-12 col-md-12">
			 <div class="page-block wide page-block-first tour-block">
         <h2><?php echo $tour_cat->title;?></h2>
         <div class="page-block-content">
           <div class="page-block-sub-content">
             <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <?php if(!empty($tour_cat->image) && file_exists(textbox_image_path($tour_cat->image))):?>
                    <img class="cat-image" src="<?php echo textbox_image_src($tour_cat->image, $is_preview);?>" alt="" />
                  <?php endif;?>
               </div>
               <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                 <?php echo $tour_cat->description;?>
                 <a href="<?php echo template_tour_category_page_link($site_id, 'subtours', $tour_cat,$is_preview);?>" class="page-block-innerbutton"><?php echo lang("view-tours-this-category", 'View tours in this category'); ?> &rarr;</a><span class="clear"></span>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
		<?php endforeach;?>
	<?php endif;?>





