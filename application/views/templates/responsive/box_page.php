<?php if($section == 'online-booking' && count($boxes) > 0):?>
	<?php
	foreach ($boxes as $box) {
		if(strtolower($box->title) == 'online booking') {
			$online_booking_top = $box;
		}else{
			$online_booking_down = $box;
		}
	}
	?>
<?php endif;?>

<?php if($section == 'about-us' && count($boxes)>=2):?>
  <?php
  foreach ($boxes as $box) {
    if($box->type != 'testimonial') {
      $about_us_box = $box;
    }
  }
  ?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="page-block wide page-block-first">
      <h2><?php echo lang("about-us",'About us');?></h2>
      <div class="page-block-content">
        <?php if($about_us_box->image):?>
        <div class="page-block-sub-content">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <img src="<?php echo textbox_image_src($about_us_box->image, $is_preview, "responsive")?>"/>
            </div>
          </div>
        </div>
        <?php endif;?>
        <div class="page-block-sub-content">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <h3 class="sub-header"><?php echo $about_us_box->title;?></h3>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <?php echo $about_us_box->content;?>
            </div>
          </div>
        </div>
        <div class="page-block-sub-content">
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <h3 class="sub-header"><?php echo lang('online-enquiry-form', 'online enquiry form'); ?></h3>
              <p class="sub-header"><?php echo lang('online-enquiry-form-note', 'For enquiries, changes to bookings, cancellations or other feedback you can also use our handy online enquiry form'); ?>:</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <form id="enquiry-form" method="post" action="<?php echo template_link($site, 'about-contact-form', $is_preview);?>">
                <label><?php echo lang('name', 'Name'); ?>*</label>
                <input placeholder="<?php echo lang("Your-full-name", "Your full name");?>"  required="required" name="name">
                <label><?php echo lang('email', 'Email'); ?>*</label>
                <input placeholder="<?php echo lang("Your-email-address", "Your email address");?>" required="required" name="email">
                <label><?php echo lang('subject', 'Subject'); ?>*</label>
                <input placeholder="<?php echo lang("The-subject-of-your-enquiry", "The subject of your enquiry");?>" required="required" name="subject">
                <label><?php echo lang('message', 'Message'); ?>*</label>
                <textarea placeholder="<?php echo lang("Your-message", "Your message...");?>" name="message" required="required"></textarea>
                <input type="hidden" name='market' value="<?php echo $site->name ;?>">
                <input id="booking_country" type="hidden" name="booking_country" value="">
                <input id="booking_state" type="hidden" name="booking_state" value="">
                <input id="booking_city" type="hidden" name="booking_city" value="">
                <input id="contact-submit" class="page-block-innerbutton" type="submit" value="<?php echo lang('enquire-now', 'Enquire now'); ?>">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php elseif($section == 'online-booking'):?>
<?php
if($site-> lang_id == 2){
  $site->online_book_url  = str_replace(' ','', 'https://royal-asia-limo.com/booking/' . strtolower(str_replace(' Car Service', '', $site->name)) . '/');
}else{
  $site->online_book_url  = str_replace(' ','', 'https://' . strtolower($website_country->name) . 'carservice.com/booking/' . strtolower(str_replace(' Car Service', '', $site->name)) . '/');
}

$site->online_quote_url = str_replace(' ','', $site->online_book_url . 'quote/');
?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="page-block wide page-block-first">
	   <h2><?php echo lang("online-booking", 'Online Booking'); ?></h2>
	   <div class="page-block-content">
       <div class="page-block-sub-content">
         <div class="pay-block row">
           <div class="col-lg-12 col-md-12 col-sm-12">

            	<?php if(isset($online_booking_top)):?>
            		<?php echo $online_booking_top->content;?>
            	<?php else:?>

            	Our on-line booking system processes your MasterCard, Visa or Amex Cards through Authorize.Net, an American credit card system. As with any transaction in the US or Europe your purchases are protected if you do not get what we promise! We also accept payment in cash, via wire transfer and PayPal.
          	<div class="row">
              <div class="col-sm-12">
                <img src="<?php  echo template_image_src('visa_512.png', $is_preview, "responsive")?>" alt="#" />
                <img src="<?php  echo template_image_src('amex_512.png', $is_preview, "responsive")?>" alt="#"/>
                <img src="<?php  echo template_image_src('mastercard_512.png', $is_preview, "responsive")?>" alt="#"/>
                <img src="<?php  echo template_image_src('paypal_512.png', $is_preview, "responsive")?>" alt="#"/>
              </div>
            </div>
            	At the end of each day our driver will have you verify the days toll and parking receipts, the odometer readings (start and finish) and the pick up / drop off times on an invoice. We will process the invoice and charge your credit card as per your booking and email you a receipt.
            	<?php endif;?>
            	<div class="row">
            	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            	    <a target="_blank" class="page-block-long-button" href="<?php echo $site->online_book_url;?>"><?php echo lang("secure_booking", "Make a Secure Online Booking Now"); ?></a>
            	  </div>
            	  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            	    <a target="_blank" class="page-block-long-button" href="<?php echo $site->online_quote_url;?>"><?php echo lang("request_quote", "Request a Quote or Information");?> </a>
	              </div>
	            </div>
	          </div>
	        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="page-block wide page-block-first">
      <h2><?php echo lang("cancellation-policy", 'Cancellation Policy'); ?></h2>
      <div class="page-block-content">
        <div class="page-block-sub-content">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
          	<?php if(isset($online_booking_down)):?>
          		<?php echo $online_booking_down->content;?>
          	<?php else:?>
          	<p>Cancellations are accepted by email (bookings@chinacarservice.com) and phone (86 755 2595 1800) only.   Urgent after hour changes can be made on (86 1363 266 7585).</p>

          	<p>Full reservation amount will be charged for cancellations made less than 12 hrs before scheduled pick up.  50% of the invoice value will be charged for cancellation made less than 24 hrs before scheduled pick up. Cancellations made in excess of 24 hours will not incur any charge.</p>

          	<p>No shows will be charged the full reservation fare.</p>
	          <?php endif;?>
  	          <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 unimportant-links">
                  <a target="_blank" href="<?php echo template_link($site_id, 'policies', $is_preview)?>"><?php echo lang("policies", 'Policies'); ?></a>
                  |
                  <a target="_blank"  href="<?php echo template_link($site_id, 'faqs', $is_preview)?>"><?php echo lang("faqs", 'FAQ’s'); ?></a>
                </div>
              </div>
        	  </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php elseif($section == 'testimonials'):?>
  <?php
    $testimonials_boxes = array();
    foreach ($boxes as $box) {
      if($box->type == 'testimonial') {
        $testimonials_boxes[] = $box;
      }
    }
  ?>
  <div class="page-block wide page-block-first">
    <h2><?php echo lang("what-our-customers-say-about-us", 'What our customers say about us'); ?> </h2>
    <div class="page-block-content">
      <?php
        foreach($testimonials_boxes as $testimonial):
          $testimonial_info_name = array('name', 'position', 'company', 'month', 'year');
          $testimonial_info = array();
          if($testimonial -> title):
            $info = explode("-", $testimonial -> title);
            for($i = 0; $i < count($info); $i ++):
              $testimonial_info[$testimonial_info_name[$i]] = $info[$i];
            endfor;
          endif;
      ?>
      <div class="page-block-sub-content page-block-testimonials">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php if($testimonial -> image):?>
            <img src="<?php echo textbox_image_src($testimonial -> image, $is_preview);?>"/>
            <?php else:?>
              <img src="<?php echo template_image_src('avatar.png', $is_preview, "responsive")?>"/>
            <?php endif;?>

            <!-- <p><strong>Shenzhen Car Service</strong> was the absolute dogs bollocks! They took me to get all elephant and trunk where it all went fucking pete tong. I got in a scrap with a gang of Triads and the driver even smacked one of them in the chevy chase. At which point we legged it to the safety of the luxurious bulletproof Humvee I fackin rented. Worth every Cock and Hen!  </p> -->
            <div class="testimonial-text">
            <?php echo $testimonial -> content;?>
            <?php
              if(isset($testimonial_info['name'])):
                echo "<p class='name'><strong>".$testimonial_info['name']."</strong></p>";
              endif;
              if(isset($testimonial_info['position']) && isset($testimonial_info['company'])):
                echo "<p>".$testimonial_info['position'].", ".$testimonial_info['company']."</p>";
              elseif(isset($testimonial_info['position'])):
                echo "<p>".$testimonial_info['position']."</p>";
              elseif(isset($testimonial_info['company'])):
                echo "<p>".$testimonial_info['company']."</p>";
              endif; ?>
              <?php
                if(isset($testimonial_info['month'])):
                  echo "<p>".$testimonial_info['month'].", ".$testimonial_info['year']."</p>";
                endif;
              ?>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
<?php else:?>
	<?php
		if($section == 'services' && !empty($top_boxes)):
      foreach ($top_boxes as $top_box):
  ?>
  <div class="page-block page-block-first wide">
    <h2><?php echo $top_box->title; ?></h2>
    <div class="page-block-content service">
      <div class="page-block-sub-content">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <?php if($top_box->image) {
              echo '<img src="'.textbox_image_src($top_box->image, $is_preview).'" alt="" />';
                  }
            ?>
          </div>
          <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
            <?php echo $top_box->content; ?>
                <a href='<?php echo template_link($site_id, 'online-booking',$is_preview);?>' class="page-block-innerbutton marginTop20" ><?php echo lang('enquire-now','Enquire now');?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
      endforeach;
    endif;
  ?>
    <?php
		$counter = 0;
    $tabCounter = 0;
		if($section == 'rates') {
			$english_speaking_rates = null;
			foreach ($boxes as $box) {
				if($box->ranking == '-1') {
					$english_speaking_rates = $box;
					break;
				}
			}
			if($english_speaking_rates) {
				$this->load->view('templates/responsive/inc/text_box', array('box'=>$english_speaking_rates, 'box_counter'=>$counter));
			}
      if(count($childBoxes)){
        $str =  "<div class='child_tab_selector_container'>";
        $str = $str."<ul>";
        foreach($childBoxes as $key => $boxesCollection){
            if($tabCounter == 0){
               $str = $str."<li><a id='$key' class='child_tab_selector active_child page-child-button'>".ucwords($key)." rates</a></li>";
            }else{
               $str = $str."<li><a id='$key' class='child_tab_selector page-child-button'>".ucwords($key)." rates</a></li>";
            }
            $tabCounter++;
        }
        $str = $str."</ul></div>";
        echo $str;
        $tabCounter = 0;
        foreach($childBoxes as $key => $boxesCollection){
          if($tabCounter == 0){
            echo "<div class='child_tab $key'>";
          }else{
            echo "<div class='child_tab $key display_none'>";
          }
          foreach($boxesCollection as $childBox){
            $this->load->view('templates/responsive/inc/text_box', array('box'=>$childBox, 'box_counter'=>$counter));
            $counter++;
          }
          echo "</div>";
          $tabCounter++;
        }
      }else{
        foreach ($boxes as $box) {
          if($box->ranking == '-1') continue;
          $this->load->view('templates/responsive/inc/text_box', array('box'=>$box, 'box_counter'=>$counter));
          $counter++;
        }
      }

		}else {
			$counter = 0;
			foreach ($boxes as $box) {

				$this->load->view('templates/responsive/inc/text_box', array('box'=>$box, 'box_counter'=>$counter));

				$counter++;
			}
		}

	?>
<?php endif;?>
