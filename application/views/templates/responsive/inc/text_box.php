<?php
$block_css = 'about-block';
$header = "";


if($section == 'rates') {
	$block_css = 'rates-block';
  $header = $box -> title." ".lang($section,'');
	if($section == 'rates' && $box->ranking ==  -1) {
		$block_css .= ' rates-eng-driver-box'; //shade first column
	}
} elseif($section == 'services') {
  $header = $box -> title;
}else{
  $header = lang($section,'');
}

?>
<div class="page-block page-block-first wide <?php echo $block_css;?>">
    <h2><?php echo $header;?></h2>
    <div class="page-block-content <?php echo $section ?>">
      <div class="page-block-sub-content">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-xs-12">
            <?php echo $box->content;?>
          </div>
        </div>
      </div>
      <?php if($section == "about-us"):?>
      <div class="page-block-sub-content">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <h3 class="sub-header"><?php echo lang('online-enquiry-form', 'online enquiry form'); ?></h3>
            <p class="sub-header"><?php echo lang('online-enquiry-form-note', 'For enquiries, changes to bookings, cancellations or other feedback you can also use our handy online enquiry form'); ?>:</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <form id="enquiry-form" method="post" action="<?php echo template_link($site, 'about-contact-form', $is_preview);?>">
              <label><?php echo lang('name', 'Name'); ?>*</label>
              <input placeholder="<?php echo lang("Your-full-name", "Your full name");?>"  required="required" name="name">
              <label><?php echo lang('email', 'Email'); ?>*</label>
              <input placeholder="<?php echo lang("Your-email-address", "Your email address");?>" required="required" name="email">
              <label><?php echo lang('subject', 'Subject'); ?>*</label>
              <input placeholder="<?php echo lang("The-subject-of-your-enquiry", "The subject of your enquiry");?>" required="required" name="subject">
              <label><?php echo lang('message', 'Message'); ?>*</label>
              <textarea placeholder="<?php echo lang("Your-message", "Your message...");?>" name="message" required="required"></textarea>
              <input type="hidden" name='market' value="<?php echo $site->name ;?>">
              <input id="contact-submit" class="page-block-innerbutton" type="submit" value="<?php echo lang('enquire-now', 'Enquire now'); ?>">
            </form>
          </div>
        </div>
      </div>
      <?php endif;?>
    </div>
  </div>