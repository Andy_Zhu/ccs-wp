<?php if($section == 'contact-us'):?>

<div class="contact-column">
  <div class="box-out">
    <div class="box-in">
      <h3><?php echo $contact_box ? $contact_box->title: '';?></h3>
      <div class="frame">
        <?php echo $contact_box ? $contact_box->content: '';?>
      </div>
    </div>
      <div class="UNL_image">
      <img src="images/UNL.png" alt="" />
    </div>
  </div>
</div>

<?php else:?>

  <?php  if($section != "home"):?>
  <?php  $market = $site -> shortname;?>
  <div class="row sidebar-first">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="page-block booking">
        <h3><?php echo lang('secure-online-booking', 'secure online booking'); ?></h3>
        <form method="get" action="<?php echo template_link($site, 'booking-form', $is_preview);?>/">
          <div class="page-block-content">
            <div class = "pay-block">
              <img src="<?php  echo template_image_src('visa_512.png', $is_preview, "responsive")?>" alt="#" />
              <img src="<?php  echo template_image_src('amex_512.png', $is_preview, "responsive")?>" alt="#"/>
              <img src="<?php  echo template_image_src('mastercard_512.png', $is_preview, "responsive")?>" alt="#"/>
              <img src="<?php  echo template_image_src('paypal_512.png', $is_preview, "responsive")?>" alt="#"/>
            </div>
            <label><?php echo lang('service-required', 'Service required');?>:</label>
            <select name="service_required">
              <option selected="selected" value="Airport Pickup"><?php echo lang('airport-pick-up', 'Airport Pickup'); ?></option>
              <option value="Hotel Pickup"><?php echo lang('hotel-pick-up', 'Hotel Pickup'); ?></option>
              <option value="Half/Full Day Hire"><?php echo lang('half-full-dayhire', 'Half/Full Day Hire'); ?></option>
            </select>
            <label class="showForDayhire"><?php echo lang('duration-required', 'Duration required'); ?>:</label>
            <input class="showForDayhire" placeholder="<?php echo lang("required-duration", "No. of hours or days required?");?>" name="duration"/>
            <label class="showForDayhire"><?php echo lang('itinerary', 'Itinerary'); ?>:</label>
            <textarea class="showForDayhire" name="itinerary" placeholder="<?php echo lang("detailed-pickup-dropoff", "Provide a detailed list of pick-up & drop-off locations and times if known."); ?>"></textarea>
            <label class="hideForDayhire"><?php echo lang('pickup-from', 'Pickup from'); ?>:</label>
            <select class="hideForDayhire" name="airport_location">
              <?php foreach($arrivals as $arrival):?>
                <option value="<?php echo $arrival->content;?>">
                  <?php echo $arrival->content;?>
                </option>
              <?php endforeach;?>
            </select>
            <input class="hideForDayhire" name="pickup_location" placeholder="<?php echo lang("Address-Airport-Hotel","Address, Airport, Hotel ..."); ?>" style="display:none;">
            <label class="hideForDayhire"><?php echo lang('destination', 'Destination'); ?>:</label>
            <input class="hideForDayhire" placeholder="<?php echo lang("Address-Airport-Hotel","Address, Airport, Hotel ..."); ?>" name="destination">
            <input type="hidden" name="market" value="<?php echo $market;?>">
            <label><?php echo lang('date', 'Date'); ?>:</label>
            <div class="input-append date" id="dp" data-date="" data-date-format="mm/dd/yyyy">
              <input class="datepicker-field" type="text" placeholder="<?php echo lang("form-date-format", "mm/dd/yyyy"); ?>" name="service_date">
              <span class="add-on"><i class="fa fa-calendar plus-size"></i></span>
            </div>
            <label><?php echo lang('time', 'Time'); ?>:</label>
            <div class = "service-time">
              <select id = "arrival-hour" name="arrival-hour">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
              <select id = "arrival-mins" name="arrival-mins">
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
              </select>
              <select id = "AM-PM" name="AM-PM">
                <option value="AM"><?php echo lang('am', 'AM'); ?></option>
                <option value="PM"><?php echo lang('pm', 'PM'); ?></option>
              </select>
            </div>
           <div class="row txtC">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <img class="booking-nla" src="<?php  echo template_image_src('nla-logo.jpg', $is_preview, "responsive")?>">
                <img class="booking-nla" src="<?php  echo template_image_src('GBTA_logo.jpg', $is_preview, "responsive")?>">
              </div>
            </div>
          </div>
          <input id="booking_country" type="hidden" name="booking_country" value="">
          <input id="booking_state" type="hidden" name="booking_state" value="">
          <input id="booking_city" type="hidden" name="booking_city" value="">
          <input type="submit" class="page-block-button" value="<?php echo lang('book-online-today', 'Book Online Today'); ?>">
        </form>
      </div>
    </div>
  </div>

  <?php if(!in_array($section, array('tours','subtours'))):?>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="page-block">
          <h3> <?php echo lang('our_fleet_includes', 'Our fleet includes:'); ?> </h3>
            <div class="page-block-content">
              <ul class="page-block-fleet-list">
          <?php
            $v_num = 0;
            foreach ($site_vehicles as $s_menu_v):

              $menu_v = $vehicles[$s_menu_v->vehicle_id];
              //$tmp_section = str_replace(' ', '-', trim($page_vehicle->short_name));

              if($v_num >= $site->sidebar_max_cars) break;
              $v_num++;
          ?>
            <li class="page-block-car">
              <a href="<?php echo template_car_page_link($site_id, $menu_v->page_name,$menu_v->page_name, $is_preview)?>">
                <?php if(strlen(trim($menu_v->icon))):?>
                <img src="<?php echo vehicle_image_single_thumb_src($menu_v->icon, $is_preview);?>" alt="" />
                <?php endif;?>
              <?php echo $menu_v->short_name;?>
              </a>
            </li>
          <?php endforeach;?>
          </ul>
        </div>
        <a href="<?php echo template_link($site_id, 'fleet', $is_preview); ?>" class="page-block-button" ><?php echo lang("see_entire_fleet", "See Our Entire Fleet"); ?></a>
        </div>
      </div>
    </div>
    <?php endif;?>

  <?php endif; ?>
  <?php if(isset($testimonial)):?>

    <?php
      if(strlen($testimonial -> content) > 120){
        $excerpt = substr(strip_tags($testimonial -> content), 0, 120)."...";
      }else{
        $excerpt = strip_tags($testimonial -> content);
      }
      $info = explode("-", $testimonial -> title);
      $testimonial_info_name = array('name', 'position', 'company', 'month', 'year');
      $testimonial_info = array();
      for($i = 0; $i < count($info); $i ++):
        $testimonial_info[$testimonial_info_name[$i]] = $info[$i];
      endfor;
   ;?>
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="page-block testimonial">
        <h3><?php echo lang("satisfied-customer", 'Our Satisfied Customers'); ?></h3>
        <div class="page-block-content">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
             <?php if($testimonial -> image):?>
              <img src="<?php echo textbox_image_src($testimonial -> image, $is_preview);?>"/>
              <?php else:?>
                <img src="<?php echo template_image_src('avatar.png', $is_preview, "responsive")?>"/>
              <?php endif;?>
              <div class="sidebar-testimonial-name">
                <?php if(isset($testimonial_info['name'])): ?>
                <p><?php echo $testimonial_info['name']; ?></p>
                <?php endif; ?>
                <?php if(isset($testimonial_info['position'])): ?>
                <p><?php echo $testimonial_info['position']; ?></p>
                <?php endif; ?>
                <?php if(isset($testimonial_info['company'])): ?>
                <p><?php echo $testimonial_info['company']; ?></p>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <p><?php echo $excerpt; ?></p>
        </div>
          <a class="page-block-button" href="<?php echo template_link($site_id, 'testimonials',  $is_preview);?>"><?php echo lang("more-testimonials", 'More Testimonials'); ?></a>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <div class="row">
    <?php foreach($top_boxes as $top_box):?>
    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
      <div class="page-block">
        <?php if($top_box->image): ?>
        <img src="<?php echo textbox_image_src($top_box->image, $is_preview); ?>"/>
        <?php endif;?>
          <div class="page-block-content">
            <p><?php echo $top_box -> title;?></p>
          </div>
          <a class="page-block-button" href="<?php echo template_link($site_id, 'services',$is_preview);?>"><?php echo lang("see-our-services", "See Our Services");?></a>
      </div>
    </div>
    <?php break;endforeach;?>
    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
      <div class="page-block contact">
        <h3><?php echo lang("contact-us", 'Contact Us'); ?></h3>
          <div class="page-block-content skype">
            <?php if($site-> skype){ ?>
            <div>
              <a class="skype-call" href="skype:<?php echo $site-> skype;?>?call">
                <i class="fa fa-skype "></i>&nbsp; free call &nbsp;<i class="fa fa-phone fa-flip-horizontal"></i>
              </a>
            </div>
            <a class="marginBtm20 displayInlineBlock">Skype ID - <?php echo $site-> skype;?></a>
            <?php } ?>
            <p><?php echo $site->opening_hours;?><br/><?php echo $site->timezone;?></p>
            <p class="phone"><i class="fa fa-phone"></i> <?php echo $site->phone?></p>
            <?php if($site -> phone_after_hours){ ?>
              <p><?php echo $site -> phone_after_hours;?></p>
            <?php } ?>
            <a class="mail" href="mailto:<?php echo $site->contact_form_email;?>"><i class="fa fa-envelope"></i> <?php echo lang("email-us", 'E-mail Us'); ?></a>
          </div>
          <a class="page-block-button" href="<?php echo template_link($site_id, 'online-booking', $is_preview); ?>"><?php echo lang("make-online-enquiry", 'Make Online Enquiry'); ?></a>
      </div>
    </div>
  </div>
<?php endif; ?>

<?php if(($section=='subtours' || $section=='tours')):?>
  <?php if(false)://comment?>
  <div class="tours-cat-navi-container">
    <h2><?php echo lang("explore-more-tours", 'Explore more tours!'); ?></h2>
    <ul class="tours-cats-navi">
      <li>&nbsp;</li>
      <?php foreach ($tours_categories as $tour_cat):?>
        <li><a href="<?php echo template_tour_category_page_link($site_id, $section, $tour_cat, $is_preview)?>"><?php echo htmlentities($tour_cat->title);?></a></li>
      <?php endforeach;?>
    </ul>
  </div>

  <?php endif;?>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="page-block">
      <h3> <?php echo lang('tours_cat', 'Tour categories'); ?> </h3>
      <div class="page-block-content">
        <ul class="page-block-fleet-list">
            <?php foreach ($tours_categories as $tour_cat):?>
              <li class="page-block-car">
                <a href="<?php echo template_tour_category_page_link($site_id, 'subtours', $tour_cat, $is_preview)?>">
                <?php if(strlen(trim($tour_cat->image))):?>
                <img style="" src="<?php echo textbox_image_src($tour_cat->image, $is_preview);?>" class="icon" alt="" />
                <?php endif;?>
                <?php echo htmlentities($tour_cat->title);?>
                </a>
              </li>
            <?php endforeach;?>
        </ul>
        <a href="<?php echo template_link($site_id, 'tours', $is_preview);?>" class="page-block-button" >See All Categories</a>
      </div>
    </div>
  </div>
</div>
<?php endif;?>

