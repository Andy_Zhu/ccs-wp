<div class="main-body">
	<?php if($section !== 'home'):?>
	<div class = "bannerback">
    <div class = "container">
      <div class = "row">
        <div class = "col-lg-12 col-md-12">
          <h1 class = "bannerlogo">
		<?php
		if(isset($carpage)) {
			echo "<i class='fa fa-car hidden-xs'></i>".$page_vehicle->long_name." CHAUFFEUR SERVICE";
		}else if($section=='subtours' && isset($tour_category)){
			echo $tour_category->title;
		}else{

	     		//TODO: Refactor into CMS/translations section
			$section_title =  trim(ucwords(strtolower(str_replace('-', ' ', $section))));

			if ($site->lang_id == 2){

  			switch(strtolower($section_title)){
    			case "home":
    				echo "主页";
    			break;

    			case "about us":
    				echo "<i class='fa fa-car hidden-xs'></i>公司简介";
    			break;
    			case "fleet":
    				echo "<i class='fa fa-car hidden-xs'></i>车型展示";
    			break;
    			case "services":
    				echo "<i class='fa fa-thumbs-up hidden-xs'></i>服务范围";
    			break;
    			case "rates":
    				echo "<i class='fa fa-dollar hidden-xs'></i>租车价格 ";
    			break;
          case "testimonials":
            echo "<i class='fa fa-comments hidden-xs'></i>客人的评价 ";
          break;
    			case "online booking":
    				echo "<i class='fa fa-car hidden-xs'></i>在线订车";
    			break;
    			case "contact":
    				echo "<i class='fa fa-phone hidden-xs'></i>联系我们";
    			break;
    			default:
    				echo $section_title ;
  			}

			}else{
			  $city = str_replace(" Car Service", "", $site->name);
				switch(strtolower($section_title)){
          case "home":
            echo "home";
          break;

          case "about us":
            echo "<i class='fa fa-car hidden-xs'></i>"."About ".$site->name;
          break;
          case "fleet":
            echo "<i class='fa fa-car hidden-xs'></i>".$site->name."'s ".$section_title;
          break;
          case "services":
            echo "<i class='fa fa-thumbs-up hidden-xs'></i>$city Airport Transfers, $city Limo Service and More";
          break;
          case "rates":
            echo "<i class='fa fa-dollar hidden-xs'></i>".$site->name."'s ".$section_title;
          break;
          case "testimonials":
            echo "<i class='fa fa-comments hidden-xs'></i>".$site->name." Customer Reviews";
          break;
          case "online booking":
            echo "<i class='fa fa-car hidden-xs'></i>Reserve Your $city Limo Service Now! Quick & Secure Booking";
          break;
          case "contact":
            echo "<i class='fa fa-phone hidden-xs'></i>".$site->name." Customer Service is Available 24 x 7";
          break;
          case "tours":
            echo "<i class='fa fa-phone hidden-xs'></i>".$site->name."'s Private Guided Tours";
            break;
          default:
            echo $section_title ;
      }
      }
		}?>
		      </h1>
        </div>
      </div>
    </div>
  </div>
  <div class="post-form-reply">
    Thank you for your enquiry. We will reply asap
  </div>
	<?php endif;?>
  <div class="container">
	 <?php
	  if($section == 'home'):
	    echo $this->load->view($page_view);
    else:
    ?>
        <div class="row">
          <div class="col-lg-12 marginTop10 breadcrumbs">
            <?php if(isset($carpage)):?>
              <p> <?php echo lang("You are here", ''); ?>: <a href="<?php echo template_link($site_id, 'home', $is_preview);?>"> <?php echo lang("home", 'Home'); ?> </a> > <a href="<?php echo template_link($site_id, 'fleet', $is_preview);?>"> <?php echo lang("fleet", 'Fleet'); ?> </a> > <?php  echo $section;?></p>
            <?php else: ?>
              <p> <?php echo lang("You are here", ''); ?>: <a href="<?php echo template_link($site_id, 'home', $is_preview);?>"> <?php echo lang("home", 'Home'); ?> </a> > <?php echo lang( $section, ''); ?></p>
            <?php endif; ?>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-9 col-md-9">
    <?php
      echo $this->load->view($page_view);
    ?>
          </div>
          <div class="col-lg-3 col-md-3 sidebar">
    <?php
	    echo $this->load->view('templates/responsive/inc/sidebar');
    endif;
	 ?>
	        </div>
        </div>
      </div>
  </div>

