
<?php
    $country = $site->country_id;
    if ($country == "9" || $country == "1") {
      $parent_market = "asia";
    } else {
      $parent_market = "china";
    }

  ?>
<footer>
  <div class="container">
    <div class="row footer-top marginTop20">
      <div class="col-lg-12 col-md-12 col-xs-12">
        <?php echo $site->footer_content;?>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
          <ul>
            <li><a href="<?php echo template_link($site_id, 'faqs', $is_preview); ?>"><span><?php echo lang("faqs", 'FAQs'); ?></span></a></li>
            <li><a href="<?php echo template_link($site_id, 'policies', $is_preview); ?>"><span><?php echo lang("policies", 'Policies'); ?></span></a></li>
            <li><a href="<?php echo template_link($site, 'contact-us', $is_preview); ?>"><span><?php echo lang("contact", 'Contact'); ?></span></a></li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
        <?php if($site -> lang_id != 2): ?>
          <p> Copyright © 2001 - <?php echo date('Y') ?>  <?php echo ucfirst($parent_market); ?> Car Service,  All Rights Reserved.</p>
        <?php else:?>
          <br><center>粤ICP备14069779号@</center></p>
        <?php endif;?>
        </div>
      </div>
    </div>
  </div>
</footer>


<?php if($hasBanner):?>
<?php
if($site -> lang_id == 2){
      $contactus_url = "https://royal-asia-limo.com/booking/$site->shortname/quote/";
    }else{
      $contactus_url = "https://{$parent_market}carservice.com/booking/$site->shortname/quote/";
    }
?>
	<div id="banner" style="<?php echo $banner->styles;?> <?php echo $banner->position;?>: 0;">
		<a href="nowhere" id="banner-close-btn"><img alt="close" src="<?php echo template_image_src('banner-close-btn.png', $is_preview);?>" /></a>
		<div class="banner-left-half">
		  <img src="<?php echo banner_image_src($banner->image);?>"/>
		</div>
		<div class="banner-right-half">
		  <h3><?php echo $banner->heading;?></h3>
      <?php echo $banner->content;?>
      <a class="banner-btn" href="<?php echo $contactus_url;?>">Enquire now!</a>
    </div>
	</div>
<?php endif;?>


<?php
echo $site->g_analytics;
?>
</body>
</html>