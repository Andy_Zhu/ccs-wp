<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $site -> name . " - " . ($meta_title ? $meta_title : ($page && $page -> meta_title ? $page -> meta_title : $section)); ?></title>
	<meta name="description" content="<?php echo htmlentities($meta_description ? $meta_description : ($page ? $page -> meta_description : $site -> meta_desc)); ?>" />
	<meta name="keywords" content="<?php echo htmlentities($meta_keywords ? $meta_keywords : ($page ? $page -> meta_keywords : $site -> meta_keywords)); ?>" />
  <![if !IE]>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><!-- Remove if you're not building a responsive site. (But then why would you do such a thing?) -->
  <![endif]>
  <!--[if gte IE 9]
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo template_css_src('css3.css', $is_preview, "responsive"); ?>" media="all" />
	<link rel="stylesheet" href="<?php echo template_css_src('style2.css', $is_preview, "responsive"); ?>" type="text/css" media="screen" />
	<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo template_css_src('contact-form.css', $is_preview, "responsive"); ?>' type='text/css' media='all' />
  <link rel='stylesheet' id='bootstrap-css'  href='<?php echo template_css_src('bootstrap.min.css', $is_preview, "responsive"); ?>' type='text/css' media='all' />
  <link rel='stylesheet' id='datepicker-css'  href='<?php echo template_css_src('datepicker.css', $is_preview, "responsive"); ?>' type='text/css' media='all' />
  <link  rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<script type='text/javascript' src='<?php echo template_js_src('js1.js', $is_preview, "responsive"); ?>'></script>
	<script type='text/javascript' src='<?php echo template_js_src('jquery-1.11.1.min.js', $is_preview, "responsive"); ?>'></script>
	<script type='text/javascript' src='<?php echo template_js_src('bootstrap.min.js', $is_preview, "responsive"); ?>'></script>
	<script type='text/javascript' src='<?php echo template_js_src('bootstrap-datepicker.js', $is_preview, "responsive"); ?>'></script>
	<script type='text/javascript' src='<?php echo template_js_src('hamburger.js', $is_preview, "responsive"); ?>'></script>
	<script type='text/javascript' src='<?php echo template_js_src('site.js', $is_preview, "responsive"); ?>'></script>
  <!--[if lt IE 9]>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type='text/javascript' src='<?php echo template_js_src('css3-mediaqueries.js', $is_preview, "responsive"); ?>'></script>
	<?php if($section == 'home'):?>
	  <link rel='stylesheet' id='jcarousel-css'  href='<?php echo template_css_src('jcarousel.css', $is_preview, "responsive"); ?>' type='text/css' media='all' />
    <link rel='stylesheet' id='jcarousel-basic-css'  href='<?php echo template_css_src('jcarousel.basic.css', $is_preview, "responsive"); ?>' type='text/css' media='all' />
    <script type='text/javascript' src='<?php echo template_js_src('jquery.jcarousel.min.js', $is_preview, "responsive"); ?>'></script>
    <script type='text/javascript' src='<?php echo template_js_src('jcarousel.basic.js', $is_preview, "responsive"); ?>'></script>
	<?php endif; ?>
	<?php if($section == 'about-us'):?>
	  <script type='text/javascript' src='<?php echo template_js_src('contact.js', $is_preview, "responsive"); ?>'></script>
	<?php endif;?>
	<?php if($hasBanner && $banner):?>
		<script type='text/javascript' src='<?php echo template_js_src('banner.js', $is_preview, "responsive"); ?>'></script>
	<?php endif; ?>
	<script type='text/javascript' src='<?php echo template_js_src('tabs.js', $is_preview, "responsive"); ?>'></script>
	<?php if($section == 'faqs'):?>
	<script type='text/javascript' src='<?php echo template_js_src('jquery.scrollTo.js', $is_preview, "responsive"); ?>'></script>
		<script type='text/javascript'>
      jQuery(document).ready(function($) {

        $('#content ul li a').click(function(evt) {
          evt.preventDefault();
          $.scrollTo('a[name="' + $(this).attr('href').replace('#', '') + '"]', 'slow');
        });
      });
		</script>
	<?php endif; ?>
	<style type="text/css">

		.header-area .logo{
			<?php if(!empty($site->logo)):?>
				background-image:url(<?php echo site_logo_src($site->logo,  $is_preview)?>
          );
			<?php else: ?>
        background-image:none;
			<?php endif; ?>
        }
	</style>
    <?php
    echo $site -> livechat;
	?>
</head>
<body id="body-wrapper" class="<?php echo $section?>">
  <header>
    <div class="container">
      <div class="row full-height">
        <div id="mobile-hamburger-menu" class="col-xs-12 displayN visible-xs visible-sm">
          <div class = "row">
            <div class = "col-xs-12 col-sm-12">
              <i id="mobile-hamburger-close" class="fa fa-close fa-2x"></i>
            </div>
          </div>
          <div class = "row">
            <ul class="col-xs-12 mobile-menu-links">
              <li><a href="<?php echo template_link($site_id, 'home', $is_preview); ?>"><span><?php echo lang("home", 'Home'); ?></span></a></li>
              <li><a href="<?php echo template_link($site_id, 'about-us', $is_preview); ?>"><span><?php echo lang("about-us-responsive", 'About'); ?></span></a></li>
              <li><a href="<?php echo template_link($site_id, 'fleet', $is_preview); ?>"><span><?php echo lang("fleet", 'Fleet'); ?></span></a></li>
              <li><a href="<?php echo template_link($site_id, 'services', $is_preview); ?>"><span><?php echo lang("services", 'Services'); ?></span></a></li>
              <li><a href="<?php echo template_link($site_id, 'testimonials', $is_preview); ?>"><span>Testimonials</span></a></li>
              <li><a href="<?php echo template_link($site_id, 'rates', $is_preview); ?>"><span><?php echo lang("rates", 'Rates'); ?></span></a></li>
               <?php if($has_tours):?>
                <li id="menu-item-392" class="has-submenu <?php
                if ($section == 'tours')
                  echo "current-menu-item ";
              ?>menu-item menu-item-type-custom menu-item-object-custom menu-item-391">
                  <a href="<?php echo template_link($site_id, 'tours', $is_preview); ?>"><span><?php echo lang("tours", 'Tours'); ?></span></a>
                </li>
              <?php endif; ?>
              <li><a href="<?php echo template_link($site_id, 'online-booking', $is_preview); ?>"><span><?php echo lang("online-bookings", 'Online Bookings'); ?></span></a></li>
              <li><a href="<?php echo template_link($site, 'contact-us', $is_preview); ?>"><span><?php echo lang("contact", 'Contact'); ?></span></a></li>
              <li><a href="<?php echo template_link($site_id, 'faqs', $is_preview); ?>"><span><?php echo lang("faqs", 'FAQs'); ?></span></a></li>
              <li><a href="<?php echo template_link($site_id, 'policies', $is_preview); ?>"><span><?php echo lang("policies", 'Policies'); ?></span></a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row full-height">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 logo">
              <span class="vertical-helper"></span><a href="/"><img src="<?php echo site_logo_src($site->logo,  $is_preview)?>"></a>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 header-right">
              <div class="row hidden-xs">
                  <div class="nav col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="corner-nav navbar">
                      <li><i class="fa fa-phone"></i> <?php echo $site -> phone;?></li>
                      <!-- <li><a href="http://ccs.titan-is.co.uk/register" class="dotunderline"><i class="fa fa-pencil"></i> Register</a></li>
                      <li><a href="http://ccs.titan-is.co.uk/login" class="loginlogo"><i class="fa fa-user plus-size"></i> Login </a></li> -->
                    </ul>
                  </div>
              </div>
              <div class="row">
                <div id="mobile-hamburger" class="visible-xs col-xs-12">
                  <div class="row">
                    <div class="col-xs-10">
                      <ul class="corner-nav navbar">
                        <li><i class="fa fa-phone"></i> <?php echo $site -> phone;?></li>
                        <!-- <li><a href="http://ccs.titan-is.co.uk/register" class="dotunderline"><i class="fa fa-pencil"></i> Register</a></li> -->
                        <!-- <li><a href="http://ccs.titan-is.co.uk/register" class="dotunderline"><i class="fa fa-user"></i> Login</a></li> -->
                      </ul>
                    </div>
                    <div class="col-xs-2">
                      <i class="fa fa-bars"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <nav id="top-nav">
            <ul class="menu hidden-xs">
              <li class="<?php
              if ($section == 'home')
                echo "current-menu-item ";
            ?>"><a href="<?php echo template_link($site_id, 'home', $is_preview); ?>"><span><?php echo lang("home", 'Home'); ?></span></a></li>
              <li class="has-submenu <?php
              if ($section == 'about-us')
                echo "current-menu-item ";
              ?>"><a href="<?php echo template_link($site_id, 'about-us', $is_preview); ?>"><span><?php echo lang("about-us-responsive", 'About'); ?></span></a>
                <ul class="submenu">
                    <li><a href="<?php echo template_link($site_id, 'testimonials', $is_preview); ?>"><span><?php echo lang("customer-reviews", 'Customer Reviews'); ?></span></a></li>
                </ul>
              </li>
              <li class="<?php
              if ($section == 'fleet')
                echo "current-menu-item ";
              ?>"><a href="<?php echo template_link($site_id, 'fleet', $is_preview); ?>"><span><?php echo lang("fleet", 'Fleet'); ?></span></a></li>
              <li class="<?php
              if ($section == 'services')
                echo "current-menu-item ";
              ?>"><a href="<?php echo template_link($site_id, 'services', $is_preview); ?>"><span><?php echo lang("services", 'Services'); ?></span></a></li>
               <?php if(isset($testimonial)): ?>
              <?php endif; ?>
              <li class="<?php
              if ($section == 'rates')
                echo "current-menu-item ";
              ?>"><a href="<?php echo template_link($site_id, 'rates', $is_preview); ?>"><span><?php echo lang("rates", 'Rates'); ?></span></a></li>
              <?php if($has_tours):?>
                <li id="menu-item-392" class="has-submenu <?php
                if ($section == 'tours' || $section == 'subtours')
                  echo "current-menu-item ";
              ?>menu-item menu-item-type-custom menu-item-object-custom menu-item-391">
                  <a href="<?php echo template_link($site_id, 'tours', $is_preview); ?>"><span><?php echo lang("tours", 'Tours'); ?></span></a>
                  <ul class="submenu">
                    <?php foreach ($tours_categories as $tmp_tr_cat):?>
                      <li><a href="<?php echo template_tour_category_page_link($site_id, 'subtours', $tmp_tr_cat, $is_preview)?>"><?php echo $tmp_tr_cat -> title; ?></a></li>
                    <?php endforeach; ?>
                  </ul>
                </li>
              <?php endif; ?>
              <li class="<?php
              if ($section == 'online-booking')
                echo "current-menu-item ";
              ?>"><a href="<?php echo template_link($site_id, 'online-booking', $is_preview); ?>"><span><?php echo lang("online-bookings", 'Online Bookings'); ?></span></a></li>
              <li class="<?php
              if ($section == 'contact-us')
                echo "current-menu-item ";
              ?>"><a href="<?php echo template_link($site, 'contact-us', $is_preview); ?>"><span><?php echo lang("contact", 'Contact'); ?></span></a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>
