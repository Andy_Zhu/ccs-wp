<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="page-block wide page-block-first">
      <h2><?php echo $static_page ? lang(strtolower($static_page->name), $static_page->name) : '';?></h2>
      <div class="page-block-content">
        <div class="page-block-sub-content">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <?php echo $static_page ? $static_page->content: '';?>
	          </div>
	        </div>
	      </div>
		  </div>
	  </div>
  </div>
</div>