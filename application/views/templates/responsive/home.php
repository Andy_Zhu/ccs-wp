<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete" async defer></script>
<?php
$defaults['provided_service'] = "Airport pickup - From Beijing and Tianjin International Airports\n
Cruise ship transfers - Cruise ship terminal to Beijing city or airport.\n
Airport VIP meet and greet service - fast-track immigration and customs.\n
Professional translator / interpreter services\n
English speaking guides - Ideal for shopping, touring, dining, and experiencing the culture of region.\n
Escorted tours - visit the many famous historical sites of the region
";

$defaults['fleet_include'] = "Luxury limousine saloons, \n
Business and intermediate saloons, \n
MPV's (multipurpose vehicles) and \n
Minivans and buses\n
";
?>
<?php  $market = $site -> shortname;?>
<?php foreach ($home_boxes as $box):?>
	<?php
  if ($box -> title == 'fleet include') {
    $fleet_include_box = $box;
  } else if ($box -> title == 'provided services') {
    $provided_services_box = $box;
  }else if($box->title == 'carousel title 1') {
    $carousel_title_1_box = $box;
  }else if($box->title == 'carousel title 2') {
    $carousel_title_2_box = $box;
  }
	?>
<?php endforeach; ?>
<div class = "row">
  <div class="mobile-header col-xs-12 visible-xs">
      <?php if(isset($carousel_title_1_box)) echo "<h1>".$carousel_title_1_box->content."</h1>"?>
      <?php if(isset($carousel_title_2_box)) echo "<h2>".$carousel_title_2_box->content."</h2>"?>
  </div>
</div>
<div class="row">
  <div class="home-left col-lg-3 col-md-3 col-sm-12">
    <div class="page-block booking homepage-main">
      <h3><?php echo lang('24-7-online-quote', '24/7 online quote'); ?></h3>
      <form method="get" action="<?php echo template_link($site, 'enquire-form', $is_preview);?>/">
          <input type="hidden" name="book-link" value="<?php echo template_link($site, 'enquire-form', $is_preview);?>" />
          <div class="page-block-content">
            <div class = "pay-block row">
              <div class="col-sm-12">
                <img src="<?php  echo template_image_src('visa_80.png', $is_preview, "responsive")?>" alt="#" />
                <img src="<?php  echo template_image_src('amex_80.png', $is_preview, "responsive")?>" alt="#"/>
                <img src="<?php  echo template_image_src('mastercard_80.png', $is_preview, "responsive")?>" alt="#"/>
                <img src="<?php  echo template_image_src('paypal_80.png', $is_preview, "responsive")?>" alt="#"/>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <label><?php echo lang('service-required', 'Service required');?>:</label>
                <select name="service_required">
                    <option selected="selected" value="Airport Pickup"><?php echo lang('airport-pick-up', 'Airport Pickup'); ?></option>
                    <option value="Hotel Pickup"><?php echo lang('hotel-pick-up', 'Hotel Pickup'); ?></option>
                    <option value="Half/Full Day Hire"><?php echo lang('half-full-dayhire', 'Half / Full Day Hire'); ?></option>
                </select>
              </div>
            </div>
            <div class="row showForDayhire">
              <div class="col-sm-12">
                <label><?php echo lang('duration-required', 'Duration required'); ?>:</label>

                <select name="duration">
                    <?php for($i=4; $i<13; $i++):?>
                        <option value="<?php echo $i; ?>"><?php echo $i." hours"; ?></option>
                    <?php endfor;?>
                        <option value="on_request"> > 12 hours, please enquire</option>
                </select>

                <label><?php echo lang('itinerary', 'Itinerary'); ?>:</label>
                <textarea name="itinerary" placeholder="<?php echo lang("detailed-pickup-dropoff", "Provide a detailed list of pick-up & drop-off locations and times if known."); ?>"></textarea>
              </div>
            </div>
            <div class="row hideForDayhire">
              <div class="col-sm-12">
                <label><?php echo lang('pickup-from', 'Pickup from'); ?>:</label>
                <select name="airport_location">
                  <?php foreach($arrivals as $arrival):?>
                    <option value="<?php echo $arrival->content;?>">
                      <?php echo $arrival->content;?>
                    </option>
                  <?php endforeach;?>
                </select>
                <input name="pickup_location" id="autocomplete-pickup" placeholder="<?php echo lang("Address-Airport-Hotel", "Address, Airport, Hotel ..."); ?>" style="display:none;">
                <label><?php echo lang('destination', 'Destination'); ?>:</label>
                <input id="autocomplete-dropoff" placeholder="<?php echo lang("Address-Airport-Hotel","Address, Airport, Hotel ..."); ?>" name="destination">
              </div>
            </div>

            <input type="hidden" name="market" value="<?php echo $market;?>">

            <div class="row booking-date">
              <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 booking-date-left">
                <label><?php echo lang('date', 'Date'); ?>:</label>
                <input id="dp" class="datepicker-field" type="text" placeholder="<?php echo lang("form-date-format", "mm/dd/yyyy"); ?>" name="service_date">
              </div>
              <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 booking-date-right">
                <label><?php echo lang('time', 'Time'); ?>:</label>
                <div class = "service-time">
                  <select id = "arrival-hour" name="arrival-hour" class="sm-font-select">
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select>
                  <select id = "arrival-mins" name="arrival-mins" class="sm-font-select">
                    <option value="00">00</option>
                    <option value="15">15</option>
                    <option value="30">30</option>
                    <option value="45">45</option>
                  </select>
                  <select id = "AM-PM" name="AM-PM" class="sm-font-select">
                    <option value="AM"><?php echo lang('am', 'AM'); ?></option>
                    <option value="PM"><?php echo lang('pm', 'PM'); ?></option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row marginBtm10 txtC">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <img class="booking-nla" src="<?php  echo template_image_src('nla-logo.jpg', $is_preview, "responsive")?>">
                <img class="booking-nla" src="<?php  echo template_image_src('GBTA_logo.jpg', $is_preview, "responsive")?>">
              </div>
            </div>
            <div class="txtC">
              <a href="<?php echo template_link($site_id, 'online-booking', $is_preview); ?>" class="page-block-book-now-button"><?php echo lang('home-form-book-now', 'ready to book? book now'); ?></a>
            </div>
            <input id="booking_country" type="hidden" name="booking_country" value="">
            <input id="booking_state" type="hidden" name="booking_state" value="">
            <input id="booking_city" type="hidden" name="booking_city" value="">
          </div>
        <div id="btn-get-quote-desk">
            <input type="submit" id="btn-get-quote-desk" class="page-block-button" value="<?php echo lang('get-quote-now', 'Get quote now'); ?>">
        </div>
        </form>
        <button id="btn-get-quote-mobile" class="page-block-button"><?php echo lang('get-quote-now', 'Get quote now'); ?></button>
    </div>
  </div>

  <div class="home-right col-lg-9 col-md-9 hidden-sm hidden-xs">
    <div class="page-block homepage-main jcarousel-wrapper">
  	  <div class='jcarousel'>
        <ul>
          <?php foreach ($slideshow_images as $slideshow):?>
            <li><img src="<?php echo slideshow_image_src($slideshow -> filename, $is_preview); ?>" alt="#"  width="100%" height="450" /></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <p class="jcarousel-pagination"></p>
      <div class = "home-right-txt hidden-xs">
        <?php if(isset($carousel_title_1_box)) echo "<h1>".$carousel_title_1_box->content."</h1>"?>
        <?php if(isset($carousel_title_2_box)) echo "<h2>".$carousel_title_2_box->content."</h2>"?>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-9 col-md-9">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="page-block thin block-padded">
          <h2><?php echo lang('our_fleet_includes', 'Our fleet includes:'); ?></h2>
          <div class="page-block-content">
            <ul class="page-block-fleet-list">
              <?php
              if (isset($fleet_include_box)) {
                $counter = 0;
                foreach ($site_vehicles as $s_vehicle) {
                  if(!$s_vehicle -> homepage) continue;
                  $vehicle = $vehicles[$s_vehicle -> vehicle_id];
                  $page_url = template_car_page_link($site_id, $vehicle -> page_name, $vehicle -> page_name, $is_preview);
                  echo "<li class='page-block-car'><a href='" . $page_url . "'>";
                  echo "<img src=" . vehicle_image_single_thumb_src($vehicle -> thumb, $is_preview) . "alt='' />" . $vehicle -> short_name . "</a></li>";
                  $counter++;
                }
              } else {
                foreach (explode("\n", $defaults['fleet_include']) as $line) {
                  if (strlen(trim($line)) == 0)
                    continue;
                  echo "<li class='page-block-car'><a href='#'>$line</a></li>";
                }
              }
              ?>
            </ul>
            <div class="page-block-sub-content">
              <p> <strong><?php echo lang('our_fleet_includes_a_range_of_vehicles', 'Our fleet includes a wide range of vehicles, such as:'); ?> </strong></p>
              <p>
                <?php
                if (isset($fleet_include_box)) {
                  $lines = array();
                  foreach (explode("\n", $fleet_include_box->content) as $line) {
                    if (strlen(trim($line)) == 0) continue;
                    $lines[] = $line;
                  }
                  echo implode(", ", $lines);
                }else{
                  foreach (explode("\n", $defaults['fleet_include']) as $line) {
                    if (strlen(trim($line)) == 0)
                      continue;
                    echo $line;
                  }
                }
                ?>
                </p>
            </div>
          </div>
          <a href='<?php echo template_link($site_id, 'fleet', $is_preview); ?>' class="page-block-button"><?php echo lang("see_entire_fleet", "See Our Entire Fleet"); ?></a>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="page-block thin homepage-services-txt">
          <h2><?php if($site -> lang_id != 2) echo $site -> name; ?> <?php echo lang("provides", "provides:"); ?></h2>
          <div class="page-block-content">
            <?php
            if (isset($provided_services_box)) {

              foreach (explode("\n", $provided_services_box->content) as $line) {
                if (strlen(trim($line)) == 0)
                  continue;
                echo '<div class="marginBtm10">' . $line . "</div>";
              }
            } else {

              foreach (explode("\n", $defaults['provided_service']) as $line) {
                if (strlen(trim($line)) == 0)
                  continue;
                echo '<li>' . htmlentities($line) . '</li>';
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="page-block wide">
          <h2><?php echo lang("about", 'About'); ?> <?php if($site -> lang_id != 2) echo $site -> name; ?></h2>
          <div class="page-block-content">
            <div class="page-block-sub-content">
              <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                <?php echo $site->short_about; ?>
                </div>
              </div>
            </div>
            <?php if(isset($home_about_boxes) && count($home_about_boxes)): ?>
              <?php foreach($home_about_boxes as $about_box):?>
            <div class="page-block-sub-content">
              <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <img src="<?php echo textbox_image_src($about_box->image, $is_preview); ?>"/>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                  <?php echo $about_box -> content;?>
                </div>
              </div>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-md-3 sidebar">
    <?php echo $this->load->view('templates/responsive/inc/sidebar');?>
  </div>
</div>


<div class="modal fade" id="myModal-quote" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <span class="car-service-type"></span>
            <i class="fa fa-times fa-2x close" data-dismiss="modal" aria-hidden="true"></i>
        </div>
         <div class="modal-body"> 
            <div class="myModal-body">
            
            <?php foreach($vehicle_names as $vehicle_name):?>
            <div class="row car-class">
                <div class="col-md-6 col-xs-6">
                    <img src="/images/vehicles/thumbs/<?php echo $vehicle_name->b_img;?>" />
                </div>             
                <div class="col-md-6 col-xs-6 car-right">
                    <div class="car-name">
                    <?php echo $vehicle_name->class;?><br />(<?php echo $vehicle_name->pax;?>&nbsp;<i class="fa fa-male" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $vehicle_name->lug;?>&nbsp;<i class="fa fa-suitcase" aria-hidden="true"></i>)
                    </div>
                    <div class="car-price">
                        <span>Estimated Price</span><br />
                        <span class="car-price-1 hidden">From&nbsp;<?php echo $vehicle_name -> airport_rate;?> USD</span>
                        <span class="car-price-2 hidden"><span class="car-price-8"></span>
                            <span class="car-price-3 hidden"> <?php echo $vehicle_name -> hd_rate;?> </span>
                            <span class="car-price-4 hidden"> <?php echo $vehicle_name -> fd_rate;?> </span>
                            <span class="car-price-5 hidden"> <?php echo $vehicle_name -> hd_time;?> </span>
                            <span class="car-price-6 hidden"> <?php echo $vehicle_name -> fd_time;?> </span>
                            <span class="car-price-7 hidden"> <?php echo $vehicle_name -> hourly_rate;?> </span>
                        </span>
                    </div>
                </div>             
            </div>
            <?php endforeach; ?>
        </div>

         </div>
         <div class="modal-footer">
            <button class="button" id="btn-enquire-more">Make Enquiry</button>
            <button class="button" id="btn-book-now">Book&nbsp;&nbsp;now</button>
         </div>
      </div>
    </div>
</div>

<script type="text/javascript">
function get_price($s_duration, $hd_r,$fd_r,$hd_t,$fd_t,$hourly_r)
{
    var $price = '';
    var $price_arr = [];
     if($s_duration == 'on_request')
     {
         $price = "Please enquire"; 
     }else{
         var $du = parseInt($s_duration);
         var $half_t = parseInt($hd_t);
         var $full_t = parseInt($fd_t);
         if($du <= $half_t)
         {
             $price = $hd_r;
         }else if($du < $full_t){
             var $temp_price = parseInt($hd_r) + (($du - $half_t) * parseInt($hourly_r));
             if($temp_price < parseInt($fd_r))
             {
                 $price = $temp_price.toString();
             }else{
                 $price = $fd_r;
             }
         }else if($du >= $full_t){
             $price = parseInt($fd_r) + (($du-$full_t) * parseInt($hourly_r));
         }
    }
     return $price;
 }
$(function(){
        var $m_btn = $('#btn-get-quote-mobile');
        var $modal = $('#myModal-quote');
        $m_btn.on('click', function(){
            $modal.modal({backdrop: 'static'});

            var $market = $('input[name="market"]').val();
            var $link = $('input[name="book-link"]').val();
            var $service_type = $('select[name="service_required"]').val();
            var $s_duration = '';
            $link += '?service_required='+ $service_type.replace(/\s/g, '+');
            if ($service_type == "Airport Pickup")
            {
                var $s_from = $('select[name="airport_location"]').val();
                var $s_to = $('input[name="destination"]').val();
                $link += '&airport_location=';
                $link += $s_from.replace(/\s/g, '+');
                $('.car-price-1').removeClass('hidden');
                $('.car-price-2').addClass('hidden');
            }else if($service_type == "Hotel Pickup"){
                var $s_from = $('input[name="pickup_location"]').val();
                var $s_to = $('input[name="destination"]').val();
                $link += '&pickup_location=';
                $link += $s_from.replace(/\s/g, '+');
                $('.car-price-1').removeClass('hidden');
                $('.car-price-2').addClass('hidden');
            }else if ($service_type == "Half/Full Day Hire") {
                $('.car-price-1').addClass('hidden');
                $('.car-price-2').removeClass('hidden');
                $s_duration = $('select[name="duration"]').val();
                var $s_to = $('textarea[name="itinerary"]').val();
                $('.car-price-2').each(function(){
                    $s_hd_rate = $(this).children('span.car-price-3').text();
                    $s_fd_rate = $(this).children('span.car-price-4').text();
                    $s_hd_time = $(this).children('span.car-price-5').text();
                    $s_fd_time = $(this).children('span.car-price-6').text();
                    $s_hourly_rate = $(this).children('span.car-price-7').text();
                    $price_hire = get_price($s_duration, $s_hd_rate,$s_fd_rate,$s_hd_time,$s_fd_time, $s_hourly_rate);
                    if($price_hire.toString() != 'Please enquire' && $price_hire.toString() != 'On Request')
                    {
                        $price_hire = 'From ' + $price_hire + ' USD';
                    }
                    $(this).children('span.car-price-8').html($price_hire);
                });
            }
            
            $('.car-service-type').html($service_type);

            var $s_date = $('#dp').val();
            var $s_hour = $('#arrival-hour').val();
            var $s_mins = $('#arrival-mins').val();
            var $s_am_pm = $('#AM-PM').val();
            var $s_country = $('#booking_country').val();
            var $s_state = $('#booking_state').val();
            var $s_city = $('#booking_city').val();

            $link += '&itinerary='+$s_to.replace(/\s/g, '+')+'&destination='+ $s_to.replace(/\s/g, '+') +'&market='+ $market +'&duration='+$s_duration+'&service_date='+ $s_date +'&arrival-hour='+ $s_hour +'&arrival-mins='+ $s_mins +'&AM-PM='+ $s_am_pm +'&booking_country='+ $s_country +'&booking_state='+ $s_state.replace(/\s/g, '+') +'&booking_city='+ $s_city.replace(/\s/g, '+');
            $('#btn-book-now').one('click', function(){
                //window.location.replace($link);
                window.open('/booking/'+$market+'/', '_blank');
            });
            $('#btn-enquire-more').one('click', function(){
                //window.location.replace($link);
                window.open($link, '_blank');
            });

        });

    $('.car-class').click(function(){
        $(this).children('div.car-right').addClass('car-clicked');
        $(this).siblings().children('div.car-right').removeClass('car-clicked');
    });

 //   $modal.on('show.bs.modal', function(){
 //     var $this = $(this);
 //     var $modal_dialog = $this.find('.modal-dialog');
 //     $this.css('display', 'block');
 //     $modal_dialog.css({'margin-top': Math.max(0, ($(window).height() - $modal_dialog.height()) / 2 ) });
 //   });

    $modal.on('hidden.bs.modal', function(){
        $('.car-right').removeClass('car-clicked');
        $('.car-price-1').addClass('hidden');
        $('.car-price-2').addClass('hidden');
        $('.car-price-8').html('');
    });

});

function initAutocomplete() {
    var s_input_pickup = document.getElementById('autocomplete-pickup');
    var s_input_dropoff = document.getElementById('autocomplete-dropoff');
  //  var autocomplete_p = new google.maps.places.Autocomplete(s_input_pickup);
 //   var autocomplete_d = new google.maps.places.Autocomplete(s_input_dropoff);
    var searchBox_p = new google.maps.places.SearchBox(s_input_pickup);
    var searchBox_d = new google.maps.places.SearchBox(s_input_dropoff);
}
</script>
