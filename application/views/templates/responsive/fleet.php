<?php
$counter = 0;
$tabCounter = 0;
if(count($child_site_vehicles)){
  $str =  "<div class='child_tab_selector_container'>";
  $str = $str."<ul>";
  foreach($child_site_vehicles as $key => $boxesCollection){
    if($tabCounter == 0){
       $str = $str."<li><a id='$key' class='child_tab_selector active_child page-child-button'>".ucwords($key)."</a></li>";
    }else{
       $str = $str."<li><a id='$key' class='child_tab_selector page-child-button'>".ucwords($key)."</a></li>";
    }
    $tabCounter++;
  }
  $str = $str."</ul></div>";
  echo $str;
  $tabCounter = 0;

  foreach($child_site_vehicles as $key => $boxesCollection){

?>
    <div class='child_tab <?php echo $key; if($tabCounter != 0){echo ' display_none';} ?>'>
      <div class="page-block page-block-first thin">
        <h2> <?php echo lang("professional-chauffuers-quality-vehicles", 'professional chauffuers and quality vehicles'); ?></h2>
        <div class="page-block-content">
          <div class="page-block-sub-content">
            <div class="row marginBtm10 bold-txt">
              <div class="col-lg-12 col-md-12 col-xs-12">
        <?php echo $childBoxes[$key] ? $childBoxes[$key]->content : '';?>
              </div>
            </div>
          </div>

<?php foreach ($child_site_vehicles[$key] as $s_vehicle):?>
<?php
  $vehicle = $child_vehicles[$key];
  $vehicle = $vehicle[$s_vehicle->vehicle_id];
  //$page_url = template_car_page_link($child_sites[$key]->id, $vehicle->page_name, $vehicle->page_name, false );
  //$page_url = $child_sites[$key] -> url."/".$page_url;
  if($is_preview){
    $page_url = "/index.php/admin/preview?id=113&section=rates";
    $vehicle_img = vehicle_image_single_thumb_src($vehicle->thumb, $is_preview);
  }else{
    $page_url = "/rates.html";
    $vehicle_img = $child_sites[$key] -> url.vehicle_image_single_thumb_src($vehicle->thumb, $is_preview);
  }


?>
<div class="page-block-sub-content">
      <div class="row">
        <div class="col-lg-5 col-md-6">
          <div class="vertical-align">
            <a href="<?php echo template_link($site_id, 'rates',  $is_preview);?>"><img class="fleet-img" src="<?php echo $vehicle_img;?>"></a>
          </div>
        </div>
        <div class="col-lg-7 col-md-6">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
              <h4><?php echo strtoupper($vehicle->short_name);?></h4>
            </div>
          </div>
          <div class="row marginBtm10">
            <div class="col-lg-12 col-md-12 col-xs-12">
              <?php echo $vehicle->description;?>
            </div>
          </div>
          <div class="row marginBtm10">
            <div class="col-lg-12 col-md-12">
              <?php if($vehicle->passengers > 5):?>
                <?php echo $vehicle->passengers;?> X <i class="fa fa-male fa-3x"></i>
              <?php else:?>
                <?php for ($i=0; $i<$vehicle->passengers; $i++):?>
                  <i class="fa fa-male fa-3x marginRt5"></i>
                <?php endfor; ?>
              <?php endif; ?>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <?php if($vehicle->luggage > 5):?>
                <?php echo $vehicle->luggage?> X <i class="fa fa-suitcase fa-2x"></i>
              <?php else:?>
                <?php for ($i=0; $i<$vehicle->luggage; $i++):?>
                <i class="fa fa-suitcase fa-2x marginRt5"></i>
                <?php endfor; ?>
              <?php endif; ?>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <a class="page-block-innerbutton fleet" href="<?php echo template_link($site_id, 'rates',  $is_preview);?>"><?php echo lang("click-to-find-rates", 'Click to see our rates'); ?></a>
            </div>
          </div>
      </div>
    </div>
  </div>

      <?php endforeach;?>
      </div>
    </div>
  </div>
<?php
    $tabCounter++;
  }

}else{
?>
<div class="page-block page-block-first thin">
  <h2> <?php echo lang("professional-chauffuers-quality-vehicles", 'professional chauffuers and quality vehicles'); ?></h2>
  <div class="page-block-content">
    <div class="page-block-sub-content">
      <div class="row marginBtm10 bold-txt">
        <div class="col-lg-12 col-md-12 col-xs-12">
      <?php echo $fleet_box ? $fleet_box->content : '';?>
        </div>
      </div>
    </div>
    <?php foreach ($site_vehicles as $s_vehicle):?>
    <?php
      $vehicle = $vehicles[$s_vehicle->vehicle_id];
      $page_url = template_car_page_link($site_id, $vehicle->page_name, $vehicle->page_name, $is_preview );
    ?>
    <div class="page-block-sub-content">
      <div class="row">
        <div class="col-lg-5 col-md-7">
          <div class="vertical-align">
            <a href="<?php echo $page_url;?>"><img class="fleet-img" src="<?php echo vehicle_image_single_thumb_src($vehicle->thumb, $is_preview);?>"></a>
          </div>
        </div>
          <div class="col-lg-7">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-xs-12">
                <h3><?php echo strtoupper($vehicle->short_name);?></h3>
              </div>
            </div>
            <div class="row marginBtm10">
              <div class="col-lg-12 col-md-12 col-xs-12">
                <?php echo $vehicle->description;?>
              </div>
            </div>
            <div class="row marginBtm10">
              <div class="col-lg-12">
                <?php if($vehicle->passengers > 5):?>
                  <?php echo $vehicle->passengers;?> X <i class="fa fa-male fa-3x"></i>
                <?php else:?>
                  <?php for ($i=0; $i<$vehicle->passengers; $i++):?>
                    <i class="fa fa-male fa-3x marginRt5"></i>
                  <?php endfor; ?>
                <?php endif; ?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <?php if($vehicle->luggage > 5):?>
                  <?php echo $vehicle->luggage?> X <i class="fa fa-suitcase fa-2x"></i>
                <?php else:?>
                  <?php for ($i=0; $i<$vehicle->luggage; $i++):?>
                  <i class="fa fa-suitcase fa-2x marginRt5"></i>
                  <?php endfor; ?>
                <?php endif; ?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <a class="page-block-innerbutton fleet" href="<?php echo $page_url;?>"><?php echo lang("click-to-find-more", 'Click to find out more about the'); ?> <?php echo $vehicle->short_name;?></a>
              </div>
            </div>
        </div>
      </div>
    </div>
    <?php endforeach;?>
  </div>
</div>

<?php }?>
