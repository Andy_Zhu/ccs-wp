<?php if(isset($page_site_vehicle) && isset($page_vehicle)):?>
	<div class="page-block page-block-first thin">
    <h2> The Popular <?php echo $page_vehicle->long_name; ?> </h2>
    <div class="page-block-content">
      <div class="page-block-sub-content">
        <p class="marginBtm20"><strong><?php echo $page_vehicle->description;?></strong></p>
        <?php if (!empty($page_vehicle_images)):?>
        <div class="row">
          <?php $counter = 1;?>
          <?php foreach ($page_vehicle_images as $v_image):?>
          <div class="col-lg-12 col-md-12 img-<?php echo $counter; if($counter == 1){ echo " active";} $counter ++; ?> img-big ">
            <img src='<?php echo vehicle_image_src($v_image->filename, $is_preview);?>' class="slideshow-image" alt="" width="600" height="400" />
          </div>
          <?php endforeach;?>
        </div>
        <div class="row vehicle-prev-list">
          <ul class="col-lg-9 col-md-9 col-xs-12">
            <?php $counter = 1;?>
            <?php foreach ($page_vehicle_images as $v_image):?>
            <li class="img-prev" id="img-<?php echo $counter; $counter ++; ?>">
              <img src='<?php echo vehicle_image_thumb_src($v_image->filename, $is_preview);?>' class="slideshow-thumb" alt="" />
            </li>
            <?php endforeach;?>
          </ul>
          <div class="col-lg-3 col-md-3 col-xs-12 txtR">
            <div class="row paddingTop10">
              <div class="col-lg-12">
                <?php if($page_vehicle->passengers > 5):?>
                    <?php echo $page_vehicle->passengers;?> X <i class="fa fa-male fa-3x"></i>
                <?php else:?>
                  <?php for ($i=0; $i<$page_vehicle -> passengers; $i++):?>
                    <i class="fa fa-male fa-3x marginRt5"></i>
                  <?php endfor; ?>
                <?php endif; ?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <?php if($page_vehicle->luggage > 5):?>
                    <?php echo $page_vehicle->luggage?> X <i class="fa fa-suitcase fa-2x"></i>
                <?php else:?>
                  <?php for ($i=0; $i<$page_vehicle -> luggage; $i++):?>
                    <i class="fa fa-suitcase fa-2x marginRt5"></i>
                  <?php endfor; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        <? endif; ?>
        <div class="row">
          <div class = "col-lg-12">
            <h3 class="vehicle-rates"> <?php echo $page_vehicle->long_name; ?> <?php echo lang("rates", 'Rates'); ?> </h3>
          </div>
        </div>
        <div class="row txtC marginTop20 marginBtm20 fleet-rate">
          <div class="col-lg-12">                 <!--will need to alter this once data structures of CMS understood-->
            <div class="row">
              <div class="col-lg-6 co-md-6 col-xs-6 right-border">
                <?php echo lang('airport-transfer','Airport Transfer'); ?>
              </div>
              <div class="col-lg-6 co-md-6 col-xs-6">
                $<?php echo $page_site_vehicle->airport_transfer_price;?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 co-md-6 col-xs-6 right-border">
                <?php echo lang('half-day','Half Day').' ('.$page_site_vehicle->half_day_hour." ".lang('hours','Hours').')'; ?>
              </div>
              <div class="col-lg-6 co-md-6 col-xs-6">
                $<?php echo $page_site_vehicle->half_day_price;?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 co-md-6 col-xs-6 right-border">
                <?php echo lang('full-day','Full Day').' ('.$page_site_vehicle->full_day_hour." ".lang('hours','Hours').')'; ?>
              </div>
              <div class="col-lg-6 co-md-6 col-xs-6">
                $<?php echo $page_site_vehicle->full_day_price;?>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 co-md-6 col-xs-6 right-border">
                <?php echo lang('extra','Extra').' '.lang('hours-plain','Hours'); ?>
              </div>
              <div class="col-lg-6 co-md-6 col-xs-6">
                $<?php echo $page_site_vehicle->extra_hours_price;?> / <?php echo lang('hours-plain','Hours'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <?php echo $page_site_vehicle->other_info;?>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <a class="page-block-innerbutton" href="<?php echo template_link($site_id, 'online-booking',$is_preview);?>"><?php echo lang('enquire-now', 'Enquire now'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php if (!empty($page_vehicle_images)):?>

<script type="text/javascript">
	$(document).ready(function(){
  $(".img-prev").click(function(){
    imgid = $(this).attr("id");
    console.log(imgid);
    $(".img-big").hide();
    $(".img-big").removeClass('active');
    $("."+imgid).addClass('active');
    $("."+imgid).fadeToggle('slow');

  });
});
</script>
  <?php endif;?>
<?php else:?>
	Missing data!!!
<?php endif;?>