<div>
	<?php if(empty($tours)):?>

		<h1><?php echo lang("comming-soon", 'Comming soon.'); ?></h1>

	<?php else:?>

		<?php $counter=1; foreach ($tours as $tour_id => $tour):?>
	 <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="page-block wide page-block-first tour-block">
          <h2><?php echo htmlentities($tour->title);?></h2>
          <div class="page-block-content">
            <?php echo $tour->intro;?>
            <?php if(array_key_exists($tour_id, $tours_boxes)):?>
              <?php foreach ($tours_boxes[$tour->id] as $box):?>
                <div class="page-block-sub-content">
                  <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <?php if(!empty($box->image) && file_exists(textbox_image_path($box->image))):?>
                        <img src="<?php echo textbox_image_src($box->image, $is_preview);?>" alt="" />
                      <?php endif;?>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                      <?php echo $box->content; ?>
                    </div>
                  </div>
                </div>
              <?php endforeach;?>
            <?php endif;?>
            <div class="page-block-sub-content">
              <div class="row">
                <div class="col-lg-12 col-md-12">
                  <?php echo $tour->extra;?>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <a class="page-block-innerbutton" href="<?php echo template_link($site_id, 'online-booking',$is_preview);?>">Enquire now</a>
                </div>
              </div>
            </div>
          </div>
       </div>
     </div>
   </div>
		<?php endforeach;?>
	<?php endif;?>
</div>




