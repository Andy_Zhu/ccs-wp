$(document).ready(function(){

/* This brings up calendar when clicking in the textfield and sets date-limits*/
	var datepicker_box = $('#dp').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '+0d',
        todayHighlight:true
    });
	$(".datepicker-field").click(function(){
		datepicker_box.datepicker("show");
	});

/* This hides the calendar once a date has been selected */
    $('#dp').datepicker().on('changeDate', function(ev){
    $(this).datepicker('hide');
    });

 $("select[name='service_required']").change(function(){
      var type = $(this).val();
      if(type == "Airport Pickup"){
        $('.hideForDayhire').show();
        $('.showForDayhire').hide();
        $("input[name='pickup_location']").val("");
        $("input[name='pickup_location']").hide();
        $("select[name='airport_location']").show();
      }else if(type == "Hotel Pickup"){
        $('.hideForDayhire').show();
        $('.showForDayhire').hide();
        $("select[name='airport_location']").val("");
        $("select[name='airport_location']").hide();
        $("input[name='pickup_location']").show();
      }else if(type == "Half/Full Day Hire"){
        $("select[name='airport_location']").val("");
        $("input[name='pickup_location']").val("");
        $('.hideForDayhire').hide();
        $('.showForDayhire').show();
      }
  });

  if($("input[name='booking_country']").length){
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function(){
      if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
        var location = (JSON.parse(xmlhttp.responseText));
        document.getElementById("booking_country").value = location.country;
        document.getElementById("booking_state").value =  location.region;
        document.getElementById("booking_city").value = location.city;
      }
    }
    xmlhttp.open("GET", "http://ipinfo.io/json", false);
    xmlhttp.send();
  }

});
