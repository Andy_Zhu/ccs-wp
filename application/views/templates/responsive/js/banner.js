jQuery(document).ready(function($){

	var bannerWidth = $('#banner').width();
	var bannerHeight = $('#banner').height();

	$('#banner').css({'bottom':-bannerHeight - 100, visibility:'visible'}).delay( 3000 ).animate({bottom:0}, 'slow');

	$('#banner-close-btn').click(function(evt){
		evt.preventDefault();
		$('#banner').fadeOut('fast', function(){$(this).remove();});
	});
});

