/**
 * @author h2o
 */

jQuery(document).ready(function($){
  $('.child_tab_selector').click(function(){
    selector = $(this).attr('id');
    $('.child_tab_selector').removeClass('active_child');
    $('#'+selector).addClass('active_child');
    $('.child_tab').fadeOut();
    $('.'+selector).fadeIn();
  });

});