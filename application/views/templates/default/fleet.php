<?php
$counter = 0;
$tabCounter = 0;
if(count($child_site_vehicles)){
  $str =  "<div class='child_tab_selector_container'>";
  $str = $str."<ul>";
  foreach($child_site_vehicles as $key => $boxesCollection){
    if($tabCounter == 0){
       $str = $str."<li><a id='$key' class='child_tab_selector active_child'>".ucwords($key)."</a></li>";
    }else{
       $str = $str."<li><a id='$key' class='child_tab_selector'>".ucwords($key)."</a></li>";
    }
    $tabCounter++;
  }
  $str = $str."</ul></div>";
  echo $str;
  $tabCounter = 0;

  foreach($child_site_vehicles as $key => $boxesCollection){

?>
    <div class='child_tab <?php echo $key; if($tabCounter != 0){echo ' display_none';} ?>'>
      <div class="fleet-block">
        <?php echo $childBoxes[$key] ? $childBoxes[$key]->content : '';?>
      </div>
      <div class="fleet-holder">

<?php foreach ($child_site_vehicles[$key] as $s_vehicle):?>
<?php
  $vehicle = $child_vehicles[$key];
  $vehicle = $vehicle[$s_vehicle->vehicle_id];
  //$page_url = template_car_page_link($child_sites[$key]->id, $vehicle->page_name, $vehicle->page_name, false );
  //$page_url = $child_sites[$key] -> url."/".$page_url;
  if($is_preview){
    $page_url = "/index.php/admin/preview?id=113&section=rates";
    $vehicle_img = vehicle_image_single_thumb_src($vehicle->thumb, $is_preview);
  }else{
    $page_url = "/rates.html";
    $vehicle_img = $child_sites[$key] -> url.vehicle_image_single_thumb_src($vehicle->thumb, $is_preview);
  }


?>
        <div class="block">
          <div class="img"><a href="<?php echo $page_url;?>">

          <img src="<?php echo $vehicle_img; ?>" alt="" /></a></div>

          <div class="txt">
            <a href="<?php echo $page_url;?>"><h2><?php echo $vehicle->long_name;?></h2></a>
            <p>
              <?php echo $vehicle->description;?>
            </p>

            <?php if($vehicle->passengers > 5):?>
              <?php echo $vehicle->passengers;?> X <img src='<?php echo template_image_src('passenger.gif', $is_preview)?>' alt="" />
            <?php else:?>
              <?php for($i=0; $i<$vehicle->passengers; $i++):?>
              <img src='<?php echo template_image_src('passenger.gif', $is_preview)?>' alt="" />
              <?php endfor;?>
            <?php endif?>
            <br />

            <?php if($vehicle->luggage > 5):?>
              <?php echo $vehicle->luggage?> X <img src='<?php echo template_image_src('bag.gif', $is_preview);?>' style='padding-right:1px;' alt="" />
            <?php else:?>
              <?php for($i=0; $i<$vehicle->luggage; $i++):?>
                <img src='<?php echo template_image_src('bag.gif', $is_preview);?>' style='padding-right:1px;' alt="" />
              <?php endfor;?>
            <?php endif;?>
            <br />

            <span class="btn"><a href="<?php echo $page_url;?>"><?php echo lang("click-to-find-rates", 'Click to see our rates'); ?></a></span>

          </div>
        </div>

      <?php endforeach;?>
      </div>
    </div>
<?php
    $tabCounter++;
  }

}else{
?>

<div class="fleet-block">
	<?php echo $fleet_box ? $fleet_box->content : '';?>
</div>
<div class="fleet-holder">

<?php foreach ($site_vehicles as $s_vehicle):?>
<?php
	$vehicle = $vehicles[$s_vehicle->vehicle_id];
	$page_url = template_car_page_link($site_id, $vehicle->page_name, $vehicle->page_name, $is_preview );
?>

<div class="block">
	<div class="img"><a href="<?php echo $page_url;?>">

	<img src="<?php echo vehicle_image_single_thumb_src($vehicle->thumb, $is_preview);?>" alt="" /></a></div>

	<div class="txt">
		<a href="<?php echo $page_url;?>"><h2><?php echo $vehicle->long_name;?></h2></a>
		<p>
			<?php echo $vehicle->description;?>
		</p>

		<?php if($vehicle->passengers > 5):?>
			<?php echo $vehicle->passengers;?> X <img src='<?php echo template_image_src('passenger.gif', $is_preview)?>' alt="" />
		<?php else:?>
			<?php for($i=0; $i<$vehicle->passengers; $i++):?>
			<img src='<?php echo template_image_src('passenger.gif', $is_preview)?>' alt="" />
			<?php endfor;?>
		<?php endif?>
		<br />

		<?php if($vehicle->luggage > 5):?>
			<?php echo $vehicle->luggage?> X <img src='<?php echo template_image_src('bag.gif', $is_preview);?>' style='padding-right:1px;' alt="" />
		<?php else:?>
			<?php for($i=0; $i<$vehicle->luggage; $i++):?>
				<img src='<?php echo template_image_src('bag.gif', $is_preview);?>' style='padding-right:1px;' alt="" />
			<?php endfor;?>
		<?php endif;?>
		<br />

		<span class="btn"><a href="<?php echo $page_url;?>"><?php echo lang("click-to-find-more", 'Click to find out more about the'); ?> <?php echo $vehicle->short_name;?></a></span>

	</div>
</div>

<?php endforeach;?>
</div>

<?php }?>
