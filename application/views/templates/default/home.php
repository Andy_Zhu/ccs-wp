<?php
$defaults['provided_service'] = "Airport pickup - From Beijing and Tianjin International Airports\n
Cruise ship transfers - Cruise ship terminal to Beijing city or airport.\n
Airport VIP meet and greet service - fast-track immigration and customs.\n
Professional translator / interpreter services\n
English speaking guides - Ideal for shopping, touring, dining, and experiencing the culture of region.\n
Escorted tours - visit the many famous historical sites of the region
";

$defaults['fleet_include'] = "Luxury limousine saloons,\n
Business and intermediate saloons\n
MPV's (multipurpose vehicles) and\n
Minivans and buses\n
";
?>
<?php foreach ($home_boxes as $box):?>
	<?php
		if($box->title == 'fleet include') {
			$fleet_include_box = $box;
		}else if($box->title == 'provided services') {
			$provided_services_box = $box;
		}
	?>
<?php endforeach;?>

<script type="text/javascript">

function theRotator() {
	//Set the opacity of all images to 0
	jQuery('div.rotator ul li').css({opacity: 0.0});

	//Get the first image and display it (gets set to full opacity)
	jQuery('div.rotator ul li:first').css({opacity: 1.0});

	//Call the rotator function to run the slideshow, 6000 = change to next image after 6 seconds

	setInterval('rotate()',6000);

}

function rotate() {
	//Get the first image
	var current = (jQuery('div.rotator ul li.show')?  jQuery('div.rotator ul li.show') : jQuery('div.rotator ul li:first'));

    if ( current.length == 0 ) current = jQuery('div.rotator ul li:first');

	//Get next image, when it reaches the end, rotate it back to the first image
	var next = ((current.next().length) ? ((current.next().hasClass('show')) ? jQuery('div.rotator ul li:first') :current.next()) : jQuery('div.rotator ul li:first'));

	//Un-comment the 3 lines below to get the images in random order

	//var sibs = current.siblings();
        //var rndNum = Math.floor(Math.random() * sibs.length );
        //var next = jQuery( sibs[ rndNum ] );


	//Set the fade in effect for the next image, the show class has higher z-index
	next.css({opacity: 0.0})
	.addClass('show')
	.animate({opacity: 1.0}, 1000);

	//Hide the current image
	current.animate({opacity: 0.0}, 1000)
	.removeClass('show');

};


<?php if(count($slideshow_images) > 1):?>
jQuery(document).ready(function() {
	//Load the slideshow
	theRotator();
	jQuery('div.rotator').fadeIn(1000);
    jQuery('div.rotator ul li').fadeIn(1000); // tweek for IE
});
<?php endif;?>

</script>
	<div class="rotator">
		<ul>
			<?php foreach ($slideshow_images as $slideshow):?>
			<li><img src="<?php echo slideshow_image_src($slideshow->filename, $is_preview);?>" alt="#" /></li>
			<?php endforeach;?>
		</ul>
	</div>

	<div class="two-boxes">
									<div class="two-boxes-out">
										<div class="two-boxes-in">
											<div class="box">
												<h2><?php echo lang('our_fleet_includes', 'Our fleet includes:'); ?></h2>
												<ul>
													<?php
														if(isset($fleet_include_box)) {
															foreach (explode("\n", $fleet_include_box->content) as $line) {
																if(strlen(trim($line)) == 0) continue;
																echo "<li>$line</li>";
															}
														}else{
															foreach (explode("\n", $defaults['fleet_include']) as $line) {
																if(strlen(trim($line)) == 0) continue;
																echo "<li>$line</li>";
															}
														}
													?>
												</ul>
												<div class="btn-holder">
													<a href="<?php echo template_link($site_id, 'fleet', $is_preview);?>"><span><?php echo lang("read_more_fleet", "Read More"); ?></span></a>
												</div>
											</div>
											<div class="box box2">
												<h2><?php echo $site->name;?> <?php echo lang("can_also_provide", "can also provide:"); ?></h2>
												<ul>
													<?php
														if(isset($provided_services_box)) {

															foreach (explode("\n", $provided_services_box->content) as $line) {
																if(strlen(trim($line)) == 0) continue;
																echo '<li>'.htmlentities($line).'</li>';
															}
														}else{

															foreach (explode("\n", $defaults['provided_service']) as $line) {
																if(strlen(trim($line)) == 0) continue;
																echo '<li>'.htmlentities($line).'</li>';
															}
														}
													?>
												</ul>
												<div class="btn-holder">
													<a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><span><?php echo lang("read-more", 'Read More'); ?></span></a>
												</div>
											</div>
										</div>
									</div>
								</div>



								<div class="txt-content">

								<h3><?php echo lang("about", 'About'); ?> <?php echo $site->name;?></h3>
									<div>
									<?php echo $site->short_about ? $site->short_about : 'Data Is Missing!';?>
									</div>
									<div class="btn-holder">
										<a href="<?php echo template_link($site_id, 'about-us',$is_preview);?>"><span><?php echo lang("read-more", 'Read More'); ?></span></a>
									</div>

								</div>

