<div id="footer">
	<span class="car">car</span>
	<?php echo $site->footer_content;?>
</div>

			</div>
		</div>
	</div>
</div>

<?php if($hasBanner):?>
  <?php
    $country = $site->country_id;
    if ($country == "9" || $country == "1") {
      $parent_market = "asia";
    } else {
      $parent_market = "china";
    }
      $contactus_url = "https://{$parent_market}carservice.com/booking/$site->shortname/contact";
  ?>
	<div id="banner" style="<?php echo $banner->styles;?> <?php echo $banner->position;?>: 0;">
		<a href="nowhere" id="banner-close-btn"><img alt="close" src="<?php echo template_image_src('banner-close-btn.png', $is_preview);?>" /></a>
		<div class="banner-left-half" style="position: relative; height: 120px; width: 220px">
		  <img src="<?php echo banner_image_src($banner->image);?>" style="height:100%; width: 100%;"/>
		</div>
		<div class="banner-right-half" style="position: relative; height: 120px;width: auto;">
		  <h3 style="text-transform: uppercase; text-align: center; font-size: 22px;"><?php echo $banner->heading;?></h3>
      <?php echo $banner->content;?>
      <a class="banner-btn" href="<?php echo $contactus_url;?>">Enquire now!</a>
    </div>
	</div>
<?php endif;?>

</div>
<?php
echo $site->g_analytics;
?>
</body>
</html>
