<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $site->name. " - " . ($meta_title ? $meta_title : ($page && $page->meta_title? $page->meta_title : $section));  ?></title>
	<meta name="description" content="<?php echo htmlentities($meta_description ? $meta_description : ($page ? $page->meta_description : $site->meta_desc)); ?>" />
	<meta name="keywords" content="<?php echo htmlentities($meta_keywords ? $meta_keywords : ( $page ? $page->meta_keywords : $site->meta_keywords)); ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo template_css_src('css3.css', $is_preview);?>" media="all" />
	<link rel="stylesheet" href="<?php echo template_css_src('style.css', $is_preview);?>" type="text/css" media="screen" />
	<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo template_css_src('contact-form.css', $is_preview);?>' type='text/css' media='all' />

	<script type='text/javascript' src='<?php echo template_js_src('js1.js', $is_preview);?>'></script>
	<script type='text/javascript' src='<?php echo template_js_src('jquery.1.6.js', $is_preview);?>'></script>
	<?php if($hasBanner && $banner):?>
		<script type='text/javascript' src='<?php echo template_js_src('banner.js', $is_preview);?>'></script>
	<?php endif;?>
	<script type='text/javascript' src='<?php echo template_js_src('tabs.js', $is_preview);?>'></script>
	<?php if($section == 'faqs'):?>

	<script type='text/javascript' src='<?php echo template_js_src('jquery.scrollTo.js', $is_preview);?>'></script>
		<script type='text/javascript'>
			jQuery(document).ready(function($){

				$('#content ul li a').click(function(evt){
					evt.preventDefault();
					$.scrollTo('a[name="' + $(this).attr('href').replace('#', '') +'"]', 'slow');
				});
			});
		</script>
	<?php endif;?>
	<style type="text/css">

		.header-area .logo{
			<?php if(!empty($site->logo)):?>
				background-image:url(<?php echo site_logo_src($site->logo,  $is_preview)?>);
			<?php else:?>
				background-image:none;
			<?php endif;?>
		}
	</style>
    <?php
	echo $site->livechat;
	?>
</head>
<body class="<?php echo $section?>">
<div class="wrapper">
	<div class="w1">
		<div class="w2">
			<div id="wrapper">
				<div id="header">
					<div class="header-area">
						<h1 class="logo"><a href="/"><?php echo $site->name?></a></h1>
						<div class="holder">
							<div class="nav">
								<ul>
									<?php //foreach ($other_sites as $os):?>
										<!-- <li>
											<a href="<?php //echo $os['website']->url; ?>">
												<img width=15 src="/<?php //echo $this->Languages_Model->get_small_flag_path($os['lang']->id); ?>">
											</a>
										</li> -->
									<?php //endforeach; ?>

                                    <li><a href="<?php echo template_link($site_id, 'policies', $is_preview);?>"><?php echo lang("policies", 'Policies'); ?></a></li>
									<li><a href="<?php echo template_link($site_id, 'faqs', $is_preview);?>"><?php echo lang("faqs", 'FAQs'); ?></a></li>
									<li><a href="<?php echo template_link($site, 'contact-us', $is_preview);?>"><?php echo lang("contact-us", 'Contact Us'); ?></a></li>
								</ul>

							</div>
							<div class="pay-block">
								<a href="#"><img src="<?php if ($site_id == 80|| $site_id == 83|| $site_id == 85 )  echo template_image_src('pay-icons1.png') ;
								else  echo template_image_src('pay-icons.png')?>" alt="#" /></a>
							</div>
						</div>
					</div>

					<ul id="menu-top-navigation" class="menu <?php if($has_tours) echo "tours-menu";?>">
					<li id="menu-item-329" class="<?php if($section== 'home') echo "current-menu-item ";?>menu-item menu-item-type-custom menu-item-object-custom menu-item-329"><a href="<?php echo template_link($site_id, 'home', $is_preview);?>"><span><?php echo lang("home", 'Home'); ?></span></a></li>
					<li id="menu-item-333" class="<?php if($section== 'about-us') echo "current-menu-item ";?>menu-item menu-item-type-post_type menu-item-object-page menu-item-333"><a href="<?php echo template_link($site_id, 'about-us',$is_preview);?>"><span><?php echo lang("about-us", 'About Us'); ?></span></a></li>
					<li id="menu-item-332" class="<?php if($section== 'fleet') echo "current-menu-item ";?>menu-item menu-item-type-post_type menu-item-object-page menu-item-332"><a href="<?php echo template_link($site_id, 'fleet', $is_preview);?>"><span><?php echo lang("fleet", 'Fleet'); ?></span></a></li>
					<li id="menu-item-335" class="<?php if($section== 'services') echo "current-menu-item ";?>menu-item menu-item-type-post_type menu-item-object-page  page_item page-item-10 current_page_item menu-item-335"><a href="<?php echo template_link($site_id, 'services',$is_preview);?>"><span><?php echo lang("services", 'Services'); ?></span></a></li>
					<li id="menu-item-334" class="<?php if($section== 'rates') echo "current-menu-item ";?>menu-item menu-item-type-post_type menu-item-object-page menu-item-334"><a href="<?php echo template_link($site_id, 'rates',  $is_preview);?>"><span><?php echo lang("rates", 'Rates'); ?></span></a></li>
					<?php if($has_tours):?>
						<li id="menu-item-392" class="has-submenu <?php if($section== 'tours' || $section=='subtours') echo "current-menu-item ";?>menu-item menu-item-type-custom menu-item-object-custom menu-item-391">
							<a href="<?php echo template_link($site_id, 'tours', $is_preview);?>"><span><?php echo lang("tours", 'Tours'); ?></span></a>
							<ul class="submenu">
								<?php foreach ($tours_categories as $tmp_tr_cat):?>
									<li><a href="<?php echo template_tour_category_page_link($site_id, 'subtours', $tmp_tr_cat, $is_preview)?>"><?php echo $tmp_tr_cat->title;?></a></li>
								<?php endforeach;?>
							</ul>
						</li>
					<?php endif;?>
					<li id="menu-item-330" class="<?php if($section== 'online-booking') echo "current-menu-item ";?>menu-item menu-item-type-post_type menu-item-object-page menu-item-330"><a href="<?php echo template_link($site_id, 'online-booking',$is_preview);?>"><span><?php echo lang("online-bookings", 'Online Bookings'); ?></span></a></li>
					<li id="menu-item-391" class="<?php if($section== 'contact-us') echo "current-menu-item ";?>menu-item menu-item-type-custom menu-item-object-custom menu-item-391"><a href="<?php echo template_link($site, 'contact-us', $is_preview);?>"><span><?php echo lang("contact-us", 'Contact Us'); ?></span></a></li>
					</ul>

			</div>

