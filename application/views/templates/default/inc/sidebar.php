<div id="sidebar" <?php if($section == 'contact-us') echo 'style="width: 328px; overflow:hidden; "';?>>

<?php if($section == 'contact-us'):?>

<div class="contact-column">
	<div class="box-out">
		<div class="box-in">
			<h3><?php echo $contact_box ? $contact_box->title: '';?></h3>
			<div class="frame">
				<?php echo $contact_box ? $contact_box->content: '';?>
			</div>
		</div>
    	<div class="UNL_image">
			<img src="images/UNL.png" alt="" />
		</div>
	</div>
</div>

<?php else:?>

<?php if(($section=='subtours' || $section=='tours')):?>
<?php if(false)://comment?>
<div class="tours-cat-navi-container">
	<h2><?php echo lang("explore-more-tours", 'Explore more tours!'); ?></h2>
	<ul class="tours-cats-navi">
		<li>&nbsp;</li>
		<?php foreach ($tours_categories as $tour_cat):?>
			<li><a href="<?php echo template_tour_category_page_link($site_id, $section, $tour_cat, $is_preview)?>"><?php echo htmlentities($tour_cat->title);?></a></li>
		<?php endforeach;?>
	</ul>
</div>

<?php endif;?>

<ul class="car-menu">
	<li>
		<ul>
		<?php foreach ($tours_categories as $tour_cat):?>
			<li class="car-menu-item">
				<a href="<?php echo template_tour_category_page_link($site_id, 'subtours', $tour_cat, $is_preview)?>">
				<?php if(strlen(trim($tour_cat->image))):?>
				<img style="margin-top:5px;width:65px;" src="<?php echo textbox_image_src($tour_cat->image, $is_preview);?>" class="icon" alt="" />
				<?php endif;?>
				<?php echo htmlentities($tour_cat->title);?>
				</a>
			</li>
		<?php endforeach;?>
		</ul>
	</li>
</ul>
<?php endif;?>

<?php if(isset($carpage)) :?>

<div style="text-align:left;width:250px;margin-bottom:30px;clear:both;">
	<?php if(strlen($site->online_book_url) != 0) :?>
		<a href="<?php echo $site->online_book_url;?>" style="display:block;clear:both;padding:10px;color:#fff;background:#333;border:4px solid #c30;text-align:center;font-weight:bold;width:240px;;"><?php echo lang("book-now", 'Book Now'); ?></a><br />
	<?php endif;?>

	<h1 style="padding-left:20px;font-size:30px;text-align:center"><?php echo $page_vehicle->long_name;?></h1><br />
	<img style="width:270px;" src="<?php echo vehicle_image_single_thumb_src($page_vehicle->thumb);?>" alt="<?php echo $page_vehicle->long_name;?>" />

	<?php if($page_vehicle->passengers > 5):?>
		<?php echo $page_vehicle->passengers;?> X <img src='<?php echo template_image_src('passenger.gif')?>' alt="" />
	<?php else:?>
		<?php for($i=0; $i<$page_vehicle->passengers; $i++):?>
			<img src='<?php echo template_image_src('passenger.gif')?>' alt="" />
		<?php endfor;?>
	<?php endif;?>
	<br />

	<?php if($page_vehicle->luggage > 5):?>
		<?php echo $page_vehicle->luggage;?> X <img src='<?php echo template_image_src('bag.gif');?>' style='padding-right:1px;' alt="" />
	<?php else:?>
		<?php for($i=0; $i<$page_vehicle->luggage; $i++):?>
			<img src='<?php echo template_image_src('bag.gif');?>' style='padding-right:1px;' alt="" />
		<?php endfor;?>
	<?php endif;?>
	<br />
</div>

<?php endif;?>

<?php if(!in_array($section, array('tours','subtours'))):?>
	<ul class="car-menu">
	<li>
		<ul>
		<?php
			$v_num = 0;
			foreach ($site_vehicles as $s_menu_v):

			$menu_v = $vehicles[$s_menu_v->vehicle_id];
			//$tmp_section = str_replace(' ', '-', trim($page_vehicle->short_name));

			if($v_num >= $site->sidebar_max_cars) break;
			$v_num++;
		?>
		<li class="car-menu-item">
			<a href="<?php echo template_car_page_link($site_id, $menu_v->page_name,$menu_v->page_name, $is_preview)?>">
				<?php if(strlen(trim($menu_v->icon))):?>
				<img src="<?php echo vehicle_image_single_thumb_src($menu_v->icon, $is_preview);?>" class="icon" alt="" />
				<?php endif;?>
			<?php echo $menu_v->short_name;?>
			</a>
		</li>
		<?php endforeach;?>
		</ul>
	</li>
	</ul>
<?php endif;?>
	<div class="menu-box">
	<div class="box-out">
	<div class="box-in">
		<ul id="menu-sidebar-menu-needed" class="menu">
			<li id="menu-item-395" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-395">
				<a href="<?php echo template_link($site_id, 'online-booking', $is_preview);?>"><?php echo lang("book-now", 'Book Now'); ?></a>
			</li>
		</ul>
	</div>
	</div>
	</div>

	<div class="box-contact">
									<div class="box-out">
										<div class="box-in">
											<h3><?php echo lang("contact-us", 'Contact Us'); ?></h3>
                                           <!-- <div style="width:200px; padding-left:17px;"><b style="color:#A40000;">Call us on Skype:</b>
  <?php if($site_id == 85||$site_id == 80||$site_id == 83 || $site_id ==79) echo '
                                            <script type="text/javascript" src="http://cdn.dev.skype.com/uri/skype-uri.js"></script>
<div id="SkypeButton_Call_asia_car_service_1" align="right">
  <script type="text/javascript">
    Skype.ui({
      "name": "call",
      "element": "SkypeButton_Call_asia_car_service_1",
      "participants": ["asia_car_service"],
      "imageSize": 20
    });
  </script>
</div> ' ;
else echo '<script type="text/javascript" src="http://cdn.dev.skype.com/uri/skype-uri.js"></script>
<div id="SkypeButton_Call_China_Car_Service_1">
  <script type="text/javascript">
    Skype.ui({
      "name": "call",
      "element": "SkypeButton_Call_China_Car_Service_1",
      "participants": ["China_Car_Service"],
      "imageSize": 20
    });
  </script>
</div>';?>
</div>

-->
											<div class="cell">
												<address style="color:black;">
													<b style="color:#A40000;"><?php echo lang("ph", 'Ph:'); ?></b><br/>
													<strong><span class="skype_pnh_print_container_1315551970"><?php echo $site->phone?></span><span class="skype_pnh_container" dir="ltr"><span class="skype_pnh_mark"></span></span></strong> <br/>

                          <?php if($site->skype != ""){ ?>
                            <br/>
                          <b style="color:#A40000;"> <?php  echo lang("skype_call", "Call us on Skype:"); ?> </b> <br/>
                          <span class="skype_pnh_print_container_1315551970"><strong><a href="skype:<?php echo $site-> skype;?>?call"><img style="width: 100%;"src="<?php echo template_image_src('skype.png')?>"></a></strong></span><span class="skype_pnh_container" dir="ltr"><span class="skype_pnh_mark"></span></span>
                          <p style="font-size: 11px;">Skype ID: <?php echo $site-> skype;?></p>
                          <br/>
													<?php } ?>
													<b style="color:#A40000;"><?php echo lang("office-hours", 'Office hours:'); ?></b> <br/>
														<strong><?php echo $site->opening_hours;?><br /><?php echo $site->timezone;?></strong>
													<br/>
													<a style="color:#A40000;" href="mailto:<?php echo $site->contact_form_email;?>"><?php echo lang("email-us", 'E-mail Us'); ?></a>


												</address>
											</div>
										</div>
									</div>
								</div>

<?php endif;?>
                                <div class="UNL_image">
									<img src="images/UNL.png" alt="" />
                                </div>


</div>
