<div class="main-frame">
<div class="main-area">
<div id="main">
	<?php if($section !== 'home'):?>
	<div class="universal-ttl">
		<h1>
		<?php
		if(isset($carpage)) {
			echo $page_vehicle->long_name;
		}else if($section=='subtours' && isset($tour_category)){
			echo $tour_category->title;
		}else{

	     		//TODO: Refactor into CMS/translations section
			$section_title =  trim(ucwords(strtolower(str_replace('-', ' ', $section))));

			if ($site->lang_id == 2){

			switch(strtolower($section_title)){

			case "home":
				echo "主页";
			break;

			case "about us":
				echo "公司简介";
			break;
			case "fleet":
				echo "车型展示";
			break;
			case "services":
				echo "服务范围";
			break;
			case "rates":
				echo "租车价格 ";
			break;
			case "online booking":
				echo "在线订车";
			break;
			case "contact":
				echo "联系我们";
			break;
			default:
				echo $section_title ;

			}


			}
else{

				echo $section_title ;
}


		}?>
		</h1>
	</div>
	<?php endif;?>
<div class="main-wrapper">
	<div id="content" <?php if($section == 'contact-us') echo 'style="width:601px;"';?>>
	<?php echo $this->load->view($page_view);?>
	</div>
	<?php echo $this->load->view('templates/default/inc/sidebar');?>
</div>
</div>
</div>

