<?php 
$block_css = 'about-block';

if($section == 'rates') {
	$block_css = 'rates-block';
	if($section == 'rates' && $box->ranking ==  -1) {
		$block_css .= ' rates-eng-driver-box'; //shade first column
	}
}

?>
<div class="<?php echo $block_css;?> <?php if( ($box_counter % 2 != 0) || $section=="rates") echo 'about-block2'; ?>">
  <div class="top">
<h2><?php echo $box->title;?></h2>
</div>
<div class="holder">
<div class="holder-in">
<?php echo $box->content;?>
</div>
</div>
</div>