<?php
$market = strtolower(trim(str_ireplace('Car Service', '', $website->name)));
$country = $site->country_id;
if ($country == "9" || $country == "1") {
    $parent_market = "asia";
} else {
    $parent_market = "china";
}

if ($is_preview) {
    header ("Location: http://testbookingforms.chinacarservice.com/booking/{$market}/contact?testing=1");
}