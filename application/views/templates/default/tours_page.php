<div>
	<?php if(empty($tours_categories)):?>
	
		<h1><?php echo lang("comming-soon", 'Comming soon.'); ?></h1>
	
	<?php else:?>
		
		<?php $counter=1; foreach ($tours_categories as $tour_id => $tour_cat):?>
			
			<div class="tour-category <?php if($counter++ >= count($tours_categories)) echo 'last '; if($counter%2 != 0) echo 'even'?>">
				<h2 class="title"><?php echo $tour_cat->title;?></h2>
				<div class="tour-left">
					<?php if(!empty($tour_cat->image) && file_exists(textbox_image_path($tour_cat->image))):?>
						<img class="cat-image" src="<?php echo textbox_image_src($tour_cat->image, $is_preview);?>" alt="" />
					<?php endif;?>
				</div>
				<div class="tour-right">
					<div class="intro">
						<?php echo $tour_cat->description;?>
					</div>
					<p>
						<a href="<?php echo template_tour_category_page_link($site_id, 'subtours', $tour_cat,$is_preview);?>" class="view-tours-btn"><?php echo lang("view-tours-this-category", 'View tours in this category'); ?> &rarr;</a><span class="clear"></span>
					</p>
				</div>
				<div style="clear:both;"></div>
			</div>
		<?php endforeach;?>
	<?php endif;?>
</div>

							
								

