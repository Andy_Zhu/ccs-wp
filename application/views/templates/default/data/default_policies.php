<h1 style="color:#CE1E0B;">Privacy Policy</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service does not sell, rent or disseminate your personal and financial information to any outside third parties.</p>
<br>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;">China Car Service Inc. reserves the right to use the information supplied to provide requested services and collect payment for services rendered in accordance with our rates, terms and policies.</p>

<br>

<h1 style="color:#CE1E0B;">Airport Arrival Transfer Wait Time</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service provides up to 30 minutes of free wait time (up to 45 minutes of free wait time on International Arrivals requiring Customs Clearance) on each Airport Arrival Transfer for you to deplane and pick-up your luggage. If you do not meet your Chauffeur or call us within 45 minutes (60 minutes for International. Arrivals requiring Customs Clearance) of your plane landing we will consider you a “no-show” and you will be charged in full for the reservation.</p>

<br><h1 style="color:#CE1E0B;">No-Show Policy</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>On all reservations you will be considered a no-show at 45 minutes past your scheduled pick-up time; if you have not met your Chauffeur or contact us by phone, you will be charged in full for the reservation. To avoid being charged as a no-show, do not leave your location without contacting China Car Service by phone.</p>
<br>

<h1 style="color:#CE1E0B;">Cancellation Policy</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>Cancellations are accepted by email (bookings@chinacarservice.com) and phone (<span class="skype_pnh_print_container_1316275487">+86-755-2594 1385</span><span tabindex="-1" dir="ltr" class="skype_pnh_container"><span class="skype_pnh_mark"></span></span>) only.</p>
<br>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;">Full reservation amount will be charged for cancellations made less than 12 hrs before scheduled pick up. 50% of the invoice value will be charged for cancellations made less than 24 hrs before scheduled pick up. Cancellations made in excess of 24 hours will not incur any charge.</p>
<br>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;">No shows will be charged the full reservation fare.</p>
<br>

<h1 style="color:#CE1E0B;">Change Policy</h1>
<br>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;">China Car Service requires that all changes be received by phone or email (changes cannot be made with drivers) at least 12 hours prior to pick-up. If, a change cannot be accommodated &amp; results in a cancellation you will be charged in full for the order.</p>
<br>

<h1 style="color:#CE1E0B;">Vehicle Request Policy</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service and Affiliates reserve the right to upgrade a vehicle request to accommodate a client’s reservation at no charge to the client if deemed necessary by China Car Service or its Affiliates.</p>
<br>

<h1 style="color:#CE1E0B;">Rates &amp; Billing:</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>All Rates are subject to audit after order completion; to include actual tolls, parking fees, additional. stops &amp; time allotted, etc. Extra hours are billed for the number of hours above that you ordered the vehicle in 1/2 hour increments. Minimum posted number of hours is only the minimum number of hours we will accept an order for not what you will be billed for.</p>
<br>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;">China Car Service rates are inclusive of taxes but not any gratuity. If you feel would like to provide a gratuity it is at your sole discretion.</p>
<br>

<h1 style="color:#CE1E0B;">Surcharges</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>All out of pocket charges, including, but not limited to: road tolls, parking, airport fees and carts will be billed at cost to the client.</p>
<br>

<h1 style="color:#CE1E0B;">Payment Methods</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service accepts Visa, MasterCard and American Express. All charges are billed to your credit card account. Payment via cash, wire transfer or PayPal is by arrangement. For payment in cash (RMB, HKD, USD and Euros only), prior notice is necessary. Change will be given in local currency only.</p>
<br>

<h1 style="color:#CE1E0B;">Affiliate Service Policy</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service utilizes, at its own discretion, Affiliates to provide Limousine and Ground Transportation services as requested by our clients.</p>
<br>

<h1 style="color:#CE1E0B;">Vehicle Images</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>Vehicle Images listed on the China Car Service Websites may differ from the actual vehicle; all China Car Service Affiliated vehicles are late model and impeccably maintained.</p>
<br>

<h1 style="color:#CE1E0B;">Lost or Damaged Items</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service and its Affiliates are not responsible for items that are left in the vehicle, lost or damaged. China Car Service reserves the right to charge a delivery fee for returning lost items if found.</p>
<br>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;">China Car Service Affiliates and their Chauffeurs will assist with luggage at a client’s request, but assume no liability for doing so.</p>
<br>

<h1 style="color:#CE1E0B;">Uncontrollable Acts, Acts of God and/or Acts of Mother Nature</h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service and its Affiliates are not responsible for acts of God, acts of Mother Nature and/or circumstances that are beyond our control; including, but not limited to traffic congestion, road closures, accidents, flight delays, weather delays etc.</p>											
								