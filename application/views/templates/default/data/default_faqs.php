<ul style="padding-bottom:3px;">
<li style="margin-top: 1em;"><a href="#1" style="color:#CE1E0B;">What happens if my plane is early or late arriving?</a></li>
<li style="margin-top: 1em;"><a href="#2" style="color:#CE1E0B;">How much time do I have to meet the Chauffeur on my Airport Transfer Reservation?</a></li>
<li style="margin-top: 1em;"><a href="#3" style="color:#CE1E0B;">Can I instruct the Chauffeur to take a scenic route to my hotel from the Airport?</a></li>
<li style="margin-top: 1em;"><a href="#4" style="color:#CE1E0B;">Where do I meet my Chauffeur for pick-up?</a></li>
<li style="margin-top: 1em;"><a href="#5" style="color:#CE1E0B;">What is your Cancellation Policy?</a></li>
<li style="margin-top: 1em;"><a href="#6" style="color:#CE1E0B;">What is your Change Policy?</a></li>
<li style="margin-top: 1em;"><a href="#7" style="color:#CE1E0B;">When is my credit card charged?</a></li>
<li style="margin-top: 1em;"><a href="#8" style="color:#CE1E0B;">Can I pay for the reservation in Cash instead of using the credit card that was provided when I made the reservation?</a></li>
<li style="margin-top: 1em;"><a href="#9" style="color:#CE1E0B;">Is it possible to place, change or cancel a reservation with my Chauffeur?</a></li>
<li style="margin-top: 1em;"><a href="#10" style="color:#CE1E0B;">How far in advance should I place my reservation?</a></li>
</ul>

<br><br>

<h1><a name="1" style="color:#CE1E0B;">What happens if my plane is early or late arriving?</a></h1>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>
China Car Service updates all flight arrival times and we arrive when your plane lands, no matter whether you arrive late or early provided we have a correct flight number.
</p>

<br><h1><a name="2" style="color:#CE1E0B;">How much time do I have to meet the Chauffeur on my Airport Transfer Reservation?</a></h1>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service provides 30 minutes (45 minutes for International Arrivals requiring customs clearance) of free wait time on each Airport Arrival Transfer for you to deplane and pick-up your luggage. If you do not meet your Chauffeur or call us within 45 minutes (60 minutes for Intl. Arrivals) of your plane landing we will consider you a “no-show” and you will be charged in full for the reservation.</p>

<br><h1><a name="3" style="color:#CE1E0B;">Can I instruct the Chauffeur to take a scenic route to my hotel from the Airport?</a></h1>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service Airport Transfer rates are based on a point to point transfer and your Chauffeur is instructed to use the most direct route for expediency. If you would prefer the Chauffeur use a specific route our hourly rates and minimum will apply, plus applicable fees.</p>

<br><h1><a name="4" style="color:#CE1E0B;">Where do I meet my Chauffeur for pick-up</a></h1>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>The chauffeur will meet you at the Arrival Hall with your name/company on a greeting sign. We will confirm all other pick-up locations at reservation placement.</p>

<br><h1><a name="5" style="color:#CE1E0B;">What is your Cancellation Policy?</a></h1>
<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>Cancellations are accepted by email (bookings@chinacarservice.com) and phone (<span class="skype_pnh_print_container_1316275487">+86-755-2594 1385</span><span tabindex="-1" dir="ltr" class="skype_pnh_container"><span class="skype_pnh_mark"></span></span>) only. Full reservation amount will be charged for cancellations made less than 12 hrs before scheduled pick up. 50% of the invoice value will be charged for cancellations made less than 24 hrs before scheduled pick up. Cancellations made in excess of 24 hours will not incur any charge. No shows will be charged the full reservation fare.</p>

<br><h1><a name="6" style="color:#CE1E0B;">What is your Change Policy?</a></h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service requires that all changes be received by phone or email (changes cannot be made with drivers) at least 12 hours prior to pick-up. If, a change cannot be accommodated &amp; results in a cancellation you will be charged in full for the order.</p>

<br><h1><a name="7" style="color:#CE1E0B;">When is my credit card charged?</a></h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>China Car Service will charge your credit card after the booking is complete based on the car type used, actual period of the booking, and any applicable extras (extra kms, tolls etc). Your statement will show the transaction description as China Car Service, Baltimore, MD.</p>

<br><h1><a name="8" style="color:#CE1E0B;">Can I pay for the reservation in Cash instead of using the credit card that was provided when I made the reservation?</a></h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>Yes! Payment can be also made in cash (RMB, HKD, USD and Euros only). Prior notice is requested if more than a small amount of change is needed. Change will be given in local currency only. We still require that you provide your credit card details to enable a booking. All Rates are subject to audit after order completion; to include actual tolls, parking fees, additional stops &amp; time allotted, etc. Extra hours are billed for the number of hours above that you ordered the vehicle in 1/2 hour increments. Minimum posted number of hours is only the minimum number of hours we will accept an order for not what you will be billed for. China Car Service rates are inclusive of taxes but not any gratuity. If you feel would like to provide a gratuity it is at your sole discretion.</p>

<br><h1><a name="9" style="color:#CE1E0B;">Is it possible to place, change or cancel a reservation with my Chauffeur?</a></h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>Cancellations and changes are accepted by email (bookings@chinacarservice.com) and phone (<span class="skype_pnh_print_container_1316275487">86 1363 266 7585</span><span tabindex="-1" dir="ltr" class="skype_pnh_container"><span class="skype_pnh_mark"></span></span>) only. If you want to modify your order after pick up have the driver/guide call the bookings office.</p>

<br><h1><a name="10" style="color:#CE1E0B;">How far in advance should I place my reservation?</a></h1>

<p style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height=19px;color:black;margin-top:3px;margin-bottom:3px;"><br>Availability changes daily and we suggest you place reservation as soon as you have firm travel dates and times. This is especially important if major trade shows are in town.</p>											
								