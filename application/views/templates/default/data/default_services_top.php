<ul class="cars-list">
	<li>
<div class="img"><img alt="" src="http://beijingcarservice.com/wp-content/themes/ccsnew/images/img02.jpg"></div>
<div class="txt">

<strong class="mark">Car and Driver:</strong> We provide clean new cars and vans, chauffeured by an English speaking driver. You can rent our cars and drivers by the half-day / day / week or month to take you where you need to go and return you to your hotel. Our drivers and vehicles are available 24 hours a day and 7 days a week, year round.

</div></li>
	<li>
<div class="img"><img alt="" src="http://beijingcarservice.com/wp-content/themes/ccsnew/images/img03.jpg"></div>
<div class="txt">

<strong class="mark">Airport Pick-Up:</strong> We can meet you at the arrival hall of Beijing or Tianjin international airports and transport you directly to your hotel. Our driver will meet you at the arrival hall and your surname will be prominently displayed on a card held by the driver.

</div></li>
	<li>
<div style="display: none;" class="img"><img alt="" src="http://beijingcarservice.com/wp-content/themes/ccsnew/images/img03.jpg"></div>
<div class="txt">

<strong class="mark">Cruise Ship Pick-Up:</strong> We can meet you at the Tianjin, Tanggu International Seaport’s arrival hall and transport you directly to Beijing.  If you are only in town for the day, a tour can be arranged to optimize your time in port

</div></li>
</ul>