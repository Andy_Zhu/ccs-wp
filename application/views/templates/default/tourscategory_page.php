<div>
	<?php if(empty($tours)):?>
	
		<h1><?php echo lang("comming-soon", 'Comming soon.'); ?></h1>
	
	<?php else:?>
		
		<?php $counter=1; foreach ($tours as $tour_id => $tour):?>
			
			<div class="tour <?php if($counter++ >= count($tours)) echo 'last '; if($counter%2 !=0) echo 'odd';?>">
				<h2 class="title"><?php echo htmlentities($tour->title);?></h2>
				<div class="intro">
					<?php echo $tour->intro;?>
				</div>
				<?php if(array_key_exists($tour_id, $tours_boxes)):?>
					<?php foreach ($tours_boxes[$tour->id] as $box):?>
						
						<div class="tour-box">
							<div>
							<?php if(!empty($box->image) && file_exists(textbox_image_path($box->image))):?>
							<img src="<?php echo textbox_image_src($box->image, $is_preview);?>" alt="" />
							<?php endif;?>
							<?php echo $box->content; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach;?>
				<?php endif;?>
				<div class="extra">
					<?php echo $tour->extra;?>
				</div>
				<div class="clear"></div>
			</div>
		<?php endforeach;?>
	<?php endif;?>
</div>

							
								

