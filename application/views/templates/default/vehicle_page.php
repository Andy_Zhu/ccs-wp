<div>
<?php if(isset($page_site_vehicle) && isset($page_vehicle)):?>
	<p style="font-size:16px;padding:0px 10px 10px;"><em>
		<?php echo $page_vehicle->description;?>
	</em></p>

	<br /><br />

	<table id="pricing" style="margin-bottom:10px;">
		<!-- header-->
		<thead>
			<?php if($site_id ==85) echo '
								<tr>
						<th scope="col">'.lang('half-day','Half Day').' <br />(4 '.lang('hours','Hours').')</th>
						<th scope="col" class="fullday">'.lang('full-day','Full Day').' <br />(10 '.lang('hours','Hours').')</th>
						<th scope="col">'.lang('extra','Extra').' <br />Hours</th>
					</tr> ';
				else echo '<tr>
						<th scope="col">'.lang('half-day','Half Day').' <br />(4 '.lang('hours','Hours').')</th>
						<th scope="col" class="fullday">'.lang('full-day','Full Day').' <br />(8 '.lang('hours','Hours').')</th>
						<th scope="col">'.lang('extra','Extra').' <br /></th>
					</tr> ';  ?>

				</thead>

				<tbody>
						<tr>

							<!--<td>$<?php echo number_format($page_site_vehicle->half_day_price, 2);?></td>
                            <td class="fullday">$<?php echo number_format($page_site_vehicle->full_day_price, 2);?></td>
							<td>$<?php echo number_format($page_site_vehicle->extra_hours_price, 2);?><br />/hour</td>-->


                         <td> $ <?php echo $page_site_vehicle->half_day_price;?></td>
							<td class="fullday">$<?php echo $page_site_vehicle->full_day_price;?></td>
							<td>$<?php echo $page_site_vehicle->extra_hours_price;?><br />/<?php echo  lang("vehicle-extra-hour",'hour');?></td>

						</tr>

				</tbody>



			</table>

			<br />
			<div style="padding:0px 10px 0px">
				<?php echo $page_site_vehicle->other_info;?>
      		</div>

			<br /><br />

			<?php if (!empty($page_vehicle_images)):?>
					<h2><?php echo lang("images-of-the", 'Images Of The'); ?> <?php echo $page_vehicle->short_name;?></h2>
					<div class="slideshow-images">
						<?php foreach ($page_vehicle_images as $v_image):?>
						<img src='<?php echo vehicle_image_src($v_image->filename, $is_preview);?>' class="slideshow-image" alt="" />
						<?php endforeach;?>
					</div>
					<div class="slideshow-thumbs">
					<?php foreach ($page_vehicle_images as $v_image):?>
						<img src='<?php echo vehicle_image_thumb_src($v_image->filename, $is_preview);?>' class="slideshow-thumb" alt="" />
					<?php endforeach;?>
					</div>

					<script type="text/javascript">
						var currentIndex = 0;
						var images, thumbs;
						var duration = 2000;

						function changeImage() {
							jQuery(images[currentIndex]).removeClass('current').fadeOut();
							jQuery(thumbs[currentIndex]).removeClass('current');

							currentIndex++;
							if(currentIndex >= jQuery('.slideshow-images img').length ) currentIndex = 0;

							jQuery(thumbs[currentIndex]).addClass('current');
							jQuery(images[currentIndex]).addClass('current').fadeIn();
							setTimeout(changeImage, duration);
						}

						jQuery(document).ready(function($){
							$('.slideshow-images img:not(:first)').hide();
							images = $('.slideshow-images img');
							thumbs = $('.slideshow-thumbs .slideshow-thumb');
							$(thumbs[currentIndex]).addClass('current').show();
							$(images[currentIndex]).addClass('current');

							setTimeout(changeImage, duration);
						});
					</script>
			<?php endif;?>
<?php else:?>
	Missing data!!!
<?php endif;?>
</div>
