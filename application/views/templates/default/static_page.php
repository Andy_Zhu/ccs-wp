<div class="about-block">
	<div class="top">
		<h2><?php echo $static_page ? $static_page->name : '';?></h2>
	</div>
	<div class="holder">
		<div class="holder-in">
			<?php echo $static_page ? $static_page->content: '';?>
		</div>
	</div>
</div>