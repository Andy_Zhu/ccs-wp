<?php if($section == 'online-booking' && count($boxes) > 0):?>
	<?php
	foreach ($boxes as $box) {
		if(strtolower($box->title) == 'online booking') {
			$online_booking_top = $box;
		}else{
			$online_booking_down = $box;
		}
	}
	?>
<?php endif;?>

<?php if($section == 'about-us' && count($boxes)>=2):?>

	<?php
		$testimonials_boxes = array();
		foreach ($boxes as $box) {
			if($box->type == 'testimonial') {
				$testimonials_boxes[] = $box;
			}else{
				$about_us_box = $box;
			}
		}
	?>

	<div class="about-block">
	  <div class="top">
	<h2><?php echo lang("about_us_2",'About us');?></h2>
	</div>
	<div class="holder">
	<div class="holder-in">
	<?php echo $about_us_box->content;?>
	</div>
	</div>
	</div>

	<div class="about-block about-block2">
		  <div class="top">
		<h2><?php echo lang("what-our-customers-say-about-us", 'What our customers say about us'); ?></h2>
		</div>
		<div class="holder">
		<div class="holder-in">

		<?php $counter=0; foreach ($testimonials_boxes as $testimonial_box):?>
			<div class="testimonial <?php if($counter++ % 2 == 0 ) echo 'testimonial-even';?>">

			<?php echo $testimonial_box->content;?>

			</div>
		<?php endforeach;?>

	</div>
		</div>
		</div>
<?php elseif($section == 'online-booking'):?>

	<div class="about-block">
	<div class="top">
	<h2><?php echo lang("online-booking", 'Online Booking'); ?></h2>
	</div>
	<div class="holder">
	<div class="holder-txt">

	<?php if(isset($online_booking_top)):?>
		<?php echo $online_booking_top->content;?>
	<?php else:?>

	Our on-line booking system processes your MasterCard, Visa or Amex Cards through Authorize.Net, an American credit card system. As with any transaction in the US or Europe your purchases are protected if you do not get what we promise! We also accept payment in cash, via wire transfer and PayPal.
	<div><img src="<?php echo template_image_src('pay-icons.png', $is_preview);?>" alt="#"></div>
	At the end of each day our driver will have you verify the days toll and parking receipts, the odometer readings (start and finish) and the pick up / drop off times on an invoice. We will process the invoice and charge your credit card as per your booking and email you a receipt.
	<?php endif;?>
	</div>

<?php
if($site-> lang_id == 2){
  $site->online_book_url  = str_replace(' ','', 'https://royal-asia-limo.com/booking/' . strtolower(str_replace(' Car Service', '', $site->name)) . '/');
}else{
  $site->online_book_url  = str_replace(' ','', 'https://' . strtolower($website_country->name) . 'carservice.com/booking/' . strtolower(str_replace(' Car Service', '', $site->name)) . '/');
}

$site->online_quote_url = str_replace(' ','', $site->online_book_url . 'quote/');
?>

	<div class="btns-holder"><span class="blue-btn"><a target="_blank" href="<?php echo $site->online_book_url;?>"><?php echo lang("secure_booking", "Make a Secure Online Booking Now"); ?></a></span><span class="orange-btn"><a target="_blank" href="<?php echo $site->online_quote_url;?>"><?php echo lang("request_quote", "Request a Quote or Information");?> </a></span></div>
	</div>
	</div>
	<div class="about-block">
	<div class="top">
	<h2><?php echo lang("cancellation-policy", 'Cancellation Policy'); ?></h2>
	</div>
	<div class="holder">
	<div class="holder-txt">

	<?php if(isset($online_booking_down)):?>
		<?php echo $online_booking_down->content;?>
	<?php else:?>
	<p>Cancellations are accepted by email (bookings@chinacarservice.com) and phone (86 755 2595 1800) only.   Urgent after hour changes can be made on (86 1363 266 7585).</p>

	<p>Full reservation amount will be charged for cancellations made less than 12 hrs before scheduled pick up.  50% of the invoice value will be charged for cancellation made less than 24 hrs before scheduled pick up. Cancellations made in excess of 24 hours will not incur any charge.</p>

	<p>No shows will be charged the full reservation fare.</p>
	<?php endif;?>
	</div>
	<div class="btns-holder"><span class="blue-btn"><a href="<?php echo template_link($site_id, 'policies', $is_preview)?>"><?php echo lang("policies", 'Policies'); ?></a></span><span class="orange-btn"><a href="<?php echo template_link($site_id, 'faqs', $is_preview)?>"><?php echo lang("faqs", 'FAQ’s'); ?></a></span></div>
	</div>
	</div>

<?php else:?>

	<?php
		if($section == 'services' && !empty($top_boxes)) {
			echo '<ul class="cars-list">';
			foreach ($top_boxes as $top_box) {
				echo '<li>';
				if($top_box->image) {
					echo '<div class="img"><img src="'.textbox_image_src($top_box->image, $is_preview).'" alt="" /></div>';
				}
				echo '<div class="txt">'.$top_box->content.'</div>';
				echo '</li>';
			}
			echo '</ul>';
		}

		$counter = 0;
    $tabCounter = 0;
		if($section == 'rates') {
			$english_speaking_rates = null;
			foreach ($boxes as $box) {
				if($box->ranking == '-1') {
					$english_speaking_rates = $box;
					break;
				}
			}
			if($english_speaking_rates) {
				$this->load->view('templates/default/inc/text_box', array('box'=>$english_speaking_rates, 'box_counter'=>$counter));
			}
      if(count($childBoxes)){
        $str =  "<div class='child_tab_selector_container'>";
        $str = $str."<ul>";
        foreach($childBoxes as $key => $boxesCollection){
            if($tabCounter == 0){
               $str = $str."<li><a id='$key' class='child_tab_selector active_child'>".ucwords($key)." rates</a></li>";
            }else{
               $str = $str."<li><a id='$key' class='child_tab_selector'>".ucwords($key)." rates</a></li>";
            }
            $tabCounter++;
        }
        $str = $str."</ul></div>";
        echo $str;
        $tabCounter = 0;
        foreach($childBoxes as $key => $boxesCollection){
          if($tabCounter == 0){
            echo "<div class='child_tab $key'>";
          }else{
            echo "<div class='child_tab $key display_none'>";
          }
          foreach($boxesCollection as $childBox){
            $this->load->view('templates/default/inc/text_box', array('box'=>$childBox, 'box_counter'=>$counter));
            $counter++;
          }
          echo "</div>";
          $tabCounter++;
        }
      }else{
        foreach ($boxes as $box) {
          if($box->ranking == '-1') continue;
          $this->load->view('templates/default/inc/text_box', array('box'=>$box, 'box_counter'=>$counter));
          $counter++;
        }
      }

		}else {
			$counter = 0;
			foreach ($boxes as $box) {

				$this->load->view('templates/default/inc/text_box', array('box'=>$box, 'box_counter'=>$counter));

				$counter++;
			}
		}

	?>
<?php endif;?>
