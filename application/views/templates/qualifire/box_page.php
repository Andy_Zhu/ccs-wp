<?php if($section =='services'):?>
<div style="margin-left:20px;">
<p style="margin:20px 0 50px;"><?php if($services_intro) echo htmlspecialchars($services_intro->content);?></p>


	<?php //$top_boxes = array_values($top_boxes);?>

	<?php $i=0; foreach ($top_boxes as $b):?>

		<?php if($i%2 == 0):?>
			<div class="row-fluid">
				<div class="wpb_content_element span6 column_container">
					<div class="wpb_wrapper">
						<div class="row-fluid">
							<div class="wpb_content_element span12 wpb_text_column">
								<div class="wpb_wrapper">
									<h3 style="text-align: justify;"><strong><?php echo $b->title;?></strong></h3>
									<p style="text-align: justify;"><?php echo str_replace("\n", "<br/>", $b->content);?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wpb_content_element span6 column_container">
					<div class="wpb_wrapper">
						<div class="row-fluid">
							<div class="wpb_gallery wpb_content_element span12">
								<div class="wpb_wrapper">
									<div class="wpb_gallery_slides wpb_flexslider flexslider_slide flexslider" data-interval="3" data-flex_fx="slide">
										<ul class="slides">
											<li><img width="450" height="180" src="<?php echo textbox_image_src($b->image, $is_preview);?>" class="attachment-full" alt="fp3" /></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php else:?>

			<div class="wpb_separator wpb_content_element "></div>
			<div class="row-fluid">
				<div class="wpb_content_element span6 column_container">
					<div class="wpb_wrapper">
						<div class="row-fluid">
							<div class="wpb_gallery wpb_content_element span12">
								<div class="wpb_wrapper">
									<div class="wpb_gallery_slides wpb_flexslider flexslider_slide flexslider" data-interval="3" data-flex_fx="slide">
										<ul class="slides">
											<li><img width="450" height="180" src="<?php echo textbox_image_src($b->image, $is_preview);?>" class="attachment-full" alt="cr2" /></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wpb_content_element span6 column_container">
					<div class="wpb_wrapper">
						<div class="row-fluid">
							<div class="wpb_content_element span12 wpb_text_column">
								<div class="wpb_wrapper">
									<h3 style="text-align: justify;"><strong><?php echo $b->title;?></strong></h3>
									<p style="text-align: justify; padding-right:10px;"><?php echo str_replace("\n", "<br/>", $b->content);?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif;?>

	<?php $i++; endforeach;?>
</div>
<?php else:?>
<div style="margin-top:20px; <?php if(isset($is_full_page) && $is_full_page) echo 'margin-left:20px;'?>">
<?php foreach ($boxes as $b):?>
	<div class="box" style="margin-bottom:20px;">
	<?php if(!empty($b->title)):?>
	  <?php
	   $title = $b->title;
     if($site -> lang_id == 2){
       switch($title){
        case "About us":
          $title = '关于我们';
          break;
       }
     }


	  ?>
		<h2><?php echo $title;?></h2>
	<?php endif;?>
		<div style="text-align: justify">
			<?php //get rid of any backgroud set by the WYSIWYG?>
			<?php echo preg_replace('/div(\s+)+style=\"(.*)?\"/', 'div style=""', $b->content);?>
		</div>
	</div>
<?php endforeach;?>
</div>
<?php endif;?>
