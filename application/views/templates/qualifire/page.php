<?php
$full_page_configs = array(
	'locations' => TRUE,
	'fleet' => TRUE,
	'services' => TRUE,
	'online-booking' => TRUE,
	'faqs' => TRUE
);
$extra_data = array();
if(array_key_exists($section, $full_page_configs)) {
	$extra_data['is_full_page'] = $full_page_configs[$section];
}

echo $this->load->view('templates/qualifire/inc/header', $extra_data);
echo $this->load->view('templates/qualifire/inc/main-content', $extra_data);
echo $this->load->view('templates/qualifire/inc/sidebar', $extra_data);
echo $this->load->view('templates/qualifire/inc/footer', $extra_data);
?>
