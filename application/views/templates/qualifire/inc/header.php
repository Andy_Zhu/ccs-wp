<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo  ($meta_title ? $meta_title : ($page && $page->meta_title? $page->meta_title : $section)), ' &laquo; ', $site->name;  ?></title>
<meta name="description" content="<?php echo htmlentities($meta_description ? $meta_description : ($page ? $page->meta_description : $site->meta_desc)); ?>" />
<meta name="keywords" content="<?php echo htmlentities($meta_keywords ? $meta_keywords : ( $page ? $page->meta_keywords : $site->meta_keywords)); ?>" />

<!--[if IE 6]>
    <script  type="text/javascript" src="<?php echo template_js_src("DD_belatedPNG_0.0.8a-min.js", $is_preview, "qualifire")?>"></script>
    <script  type="text/javascript">
    // <![CDATA[
	DD_belatedPNG.fix('.pngfix, img, #home-page-content li, #page-content li, #bottom li, #footer li, #recentcomments li span');
    // ]]>
    </script>
<![endif]-->

<link rel='stylesheet' id='nivo-slider-css'  href='<?php echo template_js_src("shortcodes-ultimate/css/nivoslider.css", $is_preview, "qualifire");?>?ver=3.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='jcarousel-css'  href='<?php echo template_js_src("shortcodes-ultimate/css/jcarousel.css", $is_preview, "qualifire");?>?ver=3.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='shortcodes-ultimate-css'  href='<?php echo template_js_src("shortcodes-ultimate/css/style.css", $is_preview, "qualifire");?>?ver=3.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='<?php echo template_css_src("bootstrap/css/bootstrap.css", $is_preview, "qualifire");?>?ver=3.4.12' type='text/css' media='screen' />
<link rel='stylesheet' id='js_composer_front-css'  href='<?php echo template_css_src("js_composer_front.css", $is_preview, "qualifire")?>?ver=3.4.12' type='text/css' media='screen' />
<link rel='stylesheet' id='layerslider_css-css'  href='<?php echo template_css_src("layerslider.css", $is_preview, "qualifire")?>?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='reset-css'  href='<?php echo template_css_src("common-css/reset.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='text-css'  href='<?php echo template_css_src("text.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='grid-960-css'  href='<?php echo template_css_src("common-css/960.css", $is_preview, "qualifire");?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='superfish_menu-css'  href='<?php echo template_js_src("superfish-1.4.8/css/superfish.css", $is_preview, "qualifire");?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='pagination-css'  href='<?php echo template_js_src("pagination/pagenavi-css.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='pretty_photo-css'  href='<?php echo template_js_src("prettyPhoto/css/prettyPhoto.css", $is_preview, "qualifire");?>?ver=3.1.2' type='text/css' media='screen' />
<link rel='stylesheet' id='style-css'  href='<?php echo template_css_src("style.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='jquery-foobar-2-3-css'  href='<?php echo template_css_src("jquery.foobar.2.3.css", $is_preview, "qualifire")?>?ver=2.3.3' type='text/css' media='all' />

<script type='text/javascript' src='<?php echo template_js_src("jquery-1.4.4.min.js", $is_preview, "qualifire")?>?ver=3.5.1'></script>
<script type='text/javascript' src='<?php echo template_js_src("shortcodes-ultimate/js/jwplayer.js", $is_preview, "qualifire")?>?ver=3.9.5'></script>
<script type='text/javascript' src='<?php echo template_js_src("shortcodes-ultimate/js/jcarousel.js", $is_preview, "qualifire")?>?ver=3.9.5'></script>
<script type='text/javascript' src='<?php echo template_js_src('shortcodes-ultimate/js/init.js', $is_preview,"qualifire")?>?ver=3.9.5'></script>
<script type='text/javascript' src='<?php echo template_js_src("layerslider.kreaturamedia.jquery.js", $is_preview, "qualifire");?>?ver=3.5.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("jquery.easing.1.3.js", $is_preview, "qualifire")?>?ver=1.3.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("encoder.js", $is_preview, "qualifire")?>?ver=2.3.3'></script>
<?php if($site -> lang_id != 2){ ?>
<script type='text/javascript' src='<?php echo template_js_src("cufon/cufon-yui.js", $is_preview, "qualifire")?>?ver=1.09i'></script>
<script type='text/javascript' src='<?php echo template_js_src("cufon/eurofurence_500-eurofurence_700.font.js", $is_preview, "qualifire")?>?ver=1.0'></script>
<?php } ?>
<script type='text/javascript' src='<?php echo template_js_src("jquery.cycle.all.min.js", $is_preview, "qualifire")?>?ver=2.86'></script>
<script type='text/javascript' src='<?php echo template_js_src("cycle1_script.js", $is_preview, "qualifire")?>?ver=1.0.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("prettyPhoto/js/jquery.prettyPhoto.js", $is_preview, "qualifire")?>?ver=3.1.3'></script>
<script type='text/javascript' src='<?php echo template_js_src("jquery-validate/jquery.validate.min.js", $is_preview, "qualifire")?>?ver=1.6'></script>
<script type='text/javascript' src='<?php echo template_js_src("masked-input-plugin/jquery.maskedinput.min.js", $is_preview, "qualifire")?>?ver=1.2.2'></script>
<script type='text/javascript' src='<?php echo template_js_src("superfish-1.4.8/js/superfish.js", $is_preview, "qualifire")."?ver=1.4.8"?>'></script>
<script type='text/javascript' src='<?php echo template_js_src("superfish-1.4.8/js/supersubs.js", $is_preview, "qualifire")?>?ver=0.2.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("script.js", $is_preview, "qualifire")."?ver=1.0"?>'></script>

<link rel='next' title='Booking Confirmation' href='http://www.chinacarservice.com/booking-confirmation/' />
<link rel='canonical' href='<?php echo $site->url;?>' />
<style type='text/css'>#respond, #commentform, #addcomment, .entry-comments { display: none;}</style>
<style type='text/css'> #disabled_msgCM {width: 300px; border: 1px solid red; }</style>
<link rel='stylesheet' id='flexslider-css'  href='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/js/flexslider/flexslider.css?ver=3.4.12' type='text/css' media='screen' />

 <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php  echo template_css_src("common-css/ie-all.css", $is_preview, "qualifire")?>" media="screen" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
    <link rel="stylesheet" href="<?php  echo template_css_src("common-css/ie6-7.css", $is_preview, "qualifire")?>" media="screen" type="text/css" />
<![endif]-->
<!--[if IE 6]>
    <link rel="stylesheet" href="<?php  echo template_css_src("common-css/ie6.css", $is_preview, "qualifire")?>" media="screen" type="text/css" />
    <style type="text/css">
	body{ behavior: url("<?php  echo template_js_src("csshover3.htc", $is_preview, "qualifire")?>"); }
    </style>
<![endif]-->

</head>
<body class="home page page-id-2 page-template-default wpb-js-composer js-comp-ver-3.4.12">
    <div id="wrapper-1" class="pngfix">
	<div id="top-container" class="container_24">

	    <div class="clear"></div>
	    <div id="top" class="grid_24">
		<div id="logo" class="grid_14">
		    <h1>
			<a class="pngfix" style="background: transparent url(<?php if(!empty($site->logo)) echo site_logo_src($site->logo,  $is_preview, 'qualifire');?> ) no-repeat 0 100%; width:308px; height:74px;" href="<?php echo $site->url?>">
			    <?php echo $site->name?></a>
		    </h1>
		</div>
		<div id="slogan" class="grid_17" style="top:100px; left:50px;"></div>
		<!-- end logo slogan -->
		<div id="pay-logo">
		</div>
		<?php if($site->phone):?>
		<div class="phone-number grid_7 prefix_17">

<?php echo lang("call-us-at", 'Call us at'); ?> <?php echo $site->phone?>		</div>
      <?php endif;?>
		<!-- end top-icons -->
	    </div>
	    <!-- end top -->

	    <div class="clear"></div>

<div id="dropdown-holder" class="grid_24">
<div class="nav_bg pngfix">
<div class="menu-main-menu-container"><ul id="menu-main-menu" class="sf-menu">
<li id="menu-item-5" class="<?php  if($section =='home') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-5"><a href="<?php echo template_link($site_id, 'home', $is_preview, 'qualifire');?>"><span><?php echo lang("home", 'Home'); ?></span></a></li>
<li id="menu-item-2335" class="<?php  if($section =='about-us') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-post_type menu-item-object-page menu-item-2335"><a href="<?php echo template_link($site_id, 'about-us', $is_preview);?>"><span><?php echo lang("about-us", 'About Us'); ?></span></a></li>
<li id="menu-item-2359" class="<?php  if($section =='fleet') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-post_type menu-item-object-page menu-item-2359"><a href="<?php echo template_link($site_id, 'fleet', $is_preview);?>"><span><?php echo lang("fleet", 'Fleet'); ?></span></a></li>
<li id="menu-item-2341" class="<?php  if($section =='services') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-post_type menu-item-object-page menu-item-2341"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><span><?php echo lang("services", 'Services'); ?></span></a></li>
<li id="menu-item-2338" class="<?php  if($section =='locations') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-post_type menu-item-object-page menu-item-2338"><a href="<?php echo template_link($site_id, 'locations', $is_preview);?>"><span><?php echo lang("locations", 'Locations'); ?></span></a></li>
<li id="menu-item-2339" class="<?php  if($section =='online-booking') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-post_type menu-item-object-page menu-item-2339"><a href="<?php echo template_link($site_id, 'online-booking', $is_preview);?>"><span><?php echo lang("online-booking", 'Online Booking'); ?></span></a></li>
<li id="menu-item-2349" class="<?php  if($section =='faqs') echo 'current-menu-item current_page_item ';?>menu-item menu-item-type-post_type menu-item-object-page menu-item-2349"><a href="<?php echo template_link($site_id, 'faqs', $is_preview);?>"><span><?php echo lang("faqs", 'FAQs'); ?></span></a>
<ul class="sub-menu">
<li id="menu-item-2340" class="<?php  if($section =='policies') echo 'current-menu-item current_page_item ';?> menu-item menu-item-type-post_type menu-item-object-page menu-item-2340"><a href="<?php echo template_link($site_id, 'policies', $is_preview);?>"><span><?php echo lang("policies-and-terms", 'Policies And Terms'); ?></span></a></li>
</ul>
</li>
		<li id="menu-item-2336"
			class="<?php if ($section == 'contact-us') echo 'current-menu-item current_page_item '; ?>  menu-item menu-item-type-post_type menu-item-object-page menu-item-2336">
			<a href="<?php echo template_link($site, 'contact-us', $is_preview); ?>"><span><?php echo lang("contact-us", 'Contact Us'); ?></span></a>
		</li>
		<li>
			<?php foreach ($other_sites as $os): ?>
				<a href="<?php echo $os['website']->url; ?>">
					<img width=15 src="/<?php echo $this->Languages_Model->get_small_flag_path($os['lang']->id); ?>">
				</a>
			<?php endforeach; ?>
		</li>
</ul>
</div>


		</div>
	    </div><!-- end dropdown-holder -->
	</div>
	<!-- end top-container -->

	<div class="clear"></div>

<?php if($section == 'home'): //slideshow?>

	    <div id="c1-header">
		<div id="header-content" class="container_24">

		    <div class="c1-slideshow">
			    <div class="c1-slider-navigation" style="width:940px; ">
				<a href="#"><span id="slider-prev" class="pngfix"><?php echo lang("prev", 'Prev'); ?></span></a>
				<a href="#"><span id="slider-next" class="pngfix"><?php echo lang("next", 'Next'); ?></span></a>
			    </div>
			    <ul id="c1-slider">
			    	<?php foreach ($slideshow_images as $slideshow):?>
				    	<li style="width:940px; height:360px;">
						<div class="c1-slide-img-wrapper">
							<img style="width:930px; height:300px;" src="<?php echo slideshow_image_src($slideshow->filename, $is_preview);?>" alt="" class="slide-img" />
						</div>
					    </li>
			    	<?php endforeach;?>

			    </ul>

		    </div>
		    <!-- end c1-slideshow -->
		    <span id="c1-resumeButton" style=""><a href="" title="Play" class="pngfix"><?php echo lang("play", 'Play'); ?></a></span>
		    <span id="c1-pauseButton" style=""><a href="" title="Pause" class="pngfix"><?php echo lang("pause", 'Pause'); ?></a></span>
		    <div id="c1-nav" style=""></div>

		</div>
		<!-- end header-content -->
	    </div>
	    <!-- end c1-header -->
	    <div class="clear"></div>

<?php endif;?>

<div id="<?php if($section == 'home')echo $section, '-'?>page-content">

<?php if($section !== "home"):?>

	<div class="container_24" id="page-content-header">
		<div id="page-title">
		   <h2>
		   	<?php
				if(isset($carpage)) {
					echo $page_vehicle->long_name;
				}else if($section=='subtours' && isset($tour_category)){
					echo $tour_category->title;
				}else{
					$tmp_name = $section;
          if($section == 'policies') $tmp_name = 'Policies & Terms';
          if($site -> lang_id == 2){
            switch($tmp_name){
              case "about-us":
                $tmp_name = '关于我们';
                break;
              case "fleet":
                $tmp_name = "车型展示";
                break;
              case "services":
                $tmp_name = "服务范围";
                break;
              case "locations":
                $tmp_name = "服务区域";
                break;
              case "online-booking":
                $tmp_name = "在线订车";
                break;
              case "faqs":
                $tmp_name = "常见问题";
                break;
              case "Policies & Terms":
                $tmp_name = "政策和条款";
                break;
            }
          }
					//if(isset($static_page) && $static_page->name) $tmp_name = $static_page->name;
					echo ucwords(strtolower(str_replace('-', ' ', $tmp_name)));
				}
			?>
		   </h2>
		</div>
		<div class="no-breadcrumbs-padding"></div>
	</div>
<?php endif;?>
<div id="content-container" class="container_24">

<?php if(!isset($is_full_page) || !$is_full_page):?>
<div id="main-content" class="grid_16">
<div class="main-content-padding">

<?php endif;?>
