

</div><!-- end content-container -->

<div class="clear"></div>



</div><!-- end page-content -->

<div class="clear"></div>
<div id="bottom-bg">
<div id="bottom" class="container_24">
<?php
$name = $site -> name;
if($name == "Royal Asia Limo"){
  $name = "亚洲皇家租车";
}
$chinese = false;
if($site -> lang_id == 2){
  $chinese = true;
}
?>
<div id='bottom_1' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_text substitute_widget_class"><h3 class="bottom-col-title"><?php echo lang("about-us", 'About Us'); ?></h3>			<div class="textwidget"><p style="text-align: justify;"><?php echo $name?> <?php echo lang("is-a-full-service-ground", 'is a full service ground transportation provider that offers you complete comfort, control and convenience during your visit '); ?><?php if(!$chinese) echo isset($website_country) ? 'to ' . ucfirst($website_country->name) : '';?>!</p>
<p>♦ <?php echo lang("easy-online-booking", 'Easy online booking'); ?><br />
♦ <?php echo lang("simple-payment-options", 'Simple payment options'); ?><br />
♦ <?php echo lang("24-hour-customer-service", '24 hour customer service'); ?><br />
♦ <?php echo lang("service-in-every-major-city", 'Service in every major city'); ?></p>
<p></p>
</div>
</div></div></div><!-- end bottom_1 --><div id='bottom_2' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_text substitute_widget_class"><h3 class="bottom-col-title"><?php echo lang("contact", 'Contact'); ?></h3>			<div class="textwidget"><p style="text-align: center;"><img class="aligncenter size-full wp-image-1431" alt="MagentoEx1" src="http://chinacarservice.com/wp-content/uploads/2013/01/MagentoEx1.png" width="200" height="88" /></p>
<p style="text-align: justify;"><strong><?php echo $name;?></strong><br />
<label><?php echo lang("customer-service-center", 'Customer Service Center'); ?></label><br />
<label>bookings@<?php echo str_replace('http://', '', $site->url);?></label><br />
<label><?php echo lang("tel", 'Tel'); ?>: <?php echo $site->phone;?></label></p>
</div>
</div></div></div><!-- end bottom_2 --><div id='bottom_3' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_nav_menu custom-formatting"><h3 class="bottom-col-title"><?php echo lang("services", 'Services'); ?></h3><div class="menu-services-container"><ul id="menu-services" class="menu"><li id="menu-item-1381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1381"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("airport-pick-up", 'Airport Pick-Up'); ?></a></li>
<li id="menu-item-1382" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1382"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("private-aviation", 'Private Aviation'); ?></a></li>
<li id="menu-item-1383" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1383"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("events-and-meetings", 'Events and Meetings'); ?></a></li>
<li id="menu-item-1387" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1387"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("long-term-car-leasing", 'Long Term Car Leasing'); ?></a></li>
<li id="menu-item-1384" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1384"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("cruise-ship-or-ferry-pick-up", 'Cruise ship or Ferry Pick-Up'); ?></a></li>
<li id="menu-item-1385" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1385"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("english-speaking-tour-guides", 'English Speaking Tour Guides'); ?></a></li>
<li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1386"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>"><?php echo lang("english-speaking-business-assistant", 'English Speaking Business Assistant'); ?></a></li>
</ul></div></div></div></div><!-- end bottom_3 --><div id='bottom_4' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_text substitute_widget_class"><h3 class="bottom-col-title"><?php echo lang("online-booking", 'Online Booking'); ?></h3>			<div class="textwidget"><p style="text-align: justify;"><?php echo lang("our-on-line-booking-system-processes", 'Our on-line booking system processes your MasterCard, Visa or Amex Cards through Authorize.Net, an American credit card system. As with any transaction in the US or Europe your purchases are protected if you do not get what we promise!'); ?></p>
<div class="clear"></div>
<p><a class="pngfix small-light-button align-btn-left" href="<?php echo template_link($site_id, 'online-booking', $is_preview);?>" title=""><span class="pngfix"><?php echo lang("online-booking", 'Online Booking'); ?></span></a></p>
<div class="clear"></div>
</div>
		</div></div></div><!-- end bottom_4 -->		</div>
		<!-- end bottom -->
	    </div>
	    <!-- end bottom-bg -->

	    <div class="clear"></div>




	<div id="footer-bg">
		<div id="footer" class="container_24 footer-top">
		    <div id="footer_text" class="grid_21">
			<p>
© 2001-2015 <strong><?php echo $site->name;?>, All Rights Reserved</strong>
			</p>
		    </div>
		    <div class="back-to-top">
			<a href="#top"><?php echo lang("back-to-the-top", 'Back to Top'); ?></a>
		    </div>
		</div>
	</div>

	<div class="clear"></div>

<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/shortcodes-ultimate/js/nivoslider.js?ver=3.9.5'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.9.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.9.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/jquery/ui/jquery.ui.tabs.min.js?ver=1.9.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/jquery-ui-tabs-rotate/jquery-ui-tabs-rotate.js?ver=3.4.12'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/js_composer_front.js?ver=3.4.12'></script>
<?php if($site -> lang_id != 2): ?>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/foobar/js/jquery.foobar.2.3.min.js?ver=2.3.3'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/foobar/js/jquery.easing.1.3.js?ver=2.3.3'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/?foobar-js-dynamic=js&#038;ver=3.5.1&#038;post_id=2'></script>
<?php endif;?>
<script type='text/javascript' src='<?php echo template_js_src('prettyPhoto/custom_params.js', $is_preview, 'qualifire');?>?ver=3.1.2'></script>
<script type='text/javascript' src='<?php echo template_js_src('jquery.hoverIntent.r6.js', $is_preview, 'qualifire');?>'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/flexslider/jquery.flexslider-min.js?ver=3.4.12'></script>
</div>

<!-- end wrapper-1 -->
<?php if($site -> lang_id != 2){ ?>
  <script type="text/javascript"> Cufon.now(); </script>
<?php } ?>

  <?php if($hasBanner):?>
	<div id="banner" style="position:fixed;right:0; padding:20px;z-index:1000; border:5px solid transparent;border-radius:10px;<?php echo $banner->styles;?> ">
		<a style="position:absolute;display:block;left:5px; top:5px;" href="nowhere" id="banner-close-btn"><img alt="close" src="<?php echo template_image_src('banner-close-btn.png', $is_preview);?>" /></a>
		<?php echo $banner->content;?>
	</div>
	<script type="text/javascript">
<!--
jQuery(document).ready(function($){

	var bannerWidth = $('#banner').width();
	var bannerHeight = $('#banner').height();

	$('#banner').css({'bottom':-bannerHeight - 100, visibility:'visible'}).animate({bottom:0}, 'slow');

	$('#banner-close-btn').click(function(evt){
		evt.preventDefault();
		$('#banner').fadeOut('fast', function(){$(this).remove();});
	});
});


//-->
</script>
<?php endif;?>

<?php
echo $site->g_analytics;
?>
  </body>
</html>