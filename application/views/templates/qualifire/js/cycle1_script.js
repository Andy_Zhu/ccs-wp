

// Cycle plugin config

jQuery(document).ready(function($){

    var c1TransitionType = "uncover,uncover,uncover,uncover";

    var c1Speed = 1000;

    var c1Timeout = 5000;

    var c1Sync = 0;


	/* homepage slider params */

	$('#c1-slider').cycle({

	    fx:			c1TransitionType,

	    speed:		c1Speed,

	    timeout:		c1Timeout,

	    sync:		c1Sync,

	    randomizeEffects:	0,

	    prev:		'#slider-prev',

	    next:		'#slider-next',

	    pager:		'#c1-nav'

	});

	$('.c1-slideshow').hover(

	    function() { $('#slider-prev').fadeIn(); },

	    function() { $('#slider-prev').fadeOut(); }

	);

	$('.c1-slideshow').hover(

	    function() { $('#slider-next').fadeIn(); },

	    function() { $('#slider-next').fadeOut(); }

	);



	$('#c1-pauseButton').click(function() {

	    $('#c1-slider').cycle('pause');

	    return false;

	});



	$('#c1-resumeButton').click(function() {

	    $('#c1-slider').cycle('resume', true);

	    return false;

	});

});



