<div style="margin-top:10px;">

<?php if($section == 'faqs'):?>
	<div style="margin-left:20px;">
	<?php $counter=0; foreach ($boxes as $b):?>
	
		<div class="su-spoiler su-spoiler-style-1">
		<div class="su-spoiler-title"><?php echo ++$counter;?>. <?php echo $b->title;?></div>
		<div class="su-spoiler-content">
		<p style="text-align: justify;"><?php echo $b->content;?></p>
		</div>
		</div>

	<?php endforeach;?>
	</div>
<?php else:?>
	<?php //get rid of any backgroud set by the WYSIWYG?>
	<?php echo $static_page ? preg_replace('/div(\s+)+style=\"(.*)?\"/', 'div style=""', $static_page->content): '';?>
<?php endif;?>

</div>