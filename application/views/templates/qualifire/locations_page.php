<?php
$location_intro_box = null;

foreach ($boxes as $box) {
	if($box->type == 'intro') {
		$location_intro_box = $box;
		break;
	}
}
?>
<div style="margin:20px;margin-left:40px;">
<p><?php if($location_intro_box) echo htmlspecialchars($location_intro_box->content);?></p>

</div>
<script type="text/javascript" src="<?php echo template_js_src('isotope/jquery.isotope.min.js', $is_preview, 'qualifire')?>"></script>
<style type="text/css">
<!--
@import url("<?php echo template_js_src('isotope/isotope.css', $is_preview, 'qualifire');?>")
-->
</style>
<style>
<!--
#isotope-filters {
	margin-left:40px;
	margin-bottom:20px;
	width:960px;
}

#isotope-filters a{
	font-size:20px;
	display:block;
	outline: 0;
	padding:4px 10px;
	margin-left:10px;
	margin-bottom:5px;
	float:left;
	color:#333;
}

#isotope-filters a.current {
	background:#E82A07;
	color:#fff;
	-webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;

}

.isotope,
.isotope .isotope-item {
  /* change duration value to whatever you like */
  -webkit-transition-duration: 0.8s;
     -moz-transition-duration: 0.8s;
      -ms-transition-duration: 0.8s;
       -o-transition-duration: 0.8s;
          transition-duration: 0.8s;
}

.isotope {
  -webkit-transition-property: height, width;
     -moz-transition-property: height, width;
      -ms-transition-property: height, width;
       -o-transition-property: height, width;
          transition-property: height, width;
}

.isotope .isotope-item {
  -webkit-transition-property: -webkit-transform, opacity;
     -moz-transition-property:    -moz-transform, opacity;
      -ms-transition-property:     -ms-transform, opacity;
       -o-transition-property:      -o-transform, opacity;
          transition-property:         transform, opacity;
}

/**** disabling Isotope CSS3 transitions ****/

.isotope.no-transition,
.isotope.no-transition .isotope-item,
.isotope .isotope-item.no-transition {
  -webkit-transition-duration: 0s;
     -moz-transition-duration: 0s;
      -ms-transition-duration: 0s;
       -o-transition-duration: 0s;
          transition-duration: 0s;
}
-->
</style>
<?php
	$grouped_boxes = array();
	foreach ($boxes as $box) {
		if($box->type == 'intro')continue;
		$grouped_boxes[$box->cat_id][] = $box;
	}
?>

<div id="isotope-filters">
<?php foreach ($boxes_categories as $cat):?>
	<a href="#filter<?php echo $cat->id;?>"><?php echo $cat->title;?></a>
<?php endforeach;?>
<div style="clear:both"></div>
</div>

<div style="margin-left:40px;width:960px" id="locations"  class="isotope">

<?php $i=1; foreach ($boxes as $location):?>
		<?php if($location->type == 'intro') continue;?>
		<div class="isotope-item <?php echo 'filter' . $location->cat_id;?>" style="padding:10px; width:195px; margin-bottom:20px; <?php if($i%4 != 0) echo 'margin-right:20px;'?>float:left;border:1px solid #ccc;" >

		<div style="width:195px;height:180px;background:#ccc;">
			<?php if(!empty($location->image) && file_exists(textbox_image_path($location->image))):?>
			<?php if(!empty($location->extra)):?>
				<a href="<?php echo $location->extra;?>">
			<?php endif;?>
			<img style="width:195px; height:180px;" src="<?php echo textbox_image_src($location->image, $is_preview);?>" />
				<?php if(!empty($location->extra)):?>
				</a>
				<?php endif;?>
			<?php endif;?>
		</div>

		<h4>
			<?php if(!empty($location->extra)):?>
				<a style="color:#333" href="<?php echo $location->extra;?>">
			<?php endif;?>
			<?php echo $location->title;?>
			<?php if(!empty($location->extra)):?>
				</a>
			<?php endif;?>
		</h4>
	</div>

<?php $i++; endforeach;?>
</div>



<script type="text/javascript">
<!--
$(document).ready(function(){

	$('#locations').isotope({
		itemClass:'isotope-item',
		animationEngine : 'best-available',
		  masonry: {
		    columnWidth: 240
		  }
	});

	$('#isotope-filters a').click(function(evt){
		evt.preventDefault();
		$this = $(this);
		$('#isotope-filters a.current').removeClass('current');
		$this.addClass('current');
		var filter_str = $(this).attr('href');
		filter_str = filter_str.replace('#', '.');
		$('#locations').isotope({ filter: filter_str });

	});

	$('#isotope-filters a:first').trigger('click'); //select the first one
});
//-->
</script>