<div class="wpb_wrapper">
	<div class="row-fluid">
		<?php echo $site->short_about;?>
	</div>

	<div class="row-fluid">
	<?php $c = 0; foreach ($home_boxes as $box):?>
		<?php if($box->type != 'shbox') continue;?>
		<div <?php if($c++ % 3 == 0) echo 'style="margin-left:0px;" '?> class="wpb_content_element span4 wpb_text_column">
			<div class="wpb_wrapper">
				<p style="text-align: center;"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>" title="Services"><span style="font-family: book antiqua,palatino;">&nbsp;<span style="color: #000000;"><span style="color: #000000;"><?php echo $box->title;?></span></span></span></a></p>
				<hr width="150" style="width: 150px;">
				<p style="text-align: center;"><a href="<?php echo template_link($site_id, 'services', $is_preview);?>" title="Services">
				<img width="230" height="150" src="<?php echo textbox_image_src($box->image, $is_preview);?>" alt="China Airport Transfer" style="border: 2px solid black;" class="size-full wp-image-1946 aligncenter"></a></p>
				<p></p>
			</div>
			<p></p>
		</div>
	<?php endforeach;?>
	</div>
</div>