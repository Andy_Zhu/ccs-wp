<?php

$market = strtolower(trim(str_ireplace('Car Service', '', $website->name)));
if ($website->country_id == 9) {
    $parent_market = "asia";
} else {
    $parent_market = "china";
}

if ($is_preview) {
    header ("Location: http://testbookingforms.chinacarservice.com/booking/{$market}/contact?testing=1");
}