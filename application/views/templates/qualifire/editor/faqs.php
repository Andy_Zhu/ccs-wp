<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">

	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>

	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>

	<div id="boxes-container">
	<?php if(isset($boxes) && count($boxes) > 0) : $counter=1;?>
		<?php foreach ($boxes as $box) :?>

		<div class="box clear inner-spacer">
			<a href="#" class="delete" title="delete">Delete</a>
			<div class="column column-450 left">
				<label>*Question:</label>
				<input  type="text" style="width:450px;" class="txt" name="box_title[]" value="<?php echo $box->title; ?>" />
			</div>
			<div class="column med left">
				<label>Display Order:</label>
				<input type="text" name="rankings[]" class="txt small" value="<?php echo $box->ranking;?>"/>
			</div>
			<div  class="clear ">
				<label>*Answer: </label>
				<textarea id="box-<?php echo (++$counter); ?>" rows="3" cols="50" name="box_content[]"  class="txt large"><?php echo htmlspecialchars($box->content); ?></textarea>
				<br />
			</div>

		</div>
		<?php endforeach;?>


	<?php else:?>

		<div class="box clear inner-spacer">
			<a href="#" class="delete" title="delete">Delete</a>
			<div class="column column-450 left">
				<label>*Question:</label>
				<input  type="text" class="txt" style="width:450px;" name="box_title[]" value="" />
			</div>
			<div class="column med left">
				<label>Display Order:</label>
				<input type="text" name="rankings[]" class="txt small" value=""/>
			</div>
			<div  class="clear ">
				<label>*Answer: </label>
				<textarea id="box-1" rows="3" cols="50" name="box_content[]"  class="txt large"></textarea>
				<br />
			</div>

		</div>

	<?php endif; ?>
	</div>
	<div class="clear row"><a href="#nowhere" id="add-box-btn">+ An Qestion/Answer Box</a></div>

	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>

	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>
	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	//$('#boxes-container textarea').ckeditor();

	$('#box-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var boxCounter = ($('#box-editor .box').length) + 1;
	$('#add-box-btn').click(function(evt){
		evt.preventDefault();
		id = ++boxCounter;
		var tpl = ['<div class="box inner-spacer"><a href="#" class="delete" title="delete">Delete</a>',
						'<div class="column column-450 left">',
							'<label>*Question: </label>',
							'<input type="text" class="txt" name="box_title[]" style="width:450px;" />',
						'</div>',
						'<div class="column med left">',
						'<label>Display Order:</label>',
						'<input type="text" name="rankings[]" class="txt small" value="" />',
						'</div>',
						'<div class="clear ">',
							'<label>*Answer: </label>',
							'<textarea id="box-',
							id  + '"',
							'rows="3" cols="50" name="box_content[]"  class="txt large "></textarea>',
						'</div>',
					'</div>'];
	$(tpl.join('')).appendTo('#boxes-container');
		//$('#box-' + id).ckeditor();
	});

});


//-->
</script>