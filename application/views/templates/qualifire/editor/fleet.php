<?php
$fleet_intro_box = null;

foreach ($boxes as $box) {
	if($box->type == 'intro') {
		$fleet_intro_box = $box;
		break;
	}
}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<?php if (false)://comment at php level :)?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-sortable.min.js"></script>
<?php endif;?>

<div class="" id="box-editor">

	<form method="post" enctype="multipart/form-data" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
		<div class="clear"></div>

<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Introduction:</h2>
<div style="margin-bottom:20px;">
	<textarea name="fleet_intro" style="width:735px; height:80px;" rows="5" cols="30" class="txt large"><?php echo $fleet_intro_box ? $fleet_intro_box->content : '';?></textarea>
</div>

<div id="boxes-container">

	<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Fleets:</h2>
	<div id="top-section-boxes">
		<?php if(isset($boxes) && !empty($boxes)):?>
			<?php $top_num=1; foreach ($boxes as $top_box):?>
			<?php if($top_box->type == 'intro') continue;?>
			<div class="box top-section-box clear inner-spacer">
				<a href="#" class="delete top-section-delete" title="delete">Delete</a>
				<div class="column left column-220">
						<div class="clear">
							<label >Image (420 x 200):</label>
						</div>,
						<div class="" style="width:195px; height:180px;background:#666;border:1px solid #333">
							<?php if(strlen($top_box->image)):?>
						<img style="width:100%; height:100%;" src="<?php echo textbox_image_src($top_box->image);?>" />
						<?php else:?>
							<p style="text-align:center;padding:80px 40px 0px;">Image not set.</p>
						<?php endif;?>

						</div>
						<input type="file" name="fleet_img_<?php echo $top_num;?>" />
				</div>


				<div class="column right column-450">
					<label>Text:</label>
					<input type="hidden" name="fleet_ids[]" value="<?php echo $top_box->id;?>" />
					<textarea id="top-editor-<?php echo $top_num;?>" rows="8" cols="30" name="fleet_boxes[]" class="txt column-450 top-editor"><?php echo $top_box->content;?></textarea>
					<br /><strong>Order Display</strong>
					<input type="text" class="txt small" name="fleet_rankings[]" value="<?php echo $top_box->ranking;?>" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<?php $top_num++; endforeach;?>
		<?php else:?>
			<div class="box top-section-box clear inner-spacer">
				<a href="#" class="delete top-section-delete" title="delete">Delete</a>
				<div class="column left column-220">
						<div class="clear">
							<label >Image (420 x 200):</label>
						</div>
						<div class="" style="width:195px; height:180px;background:#666;border:1px solid #333">
							<p style="text-align:center;padding:80px 40px 0px;">Image not set.</p>
						</div>
						<input type="file" name="fleet_img_1" />
				</div>
				<div class="column right column-450">
					<label>Text:</label>
					<textarea id="top-editor-1" rows="8" cols="30" name="fleet_boxes[]" class="txt column-450 top-editor"></textarea>
					<br /><strong>Order Display</strong>
					<input type="text" class="txt small" name="fleet_rankings[]" value="" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		<?php endif;?>
	</div>

	<div class="clear row"><a href="#nowhere" id="add-top-box-btn">+ New Fleet Box</a></div>


	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>

	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container textarea.editor').ckeditor();
	$('#boxes-container textarea.top-editor').ckeditor();

	$('#box-editor .box-delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box?')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	$('#box-editor .top-section-delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this top box?')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var boxCounter = ($('#box-editor .editor').length);
	var topBoxCounter = $('#top-section-boxes .top-section-box').length;


	$('#add-top-box-btn').click(function(evt){

		evt.preventDefault();
		id = ++topBoxCounter;
		var tpl = ['<div class="box top-section-box clear inner-spacer">',
					'<a href="#" class="delete top-section-delete" title="delete">Delete</a>',
					'<div class="column left column-220">',
					'<div class="clear">',
						'<label >Image (420 x 200):</label>',
					'</div>',
					'<div class="" style="width:195px; height:180px;background:#666;border:1px solid #333">',
						'<p style="text-align:center;padding:80px 40px 0px;">Image not set.</p>',
					'</div>',
					'<input type="file" name="fleet_img_' + id + '" />',
				'</div>',
					'<div class="column right column-450">',
						'<label>Text:</label>',
						'<textarea id="top-editor-' + id +'" rows="8" cols="30" name="fleet_boxes[]" class="txt column-450 top-editor"></textarea>',
						'<br /><strong>Order Display</strong>',
						'<input type="text" class="txt small" name="fleet_rankings[]" value="" />',
					'</div>',
					'<div class="clear">&nbsp;</div>',
					'</div>'];
		$(tpl.join('')).appendTo('#top-section-boxes');

		$('#top-editor-' + id).ckeditor();
	});
});

//-->
</script>