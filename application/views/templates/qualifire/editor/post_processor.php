<?php
function do_locations($section, $website, $layout_data, $redirect) {

	if($section != 'locations' || empty($_POST)) return 'Invalid data to process';

	$site_id = $website->id;

	$ci =& get_instance();

	if($ci->input->post('location_groups')) {
		//intro
		$ci->Text_Boxes_Model->clear_for_page($section, $site_id, 'intro');

		if($ci->input->post('locations_intro')) {

			$ci->Text_Boxes_Model->create(array(
					'page_uri'=>$section,
					'website_id'=>$site_id,
					'type'=>'intro',
					'content'=>$ci->input->post('locations_intro'),
					'ranking'=> null
			));
		}
		//update categories
		$boxes_cats     = $ci->input->post('loc_groups');
    $boxes_ranking  = $ci->input->post('loc_group_rankings');
		if(!is_array($boxes_cats))    $boxes_cats    = array();
    if(!is_array($boxes_ranking)) $boxes_ranking = array();
		foreach ($boxes_cats as $cat_id => $title) {
			$ci->Text_Boxes_Categories_Model->update(array(
				'title'   => $title,
				'ranking' => $boxes_ranking[$cat_id]
			), $cat_id);
		}
		$ci->session->set_flashdata('info', 'Location groups updated successfully');
		return;
	}
	//locations
	$in_l_boxes = $ci->input->post('location_boxes');
	$in_l_descs = $ci->input->post('location_desc');
	$in_l_extras = $ci->input->post('location_extras');
	$in_l_boxes_ids = $ci->input->post('location_ids');
	$in_l_boxes_rankings = $ci->input->post('location_rankings');

	if(!$in_l_boxes_ids) $in_l_boxes_ids = array();

	$current_l_boxes_ids = array_keys($layout_data['boxes']);

	//delete any deleted box!
	foreach ($current_l_boxes_ids as $l_id) {
		if(!in_array($l_id, $in_l_boxes_ids)) {
			$old_b = $layout_data['boxes'][$l_id];
			if($old_b->type !== 'location') continue;//important do not delete the intro box
			$ci->Text_Boxes_Model->delete($l_id, $site_id);
			if(strlen($old_b->image))@unlink(textbox_image_path($old_b->image));
			echo "image deleted $l_id";
		}
	}

	$box_img_num = 1;

	$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;
	//$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] = 270;
	$configThumb['height'] = 250;
	/* Load the image library */
	$ci->load->library('image_lib');

	foreach ($in_l_boxes as $key => $title) {
		$title = trim($title);
		$content = $in_l_descs[$key];
		$extra = empty($in_l_extras[$key]) ? null : $in_l_extras[$key];
		$category_id = (int) $ci->input->post('cat_id');
		if(strlen($title) == 0) continue;
		$box_data = array(
				'page_uri'=>$section,
				'website_id'=>$site_id,
				'type'=>'location',
				'content'=>$content,
				'title' => $title,
				'extra' => $extra,
				'cat_id' => $category_id,
				'ranking'=>($in_l_boxes_rankings[$key] ? $in_l_boxes_rankings[$key] : null)
		);

		$l_box_id = $in_l_boxes_ids[$key];

		$img_upload = $ci->upload->do_upload('location_img_' . $box_img_num);
		if($img_upload != FALSE) {
			$img_data = $ci->upload->data();
			if($img_data['is_image'] == 1) {
				$configThumb['source_image'] = $img_data['full_path'];
				$ci->image_lib->initialize($configThumb);
				$ci->image_lib->resize();

				//  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
				//save the data
				$box_data['image'] = $img_data['file_name'];

			}
		}

		if($l_box_id > 0) {
			$ci->Text_Boxes_Model->update($box_data, $l_box_id);
		}else{
			$ci->Text_Boxes_Model->create($box_data);
		}
		$box_img_num++;
	}

	$ci->session->set_flashdata('info', 'Locations in ' . $layout_data['selected_box_cat']->title . ' updated successfully');

}



function do_fleet($section, $website, $layout_data, $redirect) {

	if($section != 'fleet' || empty($_POST)) return 'Invalid data to process';

	$site_id = $website->id;

	$ci =& get_instance();
	//intro
	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'intro');
	if($ci->input->post('fleet_intro')) {
		$ci->Text_Boxes_Model->create(array(
				'page_uri'=>$section,
				'website_id'=>$site_id,
				'type'=>'intro',
				'content'=>$ci->input->post('fleet_intro'),
				'ranking'=> null
		));
	}
	//locations
	$in_l_boxes = $ci->input->post('fleet_boxes');
	$in_l_boxes_ids = $ci->input->post('fleet_ids');
	$in_l_boxes_rankings = $ci->input->post('fleet_rankings');

	if(!$in_l_boxes_ids) $in_l_boxes_ids = array();

	$current_l_boxes_ids = array_keys($layout_data['boxes']);
	//delete any deleted box!
	foreach ($current_l_boxes_ids as $l_id) {
		if(!in_array($l_id, $in_l_boxes_ids)) {
			$old_b = $layout_data['boxes'][$l_id];
			$ci->Text_Boxes_Model->delete($l_id, $site_id);
			if(strlen($old_b->image))@unlink(textbox_image_path($old_b->image));
		}
	}
	$box_img_num = 1;

	$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;
	//$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] = 420;
	$configThumb['height'] = 200;
	/* Load the image library */
	$ci->load->library('image_lib');

	foreach ($in_l_boxes as $key => $content) {
		$content = trim($content);
		if(strlen($content) == 0) continue;
		$box_data = array(
				'page_uri'=>$section,
				'website_id'=>$site_id,
				'type'=>'fleet',
				'content'=>$content,
				'ranking'=>($in_l_boxes_rankings[$key] ? $in_l_boxes_rankings[$key] : null)
		);

		$l_box_id = $in_l_boxes_ids[$key];

		$img_upload = $ci->upload->do_upload('fleet_img_' . $box_img_num);
		if($img_upload != FALSE) {
			$img_data = $ci->upload->data();
			if($img_data['is_image'] == 1) {
				$configThumb['source_image'] = $img_data['full_path'];
				$ci->image_lib->initialize($configThumb);
				$ci->image_lib->resize();

				//  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
				//save the data
				$box_data['image'] = $img_data['file_name'];

			}
		}

		if($l_box_id > 0) {
			$ci->Text_Boxes_Model->update($box_data, $l_box_id);
		}else{
			$ci->Text_Boxes_Model->create($box_data);
		}
		$box_img_num++;
	}


}

function do_services($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'services' || !$ci->input->post('submit')) return;

	//intro
	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'intro');
	if($ci->input->post('services_intro')) {
		$ci->Text_Boxes_Model->create(array(
				'page_uri'=>$section,
				'website_id'=>$id,
				'type'=>'intro',
				'content'=>$ci->input->post('services_intro'),
				'ranking'=> null
		));
	}


	$in_top_boxes = $ci->input->post('top_boxes');
	$in_top_boxes_ids = $ci->input->post('top_boxes_ids');
	$in_top_boxes_rankings = $ci->input->post('top_boxes_rankings');
	$in_top_boxes_titles = $ci->input->post('top_boxes_titles');

	if(!$in_top_boxes_ids) $in_top_boxes_ids = array();

	$current_top_boxes_ids = array_keys($layout_data['services_top']);
	//delete any deleted box!
	foreach ($current_top_boxes_ids as $top_id) {
		if(!in_array($top_id, $in_top_boxes_ids)) {
			$old_b = $layout_data['services_top'][$top_id];
			$ci->Text_Boxes_Model->delete($top_id, $id);
			if(strlen($old_b->image))@unlink(textbox_image_path($old_b->image));
		}
	}
	$box_img_num = 1;

	$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;
	//$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] = 450;
	$configThumb['height'] = 180;
	/* Load the image library */
	$ci->load->library('image_lib');

	foreach ($in_top_boxes as $key => $content) {
		$content = trim($content);
		if(strlen($content) == 0) continue;
		$title = $in_top_boxes_titles[$key];
		$box_data = array(
				'page_uri'=>$section,
				'website_id'=>$id,
				'type'=>'top',
				'content'=>$content,
				'title' => $title,
				'ranking'=>($in_top_boxes_rankings[$key] ? $in_top_boxes_rankings[$key] : null)
		);

		$top_box_id = $in_top_boxes_ids[$key];

		$img_upload = $ci->upload->do_upload('top_img_' . $box_img_num);
		if($img_upload != FALSE) {
			$img_data = $ci->upload->data();
			if($img_data['is_image'] == 1) {
				$configThumb['source_image'] = $img_data['full_path'];
				$ci->image_lib->initialize($configThumb);
				$ci->image_lib->resize();

				//  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
				//save the data
				$box_data['image'] = $img_data['file_name'];

			}
		}

		if($top_box_id > 0) {
			$ci->Text_Boxes_Model->update($box_data, $top_box_id);
		}else{
			$ci->Text_Boxes_Model->create($box_data);
		}
		$box_img_num++;
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_why_service($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section !='why-service' || !$ci->input->post('submit')) return;

	$content = $ci->input->post('static_content');
	$name = $ci->input->post('page_name');
	if(empty($name)) $name = ucwords(str_replace('-', ' ', $section));

	if($ci->Static_Pages_Model->has_page( $section , $id)) {
		$ci->Static_Pages_Model->update(array('content'=>$content, 'name'=>$name), $section, $id);
	}else{
		$indata = array('website_id'=>$id, 'name'=>$name,'uri'=>$section, 'content'=>$content);
		$ci->Static_Pages_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');
}

function do_faqs($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'faqs' || !$ci->input->post('submit') ) return;

	$titles = $ci->input->post('box_title'); //faq question
	$contents = $ci->input->post('box_content'); // faq answer
	$rankings = $ci->input->post('rankings'); //faq order

	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'faq');

	for($i=0; $i < count($titles); $i++) {
		if(strlen($contents[$i]) == 0 || strlen($titles[$i]) == 0 ) continue;
		$title = empty($titles[$i]) ? null : $titles[$i];
		$indata = array('ranking'=>null, 'website_id'=>$id, 'page_uri'=>$section, 'title'=>$title, 'content'=>$contents[$i]);
		$indata['ranking'] = $rankings[$i]? $rankings[$i] : $i;
		$indata['type'] = 'faq';
		$ci->Text_Boxes_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_home($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;
	$site_id = $id;

	if($section !='home' || !$ci->input->post('submit')) return;

	$config['upload_path'] = SLIDE_SHOWS_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = FALSE;

	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] =  930;
	$configThumb['height'] = 300;
	/* Load the image library */
	$ci->load->library('image_lib');

	/* We have 4 files to upload
	 * If you want more - change the 5 below as needed
	*/
	for($i = 1; $i < 5; $i++) {
		/* Handle the file upload */
		$config['file_name'] = "$id-slide-$i";
		$config['overwrite'] = TRUE;
		$ci->upload->initialize($config);
		$upload = $ci->upload->do_upload('slideshow'.$i);
		/* File failed to upload - continue */
		if($upload === FALSE){
			continue;
		}
		/* Get the data about the file */
		$data = $ci->upload->data();
		$uploadedFiles[$i] = $data;
		/* If the file is an image - create a thumbnail */
		if($data['is_image'] == 1) {
			$configThumb['source_image'] = $data['full_path'];
			$ci->image_lib->initialize($configThumb);
			$ci->image_lib->resize();

			$image = $ci->Slideshow_Images_Model->is_set($id, $i);
			if($image) {
				$ci->Slideshow_Images_Model->update(array('filename'=>$data['file_name'], 'orig_name'=>$data['client_name']), $id, $i);
			}else{
				$ci->Slideshow_Images_Model->create(array('filename'=>$data['file_name'], 'orig_name'=>$data['client_name'], 'website_id'=>$id, 'slide_id'=>$i));
			}
		}
	}


	$short_about = trim($ci->input->post('short_about'));
	$ci->load->model('Websites_Model');
	$site_update = array('short_about'=>$short_about);
	$ci->Websites_Model->update($site_update, $id);

	//upload services
	$config = array();

	$in_l_boxes = $ci->input->post('services');
	$in_l_boxes_ids = $ci->input->post('services_ids');

	if(!$in_l_boxes_ids) $in_l_boxes_ids = array();

	$current_l_boxes_ids = array_keys($layout_data['home_service_boxes']);
	//delete any deleted box!
	foreach ($current_l_boxes_ids as $l_id) {
		if(!in_array($l_id, $in_l_boxes_ids)) {
			$old_b = $layout_data['home_service_boxes'][$l_id];
			$ci->Text_Boxes_Model->delete($l_id, $site_id);
			if(strlen($old_b->image))@unlink(textbox_image_path($old_b->image));
		}
	}
	$box_img_num = 1;

	$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] = 230;
	$configThumb['height'] = 150;
	/* Load the image library */
	$ci->load->library('image_lib');

	foreach ($in_l_boxes as $key => $title) {
		$title = trim($title);
		if(strlen($title) == 0) continue;
		$box_data = array(
				'page_uri'=>$section,
				'website_id'=>$site_id,
				'type'=>'shbox',
				'title'=>$title,
				'content'=>'[NOT SET]',
				'ranking'=>$key
		);

		$l_box_id = $in_l_boxes_ids[$key];
		$config['file_name'] = $id. '-shbox-' . $ci->Websites_Model->seo_name($title);
		$ci->upload->initialize($config);

		$img_upload = $ci->upload->do_upload('shbox_img_' . $box_img_num);
		if($img_upload != FALSE) {
			$img_data = $ci->upload->data();
			if($img_data['is_image'] == 1) {
				$configThumb['source_image'] = $img_data['full_path'];
				$ci->image_lib->initialize($configThumb);
				$ci->image_lib->resize();

				//  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
				//save the data
				$box_data['image'] = $img_data['file_name'];

			}
		}

		if($l_box_id > 0) {
			$ci->Text_Boxes_Model->update($box_data, $l_box_id);
		}else{
			$ci->Text_Boxes_Model->create($box_data);
		}
		$box_img_num++;
	}


	$ci->session->set_flashdata('info', 'Update saved successfully');

}
