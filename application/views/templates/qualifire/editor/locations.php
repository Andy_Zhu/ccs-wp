<?php
$location_intro_box = null;

foreach ($boxes as $box) {
	if($box->type == 'intro') {
		$location_intro_box = $box;
		break;
	}
}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<?php if (false)://comment at php level :)?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-sortable.min.js"></script>
<?php endif;?>

<div class="" id="box-editor">

<?php if(!isset($_GET['cat_id'])): //we adding/editting a group!?>

<form method="post" enctype="multipart/form-data" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
		<div class="clear"></div>

<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Introduction:</h2>
<div style="margin-bottom:20px;">
	<textarea name="locations_intro" style="width:735px; height:80px;" rows="5" cols="30" class="txt large"><?php echo $location_intro_box ? $location_intro_box->content : '';?></textarea>
</div>

<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Location groups:</h2>
	<div id="groups">
	<?php foreach ($boxes_cats as $cat):?>
		<div class="group" style="padding:10px; background:#fff;border:1px solid #ccc;margin-bottom:5px">
				<div style="width:300px;float:left;">
					<input type="text" name="loc_groups[<?php echo $cat->id;?>]" value="<?php echo $cat->title;?>" class="txt" />
					<label>Rank:</label><input type="text" name="loc_group_rankings[<?php echo $cat->id;?>]" value="<?php echo $cat->ranking;?>" class="txt small" />
				</div>
				<div style="float:right">
					<a class="" href="<?php echo site_url("admin/edit_website?id={$id}&section={$section}&cat_id={$cat->id}")?>">View/Edit Locations</a> |
					<a class="del" href="<?php echo site_url('admin/ajax/textbox/deletecat/' . $cat->id);?>">Delete</a>

				</div><div style="clear:both"></div>
			</div>

	<?php endforeach;?>
	</div>

	<div style="margin-bottom:5px; padding:5px; background:#ccc">
			<input type="text" id="box_cat" class="txt large" style="width:580px;" />
			<input type="hidden" id="website_id" value="<?php echo $id?>"/>
			<input type="hidden" id="section" value="<?php echo $section;?>"/>
			<input type="hidden" id="submit_url" value="<?php echo site_url("admin/ajax/textbox/createcat/$id")?>"/>
			<input type="button" id="add_loc_btn" name="add_loc_btn" class="orange-btn" value="Add Group" />
	</div>


	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="hidden" name="location_groups" value="true"/>
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>



	<script type="text/javascript">
		$(document).ready(function() {
			$('#box_cat').focus(function(){
				$(this).css('border', '1px solid #ccc');
			});
			$('#add_loc_btn').click(function(evt){
				evt.preventDefault();
				if($('#box_cat').val() == '') {
					$('#box_cat').css('border', '1px solid #e30');
					return;
				}
				$.ajax({
					type:'POST',
					url: $('#submit_url').val(),
					data:{website_id: $('#website_id').val(), section:$('#section').val(), title:$('#box_cat').val() },
					beforeSend:function() {
						$('#add_loc_btn').attr('disabled', 'disabled');
					},
					success:function(json) {

						if(json.success) {
							$('#box_cat').val('');
							var cat = json.category;
							var html = [
								'<div class="group" style="padding:10px; background:#fff;border:1px solid #ccc;margin-bottom:5px">',
								'<div style="width:300px;float:left;">',
								'<input type="text" name="loc_groups[' + cat.id + ']" value="' + cat.title + '" class="txt" />',
								'</div>',
								'<div style="float:right">',
									'<a href="' + cat.edit_url + '" class="">Add Locations</a> | ',
									'<a class="del" href="' + cat.delete_url + '">Delete</a>',
								'</div><div style="clear:both"></div>',
								'</div>'
							];
							$('#groups').append($(html.join("\n")));
						}
					},
					error:function(json) {
					},
					complete:function(){
						$('#add_loc_btn').removeAttr('disabled');
					}
				});
			});

			//delete groups
			$('#groups a.del').live('click', function(evt) {
				evt.preventDefault();
				$this = $(this);
				if(confirm("Are you sure you want to delete loction group?")) {
					$.get($this.attr('href'), function(json){
						if(json.success) {
							$this.parents('.group').slideUp(function(){
								$(this).remove();
							});
						}else{
							alert('Failed to delete group');
						}
					});
				}
			});

		});
	</script>

<?php else: // we are adding/edditing a locations to a group?>

<form method="post" enctype="multipart/form-data" action="<?php echo site_url("admin/edit_website?id=$id&section=$section&cat_id=$selected_box_cat->id")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
	<div id="hidden-data"><!-- DON'T REMOVE THSE FIELDS! -->
		<input type="hidden" name="cat_id" value="<?php echo $selected_box_cat->id?>" />
		<input type="hidden" name="meta_description" value="<?php if($page) echo $page->meta_description?>"/>
		<input type="hidden" name="meta_keywords" value="<?php if($page) echo $page->meta_keywords?>"/>
		<input type="hidden" name="meta_title" value="<?php if($page) echo $page->meta_title?>"/>
		<input type="hidden" name="page_note" value="<?php if($page) echo $page->note?>"/>
	</div>

<div id="boxes-container">

	<div style="padding:5px; position:relative; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">
		<h2>Locations <?php if($selected_box_cat) echo ' In <span style="color:#000">' . $selected_box_cat->title .'</span>'?> (<?php echo count($boxes)?>):</h2>
		<div style="position:absolute;right:1px;top:1px;">
			Add locations to:<select id="groups-navi" class="txt med">
				<?php foreach ($boxes_cats as $cat):?>
					<option <?php if($cat->id == $selected_box_cat->id) echo 'selected="selected"'?> value="<?php echo $cat->id?>"><?php echo $cat->title;?></option>
				<?php endforeach;?>
			</select>
			<a href="<?php echo site_url('admin/edit_website?id=' . $id .'&section=' . $section);?>" class="orange-btn">Edit Groups &rarr;</a>
		</div>
	</div>
	<div id="top-section-boxes">
		<?php if(isset($boxes) && !empty($boxes)):?>
			<?php $top_num=1; foreach ($boxes as $location):?>
			<?php if($location->type == 'intro') continue;?>
			<div class="box column-220 left inner-spacer">
				<a href="#" class="delete top-section-delete" title="delete">Delete</a>
				<div class="column left column-220">
					<div class="clear">
						<label >Image (270 x 250):</label>
					</div>
					<div class="" style="width:195px; height:180px;background:#666;border:1px solid #333">

						<?php if(strlen($location->image) && file_exists(textbox_image_path($location->image))):?>
						<img style="width:195px; height:180px;" src="<?php echo textbox_image_src($location->image);?>" />
						<?php else:?>
							<p style="text-align:center;padding:80px 40px 0px;">Image not set.</p>
						<?php endif;?>
					</div>
					<input type="file" name="location_img_<?php echo $top_num;?>" />
				</div>
				<div class="column column-220 clear">
					<label>Title:</label>
					<input type="hidden" name="location_ids[]" value="<?php echo $location->id;?>" />
					<input type="text" name="location_boxes[]" class="txt med" value="<?php echo $location->title;?>"/>
					<label>Shortname:</label>
					<textarea name="location_desc[]" style="width:200px;" rows="2" cols="20" class="txt med"><?php echo $location->content;?></textarea>
					<label>URL:</label>
					<input type="text" class="txt" style="width:200px;" name="location_extras[]" value="<?php echo $location->extra;?>"/>
					<label>Order Display (999 for country)</label>
					<input type="text" class="txt small" name="location_rankings[]" value="<?php echo $location->ranking;?>" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<?php $top_num++; endforeach;?>
		<?php else:?>
			<div class="box column-220 left inner-spacer">
				<a href="#" class="delete top-section-delete" title="delete">Delete</a>
				<div class="column left column-220">
					<div class="clear">
						<label >Image (195 x 180):</label>
					</div>
					<div class="" style="width:195px; height:180px;background:#666;border:1px solid #333">
						<p style="text-align:center;padding:80px 40px 0px;">Image not set.</p>
					</div>
					<input type="file" name="location_img_1" />
				</div>
				<div class="column clear column-220">
					<label>Title:</label>
					<input type="text" name="location_boxes[]" class="txt med"/>
					<label>Short Desc:</label>
					<textarea name="location_desc[]" style="width:200px;" rows="2" cols="20" class="txt med"></textarea>
					<label>URL:</label>
					<input type="text" class="txt" style="width:200px;" name="location_extras[]" />
					<label>Order Display</label>
					<input type="text" class="txt small" name="location_rankings[]" value="" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		<?php endif;?>
	</div>

	<div class="clear row"><a href="#nowhere" id="add-location-box-btn">+ New Location Box</a></div>

	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>


</div>
<?php endif;?>
</form>
<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container textarea.editor').ckeditor();
	$('#boxes-container textarea.top-editor').ckeditor();

	$('#box-editor .box-delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box?')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	$('#box-editor .top-section-delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this top box?')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var locationBoxCounter = $('#top-section-boxes .box').length;


	$('#add-location-box-btn').click(function(evt){

		evt.preventDefault();
		id = ++locationBoxCounter;
		var tpl = ['<div class="box column-220 left inner-spacer">',
					'<a href="#" class="delete top-section-delete" title="delete">Delete</a>',
					'<div class="column left column-220">',
						'<div class="clear">',
							'<label >Image (195 x 180):</label>',
						'</div>',
						'<div class="" style="width:195px; height:180px;background:#666;border:1px solid #333">',
							'<p style="text-align:center;padding:80px 40px 0px;">Image not set.</p>',
						'</div>',
						'<input type="file" name="location_img_' + id + '" />',
					'</div>',
					'<div class="column clear column-220">',
						'<label>Title:</label>',
						'<input type="text" name="location_boxes[]" class="txt med"/>',
						'<label>Short Desc:</label>',
						'<textarea name="location_desc[]" style="width:200px;" rows="2" cols="20" class="txt med"></textarea>',
						'<label>URL:</label>',
						'<input type="text" class="txt" style="width:200px;" name="location_extras[]"/>',
						'<label>Order Display</label>',
						'<input type="text" class="txt small" name="location_rankings[]" value="" />',
					'</div>',
					'<div class="clear">&nbsp;</div>',
					'</div>'];
		$(tpl.join('')).appendTo('#top-section-boxes');

		$('#top-editor-' + id).ckeditor();
	});


	//quick category navi
	$('#groups-navi').change(function(){

		var cat_id = $(this).val();

		var go_to = window.location.href.replace(/cat_id=[0-9]+/, 'cat_id=' + cat_id);
		window.location = go_to;

	});
});


//-->
</script>