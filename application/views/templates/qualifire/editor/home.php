<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="row" id="home-editor">
	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>" enctype="multipart/form-data">
		<div class="column left" style="margin-left:0px;">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>

		<div class="row">
			<h2 style="padding:5px 10px;margin-bottom:10px; background:#333; color:#ccc;">Slideshow images: (<?php echo $website->template =='qualifire' ? '930 X 300' :'678 X 269' ?>)</h2>
			<?php for($i=1; $i<5; $i++):?>
					<div class="image-form" style="width:343px; <?php if($i%2 == 0) echo 'margin-right:0px;'?>">

						<?php $image = array_key_exists($i, $images) ? $images[$i] : null;?>

						<div class="img-preview" id="slideshow">
							<?php if($image):?>
								<a id="<?php echo $i?>" href="<?php echo slideshow_image_src($image->filename);?>" class="lage-image" rel="gallery">
									<img src="<?php echo slideshow_image_src($image->filename);?>" class="thumb" />
								</a>
								<div class="hint">
								Click on thumbnail to view fullsize image
								</div>
							<?php else:?>
								<span class="not-set">NOT SET</span>
							<?php endif;?>
						</div>
						<div class="input">
							#<?php echo $i;?> <input type="file" name="slideshow<?php echo $i?>" />
						</div>

					</div>
				<?php endfor;?>
		</div>
		<div class="row">
			<h2 style="padding:5px 10px; margin-bottom:10px; background:#333; color:#ccc;">About <?php echo $website->name;?> (short)</h2>
			<textarea id="short_about" rows="5" cols="40" name="short_about" class="txt large editor"><?php echo $website->short_about;?></textarea>
		</div>

		<div class="row">
			<h2 style="padding:5px 10px; margin-bottom:10px; background:#333; color:#ccc;">Included Services</h2>

			<div id="services">
				<?php if(isset($home_service_boxes) && !empty($home_service_boxes)):?>
					<?php $counter=1;foreach ($home_service_boxes as $sbox):?>
						<div class="sbox" style="background:#ccc; padding:10px; width:220px; height:210px;float:left;margin-right:10px;margin-bottom:10px;position:relative;">
						<a href="#no" class="del" style="position:absolute;right:5px; top:3px;">Delete</a>
						<label>Service: <input value="<?php echo $sbox->title;?>" type="text" name="services[]" class="txt med" style="width:200px;"  /></label>
						<div class="cimage" style="width:210px;background:#666;height:100px;">
							<?php if(!empty($sbox->image)):?>
								<img style="width:210px; height:100px;margin:0;padding:0" src="<?php echo textbox_image_src($sbox->image);?>"/>
							<?php else:?>
								<p style="text-align:center; padding-top:30px;color:#ccc">
								Current Image<br> Not set
								</p>
							<?php endif;?>
						</div>
						<label>Image (230x150): <input type="file" name="shbox_img_<?php echo $counter++;?>" class="txt" style="width:200px;" /></label>
						<input type="hidden" name="services_ids[]" value="<?php echo $sbox->id?>" />
						</div>
					<?php endforeach;?>
				<?php else:?>
				<div class="sbox" style="background:#ccc; padding:10px; width:220px; height:210px;float:left;margin-right:10px;margin-bottom:10px;position:relative;">
					<a href="#no" class="del" style="position:absolute;right:5px; top:3px;">Delete</a>
					<label>Service: <input type="text" name="services[]" class="txt med" style="width:200px;"  /></label>
					<div class="cimage" style="width:210px;background:#666;height:100px;">
						<p style="text-align:center; padding-top:30px;color:#ccc">
							Current Image<br> Not set
						</p>
					</div>
					<label>Image (230x150): <input type="file" name="shbox_img_1" class="txt" style="width:200px;" /></label>
				</div>
				<?php endif;?>
			</div>

			<div class="row">
				<a href="#nowhere" id="add-service-btn">+ Add Service</a>
			</div>
		</div>
	<div class="row" style="border-top:2px solid #333; padding-top:10px;">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>


	</form>

</div>

<style type="text/css">
<!--
@import url("<?php echo CSS_PATH?>jquery.lightbox-0.5.css");
-->
</style>
<script type="text/javascript" src="<?php echo JS_PATH?>jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#home-editor .editor').ckeditor();

	$(function() {
		$('#slideshow a').lightBox({fixedNavigation:true});
	});

	$('#services .sbox .del').live('click', function(evt) {
		evt.preventDefault();
		if(confirm("Are you sure you want to delete this service?")) {
			$sbox = $(this).parent('.sbox');
			$sbox.animate({width:'hide', height:'hide'}, function(el){
				$sbox.remove();
			});
		}
	});

	var shboxCounter = $('#services .sbox').length;
	var sBoxTpl = $('#services .sbox:first').clone();
	sBoxTpl.find('.cimage').html('<p style="text-align:center; padding-top:30px;color:#ccc">Current Image<br>Not set.</p>');
	sBoxTpl.find('input[type=text]').val('');
	sBoxTpl.find('input[name="services_ids[]"]').remove();

	$("#add-service-btn").click(function(evt) {

		evt.preventDefault();
		var newBox = sBoxTpl.clone();
		newBox.find('input[type=file]').attr('name', 'shbox_img_' + (++shboxCounter));
		$('#services').append(newBox);

	});

});


//-->
</script>