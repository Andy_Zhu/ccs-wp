<?php $is_preview = TRUE;
function template_js_src($filename, $preview=true, $tempate="default") {

	if($preview) {
		return 'http://localhost/carservicecms/' . 'application/views/templates/' . $tempate .'/js/' . $filename;
	}

	return "/js/$filename";
}
function template_css_src($filename, $preview=true, $template="default") {

	if($preview) {
		return 'http://localhost/carservicecms/' . 'application/views/templates/' . $template . '/css/' . $filename;
	}

	return "/css/$filename";
}
function template_image_src($filename, $preview=true, $template="default") {

	if($preview) {
		return 'http://localhost/carservicecms/' . 'application/views/templates/' . $template . '/images/' . $filename;
	}

	return "/images/$filename";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title> China Car Service</title>


<!--[if IE 6]>
    <script  type="text/javascript" src="<?php echo template_js_src("DD_belatedPNG_0.0.8a-min.js", $is_preview, "qualifire")?>"></script>
    <script  type="text/javascript">
    // <![CDATA[
	DD_belatedPNG.fix('.pngfix, img, #home-page-content li, #page-content li, #bottom li, #footer li, #recentcomments li span');
    // ]]>
    </script>
<![endif]-->


<link rel='stylesheet' id='nivo-slider-css'  href='<?php echo template_js_src("shortcodes-ultimate/css/nivoslider.css", $is_preview, "qualifire");?>?ver=3.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='jcarousel-css'  href='<?php echo template_js_src("shortcodes-ultimate/css/jcarousel.css", $is_preview, "qualifire");?>?ver=3.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='shortcodes-ultimate-css'  href='<?php echo template_js_src("shortcodes-ultimate/css/style.css", $is_preview, "qualifire");?>?ver=3.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='<?php echo template_css_src("bootstrap/css/bootstrap.css", $is_preview, "qualifire");?>?ver=3.4.12' type='text/css' media='screen' />
<link rel='stylesheet' id='js_composer_front-css'  href='<?php echo template_css_src("js_composer_front.css", $is_preview, "qualifire")?>?ver=3.4.12' type='text/css' media='screen' />
<link rel='stylesheet' id='layerslider_css-css'  href='<?php echo template_css_src("layerslider.css", $is_preview, "qualifire")?>?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='reset-css'  href='<?php echo template_css_src("common-css/reset.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='text-css'  href='<?php echo template_css_src("text.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='grid-960-css'  href='<?php echo template_css_src("common-css/960.css", $is_preview, "qualifire");?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='superfish_menu-css'  href='<?php echo template_js_src("superfish-1.4.8/css/superfish.css", $is_preview, "qualifire");?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='pagination-css'  href='<?php echo template_js_src("pagination/pagenavi-css.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='pretty_photo-css'  href='<?php echo template_js_src("prettyPhoto/css/prettyPhoto.css", $is_preview, "qualifire");?>?ver=3.1.2' type='text/css' media='screen' />
<link rel='stylesheet' id='style-css'  href='<?php echo template_css_src("style.css", $is_preview, "qualifire")?>?ver=1.0' type='text/css' media='screen' />
<link rel='stylesheet' id='jquery-foobar-2-3-css'  href='<?php echo template_css_src("jquery.foobar.2.3.css", $is_preview, "qualifire")?>?ver=2.3.3' type='text/css' media='all' />

<script type='text/javascript' src='<?php echo template_js_src("jquery-1.4.4.min.js", $is_preview, "qualifire")?>?ver=3.5.1'></script>
<script type='text/javascript' src='<?php echo template_js_src("shortcodes-ultimate/js/jwplayer.js", $is_preview, "qualifire")?>?ver=3.9.5'></script>
<script type='text/javascript' src='<?php echo template_js_src("shortcodes-ultimate/js/jcarousel.js", $is_preview, "qualifire")?>?ver=3.9.5'></script>
<script type='text/javascript' src='<?php echo template_js_src('shortcodes-ultimate/js/init.js', $is_preview,"qualifire")?>?ver=3.9.5'></script>
<script type='text/javascript' src='<?php echo template_js_src("layerslider.kreaturamedia.jquery.js", $is_preview, "qualifire");?>?ver=3.5.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("jquery.easing.1.3.js", $is_preview, "qualifire")?>?ver=1.3.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("encoder.js", $is_preview, "qualifire")?>?ver=2.3.3'></script>
<script type='text/javascript' src='<?php echo template_js_src("cufon/cufon-yui.js", $is_preview, "qualifire")?>?ver=1.09i'></script>
<script type='text/javascript' src='<?php echo template_js_src("cufon/eurofurence_500-eurofurence_700.font.js", $is_preview, "qualifire")?>?ver=1.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("jquery.cycle.all.min.js", $is_preview, "qualifire")?>?ver=2.86'></script>
<script type='text/javascript' src='<?php echo template_js_src("cycle1_script.js", $is_preview, "qualifire")?>?ver=1.0.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("prettyPhoto/js/jquery.prettyPhoto.js", $is_preview, "qualifire")?>?ver=3.1.2'></script>
<script type='text/javascript' src='<?php echo template_js_src("jquery-validate/jquery.validate.min.js", $is_preview, "qualifire")?>?ver=1.6'></script>
<script type='text/javascript' src='<?php echo template_js_src("masked-input-plugin/jquery.maskedinput.min.js", $is_preview, "qualifire")?>?ver=1.2.2'></script>
<script type='text/javascript' src='<?php echo template_js_src("superfish-1.4.8/js/superfish.js", $is_preview, "qualifire")."?ver=1.4.8"?>'></script>
<script type='text/javascript' src='<?php echo template_js_src("superfish-1.4.8/js/supersubs.js", $is_preview, "qualifire")?>?ver=0.2.0'></script>
<script type='text/javascript' src='<?php echo template_js_src("script.js", $is_preview, "qualifire")."?ver=1.0"?>'></script>

<link rel='next' title='Booking Confirmation' href='http://www.chinacarservice.com/booking-confirmation/' />
<link rel='canonical' href='http://www.chinacarservice.com/' />
<style type='text/css'>#respond, #commentform, #addcomment, .entry-comments { display: none;}</style>

    <style type='text/css'> #disabled_msgCM {width: 300px; border: 1px solid red; }</style>
 <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php  echo template_css_src("common-css/ie-all.css", $is_preview, "qualifire")?>" media="screen" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
    <link rel="stylesheet" href="<?php  echo template_css_src("common-css/ie6-7.css", $is_preview, "qualifire")?>" media="screen" type="text/css" />
<![endif]-->
<!--[if IE 6]>
    <link rel="stylesheet" href="<?php  echo template_css_src("common-css/ie6.css", $is_preview, "qualifire")?>" media="screen" type="text/css" />
    <style type="text/css">
	body{ behavior: url("<?php  echo template_js_src("csshover3.htc", $is_preview, "qualifire")?>"); }
    </style>
<![endif]-->

</head>
<body class="home page page-id-2 page-template-default wpb-js-composer js-comp-ver-3.4.12">
    <div id="wrapper-1" class="pngfix">
	<div id="top-container" class="container_24">
		
	    <div class="clear"></div>
	    <div id="top" class="grid_24">
		<div id="logo" class="grid_14">
		    <h1>
			<a class="pngfix" style="background: transparent url( http://www.chinacarservice.com/wp-content/uploads/2013/01/car-copy3082.png ) no-repeat 0 100%; width:308px; height:74px;" href="http://www.chinacarservice.com">
			    China Car Service</a>
		    </h1>
		</div>
		<div id="slogan" class="grid_17" style="top:100px; left:50px;"></div>
		<!-- end logo slogan -->
		<div id="search" class="grid_7 prefix_17">
		    <form action="http://www.chinacarservice.com/" method="get">
			<div class="search_box">
			    <input id="search_field" name="s" type="text" class="inputbox_focus inputbox pngfix" value="" />

			</div>
		    </form>
		</div><!-- end search -->
		<div class="phone-number grid_7 prefix_17">
Call us at +86 755 2595 1800		</div>
		<!-- end top-icons -->
	    </div>
	    <!-- end top -->

	    <div class="clear"></div>

	    <div id="dropdown-holder" class="grid_24">
		<div class="nav_bg pngfix">
<div class="menu-main-menu-container"><ul id="menu-main-menu" class="sf-menu"><li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5"><a href="http://www.chinacarservice.com/"><span>Home</span></a></li>
<li id="menu-item-2335" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2335"><a href="http://www.chinacarservice.com/about-us/"><span>About Us</span></a>
<ul class="sub-menu">
	<li id="menu-item-2342" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2342"><a href="http://www.chinacarservice.com/why-china-car-service/"><span>Why China Car Service</span></a></li>
</ul>
</li>
<li id="menu-item-2359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2359"><a href="http://www.chinacarservice.com/fleet/"><span>Fleet</span></a></li>
<li id="menu-item-2341" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2341"><a href="http://www.chinacarservice.com/services/"><span>Services</span></a></li>
<li id="menu-item-2338" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2338"><a href="http://www.chinacarservice.com/locations/"><span>Locations</span></a></li>
<li id="menu-item-2339" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2339"><a href="http://www.chinacarservice.com/online-booking/"><span>Online Booking</span></a></li>
<li id="menu-item-2349" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2349"><a href="http://www.chinacarservice.com/faq/"><span>FAQs</span></a>
<ul class="sub-menu">
<li id="menu-item-2340" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2340"><a href="http://www.chinacarservice.com/privacy-policy/"><span>Policies And Terms</span></a></li>
</ul>
</li>
<li id="menu-item-2336" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2336"><a href="http://www.chinacarservice.com/contact-us/"><span>Contact Us</span></a></li>
</ul>
</div>		    


		</div>
	    </div><!-- end dropdown-holder -->
	</div>
	<!-- end top-container -->

	    <div class="clear"></div>
<div id="page-content">	    

<div id="header-content" class="container_24">
			<div id="page-title">
			    <h2><cufon class="cufon cufon-canvas" alt="Why " style="width: 69px; height: 34px;"><canvas width="114" height="36" style="width: 114px; height: 36px; top: 0px; left: -4px;"></canvas><cufontext>Why </cufontext></cufon><cufon class="cufon cufon-canvas" alt="China " style="width: 76px; height: 34px;"><canvas width="122" height="36" style="width: 122px; height: 36px; top: 0px; left: -4px;"></canvas><cufontext>China </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Car " style="width: 52px; height: 34px;"><canvas width="97" height="36" style="width: 97px; height: 36px; top: 0px; left: -4px;"></canvas><cufontext>Car </cufontext></cufon><cufon class="cufon cufon-canvas" alt="Service" style="width: 94px; height: 34px;"><canvas width="131" height="36" style="width: 131px; height: 36px; top: 0px; left: -4px;"></canvas><cufontext>Service</cufontext></cufon></h2>
		    </div>
		    </div>
		    
<div id="content-container" class="container_24">
    <div id="main-content" class="grid_16">
	<div class="main-content-padding">

		<div class="post" id="post-2">
		    <div class="entry">
<div class="row-fluid">
<div class="wpb_content_element span12 column_container">
<div class="wpb_wrapper">
<div class="row-fluid">
<div class="wpb_content_element span12 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: justify;"><strong>China Car Service</strong> is a premium quality limousine service and ground transportation company for travelers visiting China for business or leisure. We understand the importance of quality and prompt, high-level service required by discerning international business travelers, and we tailor every aspect of our business to exceeding those expectations.</p>
<p style="text-align: justify;">Our services include private car airport transfers, chauffeured car rental limo service, Airport VIP Meet &amp; Assist, intercity transfers and more.  Many of our drivers speak English and we also provide professional English speaking guides and business assistants so you can fully comfortably enjoy your China travel. With operations in over 40 major cities across China, we provide you professional ground transportation support wherever you travel while here.</p>
<h3 style="text-align: center;">Our locations include</h3>
<p>&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="text-align: justify;" width="117" height="28"><strong>Beijing</strong></td>
<td style="text-align: justify;" width="117"><strong>Chengdu</strong></td>
<td style="text-align: justify;" width="117"><strong>Chongqing</strong></td>
<td style="text-align: justify;" width="117"><strong>Dalian</strong></td>
<td style="text-align: justify;" width="117"><strong>Guangzhou</strong></td>
<td style="text-align: justify;" width="117"><strong>Hangzhou</strong></td>
</tr>
<tr>
<td style="text-align: justify;" height="28"><strong>Hong Kong</strong></td>
<td style="text-align: justify;"><strong>Harbin</strong></td>
<td style="text-align: justify;"><strong>Macau</strong></td>
<td style="text-align: justify;"><strong>Nanjing</strong></td>
<td style="text-align: justify;"><strong>Qingdao</strong></td>
<td style="text-align: justify;"><strong>Shanghai</strong></td>
</tr>
<tr>
<td style="text-align: justify;" height="28"><strong>Shenzhen</strong></td>
<td style="text-align: justify;"><strong>Suzhou</strong></td>
<td style="text-align: justify;"><strong>Tianjin</strong></td>
<td style="text-align: justify;"><strong>Taipei</strong></td>
<td style="text-align: justify;"><strong>Wuhan</strong></td>
<td style="text-align: justify;"><strong>Xi&#8217;an</strong></td>
</tr>
<tr>
<td style="text-align: center;" colspan="6" height="28"><span style="color: #000000;"><em><strong><a title="Locations" href="http://www.chinacarservice.com/locations-2/"><span style="color: #000000;">Browse all our locations across China here</span></a></strong></em></span></td>
</tr>
</tbody>
</table>
<h3 style="text-align: center;">Our services include</h3>
</p></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_content_element span4 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><span style="font-family: book antiqua,palatino;"> <span style="color: #000000;"><span style="color: #000000;">Private Airport Transfer</span></span></span></a></p>
<hr style="width: 150px;" width="150" />
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><img class="size-full wp-image-1946 aligncenter" style="border: 2px solid black;" alt="China Airport Transfer" src="http://chinacarservice.com/wp-content/uploads/2012/12/China-Airport-Transfer.jpg" width="230" height="150" /></a></p>
</p></div>
</p></div>
<div class="wpb_content_element span4 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><span style="color: #000000;"><span style="font-family: book antiqua,palatino; color: #000000;">Airport Meet &amp; Assist</span></span></a></p>
<hr style="width: 150px;" width="150" />
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><img class="size-full wp-image-1947 aligncenter" style="border: 2px solid black;" alt="VIP Meet and Assist" src="http://chinacarservice.com/wp-content/uploads/2012/12/VIP-Meet-and-Assist.jpg" width="230" height="150" /></a></p>
</p></div>
</p></div>
<div class="wpb_content_element span4 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a href="http://www.chinacarservice.com/services/"><span style="color: #000000;"><span style="font-family: book antiqua,palatino; color: #000000;">Event Transportation</span></span></a></p>
<hr style="width: 150px;" width="150" />
<p style="text-align: center;"><a href="http://www.chinacarservice.com/services/"><img class="size-full wp-image-1948 aligncenter" style="border: 2px solid black;" title="Event Transportation" alt="Event Transportation" src="http://chinacarservice.com/wp-content/uploads/2012/12/events.png" width="230" height="150" /></a></p>
</p></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_content_element span4 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><span style="color: #000000;"><span style="font-family: book antiqua,palatino; color: #000000;">Private Guided Tour</span></span></a></p>
<hr style="width: 150px;" width="150" />
<p><a title="Services" href="http://www.chinacarservice.com/services/"><img class="size-full wp-image-1949 aligncenter" style="border: 2px solid black;" title="Private Car and Driver Tour" alt="Private Car and Driver Tour" src="http://chinacarservice.com/wp-content/uploads/2012/12/Private-Tour.jpg" width="230" height="150" /></a></p></div>
</p></div>
<div class="wpb_content_element span4 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><span style="color: #000000;"><span style="font-family: book antiqua,palatino; color: #000000;">Professional Limousine Service</span></span></a></p>
<hr style="width: 150px;" width="150" />
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><img class="size-full wp-image-1945 aligncenter" style="border: 2px solid black;" alt="China Limousine Service" src="http://chinacarservice.com/wp-content/uploads/2012/12/On-hire-Car-Driver4.jpg" width="230" height="150" /></a></p>
</p></div>
</p></div>
<div class="wpb_content_element span4 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"> <span style="color: #000000;"><span style="font-family: book antiqua,palatino; color: #000000;">Experienced English Translators</span></span></a></p>
<hr style="width: 150px;" width="150" />
<p style="text-align: center;"><a title="Services" href="http://www.chinacarservice.com/services/"><img class="size-full wp-image-1950 aligncenter" style="border: 2px solid black;" title="English Translator" alt="English Translator" src="http://chinacarservice.com/wp-content/uploads/2012/12/English-Translator.jpg" width="230" height="150" /></a></p>
</p></div>
</p></div>
</p></div>
</p></div>
</p></div>
</p></div>
		    </div>
		</div>
	    <div class="clear"></div>
	</div><!-- end main-content-padding -->
    </div><!-- end main-content -->


<div id="sidebar" class="grid_8">
	    <div id="sidebarSubnav">

<div id="text-11" class="widget widget_text substitute_widget_class">			<div class="textwidget"><div class="row-fluid">
<div class="wpb_content_element span12 wpb_text_column">
<div class="wpb_wrapper">
<p style="text-align: center;"><a href="http://www.chinacarservice.com/fleet/"><img class="size-full wp-image-1885 aligncenter" alt="Business Sedan" src="http://chinacarservice.com/wp-content/uploads/2013/02/Business-Sedan.jpg" width="265" height="30" /></a></p>
</p></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_gallery wpb_content_element span12">
<div class="wpb_wrapper">
<div class="wpb_gallery_slides wpb_flexslider flexslider_slide flexslider" data-interval="0" data-flex_fx="slide">
<ul class="slides">
<li><img src="http://www.chinacarservice.com/wp-content/uploads/2013/02/AUDI-SEDAN-CHINA.png" width="300" height="180" /></li>
</ul>
</div></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_content_element span12 wpb_text_column">
<div class="wpb_wrapper">
<div class="row-fluid">
<div class="wpb_content_element span12 wpb_text_column">
<div class="wpb_wrapper"></div>
</p></div>
</p></div>
<p style="text-align: center;"><a href="http://www.chinacarservice.com/fleet/"><img class="size-full wp-image-1887 aligncenter" alt="Luxury Sedan" src="http://chinacarservice.com/wp-content/uploads/2013/02/Luxury-Sedan.jpg" width="265" height="30" /></a></p>
</p></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_gallery wpb_content_element span12">
<div class="wpb_wrapper">
<div class="wpb_gallery_slides wpb_flexslider flexslider_slide flexslider" data-interval="0" data-flex_fx="slide">
<ul class="slides">
<li><img src="http://www.chinacarservice.com/wp-content/uploads/2013/02/MERCEDES-SEDAN-CHINA.png" width="300" height="180" /></li>
</ul>
</div></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_content_element span12 wpb_text_column">
<div class="wpb_wrapper">
<div class="row-fluid">
<div class="wpb_content_element span12 wpb_text_column">
<div class="wpb_wrapper"></div>
</p></div>
</p></div>
<p style="text-align: center;"><a href="http://www.chinacarservice.com/fleet/"><img class="size-full wp-image-1895 aligncenter" alt="Minivan" src="http://chinacarservice.com/wp-content/uploads/2013/02/Minivan.jpg" width="265" height="30" /></a></p>
</p></div>
</p></div>
</p></div>
<div class="row-fluid">
<div class="wpb_gallery wpb_content_element span12">
<div class="wpb_wrapper">
<div class="wpb_gallery_slides wpb_flexslider flexslider_slide flexslider" data-interval="0" data-flex_fx="slide">
<ul class="slides">
<li><img src="http://www.chinacarservice.com/wp-content/uploads/2013/02/Minivan-China-300x180.png" width="300" height="180" /></li>
</ul>
</div></div>
</p></div>
</p></div>
</div>
		</div><div id="text-10" class="widget widget_text substitute_widget_class">			<div class="textwidget"></div>
		</div>	    </div>
	    <!-- end sidebarSubnav -->
	</div>
<!-- end sidebar -->


</div><!-- end content-container -->

<div class="clear"></div>



</div><!-- end page-content -->

<div class="clear"></div>
<div id="bottom-bg">
<div id="bottom" class="container_24">
<div id='bottom_1' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_text substitute_widget_class"><h3 class="bottom-col-title">About Us</h3>			<div class="textwidget"><p style="text-align: justify;">China Car Service is a full service ground transportation provider that offers you complete comfort, control and convenience during your visit to China!</p>
<p>♦ Easy online booking<br />
♦ Simple payment options<br />
♦ 24 hour customer service<br />
♦ Service in every major city</p>
<p></p>
</div>
</div></div></div><!-- end bottom_1 --><div id='bottom_2' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_text substitute_widget_class"><h3 class="bottom-col-title">Contact</h3>			<div class="textwidget"><p style="text-align: center;"><img class="aligncenter size-full wp-image-1431" alt="MagentoEx1" src="http://chinacarservice.com/wp-content/uploads/2013/01/MagentoEx1.png" width="200" height="88" /></p>
<p style="text-align: justify;"><strong>China Car Service</strong><br />
<label>Customer Service Center</label><br />
<label>bookings@chinacarservice.com</label><br />
<label>Tel: +86 755 2595 1800</label></p>
</div>
</div></div></div><!-- end bottom_2 --><div id='bottom_3' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_nav_menu custom-formatting"><h3 class="bottom-col-title">Services</h3><div class="menu-services-container"><ul id="menu-services" class="menu"><li id="menu-item-1381" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1381"><a href="http://www.chinacarservice.com/services/">Airport Pick-Up</a></li>
<li id="menu-item-1382" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1382"><a href="http://www.chinacarservice.com/services/">Private Aviation</a></li>
<li id="menu-item-1383" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1383"><a href="http://www.chinacarservice.com/services/">Events and Meetings</a></li>
<li id="menu-item-1387" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1387"><a href="http://www.chinacarservice.com/services/">Long Term Car Leasing</a></li>
<li id="menu-item-1384" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1384"><a href="http://www.chinacarservice.com/services/">Cruise ship or Ferry Pick-Up</a></li>
<li id="menu-item-1385" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1385"><a href="http://www.chinacarservice.com/services/">English Speaking Tour Guides</a></li>
<li id="menu-item-1386" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1386"><a href="http://www.chinacarservice.com/services/">English Speaking Business Assistant</a></li>
</ul></div></div></div></div><!-- end bottom_3 --><div id='bottom_4' class='column_1_of_4'><div class='column-content-wrapper'><div class="bottom-col-content widget_text substitute_widget_class"><h3 class="bottom-col-title">Online Booking</h3>			<div class="textwidget"><p style="text-align: justify;">Our on-line booking system processes your MasterCard, Visa or Amex Cards through Authorize.Net, an American credit card system. As with any transaction in the US or Europe your purchases are protected if you do not get what we promise!</p>
<div class="clear"></div>
<p><a class="pngfix small-light-button align-btn-left" href="#" title=""><span class="pngfix">Online Booking</span></a></p>
<div class="clear"></div>
</div>
		</div></div></div><!-- end bottom_4 -->		</div>
		<!-- end bottom -->
	    </div>
	    <!-- end bottom-bg -->

	    <div class="clear"></div>




	<div id="footer-bg">
		<div id="footer" class="container_24 footer-top">
		    <div id="footer_text" class="grid_21">
			<p>
© 2001-2013 <strong>China Car Service, All Rights Reserved</strong> Is Proudly Developed By <a href="http://webshopex.com/"><strong>WebShopEx</strong></a>				| <a href="http://www.chinacarservice.com/feed/">Entries (RSS)</a>
			</p>
		    </div>
		    <div class="back-to-top">
			<a href="#top">Back to Top</a>
		    </div>
		</div>
	</div>

	<div class="clear"></div>

<link rel='stylesheet' id='flexslider-css'  href='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/js/flexslider/flexslider.css?ver=3.4.12' type='text/css' media='screen' />
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/shortcodes-ultimate/js/nivoslider.js?ver=3.9.5'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.9.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.9.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/jquery/ui/jquery.ui.tabs.min.js?ver=1.9.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/jquery-ui-tabs-rotate/jquery-ui-tabs-rotate.js?ver=3.4.12'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/js_composer_front.js?ver=3.4.12'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/foobar/js/jquery.foobar.2.3.min.js?ver=2.3.3'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/foobar/js/jquery.easing.1.3.js?ver=2.3.3'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/?foobar-js-dynamic=js&#038;ver=3.5.1&#038;post_id=2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/themes/qualifire/scripts/prettyPhoto/custom_params.js?ver=3.1.2'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-includes/js/hoverIntent.min.js?ver=r6'></script>
<script type='text/javascript' src='http://www.chinacarservice.com/wp-content/plugins/js_composer/assets/flexslider/jquery.flexslider-min.js?ver=3.4.12'></script>
    </div><!-- end wrapper-1 -->
  <script type="text/javascript"> Cufon.now(); </script>
  </body>
</html>