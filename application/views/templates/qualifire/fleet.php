<?php 
$fleet_intro_box = null;

foreach ($boxes as $box) {
	if($box->type == 'intro') {
		$fleet_intro_box = $box;
		break;
	}
}
?>
<div style="margin:20px;">
<p><?php if($fleet_intro_box) echo htmlspecialchars($fleet_intro_box->content);?></p>

</div>

<div style="margin-left:20px;">

<?php $i=1; foreach ($boxes as $fleet):?>
	<?php if($fleet->type == 'intro') continue;?>

	<div style="border:1px solid #ccc; padding:10px;width:420px; height:350px;<?php if($i%2 != 0) echo 'margin-right:30px;'?> margin-bottom:30px; float:left;">
		
		<div style="width:420px;height:200px;background:#ccc;">
			<?php if(!empty($fleet->image) && file_exists(textbox_image_path($fleet->image))):?>
						<img style="width:100%; height:100%" src="<?php echo textbox_image_src($fleet->image, $is_preview);?>" alt="" />
			<?php endif;?>
		</div>
		<div style="width:420px; height:130px;padding-top:20px;overflow:auto;">
				<?php echo $fleet->content;?>
		</div>
	
	</div>
<?php $i++; endforeach;?>

</div>