<?php
/**
 * @var array $languages
 * @var string $language
 * @var array $paginator
 */

$filterUrl = site_url('admin/translations');

?>

    <div class="clear">
        <form id="search-form" class="search-form" method="post" action="<?php echo $filterUrl;?>">
            <div class="clear">
                <input type="hidden" name="page" value="1" />
            </div>
            <div class="row clear">
                <div class="column column-start  left">
                    <label for="premium">Filter By Language</label>
                    <select name="language" id="language" class="txt">
                        <option value="all" <?php if($language=='all') echo 'selected="selected"'?>>All</option>
                        <option value="english" <?php if($language=='english') echo 'selected="selected"'?>>English</option>
                        <option value="chinese" <?php if($language=='chinese') echo 'selected="selected"'?>>Chinese</option>
                        <option value="portuguese" <?php if($language=='portuguese') echo 'selected="selected"'?>>Portuguese</option>
                    </select>
                </div>

                <div class="column column-end left">
                    <label for="">&nbsp;</label>
                    <input type="submit" value="Filter" class="form_btn" name="filter_btn" />
                </div>

            </div>
        </form>
    </div>

    <!-- <a href="<?php //echo site_url('admin/translations?generate_translations=1')?>" class="form_btn">Generate Translations</a> -->
	<a href="javascript://" onclick="$('#language_manager').toggle()">
		Manage languages
	</a>
	<div id="language_manager">
		<script>
			//Language delete
			function delete_lang(lid) {
				if (window.confirm("Are you sure want to delete language? This action can't be undone!")){
					var url = "<?php echo site_url('admin/ajax/languages/delete/'); ?>/" + lid + "/";
					$.post(url, function (res) {
						if (res.success) {
							$("#lang_row_" + res.id).hide(300);
						}else{
							alert(res.message);
						}
					});
				}
			}

			//Language add
			function add_new_language() {
				var form = $('#add_lang_form');
				form.ajaxSubmit({
						success: function (res) {
							if (res.success) {
								var new_row = "<tr id='lang_row_" + res.id + "'>" +
									"<td class='id'>" + res.id + "</td>" +
									"<td class='code'>" + res.lang_code + "</td>" +
									"<td class='language'>" + res.language + "</td>" +
									"<td class='flag'><img src='/images/flags/lang_" + res.lang_code + "_small.jpg?h="+Math.random() + "' style='width:15px'></td>" +
									"<td>" +
									"<a href='javascript:void(0);' onclick='delete_lang(" + res.id + ");'>Delete</a><br>" +
									"<a href='javascript:void(0);' onclick='update_language_form(" + res.id + ");'>Update</a>" +
									"</td></tr>";
								if (res.insert){
									$(new_row).insertAfter('#langs_head');
								}else{
									$('#lang_row_' + res.id).replaceWith(new_row);
								}
								$('#id_language').val('');
								$('#id_lang_code').val('');
								$('#id_userfile').val('');
							} else {
								alert(res.message);
							}
						}
					}
				);
			}

			// Update language
			function update_language_form(lid){
				console.log(lid + "ololo");
				var row = $('#lang_row_' + lid);
				console.log(row);
				var id = row.find('.id').html();
				var code = row.find('.code').html();
				console.log(code);
				var language = row.find('.language').html();

				$('#id_lang_id').val(id);
				$('#id_lang_code').val(code);
				$('#id_language').val(language);
			}
		</script>
		<h2>Languages manager</h2>
		<form action="<?php echo site_url('admin/ajax/languages/add/'); ?>/0/" method="post" id="add_lang_form" enctype="multipart/form-data" >
			<table>
				<tr id="langs_head">
					<th>ID</th>
					<th>Language code</th>
					<th>Language name</th>
					<th style="width: 100px;">Flag</th>
					<th>Actions</th>
				</tr>
				<?php
				foreach ($languages as $lang) {
					$lang_row = "
					<tr id='lang_row_" . $lang->id . "'>
                        <td class='id'>" . $lang->id . "</td>
                        <td class='code'>" . $lang->lang_code . "</td>
                        <td class='language'>" . $lang->language . "</td>
                        <td class='flag'><img src='/" . lang_small($lang->id) . "' style='width:15px'></td>
                        <td>
                        	<a href='javascript:void(0);' onclick='delete_lang(" . $lang->id . ");'>Delete</a>
                        	<a href='javascript:void(0);' onclick='update_language_form(" . $lang->id . ");'>Update</a>
                        </td>
					</tr>
				";
					echo $lang_row;
				}
				?>
				<tr id="lang_new">
					<th colspan="5">New language</th>
				</tr>
				<tr>
					<td colspan="2">
						<input type="hidden" name="id" id="id_lang_id" required="false"/>
						<input type="text" name="lang_code" id="id_lang_code" placeholder="language code" required="true"/>
					</td>
					<td>
						<input type="text" name="language" id="id_language" placeholder="language name"/>
					</td>
					<td rowspan="2" style="vertical-align: middle">
						<input type="button" onclick="add_new_language();" value="Save" style="padding: 8px;"/>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<strong>Flag image:</strong> <input type="file" name="userfile" id="id_userfile"/>
					</td>
				</tr>
			</table>
		</form>
	</div>

<?php
if($paginator['totalCount'] > 0) :?>
    <table class="business-tb" id="business-tb" style="width:823px; border-spacing: 0">
        <thead>
        <tr class="head">
            <th class="id">ID</th>
            <th class="review">Label</th>
            <th class="review">Language</th>
            <th class="review">Translation</th>
            <th class="rowActions"># Actions
                <img class="global-ajax-info" src="<?php echo SITE_ROOT;?>images/admin/ajax-progress.gif" />
            </th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="6">
                <div>

                    <div class="paginator" style="padding-left:10px;">

                        <?php
                        $url = SITE_ROOT.'admin/translations/' . $language;
                        echo pagination($paginator, $url, false);
                        ?>
                    </div>
                    <div class="digg-info">Showing <?php echo $paginator['from']; ?> to <?php echo $paginator['to']; ?> / <?php echo $paginator['totalCount']; ?> items.</div>
                </div>
            </td>
            <td>
                <div class="paginator">

                </div>
            </td>
        </tr>
        </tfoot>

        <tbody>
        <?php $counter = 0;?>
        <?php foreach ($paginator['data'] as $translation):?>
            <?php $counter++;?>
            <tr class="<?php echo ($counter%2 == 0 ? 'odd' : 'even')?>" id="translation-<?php echo $translation->id; ?>">
                <td><?php echo $translation->id;?></td>
                <td class="fname"><?php echo $translation->label;?></td>
                <td class="lastname"><?php echo $translation->language;?></td>
                <td class="email"><?php echo $translation->translation;?></td>
                <td class="rowActions website-actions">
                    <a href="<?php echo site_url('admin/edit_translation') . '/' . $translation->id;?>">Edit</a>
                    <a class="ajax confirm delete"  href="<?php echo site_url('admin/ajax/translations/delete/'. $translation->id);?>">Delete</a>

                </td>
            </tr>
        <?php endforeach;?>

        </tbody>

    </table>

<?php else:?>

    <div class="error-msg error" style="width:742px;margin:0;">
        <h1 class="error-msg">Oops...</h1>
        <p>
            Sorry, nothing found. Please try again.
        </p>
    </div>

<?php endif;?>