<?php include "partials/email_templates_head.php" ?>

<form action="./" method="post">
	<label for="id_name">Name:</label>
	<input type="text" id="id_name" name="name" value="<?php echo $object ? $object->name : ""?>"/>

	<div style="margin-top: 8px">
		<input type="submit" value="Save name" class="form_btn"/>
		&nbsp;
		<input type="button" value="Cancel" class="form_btn"
			   onclick="window.location.href='<?php echo site_url("admin/email_templates/list_names/");?>';"/>
	</div>
</form>