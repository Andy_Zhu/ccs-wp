<div id="not-auth-page">
<div class="page-error">

	<p class="error-code-container">
		<span class="error-code">401.</span>
		<span class="error-name">Not Authorized</span>
		<br />Your are not authorized to access this resource.
	</p>
	
	<p class="link">
		<?php echo anchor(site_url(), '&lt; back to home page');?>
	</p>
	
</div>

</div>