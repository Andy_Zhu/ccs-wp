<?php include "partials/email_templates_head.php" ?>

<a href="<?php echo site_url('admin/email_templates/new_name/'); ?>/">
	New name
</a>


<table>
	<tr>
		<th style="width: 30px">ID</th>
		<th style="width: 330px">Name</th>
		<th style="width: 80px">Actions</th>
	</tr>
	<?php
	/** @var $names array */
	foreach ($names as $name) {
		echo "<tr>";
		echo "<td>" . $name->id . "</td>";
		echo "<td>" . $name->name . "</td>";
		echo '<td><a href="' . site_url('admin/email_templates/edit_name/' . $name->id) . '/">Edit</a>&nbsp;
    <a href="javascript://"
     onclick="delete_name(' . $name->id . ')">Delete</a>';
		echo "</tr>";
	}
	?>

</table>

<script type="text/javascript">
	function delete_name(id) {
		if (window.confirm('Are you sure want to delete this name?')) {
			$.post('<?php echo site_url('admin/email_templates/delete_name/');?>',
				{id: id}, function () {
					window.location.href = '<?php echo site_url('admin/email_templates/list_names/');?>'
				});
		}
	}
</script>