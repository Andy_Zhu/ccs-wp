<?php if(!empty($boxes)):?>

	<?php foreach ($boxes as $box):?>
		<div class="box">
			<h2><input type="checkbox" name="boxes_ids[<?php echo $box->id?>]" value="<?php echo $box->id?>" />
			<?php if(isset($box->title)) {
				echo $box->title;
      }elseif(isset($box->service_name)){
        echo $box->service_name;
			}else{
				if($box->type == 'testimonial') echo '<strong>Testimonial:</strong> ';
				echo substr(strip_tags($box->content), 0, 50). '... ...'  . substr(strip_tags($box->content), -20);
			};?></h2>

		</div>
	<?php endforeach;?>

<?php endif;?>

<?php if(!empty($banners)):?>

	<?php foreach ($banners as $banner):?>
		<div class="box">
			<h2><input type="checkbox" name="banners_uris[<?php echo $banner->page_uri?>]" value="<?php echo $banner->page_uri?>" />
			<?php
				echo '<strong>Banner/'. $banner->page_uri . ' </strong>';
				echo substr(strip_tags($banner->content), 0, 50). '... ...'  . substr(strip_tags($banner->content), -20);
			?></h2>

		</div>
	<?php endforeach;?>

<?php endif;?>

<?php if($section == 'home'):?>
	<div class="box">
		<h2>
			<input type="checkbox" name="home_about" value="true" /> About <?php echo $site->name;?>
		</h2>
	</div>
<?php endif;?>

<?php if(!empty($static_pages)):?>
	<?php foreach ($static_pages as $static_page):?>
		<div class="box">
			<h2>
				<input type="checkbox" name="static_pages_ids[<?php echo $static_page->id?>]" value="<?php echo $static_page->id?>" />
				<strong><?php echo ucwords($static_page->uri);?>: </strong>
				<?php echo substr(strip_tags($static_page->content), 0, 50) . '... ...';?>
			</h2>
		</div>
	<?php endforeach;?>
<?php endif;?>

<?php if(!empty($website_vehicles)):?>
	<?php foreach ($website_vehicles as $wb_v):?>
		<?php $vehicle = $vehicles[$wb_v->vehicle_id];?>
		<div class="box">
			<h2><input type="checkbox" name="vehicles_ids[<?php echo $wb_v->vehicle_id;?>]" value="<?php echo $wb_v->vehicle_id;?>" />
			<?php echo $vehicle->long_name, ' /$', $wb_v->half_day_price, ', $', $wb_v->full_day_price, ', $', $wb_v->extra_hours_price, '/';?>
			</h2>

		</div>
	<?php endforeach;?>
<?php endif;?>

<?php if(!empty($tours)):?>
	<?php foreach ($tours as $tour):?>
		<div class="box">
			<h2>
				<input type="checkbox" name="tours_ids[<?php echo $tour->id?>]" value="<?php echo $tour->id;?>" />
				<?php echo '<strong>Tour: </strong>', $tour->title;?>
			</h2>
		</div>
	<?php endforeach;?>
<?php endif;?>

<?php if(!empty($slideshow_images)):?>
	<h2>Slideshow:</h2>
	<?php foreach ($slideshow_images as $image):?>
		<div class="image">
			<div>
			<img src="<?php echo slideshow_image_src($image->filename);?>" />
			<input type="checkbox" name="slideshow_images_ids[<?php echo $image->id?>]" value="<?php echo $image->id;?>" />
			</div>
		</div>
	<?php endforeach;?>

<?php endif;?>

<?php if(empty($boxes) && empty($static_pages) && empty($website_vehicles) && empty($tours) && empty($slideshow_images) && empty($banners)):?>
	<div class="info">Sorry there is nothing clonable, probably because the source site is using default values. Please save the pages of the source site and try again.</div>
<?php endif;?>