<?php
/**
 * @var $active_tab string Name of currently selected section
 */
?>
<div class="navi-container" style="width: 300px; padding-top: 1px; margin-bottom: 5px;">
	<ul class="navi">
		<li>
			<a href="<?php echo site_url('admin/email_templates/list_templates/'); ?>"
				<?php if ($active_tab == 'templates') echo " class='selected'";?>>
				Templates
			</a>
		</li>
		<li>
			<a href="<?php echo site_url('admin/email_templates/list_names/'); ?>"
				<?php if ($active_tab == 'names') echo " class='selected'";?>>
				Template Names
			</a>
		</li>
	</ul>
</div>