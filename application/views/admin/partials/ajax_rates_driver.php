<table class="table1">
<tbody>
<tr>
<th class="cell1" style="width: 140px;">Vehicle Class </th>
<th class="cell3">Model</th>
<th style="width: 82px;" class="cell4">Half Day <br>
&nbsp;
(4 hrs) </th>
<th style="width: 82px;" class="cell5">Full Day <br>
&nbsp;
(8 hrs)</th>
<th style="width: 82px;" class="cell6">Extra Hour <br>
Rate<br>
</th>
<th style="width: 82px;" class="cell7">Extra Km <br>
Rate</th>
</tr>
<?php foreach ($fleet_vehicles as $f_vehicle):?>
<?php 
	$vehicle = $vehicles[$f_vehicle->vehicle_id];
	$class = $classes[$vehicle->class_id];
?>
<tr>
<td class="mark"><?php echo $class->name;?></td>
<td><?php echo $vehicle->short_name;?></td>
<td style="width: 61px;">$<?php echo number_format($f_vehicle->half_day_price, 2);?></td>
<td style="width: 82px;">$<?php echo number_format($f_vehicle->full_day_price, 2);?></td>
<td>$<?php echo number_format($f_vehicle->extra_hours_price, 2)?></td>
<td><?php echo $f_vehicle->extra_km_price ? '$' . number_format($f_vehicle->extra_km_price, 2) : 'NA'?></td>
</tr>

<?php endforeach;?>

</tbody>
</table>

<div class="txt-box">
<p>All prices are in US Dollars. Price includes car and English speaking driver.</p>
<p>Prices are inclusive of taxes and fuel. Tolls and parking charges will be charged at cost.</p>
<p>Prices are based on maximum daily travel distance of 100km.</p>
<p>A surcharge will apply for any extra kms.</p>
<p>Weekly and monthly rates available on request.</p>
</div>