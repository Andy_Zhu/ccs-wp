<li id="country-<?php echo $country->id;?>">
	<a id="cname-<?php echo $country->id;?>" class="cname" href="<?php echo site_url('admin/websites') . '?cid=' . $country->id;?>">&raquo; <span><?php echo $country->name;?></span></a>
	<div class="btns">
		<a href="#nowhere"  class="edit">Edit Parent Market</a>
		<a href="#edit-<?php echo $country->id;?>" class="edit">Edit</a>
		<?php if($this->Users->is_admin()):?>
		<a href="<?php echo site_url('admin/ajax/countries/delete/' . $country->id);?>" class="delete">Delete</a>
		<?php endif;?>
	</div>
	<div class="edit-form" id="edit-form-<?php echo $country->id;?>">
		<form method="post" action="<?php echo site_url('admin/countries');?>">
			<div>
			<input type="text" name="name" class="txt" value="<?php echo $country->name;?>" />
			<input type="hidden" name="cid" value="<?php echo $country->id?>" />
			<input type="submit" name="edit_country" value="Update" />
			<img class="ajax-progress" id="ajax-anim-<?php echo $country->id;?>" src="<?php echo SITE_ROOT;?>images/admin/ajax-progress.gif" />
			<span id="status-<?php echo $country->id?>"></span>
			</div>
		</form>
	</div>
	
</li>