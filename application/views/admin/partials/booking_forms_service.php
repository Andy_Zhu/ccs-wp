<div class="grid-title">
 <h2>Updating booking forms services for all websites</h2>
</div>
<div id="boxes-container">
	<div class="box clear inner-spacer">
	  <div class="clear">
	    <label>*New Service Title</label>
	    <input  type="text"  class="txt" name="box_service_name" value=""/>
	    <label>*Order:</label>
          <input  type="number" class="txt small" name="box_order" value=""/>
	  </div>
	  <div class="clear">
      <label class="check-label">Active:</label>
      <input  type="checkbox" class="check-input" name="active[]" />
      <input type="hidden" name="active[]" value="">
    </div>
    <div class="clear ">
      <label class="check-label">Airport pickup:</label>
      <input  type="checkbox" class="check-input" name="airport_pickup[]" />
      <input type="hidden" name="airport_pickup[]" value="">
      <label class="check-label">Hotel pickup</label>
      <input  type="checkbox" class="check-input" name="hotel_pickup[]"/>
      <input type="hidden" name="hotel_pickup[]" value="">
      <label class="check-label">Day hire:</label>
      <input  type="checkbox" class="check-input" name="dayhire[]"/>
      <input type="hidden" name="dayhire[]" value="">
      <label class="check-label">Tour:</label>
      <input  type="checkbox" class="check-input" name="tour[]"/>
      <input type="hidden" name="tour[]" value="">
      <label class="check-label">Other:</label>
      <input  type="checkbox" class="check-input" name="other[]"/>
      <input type="hidden" name="other[]" value="">
    </div>
  </div>
</div>

<div class="clear">
  <input type="submit" name="edit_booking_form_service_btn" class="form_btn" value="Save" />
</div>