<?php
$filterUrl = site_url('admin/users');

?>

<div class="clear">
	<form id="search-form" class="search-form" method="post" action="<?php echo $filterUrl;?>">
	<div class="clear">
	<input type="hidden" name="page" value="1" />
	</div>
	<div class="row clear">
		<div class="column column-start  left">
		<label for="premium">Filter By Status</label>
		<select name="status" id="status" class="txt">
			<option value="all" <?php if($status=='all') echo 'selected="selected"'?>>All</option>
			<option value="A" <?php if($status=='A') echo 'selected="selected"'?>>Active</option>
			<option value="B" <?php if($status=='B') echo 'selected="selected"'?>>Disabled</option>
		</select>
		</div>
		
		<div class="column column-start  left">
		<label for="premium">Filter By Role</label>
		<select name="role" id="role" class="txt">
			<option value="any" <?php if($role=='any') echo 'selected="selected"'?>>Any</option>
			<option value="admin" <?php if($role=='admin') echo 'selected="selected"'?>>Admins</option>
			<option value="editor" <?php if($role=='editor') echo 'selected="selected"'?>>Editors</option>
		</select>
		</div>
		
		<div class="column column-end left">
			<label for="">&nbsp;</label>
			<input type="submit" value="Filter" class="form_btn" name="filter_btn" />
		</div>
		
	</div>	
</form>
</div>

<input type="button" id="add-user-btn" value="+ New User" class="form_btn clear"/>
<div class="clear" id="add-user-container" style="padding:10px; background:#f0f0f0;border:1px solid #ccc;width:800px;">
	<form method="post" action="<?php echo site_url('admin/users')?>">
	<?php 
		if(isset($create_user_message)) echo '<div class="error">', $create_user_message, '</div>';
	?>
		<div class="column left">
			Firstname: <br />
			<input type="text" name="firstname" id="firstname" class="txt" value="<?php echo set_value('firstname')?>" />
			<br /><?php echo form_error('firstname');?>
		</div>
		<div class="column left">
			Lastname: <br />
			<input type="text" name="lastname" id="lastname" class="txt" value="<?php echo set_value('lastname')?>" />
			<br /><?php echo form_error('lastname');?>
		</div>
		<div class="clear"></div>
		<div class="column left">
			Email: <br />
			<input type="text" name="email" id="email" class="txt" value="<?php echo set_value('email')?>" />
			<br /><?php echo form_error('email');?>
		</div>
		<div class="column left">
			Role: <br />
			<select name="role" class="txt med">
				<option value="admin">Admin</option>
				<option value="editor">Editor</option>
			</select>
			<br /><?php echo form_error('role');?>
		</div>
		<div class="clear">
			<input type="submit" name="add_user_btn" class="form_btn" value="Create user" />
		</div>
	</form>
</div>
<?php if($paginator['totalCount'] > 0) :?>
<table cellpadding="0" cellspacing="0" class="business-tb" id="business-tb" style="width:823px;">
	<thead>
		<tr class="head">
			<th class="id">ID</th>
			<th class="review">Firstname</th>
			<th class="review">Lastname</th>
			<th class="review">Email</th>
			<th class="role">Role</th>
			<th class="date">Created ON</th>
			<th class="status">Status</th>
			<th class="rowActions"># Actions
					<img class="global-ajax-info" src="<?php echo SITE_ROOT;?>images/admin/ajax-progress.gif" />
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">
				<div>
	             
		             <div class="paginator" style="padding-left:10px;">
		             
		             <?php 
		             	$url = SITE_ROOT.'admin/users/' . $role . '-' . $status . '';
		             	echo pagination($paginator, $url, false);
		             ?>
		             </div>
		             <div class="digg-info">Showing <?=$paginator['from']?> to <?=$paginator['to']?> / <?=$paginator['totalCount']?> items.</div>
	       	 </div> 
			</td>
			<td>
				<div class="paginator">
				
				</div>
			</td>
		</tr>
	</tfoot>
	
	<tbody>
		<?php $counter = 0;?>
		<?php foreach ($paginator['data'] as $user):?>
			<?php $counter++;?>
			<tr class="<?php echo ($counter%2 == 0 ? 'odd' : 'even')?>" id="user-<?php echo $user->id; ?>">
			<td><?php echo $user->id;?></td>
			<td class="fname"><?php echo $user->firstname;?></td>
			<td class="lastname"><?php echo $user->lastname;?></td>
			<td class="email"><?php echo $user->email;?></td>
			<td class="role"><?php echo ucfirst($user->role);?></td>
			<td class="date"><?php echo date('m-d-Y H:i:s', strtotime($user->ts_created));?></td>
			
			<td class="statusx">
				<?php
				if($user->status == 'A') {
					echo '<span class="green-txt b status">A</span>';
				}else {
					echo '<span class="red-txt b status">B</span>';
				}
				?>
			</td>
			<td class="rowActions users-actions">
				<a href="<?php echo site_url('admin/edit_user') . '/' . $user->id;?>">Edit</a> 
				<?php if($user->status == 'A'):?>
					<a class="ajax disable"  href="<?php echo site_url('admin/ajax/users/disable/'. $user->id);?>">Disable</a>
				<?php else : ?>
					<a class="ajax enable"  href="<?php echo site_url('admin/ajax/users/enable/'. $user->id);?>">Activate</a>
				<?php endif;?>
				<a class="ajax confirm delete"  href="<?php echo site_url('admin/ajax/users/delete/'. $user->id);?>">Delete</a>
				
			</td>
			</tr>
		<?php endforeach;?>
		
	</tbody>
	
</table>

<?php else:?>

<div class="error-msg error" style="width:742px;margin:0;">
	<h1 class="error-msg">Oops...</h1>
	<p>
	Sorry, nothing found. Please try again.
	</p>
</div>
	
<?php endif;?>