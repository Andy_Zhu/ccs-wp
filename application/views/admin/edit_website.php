
<div class="edit-page" id="editor">

	<div class="grid-title row rtv">
		<h2>Updating <?php echo $website->url;?></h2>
		<div class="publish-buttons">
			<a href="<?php echo site_url('admin/preview') . "?id=$id&amp;section=" . (in_array($section, array('basic', 'banners')) ? 'home' : $section);?>" target="_blank" class="orange-btn" >Preview</a>
			<a href="<?php echo site_url('admin/publish') . "?id=$id&test=true";?>" class="orange-btn" id="test-publish-btn">Test Publish</a>
			<a href="<?php echo site_url('admin/publish') . "?id=$id";?>" class="orange-btn" id="publish-btn">Publish</a>
		</div>
	</div>
	<div class="navi-container">
		<ul class="navi">
			<li><a <?php if($section == 'basic') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=basic")?>">Settings</a></li>
			<li><a <?php if($section == 'home') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=home")?>">Home Page</a></li>
			<li><a <?php if($section == 'about-us') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=about-us")?>">About us</a></li>
			<li><a <?php if($section == 'fleet') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=fleet")?>">Fleet/Cars</a></li>
			<li><a <?php if($section == 'services') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=services")?>">Services</a></li>
			<li><a <?php if($section == 'rates') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=rates")?>">Rates</a></li>
			<li><a <?php if($section == 'online-booking') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=online-booking")?>">Online Bookings</a></li>
			<li><a <?php if($section == 'tours' || $section=='subtours' || $section=='edittour') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=tours")?>">Tours</a></li>
			<li><a <?php if($section == 'contact-us') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=contact-us")?>">Contact Us</a></li>
			<li><a <?php if($section == 'faqs') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=faqs")?>">Faqs</a></li>
			<li><a <?php if($section == 'policies') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=policies")?>">Policies</a></li>
			<li><a <?php if($section == 'why-service') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=why-service")?>">Why <?php echo $website->name;?></a></li>
      <li><a <?php if($section == 'locations') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=locations")?>">Locations</a></li>
      <li><a <?php if($section == 'booking-services') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=booking-services")?>">Booking Services</a></li>
			<li><a <?php if($section == 'banners') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_website?id=$id&section=banners")?>">Banners</a></li>
			<li>
				<a href="<?php echo site_url("admin/email_templates/?forsite=$website->id"); ?>" target="_blank">
					Email Templates
				</a>
			</li>
		</ul>
	</div>

	<div class="editor-container" id="editor-container">
		<?php $this->load->view($editor_view);?>
		<?php if ($revisions && !is_null($revisions)): ?>
			<script>
				$('#editor-container').delegate('.load_version', 'click', function(){
					return (window.confirm("Are you sure want to change to another revision? \n" +
						"Any unsaved changes will be lost!"));
				});
			</script>
			<div id="versions_list">
				<h3>Previous revisions</h3>
				<ul id="revisions">
					<?php
					foreach ($revisions as $revision) {
						$ver_id = $revision->id;
						$revision_owner = $this->Users->find($revision->author);
						echo "<li><a href='/admin/edit_website?id=$id&section=$section&load_revision=$ver_id' class='load_version'>"
							. $revision->created . " | " . $revision_owner->firstname .
							' ' . $revision_owner->lastname . "</a>";
					}
					?>
				</ul>
			</div>
		<?php endif; ?>
	</div>

</div>
