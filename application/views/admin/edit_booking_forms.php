<div class="edit-page" id="editor">
	<div class="grid-title row rtv">
		<h2>Updating booking forms</h2>		
	</div>

	<div class="navi-container">
		<ul class="navi">
			<li><a <?php if($section == 'general') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_booking_forms?section=general")?>">General text</a></li>
      <li><a <?php if($section == 'service') echo 'class="current" ';?> href="<?php echo site_url("admin/edit_booking_forms?section=service")?>">Services</a></li>
		</ul>
	</div>

	<form action="/admin/edit_booking_forms?section=<?php echo $section; ?>" method="post">
		<div class="editor-container" id="box-editor">
		 <?php echo $editor_view; ?>
		</div>
	</form>
</div>