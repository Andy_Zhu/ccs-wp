<?php include "partials/email_templates_head.php" ?>

<form action="./" method="post">
	<div style="display: inline-block">
		<label for="id_website_id">Website</label>
		<select name="website_id" id="id_website_id"
				class="lockedonedit" <?php if ($edit) echo 'disabled="disabled"' ?>>
			<option value="0">--- Select site ---</option>
			<?php
			/** @var $websites array */
			/** @var $forname int */
			foreach ($websites as $website) {
				echo "<option value='$website->id'";
				if (($object && $object->website_id == $website->id) || (!$object && $forsite == $website->id)) {
					echo "selected='selected'";
				}
				echo ">$website->name</option>";
			}
			?>
		</select>
	</div>
	<div style="display: inline-block">
		<label for="id_template_name_id">Template name</label>
		<select name="template_name_id" id="id_template_name_id"
				class="lockedonedit" <?php if ($edit) echo 'disabled="disabled"' ?>>
			<option value="0">--- Select template name ---</option>
			<?php
			/** @var $names array */
			/** @var $forsite int */
			foreach ($names as $name) {
				echo "<option value='$name->id'";
				if (($object && $object->template_name_id == $name->id) || (!$object && $forname == $name->id)) {
					echo "selected='selected'";
				}
				echo ">$name->name</option>";
			}
			?>
		</select>
	</div>

	<?php if ($edit): ?>
		<div style="display: inline-block">
			<a href="javascript:void(0);" onclick="$('.lockedonedit').removeAttr('disabled'); $(this).remove()">
				Unlock
			</a>
		</div>
	<?php endif; ?>

	<br/><br/><hr/><br/>
	<div style="border: 1px solid silver; padding: 5px">
		<h4>Available fields</h4>
		<div><a href="#" id="toggleBookingFields">Booking Forms</a></div>
		<div id="bookingFields" style="display:none;">
		  <strong>Booking specific</strong>
		  <li><b>{{bookingReference}}</b> Booking reference</li>
			<strong>Website Fields</strong>
			<ul>
				<li><b>{{websiteData_name}}</b> Name of market. (e.g hongkong)</li>
				<li><b>{{websiteData_fullName}}</b> Full Name of website. (e.g Beijing Car Service)</li>
				<li><b>{{websiteData_contact_form_email}}</b> Website contact form email address.</li>
				<li><b>{{websiteData_contact_form_cc_email}}</b> Website contact form CC email address.</li>
				<li><b>{{websiteData_phone}}</b> Phone of website.</li>
				<li><b>{{websiteData_fax}}</b> Fax of website.</li>
				<li><b>{{websiteData_url}}</b> Website's URL.</li>
				<li><b>{{websiteData_opening_hours}}</b> Opening Hours.</li>
			</ul>
			<strong>Customer Contact Info</strong>
			<ul>
				<li><b>{{bookingType}}</b> Booking Type.</li>
				<li><b>{{passengerName}}</b> or <b>{{name}}</b> Website contact form email address.</li>
				<li><b>{{companyName}}</b> Website contact form CC email address.</li>
				<li><b>{{email}}</b> Phone of website.</li>
				<li><b>{{alternateEmail}}</b> Fax of website.</li>
				<li><b>{{telephone}}</b> Website's URL.</li>
				<li><b>{{chinaPhone}}</b> China phone number</li>
				<li><b>{{pickUpNumber}}</b> Contact Number prior to pick Up</li>
			</ul>
			<strong>Airport Service</strong>
			<ul>
				<li><b>{{airportPickUpRadio}}</b> Pick up from airport? 1/0</li>
				<li><b>{{airportSelect}}</b> Name of selected airport</li>
				<li><b>{{comingFrom}}</b> Coming From</li>
				<li><b>{{flightNumber}}</b> Flight Number</li>
				<li><b>{{arrivalDate}}</b> Arrival Date</li>
				<li><b>{{arrivalHour}}</b> Arrival Hour</li>
				<li><b>{{arrivalMinute}}</b> Arrival Minute</li>
				<li><b>{{airportAddress}}</b> Drop off address &amp; Phone No</li>
				<li><b>{{airportCarType}}</b> Car type</li>
        <li><b>{{airportPeopleAmount}}</b> PeopleAmount</li>
        <li><b>{{airportLuggageAmount}}</b> Luggage Amount</li>
        <li><b>{{airportSign}}</b> Greeting Sign</li>
        <li><b>{{airportSpecialRequirement}}</b> Special requirements or instructions!</li>
        <li><b>{{airportPromo}}</b> Promo Code</li>
			</ul>
			<!-- <strong>Airport Return Transfer Service</strong>
      <ul>
        <li><b>{{returnTransferRadio}}</b> Return Transfer?</li>
        <li><b>{{pickUpDate}}</b> Hotel pick up date</li>
        <li><b>{{returnTransferHour}}</b> Hotel pick up hour</li>
        <li><b>{{returnTransferMinute}}</b> Hotel pick up minute</li>
        <li><b>{{returnTransferPickUpAddress}}</b> Hotel pick up address</li>
        <li><b>{{returnTransferDropOffAddress}}</b> Drop off address</li>
        <li><b>{{returnTransferCarType}}</b> Car type</li>
        <li><b>{{pickUpPeopleAmount}}</b> PeopleAmount</li>
        <li><b>{{pickUpLuggageAmount}}</b> Luggage Amount</li>
        <li><b>{{pickUpSign}}</b> Greeting Sign</li>
        <li><b>{{pickUpSpecialRequirement}}</b> Special requirements or instructions!</li>
        <li><b>{{pickUpPromo}}</b> Promo Code</li>
      </ul> -->
			<strong>Hotel Service</strong>
			<ul>
				<li><b>{{pickupFromHotel}}</b> Pick up from Hotel?</li>
				<li><b>{{pickUpDate}}</b> Hotel pick up date</li>
				<li><b>{{pickUpHour}}</b> Hotel pick up hour</li>
				<li><b>{{pickUpMinute}}</b> Hotel pick up minute</li>
				<li><b>{{pickUpAddress}}</b> Hotel pick up address</li>
				<li><b>{{dropOffAddress}}</b> Drop off address</li>
				<li><b>{{pickUpCarType}}</b> Car type</li>
        <li><b>{{pickUpPeopleAmount}}</b> PeopleAmount</li>
        <li><b>{{pickUpLuggageAmount}}</b> Luggage Amount</li>
        <li><b>{{pickUpSign}}</b> Greeting Sign</li>
        <li><b>{{pickUpSpecialRequirement}}</b> Special requirements or instructions!</li>
        <li><b>{{pickUpPromo}}</b> Promo Code</li>
			</ul>
			<strong>Half/Full Day Hire</strong>
			<ul>
				<li><b>{{duration}}</b> Duration</li>
				<li><b>{{itinerary}}</b> Itinerary</li>
				<li><b>{{dayhireDate}}</b> Required Date</li>
				<li><b>{{dayhireHour}}</b> Dayhire pick up hour</li>
        <li><b>{{dayhireMinute}}</b> Dayhire pick up minute</li>
				<li><b>{{dayhireCarType}}</b> Car type</li>
        <li><b>{{dayhirePeopleAmount}}</b> PeopleAmount</li>
        <li><b>{{dayhireLuggageAmount}}</b> Luggage Amount</li>
        <li><b>{{dayhireSign}}</b> Greeting Sign</li>
        <li><b>{{dayhireSpecialRequirement}}</b> Special requirements or instructions!</li>
        <li><b>{{dayhirePromo}}</b> Promo Code</li>
			</ul>
			<strong>Tour Hire</strong>
      <ul>
        <li><b>{{tourName}}</b> Tour Name</li>
        <li><b>{{tourDate}}</b> Tour Date</li>
        <li><b>{{tourCarType}}</b> Car type</li>
        <li><b>{{tourPeopleAmount}}</b> PeopleAmount</li>
        <li><b>{{tourLuggageAmount}}</b> Luggage Amount</li>
        <li><b>{{tourSign}}</b> Greeting Sign</li>
        <li><b>{{tourSpecialRequirement}}</b> Special requirements or instructions!</li>
        <li><b>{{tourPromo}}</b> Promo Code</li>
      </ul>
			<strong>Other Details</strong>
			<ul>
				<li><b>{{carType}}</b> Car Type</li>
				<li><b>{{otherRequired1}}</b> English Speaking Guide</li>
				<li><b>{{otherRequired2}}</b> Airport VIP Meet &amp; Assist</li>
				<li><b>{{otherRequired3}}</b> Accredited Interpreter / Translator</li>
				<li><b>{{otherRequired4}}</b> Private Guided Tour (Please input name of the Tour in space below)</li>
			</ul>
			<strong>Payment</strong>
			<ul>
				<li><b>{{payment}}</b> Payment type</li>
				<li><b>{{cardholderName}}</b> Card Holder Name</li>
				<li><b>{{cardType}}</b> Credit Card Type</li>
				<li><b>{{creditCardNo}}</b> Credit Card Number</li>
				<li><b>{{securityCode}}</b> Security Code</li>
				<li><b>{{expireMonth}}</b> Expiration Date</li>
				<li><b>{{expireYear}}</b> Expiration Year</li>
				<li><b>CHINESE FORMS ONLY</b></li>
				<ul>
				<li><b>{{identityID}}</b> Identification Card ID number</li>
        </ul>
        <li><b>ENGLISH FORMS ONLY</b></li>
        <ul>
				<li><b>{{street}}</b> Street Address</li>
				<li><b>{{suburb}}</b> City/Town</li>
				<li><b>{{state}}</b> State</li>
				<li><b>{{zipCode}}</b> Zip Code</li>
				<li><b>{{country}}</b> Country</li>
				</ul>
			</ul>
		</div>
		<div><a href="#" id="toggleContactFields">Contact Forms</a></div>
		<div id="contactFields" style="display:none;">
			<ul>
				<li><b>{{name}}</b> Name</li>
				<li><b>{{email}}</b> Email</li>
				<li><b>{{subject}}</b> Subject</li>
				<li><b>{{message}}</b> Message</li>
			</ul>
		</div>
		<div><a href="https://github.com/speedmax/h2o-php/wiki/Built-in-tags" target="_blank">Help on template tags</a></div>
	</div>
	<br/>

	<div>
		<label for="id_title">Email title</label>
		<input type="text" id="id_title" name="title" value="<?php echo isset($email) ? $email['title'] : ''; ?>"
			   style="width:500px"/>
	</div>

	<div>
		<div style="display: inline-block">
			<label for="id_from">FROM</label>
			<input type="text" id="id_from" name="from" value="<?php echo isset($email) ? $email['from'] : ''; ?>"
				   style="width:250px"/>
		</div>
		&nbsp;&nbsp;
		<div style="display: inline-block">
			<label for="id_to">TO</label>
			<input type="text" id="id_to" name="to" value="<?php echo isset($email) ? $email['to'] : ''; ?>"
				   style="width:250px"/>
		</div>
	</div>

	<div>
		<div style="display: inline-block">
			<label for="id_cc">CC</label>
			<input type="text" id="id_cc" name="cc" value="<?php echo isset($email) ? $email['cc'] : ''; ?>"
				   style="width:250px"/>
		</div>
		&nbsp;&nbsp;
		<div style="display: inline-block">
			<label for="id_bcc">BCC</label>
			<input type="text" id="id_bcc" name="bcc" value="<?php echo isset($email) ? $email['bcc'] : ''; ?>"
				   style="width:250px"/>
		</div>
	</div>

	<div>
		<label for="id_body">Email body</label>
		<textarea name="body" id="id_body" cols="90" rows="25"
				  style="padding: 3px; white-space: pre"><?php echo isset($email) ? str_replace("\n", "&#10;", $email['body']) : ''; ?></textarea>
	</div>
	<div style="margin-top: 8px">
		<input type="submit" value="Save template" class="form_btn" onclick="return validateme();"/>
		&nbsp;
		<input type="button" value="Cancel" class="form_btn"
			   onclick="window.location.href='<?php echo site_url("admin/email_templates"); ?>';"/>
	</div>
</form>

<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#id_body').ckeditor();
	$('#toggleBookingFields').click(function(event) {
		event.preventDefault();
		$('#bookingFields').toggle();
	});
	$('#toggleContactFields').click(function(event) {
		event.preventDefault();
		$('#contactFields').toggle();
	});
});
</script>

<script type="text/javascript">
	function validateme() {
		if ($('#id_website_id').val() == '0') {
			alert("Website should be selected!");
			return false;
		}

		if ($('#id_template_name_id').val() == '0') {
			alert("Template name should be selected!");
			return false;
		}

		return true;
	}
</script>