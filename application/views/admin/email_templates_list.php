<?php include "partials/email_templates_head.php" ?>

<a href="<?php echo site_url('admin/email_templates/new_template/?forsite=' . $forsite . "&forname= " . $forname); ?>">
	New template
</a>
&nbsp;&nbsp;&nbsp;
<strong>Filters:</strong>&nbsp;
<select id="sites_list" onchange="filter_list('forsite', $('#sites_list').find('option:selected').val());">
	<option value="">--- All websites ---</option>
	<?php foreach ($websites as $website){
		echo "<option value='" . $website->id ."'";
		if ($forsite == $website->id)
			echo " selected='selected'";
		echo ">". $website->name ."</option>\n";
	}?>
</select>
&nbsp;
<select id="names_list" onchange="filter_list('forname', $('#names_list').find('option:selected').val());">
	<option value="">--- All Template Names ---</option>
	<?php foreach ($names as $name){
		echo "<option value='" . $name->id ."'";
		if ($forname == $name->id)
			echo " selected='selected'";
		echo ">". $name->name ."</option>\n";
	}?>
</select>

<table>
	<tr>
		<th style="width: 30px">ID</th>
		<th style="width: 200px">Website</th>
		<th style="width: 330px">Name</th>
		<th style="width: 120px">Actions</th>
	</tr>

	<?php
	/** @var $templates array */
	/** @var $websites array */
	/** @var $names array */
	foreach ($templates as $template) {
		echo "<tr>";
		echo "<td>" . $template->id . "</td>\n";
		echo "<td>" . $websites[$template->website_id]->name . "</td>\n";
		echo "<td>" . $names[$template->template_name_id]->name . "</td>\n";
		echo '<td>
    <a href="' . site_url('admin/email_templates/edit_template/' . $template->id) . '/">Edit</a>&nbsp;
    <a href="javascript://"
     onclick="delete_template(' . $template->id . ')">Delete</a>&nbsp;
</td>';
		echo "</tr>";
	}

	?>
</table>
<script type="text/javascript">
	function delete_template(id) {
		if (window.confirm('Are you sure want to delete this template?')) {
			$.post('<?php echo site_url('admin/email_templates/delete_template/');?>',
				{id: id}, function () {
					window.location.href = '<?php echo site_url('admin/email_templates/');?>'
				});
		}
	}
	function updateQueryStringParameter(uri, key, value) {
		var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		if (uri.match(re)) {
			return uri.replace(re, '$1' + key + "=" + value + '$2');
		}
		else {
			return uri + separator + key + "=" + value;
		}
	}

	function filter_list(name, value){
		window.location.href = updateQueryStringParameter(window.location.href, name, value);
	}
</script>