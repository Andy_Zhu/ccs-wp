
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>

<div class="static-form">

	<form method="post" action="<?php echo site_url("admin/edit_website?section=$section&id=$id");?>">

		<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
		<div class="clear"></div>
		<div class="row clear" style="border-top:5px solid #666; padding-top:20px;">
			<label>Page Name:</label>
			<input type="text" name="page_name" class="txt column-450" value="<?php echo $static_page? $static_page->name : $section ?>"/>
		</div>
		<div class="clear row">
			<label>Page Content:</label>
			<textarea rows="30" cols="100" id="static-page-content" name="static_content"><?php echo $static_page ? htmlspecialchars( $static_page->content) : '';?></textarea>
		</div>

		<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
		<div class="clear">
			<input type="submit" name="submit" value="Save" class="form_btn" />
		</div>
	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
CKEDITOR.replace( 'static-page-content' );
//-->
</script>