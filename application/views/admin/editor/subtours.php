<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">
	<h1><a href="<?php echo site_url("admin/edit_website?id=$id&section=tours")?>">Tour categories</a> &gt; <?php echo $tour_category->title;?></h1>
	<div style="background:#666;margin-top:5px;">
		<input type="button" id="add-tour-btn" value="+ Add A Tour" class="form_btn"/>
	</div>
	<form enctype="multipart/form-data"  id="add-tour-form" method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section&tourcat={$tour_category->id}")?>">
	<div class="clear box inner-spacer tour-box">
		<div class="clear">
			<label>*Title: </label>
			<input  type="text" class="txt" name="title" />
		</div>
		<div class="clear">
		<label>Introduction: </label>
			<textarea id="note-0" rows="10" cols="50" name="intro"  class="txt large editor"></textarea>
		</div>
		<h2 style="padding:5px; background:#999;">Descriptions</h2>
		<div class="tour-desc">

			<div class="box top-section-box clear inner-spacer" style="background:#f0f0f0;">
				<a href="#" class="delete top-section-delete unsaved" title="delete">Delete</a>
				<div class="column left column-220" style="margin-right:20px;">
					<div class="clear">
						<label >Image (405 x 228):</label><input type="file" name="tour_images[]" /><br />
					</div>
					<div class="">
						<label>Current Image:</label>
						<h3>Not set</h3>
					</div>
				</div>
				<div class="column left" style="width:400px;">
					<label>Text:</label>
					<textarea id="top-editor-1" rows="8" cols="30" name="tour_boxes[]" style="width:400px;" class="txt editor"></textarea>
					<br /><strong>Order Display</strong>
					<input type="text" style="width:20px;" class="txt small" name="tour_boxes_rankings[]" value="" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
		<div class="clear row"><a href="#nowhere" id="" class="add-tour-desc-btn">+ New description</a></div>
 		<div class="clear">
 			<label>Extra (Price &amp; vehicle info...etc):</label>
 			<textarea id="extra-editor-1" rows="8" cols="30" name="extra" class="txt large editor"></textarea>
 		</div>
 		<div class="clear" style="padding-top:10px;">
			<input type="submit" name="add_tour" value="save" class="form_btn" />
		</div>

	</div>
	</form>
	<br />
	<h1 style="background:#666; color:#ccc;padding:5px 8px">Added Tours:</h1>
	<br />
	<div id="boxes-container">
		<?php if(empty($tours)):?>
			<p class="info row">No tours have been added, To add a tour Please click on <strong>Add Tours</strong> button above.</p>
		<?php else:?>
		<?php foreach ($tours as $tour):?>

		<div class="box clear inner-spacer rtv" id="tour-<?php echo $tour->id; ?>">
			<h2><?php echo $tour->title;?></h2>
			<p style="color:#666;"><?php echo substr(strip_tags($tour->intro), 0, 80) . (strlen(strip_tags($tour->intro)) > 80 ? '...' : '');?></p>
			<div class="actions" style="width:40px;position:absolute;right:10px;top:10px;">
				<a
				href="<?php echo site_url("admin/edit_website?id=$id&section=edittour&tourid={$tour->id}")?>"
				title="Edit" class="edit">Edit</a>
				<a href="<?php echo site_url('admin/ajax/tours/delete/' . $tour->id);?>" title="Delete" class="del">Delete</a>
			</div>
		</div>
		<?php endforeach;?>
		<?php endif;?>
	</div>



</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('.editor').ckeditor();

	$('#add-tour-form').hide();
	$("#add-tour-btn").click(function(evt){
		evt.preventDefault();
		$('#add-tour-form').slideToggle();
	});

	$('#box-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$_target = $(this);
			if($_target.hasClass('unsaved')) {
				$_target.parent().remove();
				return;
			}
			$.get($(this).attr('href'), function(data){
				if(data.success) {
					$('#tour-' + data.id).slideUp(function(){$(this).remove();});
				}else{
					alert(data.message);
				}
			});
		}
	});

	$('.actions .del').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this item')) {
			$_target = $(this);
			$.get($(this).attr('href'), function(data){
				if(data.success) {
					$('#tour-' + data.id).slideUp(function(){$(this).remove();});
				}else{
					alert(data.message);
				}
			});
		}
	});

	$('.add-tour-desc-btn').click(function(evt){

		evt.preventDefault();
		var id = new Date().getMilliseconds();
		var tpl = ['<div class="box top-section-box clear inner-spacer" style="background:#f0f0f0;">',
					'<a href="#" class="delete top-section-delete unsaved" title="delete">Delete</a>',
					'<div class="column left column-220" style="margin-right:20px;">',
						'<div class="clear">',
							'<label >Image (405 x 228):</label><input type="file" name="tour_images[]" /><br />',
						'</div>',
						'<div class="">',
							'<label>Current Image:</label>',
							'<h3>Not set</h3>',
						'</div>',
					'</div>',
					'<div class="column left" style="width:400px;">',
						'<label>Text:</label>',
						'<textarea id="top-editor-' + id +'" rows="8" cols="30" name="tour_boxes[]" class="txt top-editor" style="width:400px;"></textarea>',
						'<br /><strong>Order Display</strong>',
						'<input type="text" style="width:20px;" class="txt small" name="tour_boxes_rankings[]" value="" />',
					'</div>',
					'<div class="clear">&nbsp;</div>',
					'</div>'];
		$(tpl.join('')).appendTo($(this).parents('.tour-box').find('.tour-desc'));

		$('#top-editor-' + id).ckeditor();


	});
	//delete images

	$('.thumb .del-btn').click(function(evt){
		evt.preventDefault();
		$preview = $(this).parent().parent();
		$_target = $(this);
		if(confirm('Are you sure you want to delete this image?')){
			/*$preview.find('.thumb .end-row').removeClass('end-row');
			$preview.find('.thumb:even').addClass('end-row');*/
			$.get($(this).attr('href'), function(data){
				if(data.success) {
					$('#thumb-' + data.id).fadeOut(function(){$(this).remove();});
				}else{
					alert(data.message);
				}
			});

		}
	});
});

//-->
</script>