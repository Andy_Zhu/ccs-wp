<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ATH?>ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">

	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>

	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>

	<div id="boxes-container">
	<?php if(isset($boxes) && count($boxes) > 0) : $counter=1;?>
		<?php foreach ($boxes as $box) :?>

		<div class="box clear inner-spacer">
			<div class="clear">
				<h1><?php echo $box->title;?></h1>
				<input type="hidden" name="box_title[]" value="<?php echo $box->title?>" />
			</div>
			<div  class="row ">
				<textarea id="box-<?php echo $counter++;?>" rows="10" cols="50" name="box_content[]"  class="txt large"><?php echo htmlspecialchars($box->content);?></textarea>
				<br />
			</div>

		</div>
		<?php endforeach;?>


	<?php else:?>

		<div class="box clear inner-spacer">
			<div class="clear">
				<h1>Online Booking</h1>
				<input type="hidden" name="box_title[]" value="Online Booking" />
			</div>
			<div  class="row ">
				<textarea id="box-1" rows="10" cols="50" name="box_content[]"  class="txt large"><?php echo htmlspecialchars('
Our on-line booking system processes your MasterCard, Visa or Amex Cards through Authorize.Net, an American credit card system. As with any transaction in the US or Europe your purchases are protected if you do not get what we promise! We also accept payment in cash, via wire transfer and PayPal.
<div><img src="' . template_image_src('pay-icons.png', true) .'" alt="#"></div>
At the end of each day our driver will have you verify the days toll and parking receipts, the odometer readings (start and finish) and the pick up / drop off times on an invoice. We will process the invoice and charge your credit card as per your booking and email you a receipt.
');?></textarea>
				<br />
			</div>

		</div>

		<div class="box clear inner-spacer">
			<div class="clear">
				<h1>Cancellation Policy</h1>
				<input type="hidden" name="box_title[]" value="Cancellation Policy" />
			</div>
			<div  class="row ">
				<textarea id="box-2" rows="10" cols="50" name="box_content[]"  class="txt large"><?php echo htmlspecialchars("
<p>Cancellations are accepted by email (bookings@chinacarservice.com) and phone (86 755 2595 1800) only.   Urgent after hour changes can be made on (86 1363 266 7585).</p>
<p>Full reservation amount will be charged for cancellations made less than 12 hrs before scheduled pick up.  50% of the invoice value will be charged for cancellation made less than 24 hrs before scheduled pick up. Cancellations made in excess of 24 hours will not incur any charge.</p>
<p>No shows will be charged the full reservation fare.</p>");?></textarea>
				<br />
			</div>

		</div>

	<?php endif; ?>
	</div>

	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>

	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>
	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container textarea').ckeditor();
});

//-->
</script>