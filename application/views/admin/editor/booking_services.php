
<div class="" id="box-editor">
		<h2>Updating booking forms service options for: <?php echo ucfirst($website -> shortname);?></h2>

  <form method="post" enctype="multipart/form-data" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
  <div id="boxes-container">
    <div class="row">
    <div class="column left">
        <label>Booking form background (1920 X 1080)</label>
        <input type="file" name="background-image"/><br/>
        <span>(keep blank if you don't want to change it)</span>
    </div>
    <div class="column left">
        <label>Current background image</label>
        <?php if (strlen($website->booking_background_image)): ?>
            <div style="padding:10px;background:#460700;width:320px;">
                <img width=320 src="<?php echo booking_background_path($website->booking_background_image) ?>"/>
            </div>
        <?php else: ?>
            Not set.
        <?php endif; ?>
    </div>
    </div>
    <div class="row">
    <div class="column left">
        <label>Website Mobile background (550 X 310)</label>
        <input type="file" name="background-mobile-image"/><br/>
        <span>(keep blank if you don't want to change it)</span>
    </div>
    <div class="column left">
        <label>Current mobile background image</label>
        <?php if (strlen($website->background_mobile_image)): ?>
            <div style="padding:10px;background:#460700;width:320px;">
                <img width=320 src="<?php echo booking_background_path($website->background_mobile_image) ?>"/>
            </div>
        <?php else: ?>
            Not set.
        <?php endif; ?>
    </div>
    </div>
    <div class="clear" style="margin: 20px 0;">
      <label>*Promotional Text:</label>
      <input  type="text"  class="txt" name="promotional_text" value="<?php echo $website -> promotional_text; ?>"/>
    </div>
    <?php foreach($booking_services as $bs){ ?>
      <div class="box clear inner-spacer">
          <a href="#" class="delete" title="delete">Delete</a>
        <div class="clear">
          <label>*Service Title:</label>
          <input  type="text"  class="txt" name="box_title[]" value="<?php echo $bs -> service_name; ?>"/>
          <label>*Name on form (e.g otherRequired1):</label>
          <input  type="text"  class="txt" name="box_form_name[]" value="<?php echo $bs -> form_name; ?>"/>
          <label>*Description:</label>
          <input  type="text"  class="txt" name="box_description[]" value="<?php echo $bs -> description; ?>"/>
          <label>*Order:</label>
          <input  type="text"  class="txt small" name="box_order[]" value="<?php echo $bs -> ranking; ?>"/>
        </div>
        <div class="clear">
          <label class="check-label">Active:</label>
          <input  type="checkbox" class="check-input" name="active[]" <?php if($bs -> active){ echo 'checked="checked"';}?> />
          <input type="hidden" name="active[]" value="">
        </div>
        <div  class="clear ">
          <label class="check-label">Airport transfer:</label>
          <input  type="checkbox" class="check-input" name="airport[]" <?php if($bs -> airport){ echo 'checked="checked"';}?> />
          <input type="hidden" name="airport[]" value="">
          <label class="check-label">Pickup</label>
          <input  type="checkbox" class="check-input" name="pickup[]" <?php if($bs -> pickup){ echo 'checked="checked"';}?>/>
          <input type="hidden" name="pickup[]" value="">
          <label class="check-label">Day hire:</label>
          <input  type="checkbox" class="check-input" name="dayhire[]" <?php if($bs -> dayhire){ echo 'checked="checked"';}?>/>
          <input type="hidden" name="dayhire[]" value="">
          <label class="check-label">Tour:</label>
          <input  type="checkbox" class="check-input" name="tour[]" <?php if($bs -> tour){ echo 'checked="checked"';}?>/>
          <input type="hidden" name="tour[]" value="">
        </div>
      </div>

      <?php } ?>
    </div>
    <a id="add-box-btn">add new service</a>
    <div class="clear">
      <input type="submit" name="submit" class="form_btn" value="Save" />
    </div>

  </form>
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){

  $('#box-editor .delete').live('click', function(evt){
    evt.preventDefault();
    if(confirm('Are your sure you want to delete this service')) {
      $(this).parent().slideUp(function(){$(this).remove();});
    }
  });

  $('#add-box-btn').click(function(evt){
    evt.preventDefault();
    var tpl = ['<div class="box clear inner-spacer">',
        ' <a href="#" class="delete" title="delete">Delete</a>',
        '<div class="clear">',
          '<label>*Service Title:</label>',
          '<input  type="text"  class="txt" name="box_title[]" value="" />',
          '<label>*Name on form (e.g otherRequired1):</label>',
          '<input  type="text"  class="txt" name="box_form_name[]" value="" />',
          '<label>*Description:</label>',
          '<input  type="text"  class="txt" name="box_description[]" value="" />',
          '<label>*Order:</label>',
          '<input  type="text"  class="txt small" name="box_order[]" value="" />',
        '</div>',
        '<div class="clear">',
          '<label class="check-label">Active:</label>',
          '<input  type="checkbox" class="check-input" name="active[]"/>',
          '<input type="hidden" name="active[]" value="">',
        '</div>',
        '<div  class="clear ">',
          '<label class="check-label">Airport transfer:</label>',
          '<input  type="checkbox" class="check-input" name="airport_transfer[]" />',
          '<input type="hidden" name="airport_transfer[]" value="">',
          '<label class="check-label">Airport return:</label>',
          '<input  type="checkbox" class="check-input" name="airport_return[]" />',
          '<input type="hidden" name="airport_return[]" value="">',
          '<label class="check-label">Pickup</label>',
          '<input  type="checkbox" class="check-input" name="hotel_pickup[]"/>',
          '<input type="hidden" name="hotel_pickup[]" value="">',
          '<label class="check-label">Day hire:</label>',
          '<input  type="checkbox" class="check-input" name="dayhire[]"/>',
          '<input type="hidden" name="dayhire[]" value="">',
          '<label class="check-label">Tour:</label>',
          '<input  type="checkbox" class="check-input" name="tour[]"/>',
          '<input type="hidden" name="tour[]" value="">',
        '</div>',
      '</div>'];
  $(tpl.join('')).appendTo('#boxes-container');
  });
});
//-->
</script>