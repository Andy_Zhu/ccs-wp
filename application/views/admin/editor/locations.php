<?php
	$arrivals = array();
	foreach ($boxes as $box) {

		if($box->type == 'arrival') {
			$arrivals[] = $box;
		}else{
			$about_us_box = $box;
		}
	}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">

	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>

	<div id="boxes-container">


	<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Arrivals:</h2>
	<div id="arrivals-container">
	<?php if(!empty($arrivals)):?>
		<?php $counter=0;foreach ($arrivals as $box):?>
		<div class="box clear inner-spacer">
			<a href="#" class="delete" title="delete">Delete</a>
			<div class="clear">
				<label>Order Display:</label>
				<input  type="text" class="txt small" name="rankings[]" value="<?php echo $box->ranking; ?>" />
			</div>

			<div  class="clear ">
				<input id="arrival-<?php echo (++$counter); ?>" name="box_content[]" cols="50" value="<?php echo $box->content; ?>" />
				<br />
			</div>
		</div>
		<?php endforeach;?>
	<?php else:?>

		<div class="box clear inner-spacer">
			<a href="#" class="delete" title="delete">Delete</a>
			<div class="clear">
				<label>Order Display:</label>
				<input  type="text" class="txt small" name="rankings[]" value="" />
			</div>

			<div  class="clear ">
                <input id="arrival-1" cols="50" name="box_content[]" value="" />
				<br />
			</div>
		</div>
	<?php endif;?>
	</div>

	</div>
	<div class="clear row"><a href="#nowhere" id="add-box-btn">+ A arrival Box</a></div>

	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>

	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container #about_us_box').ckeditor();
	$('#boxes-container textarea.arrival').ckeditor();

	$('#box-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var boxCounter = ($('#box-editor .arrival').length);
	$('#add-box-btn').click(function(evt){
		evt.preventDefault();
		id = ++boxCounter;
		var tpl = ['<div class="box clear inner-spacer">',
					'<a href="#" class="delete" title="delete">Delete</a>',
					'<div class="clear">',
						'<label>Order Display:</label>',
						'<input  type="text" class="txt small" name="rankings[]" value="" />',
					'</div>',
					'<div  class="clear ">',
						'<label>*Arrival Destination: </label>',
						'<input id="arrival-' + id + '" name="box_content[]" />',
						'<br />',
					'</div>',
				'</div>'];
	$(tpl.join('')).appendTo('#arrivals-container');
	$('#arrival-' + id).ckeditor(/*{toolbar:arrivalToolbar}*/);
	});
});
//-->
</script>