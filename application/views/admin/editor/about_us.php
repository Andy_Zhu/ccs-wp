<?php
	$testimonials = array();
	foreach ($boxes as $box) {

		if($box->type == 'testimonial') {
			$testimonials[] = $box;
		}else{
			$about_us_box = $box;
		}
	}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">

	<form method="post" enctype="multipart/form-data" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
	<div id="boxes-container">

	<?php if(isset($about_us_box)):?>

			<div class="box clear inner-spacer">
				<div  class="clear ">
          <label >Image (width 800px):</label><input type="file" name="about_img" /><br />
          <?php if($about_us_box -> image){ ?><img width="210" src="<?php echo textbox_image_src($about_us_box->image);?>" /><?php } ?>
          <input type="hidden" name="about_db_img" value="<?php echo $about_us_box->image; ?>">
					<label>About us title</label>
					<input class="txt" name="about_us_title" value="<?php echo $about_us_box->title;?>" />
					<label>About us text</label>
					<textarea id="about_us_box" rows="10" cols="50" name="about_us"  class="txt large"><?php echo htmlspecialchars($about_us_box->content);?></textarea>
					<br />
				</div>

			</div>

	<?php else:?>
		<div class="box clear inner-spacer">
			<div  class="clear ">
				<label>About us: </label>
				<input class="txt" name="about_us_title"/>
				<textarea id="about_us_box" rows="10" cols="50" name="about_us"  class="txt large"></textarea>
				<br />
			</div>

		</div>

	<?php endif;?>
	<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Testimonials:</h2>
	<div id="testimonials-container">
	<?php if(!empty($testimonials)):?>
		<?php $counter=0;foreach ($testimonials as $box):?>
		<div class="box clear inner-spacer">
			<a href="#" class="delete" title="delete">Delete</a>
			<div class="column price left">
				<label>Order Display:</label>
				<input  type="text" class="txt small" name="rankings[]" value="<?php echo $box->ranking; ?>" />
			</div>
			<div class="column price left" >
        <label>Sidebar?:</label>
        <select name="sidebar[]" class="txt small">
          <option value="0" <?php if (!$box->extra) echo 'selected="selected"'; ?>>No</option>
          <option value="1" <?php if ($box->extra) echo 'selected="selected"'; ?>>Yes</option>
        </select>
      </div>
			<div class="clear">
        <label >Image (70 x 50):</label><input type="file" name="testimonial_img_<?php echo $counter; ?>" /><br />
        <?php if($box -> image){ ?><img src="<?php echo textbox_image_src($box->image);?>" /><?php } ?>
        <input type="hidden" name="testimonial_db_img[]" value="<?php echo $box->image; ?>">
      </div>
      <div class="clear">
        <label>title: (Will be used for sidebar, please enter in stated format: name-job title-Company-month-year)</label>
        <input  type="text" class="txt large" name="titles[]" value="<?php echo $box->title; ?>" />
      </div>
			<div  class="clear ">
				<label>*Content: </label>
				<textarea id="testimonial-<?php echo (++$counter); ?>" rows="10" cols="50" name="box_content[]"  class="txt testimonial large"><?php echo htmlspecialchars($box->content); ?></textarea>
				<br />
			</div>
		</div>
		<?php endforeach;?>
	<?php else:?>

		<div class="box clear inner-spacer">
			<a href="#" class="delete" title="delete">Delete</a>
			<div class="column price left">
				<label>Order Display:</label>
				<input  type="text" class="txt small" name="rankings[]" value="" />
			</div>
			<div class="column price left" >
        <label>Sidebar?:</label>
        <select name="sidebar[]" class="txt small">
          <option value="0" selected="selected">No</option>
          <option value="1">Yes</option>
        </select>
      </div>
			<div class="clear">
        <label >Image (70 x 50):</label><input type="file" name="testimonial_img_1" /><br />
      </div>
			<div class="clear">
        <label>title: (Will be used for sidebar, please enter in stated format: name-job title-Company-month-year)</label>
        <input  type="text" class="txt large" name="titles[]" value="" />
      </div>
			<div  class="clear ">
				<textarea id="testimonial-1" rows="10" cols="50" name="box_content[]"  class="txt testimonial large"></textarea>
				<br />
			</div>
		</div>
	<?php endif;?>
	</div>

	</div>
	<div class="clear row"><a href="#nowhere" id="add-box-btn">+ A Testimonial Box</a></div>

	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>

	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container #about_us_box').ckeditor();
	$('#boxes-container textarea.testimonial').ckeditor();

	$('#box-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var boxCounter = ($('#box-editor .testimonial').length);
	$('#add-box-btn').click(function(evt){
		evt.preventDefault();
		id = ++boxCounter;
		var tpl = ['<div class="box clear inner-spacer">',
					'<a href="#" class="delete" title="delete">Delete</a>',
					'<div class="clear">',
						'<label>Order Display:</label>',
						'<input  type="text" class="txt small" name="rankings[]" value="" />',
					'</div>',
					'<div class="clear">',
            '<label >Image (70 x 50):</label><input type="file" name="testimonial_img_'+id+'" /><br />',
          '</div>',
					'<div  class="clear ">',
						'<label>*Content: </label>',
						'<textarea id="testimonial-' + id + '" rows="10" cols="50" name="box_content[]"  class="txt testimonial large"></textarea>',
						'<br />',
					'</div>',
				'</div>'];
	$(tpl.join('')).appendTo('#testimonials-container');
	$('#testimonial-' + id).ckeditor();
	});
});
//-->
</script>