<?php
class TemplateProcessor {

function do_tours($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($ci->input->post('add_tour_cat')) {

			$ci->load->model('Websites_Tours_Categories_Model');
			$title = $ci->input->post('title');
			$desc = $ci->input->post('description');
			$meta_desc = $ci->input->post('meta_desc');
			$meta_keywords = $ci->input->post('meta_keywords');

			if(!$title || !$desc) {
				$ci->session->set_flashdata('info', 'Failed to add tour, Fields marked with * are required.');
				redirect($redirect);
			}

			$tour_cat = array(
				'title'=>$title,
				'description' => $desc,
				'meta_keywords'=>$meta_keywords,
				'meta_desc'=>$meta_desc,
				'website_id'=>$website->id,
				'image' => $ci->input->post('category_image')
			);
			//upload image
			if(!empty($_FILES['userfile']['name'])) {

				$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
		      	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
		      	$config['max_size']  = '';
		      	$config['max_width']  = '';
		      	$config['max_height']  = '';
		      	$config['overwrite'] = TRUE;
		     	//$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
		      	$ci->load->library('upload', $config);

	      		/* Create the config for image library */
	      		$configThumb = array();
	      		$configThumb['image_library'] = 'gd2';
	      		$configThumb['source_image'] = '';
	      		$configThumb['create_thumb'] = FALSE;
	     		$configThumb['maintain_ratio'] = FALSE;

	      		$configThumb['width'] = 405;
	      		$configThumb['height'] = 228;
	      		/* Load the image library */
	      		$ci->load->library('image_lib');

				$img_upload = $ci->upload->do_upload('userfile');

			    if($img_upload != FALSE) {
			    	$img_data = $ci->upload->data();
			    	if($img_data['is_image'] == 1) {
				        $configThumb['source_image'] = $img_data['full_path'];
				        $ci->image_lib->initialize($configThumb);
				        $ci->image_lib->resize();
					    //save the data
					    $tour_cat['image'] = $img_data['file_name'];
					    //delete old pic
					    $old_image = $ci->input->post('category_image');
					  	if(!empty($old_image))
					    @unlink(textbox_image_path( $ci->input->post('category_image') ));
			      	}
			     }
			}

			$tour_cat['image'] = empty($tour_cat['image']) ? NULL : $tour_cat['image'];
			$tour_cat_id = $ci->input->post('tour_cat_id');
			$message = $tour_cat_id ? 'Tour category updated successfully' : 'Tour category added successfully';
			if($tour_cat_id) {
				$ci->Websites_Tours_Categories_Model->update($tour_cat, $tour_cat_id);
			}else{
				$ci->Websites_Tours_Categories_Model->create($tour_cat);
			}

			$ci->Logs_Model->create(array(
				'website_id'=>$website->id,
				'user_id'=>$ci->Users->get_identity()->id,
				'page_uri'=>$section,
				'content'=>$message
			));
			$ci->session->set_flashdata('info', $message);
			redirect($redirect);
		}
}


function do_subtours($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($ci->input->post('add_tour')) {

		//echo '<pre>', print_r($_FILES), '</pre>'; die();

		$cat_id = $ci->input->get('tourcat');

		$ci->load->model('Websites_Tours_Model');
		$title = $ci->input->post('title');
		$intro = $ci->input->post('intro');
		$extra = $ci->input->post('extra');
		$redirect = site_url("admin/edit_website?id=$id&section=$section&tourcat={$cat_id}");
		if(!$title || !$intro) {
			$ci->session->set_flashdata('info', 'Failed to add tour, Fields marked with * are required.');
			redirect($redirect);
		}

		$tour = array(
				'title'=>$title,
				'intro'=>$intro,
				'extra' => $extra
		);

		$tour_id = $ci->input->post('tour_id');
		$is_update = $tour_id > 0 ? true : false;
		$message = $tour_id ? 'Tour updated successfully' : 'Tour added successfully';
		if($tour_id) {
			$ci->Websites_Tours_Model->update($tour, $tour_id);
		}else{
			$tour['tour_cat_id'] = $cat_id;
			$tour_id = $ci->Websites_Tours_Model->create($tour);
		}


		//text boxes
		$boxes = $ci->input->post('tour_boxes');
		$rankings = $ci->input->post('tour_boxes_rankings');
		$tour_images = $ci->input->post('tour_images');
		$box_ids = array();
		$files = $_FILES; //copy the files array
		$tour_boxes_images_names = $ci->input->post('tour_boxes_images_names');// restor images on update
		if(!is_array($tour_boxes_images_names)) $tour_boxes_images_names = array();

		$ci->load->model('Text_Boxes_Model');

		$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
		$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
		$config['max_size']  = '';
		$config['max_width']  = '';
		$config['max_height']  = '';
		$config['overwrite'] = TRUE;
		//$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
		$ci->load->library('upload', $config);

		/* Create the config for image library */
		$configThumb = array();
		$configThumb['image_library'] = 'gd2';
		$configThumb['source_image'] = '';
		$configThumb['create_thumb'] = FALSE;
		$configThumb['maintain_ratio'] = FALSE;

		$configThumb['width'] = 405;
		$configThumb['height'] = 228;
		/* Load the image library */
		$ci->load->library('image_lib');

		if(!empty($boxes)) {
			$ci->Text_Boxes_Model->clear_for_page('tours', $id, 'tour', $tour_id);
			foreach ($boxes as $index => $value) {

				$box_data = array(
						'website_id'=>$id,
						'extra_id' => $tour_id,
						'content'=>$value,
						'type'=>'tour',
						'page_uri' => 'tours',
						'ranking'=>(int)$rankings[$index]
				);

				$_FILES['userfile']['name']= $files['tour_images']['name'][$index];
				$_FILES['userfile']['type']= $files['tour_images']['type'][$index];
				$_FILES['userfile']['tmp_name']= $files['tour_images']['tmp_name'][$index];
				$_FILES['userfile']['error']= $files['tour_images']['error'][$index];
				$_FILES['userfile']['size']= $files['tour_images']['size'][$index];

				//echo '<pre>', print_r($_FILES), '<pre>';

				if(!empty($_FILES['userfile']['name'])) {
					if(array_key_exists($index, $tour_boxes_images_names)) {
						@unlink(textbox_image_path($tour_boxes_images_names[$index]));
					}
					$img_upload = $ci->upload->do_upload();

					if($img_upload != FALSE) {
						$img_data = $ci->upload->data();
						if($img_data['is_image'] == 1) {
							$configThumb['source_image'] = $img_data['full_path'];

							//  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
							//save the data
							$box_data['image'] = $img_data['file_name'];//delete old pic
							$ci->image_lib->initialize($configThumb);
							$ci->image_lib->resize();
						}
					}
				}else if($is_update && array_key_exists($index, $tour_boxes_images_names)) {
					$box_data['image'] = $tour_boxes_images_names[$index];
				}

				$box_data['image'] = empty($box_data['image']) ? NULL : $box_data['image'];
				$box_ids[] = $ci->Text_Boxes_Model->create($box_data);

			}

		}

		$ci->Logs_Model->create(array(
				'website_id'=>$id,
				'user_id'=>$ci->Users->get_identity()->id,
				'page_uri'=>$section,
				'content'=>$message
		));
		$ci->session->set_flashdata('info', $message);

		if($is_update) {
			$redirect = site_url("admin/edit_website?id=$id&section=edittour&tourid={$tour_id}");
		}
		redirect($redirect);

	}
}

function do_edittour($section, $website, $layout_data, $redirect) {
	$this->do_subtours($section, $website, $layout_data, $redirect);
}

function do_banners($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($ci->input->post('banners-submit')) {

		$website_id = $id;
		$banner_contents = $ci->input->post('contents');
		$banner_configs = $ci->input->post('banner_configs');
    $banner_positions = $ci->input->post('banner_positions');
		$banner_page_uris = $ci->input->post('page_uris');
		$bg_colors = $ci->input->post('bg_colors');
		$border_colors = $ci->input->post('border_colors');
    $banner_headers = $ci->input->post('heading');
    $banner_db_img = $ci->input->post('banner_db_img');
		$ci->Website_Banners_Model->clear_all($website_id);

    $box_img_num = 1;
    $config['upload_path'] = BANNERS_IMAGES_DIR; /* NB! create this dir! */
    $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
    $config['max_size']  = '';
    $config['max_width']  = '';
    $config['max_height']  = '';
    $config['overwrite'] = TRUE;
    //$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
    $ci->load->library('upload', $config);

    $configThumb = array();
    $configThumb['image_library'] = 'gd2';
    $configThumb['source_image'] = '';
    $configThumb['create_thumb'] = FALSE;
    $configThumb['maintain_ratio'] = FALSE;

    $configThumb['width'] = 220;
    $configThumb['height'] = 120;
    /* Load the image library */
    $ci->load->library('image_lib');

		foreach ($banner_page_uris as $key => $p_uri) {

			if($p_uri == '') continue;

			$styles = '';
			$bg_color = isset($bg_colors[$key]) ? $bg_colors[$key] : null;
			$border_color = isset($border_colors[$key]) ? $border_colors[$key] : null;

			if($bg_color != '') $styles .= "background-color:$bg_color;";
			if($border_color != '') $styles .= "border-color:$border_color;";

			$banner_data = array(
					'website_id'=>$id,
					'page_uri'=>$p_uri,
					'content'=>$banner_contents[$key],
					'enabled'=>$banner_configs[$key],
					'user_id'=>$ci->Users->get_identity()->id,
					'position'=> $banner_positions[$key],
					'heading'=>$banner_headers[$key]
			);
			if($styles != '') $banner_data['styles'] = $styles;
      $img_upload = $ci->upload->do_upload('banner_img_' . $box_img_num);
      if($img_upload != FALSE) {
        $img_data = $ci->upload->data();
        if($img_data['is_image'] == 1) {
          $configThumb['source_image'] = $img_data['full_path'];
          $ci->image_lib->initialize($configThumb);
          $ci->image_lib->resize();

          //  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
          //save the data
          $banner_data['image'] = $img_data['file_name'];

        }
      }else{
        $banner_data['image'] = $banner_db_img[$key];
      }

			if($ci->Website_Banners_Model->find($website_id, $p_uri)) {
				$ci->Website_Banners_Model->update($website_id, $p_uri, $banner_data);
			}else{
				$ci->Website_Banners_Model->create($banner_data);
			}

      $box_img_num++;

			$ci->Logs_Model->create(array(
					'website_id'=>$id,
					'user_id'=>$ci->Users->get_identity()->id,
					'page_uri'=>$section,
					'content'=>"$p_uri banner updated"
			));
		}
		$ci->session->set_flashdata('info', 'Banners updated successfully');
		redirect($redirect);
	}

}

function do_basic($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'basic' || !$ci->input->post('submit')) return;

	$indata = $ci->input->post();

	//logo upload
	$config['upload_path'] = LOGOS_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;
	$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] = 309;
	$configThumb['height'] = 64;
	/* Load the image library */
	$ci->load->library('image_lib');

	$upload = $ci->upload->do_upload('logo');
	if($upload != FALSE) {
		$logo_data = $ci->upload->data();
		if($logo_data['is_image'] == 1) {
			$configThumb['source_image'] = $logo_data['full_path'];
			$ci->image_lib->initialize($configThumb);
			$ci->image_lib->resize();

			if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
			//save the data
			$indata['logo'] = $logo_data['file_name'];

		}
	}

  $config['file_name'] = $id. '-booking-' . $ci->Websites_Model->seo_name($indata['name']);
  $ci->upload->initialize($config);
  $upload = $ci->upload->do_upload('booking_form_logo');
  if($upload != FALSE) {
    $logo_data = $ci->upload->data();
    if($logo_data['is_image'] == 1) {
      $configThumb['source_image'] = $logo_data['full_path'];
      $ci->image_lib->initialize($configThumb);
      $ci->image_lib->resize();

      if($logo_data['file_name'] != $website->booking_form_logo) @unlink(site_logo_path($website->booking_form_logo));
      //save the data
      $indata['booking_form_logo'] = $logo_data['file_name'];
    }
  }

	unset($indata['submit']);
	$indata['alt_website_id'] = implode(',', $indata['alt_website_id']);
	$ci->Websites_Model->update($indata, $id);
	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_faqs($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section !='faqs' || !$ci->input->post('submit')) return;

	$content = $ci->input->post('static_content');
	$name = $ci->input->post('page_name');
	if(empty($name)) $name = ucwords(str_replace('-', ' ', $section));

	if($ci->Static_Pages_Model->has_page( $section , $id)) {
		$ci->Static_Pages_Model->update(array('content'=>$content, 'name'=>$name), $section, $id);
	}else{
		$indata = array('website_id'=>$id, 'name'=>$name,'uri'=>$section, 'content'=>$content);
		$ci->Static_Pages_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');
}

function do_policies($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section !='policies' || !$ci->input->post('submit')) return;

	$content = $ci->input->post('static_content');
	$name = $ci->input->post('page_name');
	if(empty($name)) $name = ucwords(str_replace('-', ' ', $section));

	if($ci->Static_Pages_Model->has_page( $section , $id)) {
		$ci->Static_Pages_Model->update(array('content'=>$content, 'name'=>$name), $section, $id);
	}else{
		$indata = array('website_id'=>$id, 'name'=>$name,'uri'=>$section, 'content'=>$content);
		$ci->Static_Pages_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');
}

function do_online_booking($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'online-booking' || !$ci->input->post('submit') ) return;

	$titles = $ci->input->post('box_title');
	$contents = $ci->input->post('box_content');
	$rankings = $ci->input->post('rankings');
	$ci->Text_Boxes_Model->clear_for_page($section, $id);

	for($i=0; $i < count($titles); $i++) {
		if(strlen($contents[$i]) == 0 ) continue;
		$title = empty($titles[$i]) ? null : $titles[$i];
		$indata = array('ranking'=>null, 'website_id'=>$id, 'page_uri'=>$section, 'title'=>$title, 'content'=>$contents[$i]);
		$indata['ranking'] = $rankings[$i]? $rankings[$i] : $i;
		$ci->Text_Boxes_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_rates($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'rates' || !$ci->input->post('submit') ) return;

	$titles = $ci->input->post('box_title');
	$contents = $ci->input->post('box_content');
	$rankings = $ci->input->post('rankings');
	$ci->Text_Boxes_Model->clear_for_page($section, $id);

	for($i=0; $i < count($titles); $i++) {
		if(strlen($titles[$i]) ==0  || strlen($contents[$i]) == 0 ) continue;
		$indata = array('ranking'=>null, 'website_id'=>$id, 'page_uri'=>$section, 'title'=>$titles[$i], 'content'=>$contents[$i]);
		$indata['ranking'] = $rankings[$i]? $rankings[$i] : $i;

		$ci->Text_Boxes_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_contact_us($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'contact-us' || !$ci->input->post('submit') ) return;

	$titles = $ci->input->post('box_title');
	$contents = $ci->input->post('box_content');
	$rankings = $ci->input->post('rankings');
	$ci->Text_Boxes_Model->clear_for_page($section, $id);

	for($i=0; $i < count($titles); $i++) {
		if(strlen($titles[$i]) ==0  || strlen($contents[$i]) == 0 ) continue;
		$indata = array('ranking'=>null, 'website_id'=>$id, 'page_uri'=>$section, 'title'=>$titles[$i], 'content'=>$contents[$i]);
		if($section == 'rates') {
			$indata['ranking'] = $rankings[$i]? $rankings[$i] : null;
		}
		$ci->Text_Boxes_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_about_us($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'about-us' || !$ci->input->post('submit')) return;

  $sidebar = $ci->input->post('sidebar');
  if (count(array_keys($sidebar, 1)) > 1){
    $ci->session->set_flashdata('info', 'Update failed. Only 1 testimonial allowed on sidebar');
    return;
  }

  $config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
  $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
  $config['max_size']  = '';
  $config['max_width']  = '';
  $config['max_height']  = '';
  $config['overwrite'] = TRUE;

  //$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
  $ci->load->library('upload', $config);

  $configThumb = array();
  $configThumb['image_library'] = 'gd2';
  $configThumb['source_image'] = '';
  $configThumb['create_thumb'] = FALSE;
  $configThumb['maintain_ratio'] = TRUE;
  $configThumb['width'] = 800;
  $configThumb['height'] = 800;
	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'normal');
	$indata = array(
			'website_id'=>$id,
			'page_uri'=>$section,
			'content'=>$ci->input->post('about_us'),
			'title'=>$ci->input->post('about_us_title')
	);

  $img_upload = $ci->upload->do_upload('about_img');
  if($img_upload != FALSE) {
    $img_data = $ci->upload->data();
    if($img_data['is_image'] == 1) {
      $configThumb['source_image'] = $img_data['full_path'];
      $ci->image_lib->initialize($configThumb);
      $ci->image_lib->resize();
      //  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
      //save the data
      $indata['image'] = $img_data['file_name'];
    }
  }else{
    $about_db_img = $ci->input->post('about_db_img');
    $indata['image'] = $about_db_img;
  }

	$ci->Text_Boxes_Model->create($indata);
	//testimonials



  /* Create the config for image library */
  $configThumb['maintain_ratio'] = FALSE;

  $configThumb['width'] = 70;
  $configThumb['height'] = 50;
  /* Load the image library */
  $ci->load->library('image_lib');

	$contents = $ci->input->post('box_content');
	$rankings = $ci->input->post('rankings');
  $titles = $ci->input->post('titles');
  $testimonial_db_img = $ci->input->post('testimonial_db_img');
	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'testimonial');
	//echo  '<pre>', print_r($rankings), '</pre>'; die();

	for($i=0; $i < count($contents); $i++) {
		if(strlen($contents[$i]) == 0 ) continue;
		$indata = array(
				'ranking'=>($rankings[$i]? $rankings[$i] : null),
				'website_id'=>$id,
				'page_uri'=>$section,
				'title'=>$titles[$i],
				'content'=>$contents[$i],
				'extra'=>$sidebar[$i],
				'type'=>'testimonial'
		);
    $img_upload = $ci->upload->do_upload('testimonial_img_'.$i);
    if($img_upload != FALSE) {
      $img_data = $ci->upload->data();
      if($img_data['is_image'] == 1) {
        $configThumb['source_image'] = $img_data['full_path'];
        $ci->image_lib->initialize($configThumb);
        $ci->image_lib->resize();
        //  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
        //save the data
        $indata['image'] = $img_data['file_name'];

      }
    }else{
      $indata['image'] = $testimonial_db_img[$i];
    }

		$ci->Text_Boxes_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}


function do_locations($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section !='locations' || !$ci->input->post('submit')) return;

	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'normal');
	$indata = array(
			'website_id'=>$id,
			'page_uri'=>$section,
			'content'=>$ci->input->post('locations'),
			'title'=>$ci->input->post('locations_title')
	);
	$ci->Text_Boxes_Model->create($indata);
	//testimonials
	$contents = $ci->input->post('box_content');
	$rankings = $ci->input->post('rankings');
	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'arrival');
	//echo  '<pre>', print_r($rankings), '</pre>'; die();

	for($i=0; $i < count($contents); $i++) {
		if(strlen($contents[$i]) == 0 ) continue;
		$indata = array(
				'ranking'=>($rankings[$i]? $rankings[$i] : null),
				'website_id'=>$id,
				'page_uri'=>$section,
				'title'=>null,
				'content'=>$contents[$i],
				'type'=>'arrival'
		);

		$ci->Text_Boxes_Model->create($indata);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');
}


function do_services($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'services' || !$ci->input->post('submit')) return;

	//update non top boxes
	$titles = $ci->input->post('box_title');
	$contents = $ci->input->post('box_content');
	$rankings = $ci->input->post('rankings');
	$ci->Text_Boxes_Model->clear_for_page($section, $id, 'normal');
	for($i=0; $i < count($titles); $i++) {
		if(strlen($titles[$i]) ==0  || strlen($contents[$i]) == 0 ) continue;
		$indata = array(
				'website_id'=>$id,
				'page_uri'=>$section,
				'title'=>$titles[$i],
				'content'=>$contents[$i],
				'ranking'=>($rankings[$i] ? $rankings[$i] : null)
		);
		$ci->Text_Boxes_Model->create($indata);
	}

	$in_top_boxes = $ci->input->post('top_boxes');
  $in_top_boxes_title = $ci->input->post('top_boxes_title');
	$in_top_boxes_ids = $ci->input->post('top_boxes_ids');
	$in_top_boxes_rankings = $ci->input->post('top_boxes_rankings');

	if(!$in_top_boxes_ids) $in_top_boxes_ids = array();

	$current_top_boxes_ids = array_keys($layout_data['services_top']);
	//delete any deleted box!
	foreach ($current_top_boxes_ids as $top_id) {
		if(!in_array($top_id, $in_top_boxes_ids)) {
			$old_b = $layout_data['services_top'][$top_id];
			$ci->Text_Boxes_Model->delete($top_id, $id);
			if(strlen($old_b->image))@unlink(textbox_image_path($old_b->image));
		}
	}
	$box_img_num = 1;

	$config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;
	//$config['file_name'] = $id. '-' . $ci->Websites_Model->seo_name($indata['name']);
	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] = 405;
	$configThumb['height'] = 228;
	/* Load the image library */
	$ci->load->library('image_lib');

	foreach ($in_top_boxes as $key => $content) {
		$content = trim($content);
		if(strlen($content) == 0) continue;
		$box_data = array(
				'page_uri'=>$section,
				'website_id'=>$id,
				'type'=>'top',
				'title' => ($in_top_boxes_title[$key]? $in_top_boxes_title[$key] : null),
				'content'=>$content,
				'ranking'=>($in_top_boxes_rankings[$key] ? $in_top_boxes_rankings[$key] : null)
		);

		$top_box_id = $in_top_boxes_ids[$key];

		$img_upload = $ci->upload->do_upload('top_img_' . $box_img_num);
		if($img_upload != FALSE) {
			$img_data = $ci->upload->data();
			if($img_data['is_image'] == 1) {
				$configThumb['source_image'] = $img_data['full_path'];
				$ci->image_lib->initialize($configThumb);
				$ci->image_lib->resize();

				//  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
				//save the data
				$box_data['image'] = $img_data['file_name'];

			}
		}

		if($top_box_id > 0) {
			$ci->Text_Boxes_Model->update($box_data, $top_box_id);
		}else{
			$ci->Text_Boxes_Model->create($box_data);
		}
		$box_img_num++;
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}


function do_home($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;
	$site_id = $id;

	if($section !='home' || !$ci->input->post('submit')) return;

	$config['upload_path'] = SLIDE_SHOWS_DIR; /* NB! create this dir! */
	$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
	$config['max_size']  = '';
	$config['max_width']  = '';
	$config['max_height']  = '';
	$config['overwrite'] = TRUE;

	$ci->load->library('upload', $config);

	/* Create the config for image library */
	$configThumb = array();
	$configThumb['image_library'] = 'gd2';
	$configThumb['source_image'] = '';
	$configThumb['create_thumb'] = FALSE;
	$configThumb['maintain_ratio'] = FALSE;

	$configThumb['width'] =  865;
	$configThumb['height'] =  450;
	/* Load the image library */
	$ci->load->library('image_lib');

	/* We have 4 files to upload
	 * If you want more - change the 5 below as needed
	*/
	for($i = 1; $i < 5; $i++) {
		/* Handle the file upload */
		$config['file_name'] = "$id-slide-$i";
		$config['overwrite'] = TRUE;
		$ci->upload->initialize($config);
		$upload = $ci->upload->do_upload('slideshow'.$i);
		/* File failed to upload - continue */
		if($upload === FALSE){
			continue;
		}
		/* Get the data about the file */
		$data = $ci->upload->data();
		$uploadedFiles[$i] = $data;
		/* If the file is an image - create a thumbnail */
		if($data['is_image'] == 1) {
			$configThumb['source_image'] = $data['full_path'];
			$ci->image_lib->initialize($configThumb);
			$ci->image_lib->resize();

			$image = $ci->Slideshow_Images_Model->is_set($id, $i);
			if($image) {
				$ci->Slideshow_Images_Model->update(array('filename'=>$data['file_name'], 'orig_name'=>$data['client_name']), $id, $i);
			}else{
				$ci->Slideshow_Images_Model->create(array('filename'=>$data['file_name'], 'orig_name'=>$data['client_name'], 'website_id'=>$id, 'slide_id'=>$i));
			}
		}
	}
	$ci->Text_Boxes_Model->clear_for_page($section, $id);
	$data = array('website_id'=>$id, 'page_uri'=>$section, 'title'=>'fleet include', 'content'=>$ci->input->post('fleet_include'));
	$ci->Text_Boxes_Model->create($data);

	$data = array('website_id'=>$id, 'page_uri'=>$section, 'title'=>'provided services', 'content'=>$ci->input->post('provided_service'));
	$ci->Text_Boxes_Model->create($data);

  $data = array('website_id'=>$id, 'page_uri'=>$section, 'title'=>'carousel title 1', 'content'=>$ci->input->post('carousel_title_1'));
  $ci->Text_Boxes_Model->create($data);
  $data = array('website_id'=>$id, 'page_uri'=>$section, 'title'=>'carousel title 2', 'content'=>$ci->input->post('carousel_title_2'));
  $ci->Text_Boxes_Model->create($data);

	$short_about = trim($ci->input->post('short_about'));
	$ci->load->model('Websites_Model');
	$site_update = array('short_about'=>$short_about);
	$ci->Websites_Model->update($site_update, $id);

  $in_top_boxes = $ci->input->post('top_boxes');
  $in_top_boxes_ids = $ci->input->post('top_boxes_ids');
  $in_top_boxes_rankings = $ci->input->post('top_boxes_rankings');
  if(!$in_top_boxes_ids) $in_top_boxes_ids = array();

  $current_top_boxes_ids = array_keys($layout_data['home_about_boxes']);
  //delete any deleted box!
  foreach ($current_top_boxes_ids as $top_id) {
    if(!in_array($top_id, $in_top_boxes_ids)) {
      $old_b = $layout_data['home_about_boxes'][$top_id];
      $ci->Text_Boxes_Model->delete($top_id, $id);
      if(strlen($old_b->image))@unlink(textbox_image_path($old_b->image));
    }
  }

  $box_img_num = 1;

  $config['upload_path'] = TEXT_BOXES_IMAGES_DIR; /* NB! create this dir! */
  $configThumb['width'] =  405;
  $configThumb['height'] =  228;


  foreach ($in_top_boxes as $key => $content) {
    $config['file_name'] = $ci->input->post('top_img_' . $box_img_num);
    $ci->upload->initialize($config);
    $content = trim($content);
    if(strlen($content) == 0) continue;
    $box_data = array(
        'page_uri'=>$section,
        'website_id'=>$id,
        'type'=>'about',
        'content'=>$content,
        'ranking'=>($in_top_boxes_rankings[$key] ? $in_top_boxes_rankings[$key] : null),
    );

    $top_box_id = $in_top_boxes_ids[$key];

    $img_upload = $ci->upload->do_upload('top_img_' . $box_img_num);
    if($img_upload != FALSE) {
      $img_data = $ci->upload->data();
      if($img_data['is_image'] == 1) {
        $configThumb['source_image'] = $img_data['full_path'];
        $ci->image_lib->initialize($configThumb);
        $ci->image_lib->resize();

        //  if($logo_data['file_name'] != $website->logo) @unlink(site_logo_path($website->logo));
        //save the data
        $box_data['image'] = $img_data['file_name'];
      }
    }

    if($top_box_id > 0) {
      $ci->Text_Boxes_Model->update($box_data, $top_box_id);
    }else{
      $ci->Text_Boxes_Model->create($box_data);
    }
    $box_img_num++;
  }

	$ci->session->set_flashdata('info', 'Update saved successfully');

}


function do_fleet($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if($section != 'fleet' || !$ci->input->post('submit')) return;

	$title = 'Fleet';
	//save the into
	$content = $ci->input->post('intro');
	if(strlen($content) != 0) {
		$ci->Text_Boxes_Model->clear_for_page($section, $id);
		$data = array('website_id'=>$id, 'page_uri'=>$section, 'title'=>$title, 'content'=>$content);
		$ci->Text_Boxes_Model->create($data);
	}

	//save vehicles
	$ci->Website_Vehicles_Model->clear_for_website($id);
	$vehicles_ids = $ci->input->post('vehicle_id');
	$half_day_price = $ci->input->post('half_day_price');
	$full_day_price = $ci->input->post('full_day_price');
  $half_day_hour = $ci->input->post('half_day_hour');
  $full_day_hour = $ci->input->post('full_day_hour');
	$extra_hour_price = $ci->input->post('extra_hour_price');
  $airport_transfer_price = $ci->input->post('airport_transfer_price');
  $border_crossing_price = $ci->input->post('border_crossing_price');
	$rankings = $ci->input->post('rankings');
	$extra_km_prices = $ci->input->post('extra_km_price');
	$other_info = $ci->input->post('other_info');
  $homepage = $ci->input->post('homepage');

	for ($i=0; $i<count($vehicles_ids); $i++) {

		$v_id = $vehicles_ids[$i];
		$h_price = $half_day_price[$i];
		$f_price = $full_day_price[$i];
    $h_hour = $half_day_hour[$i];
    $f_hour = $full_day_hour[$i];
		$extra_h = $extra_hour_price[$i];
		$ranking = intval($rankings[$i]);
		$extra_k = $extra_km_prices[$i];
    $at_price = $airport_transfer_price[$i];
    $bc_price = $border_crossing_price[$i];
		$info = $other_info[$i];
    $home = $homepage[$i];

		if(!$v_id || !$h_price || !$f_price || !$extra_h) continue; //required fields

		$vehicle = array(
				'website_id'=>$id,
				'vehicle_id'=>$v_id,
				'half_day_price'=>$h_price,
				'full_day_price'=>$f_price,
				'half_day_hour'=>$h_hour,
        'full_day_hour'=>$f_hour,
				'extra_hours_price'=>$extra_h,
				'airport_transfer_price'=>$at_price,
				'border_crossing_price' => $bc_price,
				'ranking'=>$ranking,
				'extra_km_price'=>$extra_k,
				'other_info'=>$info,
				'homepage' => $home
		);

		$ci->Website_Vehicles_Model->create($vehicle);
	}

	$ci->session->set_flashdata('info', 'Update saved successfully');

}

function do_post_extra($section, $website, $layout_data, $redirect) {

	$ci =& get_instance();
	$id = $website->id;

	if(!$ci->input->post('submit')) return;

	if($section != 'basic') {

		if ($ci->Pages_Model->find($id, $section)) {
			$ci->Pages_Model->update(array(
					'note'=>$ci->input->post('page_note'),
					'meta_description'=>$ci->input->post('meta_description'),
					'meta_keywords'=>$ci->input->post('meta_keywords'),
					'meta_title' => $ci->input->post('meta_title'),
			), $id, $section);
		}else{
			$ci->Pages_Model->create(array(
					'website_id'=>$id,
					'page_uri'=>$section,
					'note'=>$ci->input->post('page_note'),
					'meta_description'=>$ci->input->post('meta_description'),
					'meta_keywords'=>$ci->input->post('meta_keywords'),
					'meta_title' => $ci->input->post('meta_title'),
			));
		}
	}

	//update last update field
	$ci->Websites_Model->update(array('last_update_date'=>date('Y-m-d H:i:s')), $id);
	//log the update
	$message = ($section == 'basic' ? 'Site Settings' : ucwords(str_replace('-', ' ', $section))) . ' Updated';

	$ci->Logs_Model->create(array(
			'website_id'=>$id,
			'user_id'=>$ci->Users->get_identity()->id,
			'page_uri'=>$section,
			'content'=>$message
	));

	redirect($redirect);
}

function do_booking_services($section, $website, $layout_data, $redirect) {
  $ci =& get_instance();
  $id = $website->id;
  if(!$ci->input->post('submit')) return;

  //logo upload
  $config['upload_path'] = BACKGROUND_DIR; /* NB! create this dir! */
  $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
  $config['max_size']  = '';
  $config['max_width']  = '';
  $config['max_height']  = '';
  $config['overwrite'] = TRUE;
  $config['file_name'] = $id. '-background';
  $ci->load->library('upload', $config);

  /* Create the config for image library */
  $configThumb = array();
  $configThumb['image_library'] = 'gd2';
  $configThumb['source_image'] = '';
  $configThumb['create_thumb'] = FALSE;
  $configThumb['maintain_ratio'] = FALSE;

  $configThumb['width'] = 1920;
  $configThumb['height'] = 1080;
  /* Load the image library */
  $ci->load->library('image_lib');

  $upload = $ci->upload->do_upload('background-image');
  if($upload != FALSE) {
    $background_data = $ci->upload->data();
    if($background_data['is_image'] == 1) {
      $configThumb['source_image'] = $background_data['full_path'];
      $ci->image_lib->initialize($configThumb);
      $ci->image_lib->resize();

      if($background_data['file_name'] != $website->booking_background_image) @unlink(booking_background_path($website->booking_background_image));
      //save the data
      $ci->Websites_Model->update(array('booking_background_image' => $background_data['file_name']), $id);
    }
  }

  $configThumb['width'] =  550;
  $configThumb['height'] =  310;
  $config['file_name'] = $id. '-background-mobile';
  $ci->upload->initialize($config);
  $upload = $ci->upload->do_upload('background-mobile-image');
  if($upload != FALSE) {
    $background_mobile_data = $ci->upload->data();
    if($background_mobile_data['is_image'] == 1) {
      $configThumb['source_image'] = $background_mobile_data['full_path'];
      $ci->image_lib->initialize($configThumb);
      $ci->image_lib->resize();

      if($background_mobile_data['file_name'] != $website->background_mobile_image) @unlink(booking_background_path($website->background_mobile_image));
      //save the data
      $ci->Websites_Model->update(array('background_mobile_image' => $background_mobile_data['file_name']), $id);
    }
  }

  $promotional_text = $ci->input->post('promotional_text');

  $ci->Websites_Model->update(array('promotional_text' => $promotional_text), $id);


  $service_name     = $ci->input->post('box_title');
  $order            = $ci->input->post('box_order');
  $form_name        = $ci->input->post('box_form_name');
  $description      = $ci->input->post('box_description');
  $active           = $ci->input->post('active');
  $airport          = $ci->input->post('airport');
  $pickup           = $ci->input->post('pickup');
  $dayhire          = $ci->input->post('dayhire');
  $tour             = $ci->input->post('tour');

  // $other =  $ci->input->post('other');
  $ci->Booking_Services_Model->clear_for_page($id);
  for($i=0; $i < count($service_name); $i++) {
    if(strlen($service_name[$i]) ==0) continue;
    (int)$order[$i] = $order[$i];
    if($active[$i] == "on"){
      $active[$i] = 1;
      unset($active[$i+1]);
      $active = array_values($active);
    }else{
      $active[$i] = 0;
    }
    if($airport[$i] == "on"){
      $airport[$i] = 1;
      unset($airport[$i+1]);
      $airport = array_values($airport);
    }else{
      $airport[$i] = 0;
    }
    if($pickup[$i] == "on"){
      $pickup[$i] = 1;
      unset($pickup[$i+1]);
      $pickup = array_values($pickup);
    }else{
      $pickup[$i] = 0;
    }
    if($dayhire[$i] == "on"){
      $dayhire[$i] = 1;
      unset($dayhire[$i+1]);
      $dayhire = array_values($dayhire);
    }else{
      $dayhire[$i] = 0;
    }
    if($tour[$i] == "on"){
      $tour[$i] = 1;
      unset($tour[$i+1]);
      $tour = array_values($tour);
    }else{
      $tour[$i] = 0;
    }
    // if($other[$i] == "on"){
      // $other[$i] = 1;
      // unset($other[$i+1]);
      // $other = array_values($other);
    // }else{
       // $other[$i] = 0;
    // }
    if($service_name[$i] == ''){
      continue;
    }
    $indata = array(
      'ranking' => $order[$i],
      'service_name'=>$service_name[$i],
      'website_id'=>$id, 'active'=>$active[$i],
      'airport'=>$airport[$i],
      'pickup'=>$pickup[$i],
      'dayhire' => $dayhire[$i],
      'tour' => $tour[$i],
      'form_name' => $form_name[$i],
      'description' => $description[$i],
    // , 'other' => $other[$i]
    );
    //var_dump($indata);
    $ci->Booking_Services_Model->create($indata);
  }
  $ci->session->set_flashdata('info', 'Update saved successfully');
}

}//end of class