<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">
	<div class="inner-spacer clear row" style="background:#fff;margin-bottom:20px;">
		<h1>Editting tour: <?php echo $tour->title;?></h1>
		<div style="font-size:15px;">
			<a href="<?php echo site_url("admin/edit_website?id=$id&section=tours")?>">Tour categories</a> &gt;
			<a href="<?php echo site_url("admin/edit_website?id=$id&section=subtours&tourcat={$tour->tour_cat_id}")?>"><?php echo $tour_category->title;?></a> &gt;
			<?php echo $tour->title;?>
		</div>
	</div>

	<form enctype="multipart/form-data"  id="add-tour-form" method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section&tourid={$tour->id}")?>">
	<div class="clear box inner-spacer tour-box">
		<div class="clear">
			<label>*Title: </label>
			<input  type="text" class="txt" name="title" value="<?php echo $tour->title;?>" />
		</div>
		<div class="clear">
		<label>Introduction: </label>
			<textarea id="note-0" rows="10" cols="50" name="intro"  class="txt large editor"><?php echo $tour->intro;?></textarea>
		</div>
		<h2 style="padding:5px; background:#999;">Descriptions</h2>
		<div class="tour-desc">
			<?php if(!empty($tour_text_boxes)):?>
				<?php foreach ($tour_text_boxes as $box):?>
					<div class="box top-section-box clear inner-spacer" id="tour-box-<?php echo $box->id;?>" style="background:#f0f0f0;">
						<a href="<?php echo site_url('admin/ajax/tours/deletebox/' . $box->id);?>" class="delete top-section-delete" title="delete">Delete</a>
						<div class="column left column-220" style="margin-right:20px;">
							<div class="clear">
								<label >Image (405 x 228):</label><input type="file" name="tour_images[]" /><br />
							</div>
							<div class="">
								<label>Current Image:</label>
								<?php if(file_exists(textbox_image_path($box->image))):?>
									<img width="210" src="<?php echo textbox_image_src($box->image, true);?>" />
								<?php else:?>
								<h3>Not set</h3>
								<?php endif;?>
								<input type="hidden" name="tour_boxes_images_names[]" value="<?php echo $box->image?>" />
							</div>
						</div>
						<div class="column left" style="width:400px;">
							<label>Text:</label>
							<textarea id="top-editor-<?php echo $box->id?>" rows="8" cols="30" name="tour_boxes[]" style="width:400px;" class="txt editor"><?php echo $box->content;?></textarea>
							<br /><strong>Order Display</strong>
							<input type="text" style="width:20px;" class="txt small" name="tour_boxes_rankings[]" value="<?php echo $box->ranking;?>" />
						</div>
						<div class="clear">&nbsp;</div>
					</div>
				<?php endforeach;?>
			<?php else:?>
			<div class="box top-section-box clear inner-spacer" style="background:#f0f0f0;">
				<a href="#" class="delete top-section-delete unsaved" title="delete">Delete</a>
				<div class="column left column-220" style="margin-right:20px;">
					<div class="clear">
						<label >Image (405 x 228):</label><input type="file" name="tour_images[]" /><br />
					</div>
					<div class="">
						<label>Current Image:</label>
						<h3>Not set</h3>
					</div>
				</div>
				<div class="column left" style="width:400px;">
					<label>Text:</label>
					<textarea id="top-editor-1" rows="8" cols="30" name="tour_boxes[]" style="width:400px;" class="txt editor"></textarea>
					<br /><strong>Order Display</strong>
					<input type="text" style="width:20px;" class="txt small" name="tour_boxes_rankings[]" value="" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<?php endif;?>
		</div>


		<div class="clear row"><a href="#nowhere" id="" class="add-tour-desc-btn">+ New description</a></div>
 		<div class="clear">
 			<label>Extra (Price &amp; vehicle info...etc):</label>
 			<textarea id="extra-editor-1" rows="8" cols="30" name="extra" class="txt large editor"><?php echo $tour->extra;?></textarea>
 		</div>
 		<div class="clear" style="padding-top:10px;">
 			<input type="hidden" name="tour_id" value="<?php echo $tour->id?>" />
 			<input type="hidden" name="tourcat" value="<?php echo $tour->tour_cat_id;?>" />
			<input type="submit" name="add_tour" value="save" class="form_btn" />
		</div>

	</div>
	</form>
	<br />

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('.editor').ckeditor();

	$('#box-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$_target = $(this);
			if($_target.hasClass('unsaved')) {
				$_target.parent().remove();
				return;
			}
			$.get($(this).attr('href'), function(data){
				if(data.success) {
					$('#tour-box-' + data.id).slideUp(function(){$(this).remove();});
				}else{
					alert(data.message);
				}
			});
		}
	});

	$('.add-tour-desc-btn').click(function(evt){
		evt.preventDefault();
		var id = new Date().getMilliseconds();
		var tpl = ['<div class="box top-section-box clear inner-spacer" style="background:#f0f0f0;">',
					'<a href="#" class="delete top-section-delete unsaved" title="delete">Delete</a>',
					'<div class="column left column-220" style="margin-right:20px;">',
						'<div class="clear">',
							'<label >Image (210 x 118):</label><input type="file" name="tour_images[]" /><br />',
						'</div>',
						'<div class="">',
							'<label>Current Image:</label>',
							'<h3>Not set</h3>',
							'<input type="hidden" name="tour_boxes_images_names[]" value="" />',
						'</div>',
					'</div>',
					'<div class="column left" style="width:400px;">',
						'<label>Text:</label>',
						'<textarea id="top-editor-' + id +'" rows="8" cols="30" name="tour_boxes[]" class="txt top-editor" style="width:400px;"></textarea>',
						'<br /><strong>Order Display</strong>',
						'<input type="text" style="width:20px;" class="txt small" name="tour_boxes_rankings[]" value="" />',
					'</div>',
					'<div class="clear">&nbsp;</div>',
					'</div>'];
		$(tpl.join('')).appendTo($(this).parents('.tour-box').find('.tour-desc'));

		$('#top-editor-' + id).ckeditor();
	});
});
//-->
</script>