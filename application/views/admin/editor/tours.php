<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">
	<div style="background:#666;margin-top:5px;">
		<input type="button" id="add-tour-cat-btn" value="+ Add Tour Category" class="form_btn"/>
	</div>
	<form enctype="multipart/form-data" id="add-tour-cat-form" method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<div class="clear box inner-spacer">
		<div class="clear">
			<label>*Title: </label>
			<input  type="text" class="txt" name="title" />
		</div>
		<div class="clear">
			<div class="left">
				<label>Meta description: </label>
					<textarea rows="1" cols="50" class="txt" name="meta_desc"><?php ?></textarea>
			</div>
			<div class="right">
				<label>Meta keywords: </label>
					<textarea rows="1" cols="50" class="txt" name="meta_keywords"><?php ?></textarea>
			</div>
		</div>
		<div class="clear">
			<div class="left">
				<label>Image (405 x 228, auto scaled):</label>
				<input type="file" name="userfile" />
			</div>
			<div class="right" style="width:480px;">
				<label>*Description:</label>
				<textarea id="note-0" rows="10" cols="50" name="description" style="width:450px;"  class="txt editor"></textarea>
			</div>
		</div>
 		<div class="clear" style="text-align:left;padding-top:10px;">
			<input type="submit" name="add_tour_cat" value="save" class="form_btn" />
		</div>
	</div>
	</form>
	<br />
	<h1 style="background:#666; color:#ccc;padding:5px 8px">Added Tours' Categories (sub-pages):</h1>
	<br />
	<div id="boxes-container">
		<?php if(empty($tours_categories)):?>
			<p class="info row">No tour categories have been added, To add a tour category Please click on <strong>Add Tour Category</strong> button above.</p>
		<?php else:?>
		<?php foreach ($tours_categories as $tour_cat):?>

		<div class="box clear inner-spacer tour-box" id="tour-cat-<?php echo $tour_cat->id; ?>">
			<form enctype="multipart/form-data" method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
			<a href="<?php echo site_url('admin/ajax/tourscats/delete/' . $tour_cat->id);?>" class="delete" title="delete">Delete</a>

				<div class="clear">
					<label>*Title: </label>
					<input  type="text" class="txt" name="title" value="<?php echo $tour_cat->title;?>" />
					<input type="hidden" name="tour_cat_id" value="<?php echo $tour_cat->id?>" />
				</div>
				<div class="clear">
					<div class="left">
						<label>Meta description: </label>
							<textarea rows="1" cols="50" class="txt" name="meta_desc"><?php echo $tour_cat->meta_desc ?></textarea>
					</div>
					<div class="right">
						<label>Meta keywords: </label>
							<textarea style="width:300px;" rows="1" cols="50" class="txt" name="meta_keywords"><?php echo $tour_cat->meta_keywords; ?></textarea>
					</div>
				</div>
				<div class="clear">
					<div class="left">
						<label>Image (600x337, auto scaled):</label>
						<input type="file" name="userfile" />
						<label>Current Image:</label>
						<?php if(!empty($tour_cat->image) && file_exists(textbox_image_path($tour_cat->image))):?>
							<img width="210" alt="" src="<?php echo textbox_image_src($tour_cat->image)?>" />
							<input type="hidden" name="category_image" value="<?php echo $tour_cat->image;?>" />
						<?php else:?>
							No Image has been set yet.
						<?php endif;?>
					</div>
					<div class="right" style="width:480px;">
						<label>*Description:</label>
						<textarea id="note-<?php echo $tour_cat->id?>" rows="10" cols="50" name="description" style="width:450px;"  class="txt editor"><?php echo $tour_cat->description?></textarea>
					</div>
				</div>
		 		<div class="clear" style="text-align:left;padding-top:10px;">
					<input type="submit" name="add_tour_cat" value="save" class="form_btn" />
					<a class="form_btn right"
					href="<?php echo site_url("admin/edit_website?id=$id&section=subtours&tourcat=" . $tour_cat->id)?>"
					style="width:200px; height:20px; display:block; background:#09c; color:#fff;border-color:#999;"
					>+ Add/Edit Tours in this category &rarr;</a>

				</div>

			</form>
			<div class="clear">&nbsp;</div>

		</div>
		<?php endforeach;?>
		<?php endif;?>
	</div>

	<h1 style="background:#666; color:#ccc;padding:5px 8px">Page setup </h1>
	<div style="background:#CCC" class="inner-spacer">

	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
		<div class="left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
		<div class="row">
			<label>Page Note:</label>
			<textarea rows="3" class="txt large" style="width:710px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
		</div>
		<div class="clear">
			<input type="submit" name="submit" class="form_btn" value="Save" />
		</div>
	</form>
	</div>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('.editor').ckeditor();

	$('#add-tour-cat-form').hide();
	$("#add-tour-cat-btn").click(function(evt){
		evt.preventDefault();
		$('#add-tour-cat-form').slideToggle();
	});

	$('#box-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$_target = $(this);
			$.get($(this).attr('href'), function(data){
				if(data.success) {
					$('#tour-cat-' + data.id).slideUp(function(){$(this).remove();});
				}else{
					alert(data.message);
				}
			});
		}
	});

});


//-->
</script>