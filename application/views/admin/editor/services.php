<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<?php if (false)://comment at php level :)?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-sortable.min.js"></script>
<?php endif;?>

<div class="" id="box-editor">

	<form method="post" enctype="multipart/form-data" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
		<div class="clear"></div>
<div id="boxes-container">

	<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">Top Section:</h2>
	<div id="top-section-boxes">
		<?php if(isset($services_top) && !empty($services_top)):?>
			<?php $top_num=1; foreach ($services_top as $top_box):?>
			<div class="box top-section-box clear inner-spacer">
				<a href="#" class="delete top-section-delete" title="delete">Delete</a>
				<div class="column left column-220">
					<div class="clear">
						<label >Image (405 x 228):</label><input type="file" name="top_img_<?php echo $top_num;?>" /><br />
					</div>
					<div class="">
						<label>Current Image:</label>
						<?php if(strlen($top_box->image)):?>
						<img width="210" src="<?php echo textbox_image_src($top_box->image);?>" />
						<?php else:?>
							Not set
						<?php endif;?>
					</div>
				</div>
				<div class="column right column-450">
				  <label>Title (Will appear as title and sidebar text):</label>
          <input id="top-title-editor-<?php echo $top_num;?>" name="top_boxes_title[]" class="txt column-450 top-editor" value="<?php echo $top_box->title;?>">
					<label>Text:</label>
					<input type="hidden" name="top_boxes_ids[]" value="<?php echo $top_box->id;?>" />
					<textarea id="top-editor-<?php echo $top_num;?>" rows="8" cols="30" name="top_boxes[]" class="txt column-450 top-editor"><?php echo $top_box->content;?></textarea>
					<br /><strong>Order Display</strong>
					<input type="text" class="txt small" name="top_boxes_rankings[]" value="<?php echo $top_box->ranking;?>" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
			<?php $top_num++; endforeach;?>
		<?php else:?>
			<div class="box top-section-box clear inner-spacer">
				<a href="#" class="delete top-section-delete" title="delete">Delete</a>
				<div class="column left column-220">
					<div class="clear">
						<label >Image (405 x 228):</label><input type="file" name="top_img_1" /><br />
					</div>
					<div class="">
						<label>Current Image:</label>
						<h3>Not set</h3>
					</div>
				</div>
				<div class="column right column-450">
				  <label>Title (Will appear as title and sidebar text):</label>
          <input id="top-title-editor-1" name="top_boxes_title[]" class="txt column-450 top-editor">
					<label>Text:</label>
					<textarea id="top-editor-1" rows="8" cols="30" name="top_boxes[]" class="txt column-450 top-editor"></textarea>
					<br /><strong>Order Display</strong>
					<input type="text" class="txt small" name="top_boxes_rankings[]" value="" />
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		<?php endif;?>
	</div>

	<div class="clear row"><a href="#nowhere" id="add-top-box-btn">+ New Top Box</a></div>

	<h2 style="padding:5px; background:#999; color:#333;border:1px solid #fff;margin-bottom:10px;">
	Main Section:</h2>

	<?php if(isset($boxes) && count($boxes) > 0) : $counter=0;?>
		<?php foreach ($boxes as $box) :?>
			<?php if($box->title == 'services-top') continue;?>
		<div class="box clear inner-spacer">
			<a href="#" class="delete box-delete" title="delete">Delete</a>
			<div class="column left">
				<label>*Box Title:</label>
				<input  type="text" class="txt" name="box_title[]" value="<?php echo $box->title; ?>" />
			</div>
			<div class="column left">
				<label>Display Order</label>
				<input type="text" class="txt small" name="rankings[]" value="<?php echo $box->ranking;?>" />
			</div>
			<div  class="clear ">
				<label>*Content: </label>
				<textarea id="box-<?php echo (++$counter); ?>" rows="10" cols="50" name="box_content[]"  class="txt editor large"><?php echo htmlspecialchars($box->content); ?></textarea>
				<br />
			</div>

		</div>
		<?php endforeach;?>


	<?php else:?>
		<div class="box clear inner-spacer">
			<a href="#" class="delete box-delete" title="delete">Delete</a>
			<div class="column left">
				<label>*Box Title: </label>
				<input  type="text" class="txt" name="box_title[]" />
			</div>
			<div class="column left">
				<label>Display Order</label>
				<input type="text" class="txt small" name="rankings[]" value="" />
			</div>
			<div  class="clear ">
				<label>*Content: </label>
				<textarea id="box-1" rows="10" cols="50" name="box_content[]"  class="txt editor large"></textarea>
				<br />
			</div>

		</div>
	<?php endif; ?>

	</div>

	<div class="clear row"><a href="#nowhere" id="add-box-btn">+ An Other Box</a></div>
	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>

	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container textarea.editor').ckeditor();
	$('#boxes-container textarea.top-editor').ckeditor();

	$('#box-editor .box-delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box?')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	$('#box-editor .top-section-delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this top box?')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var boxCounter = ($('#box-editor .editor').length);
	var topBoxCounter = $('#top-section-boxes .top-section-box').length;

	$('#add-box-btn').click(function(evt){
		evt.preventDefault();
		id = ++boxCounter;
		var tpl = ['<div class="box inner-spacer"><a href="#" class="delete box-delete" title="delete">Delete</a>',
						'<div class="column left">',
							'<label>*Box Title: </label>',
							'<input type="text" class="txt" name="box_title[]" />',
						'</div>',
						'<div class="column left">',
						'<label>Display Order</label>',
						'<input type="text" class="txt small" name="rankings[]" value="" />',
						'</div>',
						'<div class="clear ">',
							'<label>*Content: </label>',
							'<textarea id="box-',
							id  + '"',
							'rows="10" cols="50" name="box_content[]"  class="txt large "></textarea>',
						'</div>',
					'</div>'];
	$(tpl.join('')).appendTo('#boxes-container');
	$('#box-' + id).ckeditor();
	});

	$('#add-top-box-btn').click(function(evt){

		evt.preventDefault();
		id = ++topBoxCounter;
		var tpl = ['<div class="box top-section-box clear inner-spacer">',
					'<a href="#" class="delete top-section-delete" title="delete">Delete</a>',
					'<div class="column left column-220">',
						'<div class="clear">',
							'<label >Image (210 x 118):</label><input type="file" name="top_img_' + id + '" /><br />',
						'</div>',
						'<div class="">',
							'<label>Current Image:</label>',
							'<h3>Not set</h3>',
						'</div>',
					'</div>',
					'<div class="column right column-450">',
					'<label>Title (Will appear as title and sidebar text):</label>',
            '<input id="top-title-editor-' + id +'" name="top_boxes_title[]" class="txt column-450 top-editor">',
						'<label>Text:</label>',
						'<textarea id="top-editor-' + id +'" rows="8" cols="30" name="top_boxes[]" class="txt column-450 top-editor"></textarea>',
						'<br /><strong>Order Display</strong>',
						'<input type="text" class="txt small" name="top_boxes_rankings[]" value="" />',
					'</div>',
					'<div class="clear">&nbsp;</div>',
					'</div>'];
		$(tpl.join('')).appendTo('#top-section-boxes');

		$('#top-editor-' + id).ckeditor();
	});

});


//-->
</script>