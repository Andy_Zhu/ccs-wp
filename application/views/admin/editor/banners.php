<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>

<style type="text/css">
<!--
@IMPORT url("<?php echo CSS_PATH?>/colorpicker/colorpicker.css");
-->
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/colorpicker.js"></script>

<div class="" id="box-editor">
	<h1 style="background:#333; color:#ccc;padding:5px 10px;" class="row">Banners:</h1>
	<form id="banners-form" enctype="multipart/form-data" method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<div id="banners">
	<?php if(empty($banners)):?>

	<div class="banner" id="banner-1">
		<a href="#nowhere" class="delete">Delete</a>
		<div class="banner-opts clear">
			<div class="column left column-first">
				<label>Page:</label>
				<select name="page_uris[]" class="txt">
					<option value="">-- Choose One --</option>
					<?php foreach ($page_uris as $puri => $lb):?>
						<option value="<?php echo $puri?>"><?php echo $lb;?></option>
					<?php endforeach;?>
				</select>
			</div>
			<div class="column small left">
					<label title="Background Color">BG Color:</label>
					<input title="Background Color" type="text" class="txt color small" name="bg_colors[]" value="#FFFFFF"/>
			</div>
			<div class="column small left">
					<label title="Border Color">B Color:</label>
					<input title="Border Color" type="text" class="txt color small"  name="border_colors[]" value="#EE3300"/>
			</div>
			<div class="column small left">
				<label>Enabled:</label>
				<select name="banner_configs[]" class="txt small">
					<option value="YES" >YES</option>
					<option value="NO">NO</option>
				</select>
			</div>
			<div class="column small left">
        <label>Position:</label>
        <select name="banner_positions[]" class="txt small">
          <option value="left" >Left</option>
          <option value="right">Right</option>
        </select>
      </div>
		</div>
		<div class="clear banner_img_upload">
      <label >Image (220 x 120):</label><input type="file" name="banner_img_1" /><br />
    </div>
    <div class="clear">
      <label>Header:</label>
      <input  type="text"  class="txt head" name="heading[]" value=""/>
    </div>
		<div class="banner-content clear">
			<label>Content:</label>
			<textarea rows="5" cols="30" class="txt editor" id="editor-1" name="contents[]"></textarea>
		</div>
	</div>

	<?php else:?>

		<?php $counter=1; foreach ($banners as $banner):?>

			<div class="banner" id="banner-<?php echo $counter?>">
			<a href="#nowhere" class="delete">Delete</a>
			<div class="banner-opts clear">
				<div class="column left column-first">
					<label>Page:</label>
					<select name="page_uris[]" class="txt">
						<option value="">--Choose One --</option>
						<?php foreach ($page_uris as $puri => $lb):?>
							<option value="<?php echo $puri?>" <?php if($banner->page_uri == $puri) echo 'selected="selected"'?>><?php echo $lb;?></option>
						<?php endforeach;?>
					</select>
				</div>
				<?php

					if(!$banner->styles) {
						$bg_color = '#FFFFFF';
						$border_color = '#EE3300';
					}else{

						$colors = array();
						foreach (explode(';', $banner->styles) as $pair) {
							if(empty($pair)) continue;
							$color = explode(':', $pair);
							$colors[$color[0]] = $color[1];
						}

						$bg_color = array_key_exists('background-color', $colors) ? $colors['background-color'] : '#FFFFFF';
						$border_color = array_key_exists('border-color', $colors) ? $colors['border-color'] : '#EE3300';
					}
				?>
				<div class="column small left">
					<label title="Background Color">BG Color:</label>
					<input title="Background Color" type="text" class="txt color small" name="bg_colors[]" value="<?php echo $bg_color;?>"/>
				</div>
				<div class="column small left">
					<label title="Border Color">B Color:</label>
					<input title="Border Color" type="text" class="txt color small"  name="border_colors[]" value="<?php echo $border_color;?>"/>
				</div>
				<div class="column small left">
					<label>Enabled:</label>
					<select name="banner_configs[]" class="txt small">
						<option value="YES" <?php if($banner->enabled == 'YES') echo 'selected="selected"'?>>YES</option>
						<option value="NO" <?php if($banner->enabled == 'NO') echo 'selected="selected"'?>>NO</option>
					</select>
				</div>
        <div class="column small left">
        <label>Position:</label>
        <select name="banner_positions[]" class="txt small">
          <option value="left" <?php if($banner->position == 'left') echo 'selected="selected"'; ?> >Left</option>
          <option value="right" <?php if($banner->position == 'right') echo 'selected="selected"'; ?> >Right</option>
        </select>
      </div>
			</div>
      <div class="clear banner_img_upload">
        <label >Image (220 x 120):</label><input type="file" name="banner_img_<?php echo $counter; ?>" /><br />
        <?php if($banner -> image){ ?><img src="<?php echo banner_image_src($banner->image);?>" /><?php } ?>
        <input type="hidden" name="banner_db_img[]" value="<?php echo $banner->image; ?>">
      </div>
      <div class="clear">
        <label>Header:</label>
        <input  type="text"  class="txt head" name="heading[]" value="<?php echo htmlspecialchars($banner->heading)?>"/>
      </div>
			<div class="banner-content clear">
				<label>Content:</label>
				<textarea rows="5" cols="30" class="txt editor" id="editor-<?php echo $counter++;?>" name="contents[]"><?php echo htmlspecialchars($banner->content)?></textarea>
			</div>
		</div>

		<?php endforeach;?>

	<?php endif;?>
	</div><!-- end of banners -->
	<div class="row">
		<a href="#nowhere" id="add-banner-btn">+Add new banner</a>
	</div>

	<div class="btn-set">
		<input type="submit" name="banners-submit" value="Save all" class="orange-btn" />
		<input type="hidden" name="website_id" value="<?php echo $id;?>" />
	</div>
	<div id="errors-message" class="info" style="display:none">
		Fields highlighted in red are required and can not be empty. Please fix the errors and try again
	</div>
	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	var counter = $('.banner').length;
	var tpl = $('#banners .banner:first').clone();

	$('#add-banner-btn').click(function(evt){
	  counter++;
		evt.preventDefault();
		var banner = tpl.clone();
		banner.attr('id', 'banner-'+ counter);
		banner.find('.editor').attr('id', 'editor-' + counter).val('');
		banner.find('.head').val('');
		banner.find('.banner_img_upload input').attr('name', 'banner_img_'+ counter);
		banner.find('.banner_img_upload img').remove();
		banner.find("select[name='page_uris[]'] option").removeAttr('selected');
		banner.appendTo($('#banners'));
		banner.find('.editor').ckeditor();
		$('#banner-' + counter +' .color').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val('#' + hex.toUpperCase());
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		})
		.bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});
		counter++;
	});

	$('#banners .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this banner')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	$('.editor').ckeditor();

	$('#banners-form').submit(function(evt){

		var valid = true;
		//alert($("#banners .banner select[name='page_uris[]']").length);
		$('#banners .banner').each(function(item){

			var pageURI = $(this).find("select[name='page_uris[]']");
			var content = $(this).find('.editor');
			if(pageURI.val() == '') pageURI.addClass('error_txt');
			if(content.val() == '') $('#cke_editor-' + content.attr('id').split('-')[1]).addClass('error_txt');
			if(pageURI.val() == '' || content.val() == '') {
				valid = false;
			}
		});

		if(!valid) {
			evt.preventDefault();
			$('#errors-message').slideDown();
		}
	});

	//remove error highlights!
	$('#banners .banner-opts select').live('change', function(){
		var id = $(this).parent().parent().parent().attr('id').split('-')[1];

		if($(this).hasClass('error_txt')) {
			$(this).removeClass('error_txt');
			$('#cke_editor-' + id).removeClass('error_txt');
		}
		if($('#errors-message').is(':visible')) {
			if($('#banners .error_txt').length == 0) {
				$('#errors-message').slideUp();
			}
		}
	});

	//set up the color pickers
	$('#banners .color').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val('#' + hex.toUpperCase());
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});

});
//-->
</script>