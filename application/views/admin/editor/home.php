<?php

$defaults['provided_service'] = "Airport pickup - From Beijing and Tianjin International Airports\n
Cruise ship transfers - Cruise ship terminal to Beijing city or airport.\n
Airport VIP meet and greet service - fast-track immigration and customs.\n
Professional translator / interpreter services\n
English speaking guides - Ideal for shopping, touring, dining, and experiencing the culture of region.\n
Escorted tours - visit the many famous historical sites of the region
";

$defaults['fleet_include'] = "Luxury limousine saloons,\n
Business and intermediate saloons\n
MPV's (multipurpose vehicles) and\n
Minivans and buses\n
";

foreach ($boxes as $box) {
	if($box->title == 'fleet include') {
		$fleet_include_box = $box;
	}else if($box->title == 'provided services') {
		$provided_service_box = $box;
	}else if($box->title == 'carousel title 1') {
    $carousel_title_1_box = $box;
  }else if($box->title == 'carousel title 2') {
    $carousel_title_2_box = $box;
  }
}
?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="row" id="home-editor">
	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>" enctype="multipart/form-data">
		<div class="column left" style="margin-left:0px;">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>

		<div class="row">
			<h2>Slideshow images: (<?php echo $website->template =='qualifire' ? '930 X 300' :'865 X 450' ?>)</h2>
			<?php for($i=1; $i<5; $i++):?>
					<div class="image-form">

						<?php $image = array_key_exists($i, $images) ? $images[$i] : null;?>

						<div class="img-preview" id="slideshow">
							<?php if($image):?>
								<a id="<?php echo $i?>" href="<?php echo slideshow_image_src($image->filename);?>" class="lage-image" rel="gallery">
									<img src="<?php echo slideshow_image_src($image->filename);?>" class="thumb" />
								</a>
								<div class="hint">
								Click on thumbnail to view fullsize image
								</div>
							<?php else:?>
								<span class="not-set">NOT SET</span>
							<?php endif;?>
						</div>
						<div class="input">
							#<?php echo $i;?> <input type="file" name="slideshow<?php echo $i?>" />
						</div>

					</div>
				<?php endfor;?>
		</div>
		<div class="clear">
		  <label>Carousel h1 title:</label>
		  <input class="txt large" name="carousel_title_1" value="<?php echo isset($carousel_title_1_box) ? $carousel_title_1_box->content : "" ?>">
		  <label>Carousel h2 title:</label>
      <input class="txt large" name="carousel_title_2" value="<?php echo isset($carousel_title_2_box) ? $carousel_title_2_box->content : "" ?>">
		</div>
		<div class="column left" style="padding-left:0px;margin-left:0px">
			<label>Our fleet includes:</label>
			<textarea name="fleet_include" rows="10" cols="20" class="txt"><?php echo isset($fleet_include_box) ? $fleet_include_box->content : $defaults['fleet_include']?></textarea>
		</div>

		<div class="column left" style="padding-left:0px;margin-left:0px">
			<label><?php echo $website->name;?> can also provide:</label>
			<textarea name="provided_service" style="width:420px;" rows="10" cols="20" class="txt"><?php echo isset($provided_service_box) ? $provided_service_box->content : $defaults['provided_service']?></textarea>
		</div>
		<div class="row">
			<label>About <?php echo $website->name;?> (short)</label>
			<textarea rows="5" cols="40" id="short_about" name="short_about" class="txt large editor"><?php echo $website->short_about;?></textarea>
		</div>
		<div id="box-editor">
  		<div id="top-section-boxes">
      <?php if(isset($home_about_boxes) && !empty($home_about_boxes)):?>
        <?php $top_num=1; foreach ($home_about_boxes as $top_box):?>
        <div class="box top-section-box clear inner-spacer">
          <a href="#" class="delete top-section-delete" title="delete">Delete</a>
          <div class="column left column-220">
            <div class="clear">
              <label >Image (405 x 228):</label><input type="file" name="top_img_<?php echo $top_num;?>" /><br />
            </div>
            <div class="">
              <label>Current Image:</label>
              <?php if(strlen($top_box->image)):?>
              <img width="210" src="<?php echo textbox_image_src($top_box->image);?>" />
              <?php else:?>
                Not set
              <?php endif;?>
            </div>
          </div>
          <div class="column right column-450">
            <label>Text:</label>
            <input type="hidden" name="top_boxes_ids[]" value="<?php echo $top_box->id;?>" />
            <textarea id="top-editor-<?php echo $top_num;?>" rows="8" cols="30" name="top_boxes[]" class="txt column-450 top-editor"><?php echo $top_box->content;?></textarea>
            <br /><strong>Order Display</strong>
            <input type="text" class="txt small" name="top_boxes_rankings[]" value="<?php echo $top_box->ranking;?>" />
          </div>
          <div class="clear">&nbsp;</div>
        </div>
        <?php $top_num++; endforeach;?>
      <?php else:?>
        <div class="box top-section-box clear inner-spacer">
          <a href="#" class="delete top-section-delete" title="delete">Delete</a>
          <div class="column left column-220">
            <div class="clear">
              <label >Image (210 x 118):</label><input type="file" name="top_img_1" /><br />
            </div>
            <div class="">
              <label>Current Image:</label>
              <h3>Not set</h3>
            </div>
          </div>
          <div class="column right column-450">
            <label>Text:</label>
            <textarea id="top-editor-1" rows="8" cols="30" name="top_boxes[]" class="txt column-450 top-editor"></textarea>
            <br /><strong>Order Display</strong>
            <input type="text" class="txt small" name="top_boxes_rankings[]" value="" />
          </div>
          <div class="clear">&nbsp;</div>
        </div>
      <?php endif;?>
    </div>
    <div class="clear row"><a href="#nowhere" id="add-top-box-btn">+ New Top Box</a></div>
  </div>
	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>

	</form>
</div>

<style type="text/css">
<!--
@import url("<?php echo CSS_PATH?>jquery.lightbox-0.5.css");
-->
</style>
<script type="text/javascript" src="<?php echo JS_PATH?>jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#home-editor .editor').ckeditor();
	$('#box-editor .top-editor').ckeditor();
  var topBoxCounter = $('#top-section-boxes .top-section-box').length;
  $('#box-editor .top-section-delete').live('click', function(evt){
    evt.preventDefault();
    if(confirm('Are your sure you want to delete this top box?')) {
      $(this).parent().slideUp(function(){$(this).remove();});
    }
  });
  $('#add-top-box-btn').click(function(evt){

    evt.preventDefault();
    id = ++topBoxCounter;
    var tpl = ['<div class="box top-section-box clear inner-spacer">',
          '<a href="#" class="delete top-section-delete" title="delete">Delete</a>',
          '<div class="column left column-220">',
            '<div class="clear">',
              '<label >Image (210 x 118):</label><input type="file" name="top_img_' + id + '" /><br />',
            '</div>',
            '<div class="">',
              '<label>Current Image:</label>',
              '<h3>Not set</h3>',
            '</div>',
          '</div>',
          '<div class="column right column-450">',
            '<label>Text:</label>',
            '<textarea id="top-editor-' + id +'" rows="8" cols="30" name="top_boxes[]" class="txt column-450 top-editor"></textarea>',
            '<br /><strong>Order Display</strong>',
            '<input type="text" class="txt small" name="top_boxes_rankings[]" value="" />',
          '</div>',
          '<div class="clear">&nbsp;</div>',
          '</div>'];
    $(tpl.join('')).appendTo('#top-section-boxes');

    $('#top-editor-' + id).ckeditor();
  });


	$(function() {
		$('#slideshow a').lightBox({fixedNavigation:true});
	});
});
//-->
</script>