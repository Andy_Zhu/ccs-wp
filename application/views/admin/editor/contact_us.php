<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="" id="box-editor">

	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
	<?php
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>

	<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>

	<div id="boxes-container">
	<?php if(isset($boxes) && count($boxes) > 0) : $counter=0;?>
		<?php foreach ($boxes as $box) :?>

		<div class="box clear inner-spacer">
			<div class="clear">
				<h1>Customer Service</h1>
				<input type="hidden" name="box_title[]" value="Customer Service" />
			</div>
			<div  class="row ">
				<textarea id="box-1" rows="10" cols="50" name="box_content[]"  class="txt large"><?php echo htmlspecialchars($box->content);?></textarea>
				<br />
			</div>

		</div>
		<?php endforeach;?>


	<?php else:?>

		<div class="box clear inner-spacer">
			<div class="clear">
				<h1>Customer Service</h1>
				<input type="hidden" name="box_title[]" value="Customer Service" />
			</div>
			<div  class="row ">
				<textarea id="box-1" rows="10" cols="50" name="box_content[]"  class="txt large"></textarea>
				<br />
			</div>

		</div>

	<?php endif; ?>
	</div>

	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>

	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>
	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('#boxes-container textarea').ckeditor();
});
//-->
</script>