<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div id="fleet-editor">

	<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section")?>">
		<div class="column left">
			<label>Meta Description:</label>
			<textarea rows="2" cols="60"  name="meta_description" id="meta_description" class="txt"><?php if($page) echo $page->meta_description?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords:</label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php if($page) echo $page->meta_keywords;?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear" style="padding-left:10px;margin-bottom:20px;">
			<label>Meta title:</label>
			<input type="text" class="txt" style="width:350px;" name="meta_title" value="<?php if($page) echo $page->meta_title;?>" />
		</div>
		<div class="clear"></div>
		<div class="row">
			<label>Intro:</label>
			<textarea name="intro" class="txt ck large" rows="5" cols="20" id="intro"><?php echo isset($intro) ? htmlspecialchars($intro->content) : '';?></textarea>
		</div>
		<h2 class="row">Vehicles:</h2>
		<div class="row" id="cars-container">

		<?php if(!empty($site_vehicles)): ?>
			<?php $counter = 1?>
			<?php foreach ($vehicles as $temp_v):?>

				<?php
				if(!array_key_exists($temp_v->id, $site_vehicles)) continue;
				$site_vehicle = $site_vehicles[$temp_v->id];
				$counter++;
				?>
				<div class="row car-info">
				<a href="#nowhere" class="delete">Delete</a>

				<div class="row">
					<div class="vehicle left">
					<label>*Vehicle:</label>
					<select name="vehicle_id[]" class="txt vehicle">
						<option value="">-- Choose One ---</option>
						<?php foreach ($vehicles as $vehicle):?>
							<option <?php if($site_vehicle->vehicle_id == $vehicle->id) echo 'selected="selected"';?> value="<?php echo $vehicle->id;?>"><?php echo $vehicle->short_name;?></option>
						<?php endforeach;?>
					</select>
					</div>
					<div class="column price left" >
						<label>Display Order:</label>
						<input type="text" class="txt small" name="rankings[]" value="<?php echo $site_vehicle->ranking;?>" />
					</div>
					<div class="column price left" >
            <label>Homepage?:</label>
            <select name="homepage[]" class="txt small">
              <option value="0" <?php if(!$site_vehicle->homepage) echo 'selected="selected"';?>">No</option>
              <option value="1" <?php if($site_vehicle->homepage) echo 'selected="selected"';?>>Yes</option>
            </select>
          </div>
				</div>

				<div class="row">
					<div class="column price left">
						<label>*Half day Cost:</label>
						<input type="text" class="txt small" name="half_day_price[]" value="<?php echo $site_vehicle->half_day_price;?>" />$
					</div>
					<div class="column price left">
            <label>*Half day hrs:</label>
            <input type="text" class="txt small" name="half_day_hour[]" value="<?php echo $site_vehicle->half_day_hour;?>" />
          </div>
					<div class="column price left">
						<label>*Full day Cost:</label>
						<input type="text" class="txt small" name="full_day_price[]" value="<?php echo $site_vehicle->full_day_price;?>" />$
					</div>
					<div class="column price left">
            <label>*Full day hrs:</label>
            <input type="text" class="txt small" name="full_day_hour[]" value="<?php echo $site_vehicle->full_day_hour;?>" />
          </div>
					<div class="column price-large left" >
						<label>*Extra Hours Cost:</label>
						<input type="text" class="txt small" name="extra_hour_price[]" value="<?php echo $site_vehicle->extra_hours_price;?>" />$
					</div>
					<div class="column price-large left" >
						<label>*Extra Km Cost:</label>
						<input type="text" class="txt small" name="extra_km_price[]" value="<?php echo $site_vehicle->extra_km_price;?>" />$
					</div>
					<div class="column price-large left" >
            <label>*Airport Transfer Cost:</label>
            <input type="text" class="txt small" name="airport_transfer_price[]" value="<?php echo $site_vehicle->airport_transfer_price;?>" />$
          </div>
          <div class="column price-large left" >
            <label>*Border Crossing Cost:</label>
            <input type="text" class="txt small" name="border_crossing_price[]" value="<?php echo $site_vehicle->border_crossing_price;?>" />$
          </div>
				</div>

				<div class="clear">
					<label>Other Info:</label>
					<textarea id="info-<?php echo $counter;?>" class="txt ck large" name="other_info[]" rows="4" cols="50"><?php echo  htmlspecialchars($site_vehicle->other_info);?></textarea>
				</div>
			</div>

			<?php endforeach;?>

		<?php else:?>
			<div class="row car-info">
				<a href="#nowhere" class="delete">Delete</a>

				<div class="row">
					<div class="vehicle left">
					<label>*Vehicule:</label>
					<select name="vehicle_id[]" class="txt vehicle">
						<option value="">-- Choose One ---</option>
						<?php foreach ($vehicles as $vehicle):?>
							<option value="<?php echo $vehicle->id;?>"><?php echo $vehicle->short_name;?></option>
						<?php endforeach;?>
					</select>
					</div>
					<div class="column price left" >
						<label>Order:</label>
						<input type="text" class="txt small" name="rankings[]" value="1" />
					</div>
				</div>

				<div class="row">
					<div class="column price left">
						<label>*Half day Cost:</label>
						<input type="text" class="txt small" name="half_day_price[]" />$
					</div>
					<div class="column price left">
            <label>*Half day hrs:</label>
            <input type="text" class="txt small" name="half_day_hour[]" />
          </div>
					<div class="column price left">
						<label>*Full day Cost:</label>
						<input type="text" class="txt small" name="full_day_price[]" />$
					</div>
					<div class="column price left">
            <label>*Full day hrs:</label>
            <input type="text" class="txt small" name="full_day_hour[]" />
          </div>
					<div class="column price-large left" >
						<label>*Extra Hours Cost:</label>
						<input type="text" class="txt small" name="extra_hour_price[]" />$
					</div>
					<div class="column price-large left" >
						<label>*Extra Km Cost:</label>
						<input type="text" class="txt small" name="extra_km_price[]" value="" />$
					</div>
					<div class="column price-large left" >
            <label>*Airport Transfer Cost:</label>
            <input type="text" class="txt small" name="airport_transfer_price[]" value="" />$
          </div>
          <div class="column price-large left" >
            <label>*Border Crossing Cost:</label>
            <input type="text" class="txt small" name="border_crossing_price[]" value="" />$
          </div>
				</div>
				<div class="clear">
					<label>Other Info:</label>
					<textarea id="info-1" class="txt ck large" name="other_info[]" rows="4" cols="50"></textarea>
				</div>
			</div>
		<?php endif;?>

		</div>

	<div class="clear row"><a href="#nowhere" id="add-vehicle-btn">+ An Other Vehicle </a></div>
	<div class="row">
		<label>Page Note:</label>
		<textarea rows="3" class="txt large" style="width:740px;" cols="50" name="page_note"><?php if($page) echo $page->note;?></textarea>
	</div>
	<div class="clear">
		<input type="submit" name="submit" class="form_btn" value="Save" />
	</div>

	</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
<!--
$(document).ready(function(){
	var vehiculeTemplate = $('#fleet-editor .car-info:first').html();
	$('#fleet-editor .ck').ckeditor();

	$('#fleet-editor .delete').live('click', function(evt){
		evt.preventDefault();
		if(confirm('Are your sure you want to delete this box')) {
			$(this).parent().slideUp(function(){$(this).remove();});
		}
	});

	var boxCounter = ($('#cars-container .car-info').length + 1);

	$('#add-vehicle-btn').click(function(evt){
		evt.preventDefault();
		id = ++boxCounter;

		var newCar = $('<div class="car-info"></div>');
		newCar.html(vehiculeTemplate).find('textarea').val('').attr('id', 'info-' + id);
		newCar.find('input').val('');
		newCar.find('option').removeAttr('selected');
		newCar.appendTo('#cars-container');
	$('#info-' + id).ckeditor();
	});
});
//-->
</script>