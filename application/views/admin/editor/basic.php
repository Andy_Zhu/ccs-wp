<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/ckeditor/adapters/jquery.js"></script>
<div class="">


<form method="post" action="<?php echo site_url("admin/edit_website?id=$id&section=$section") ?>"
      enctype="multipart/form-data">
<?php
if (isset($create_basic_message)) echo '<div class="error">', $create_basic_message, '</div>';
?>

<input type="submit" name="submit" class="form_btn right" value="Save"/>

<div class="column left">
    <label>*Country: </label>
    <select name="country_id" id="country_id" class="txt">
        <option value="1">Other</option>
        <?php unset($countries[1]); //others?>
        <?php foreach ($countries as $country): ?>
            <option
                value="<?php echo $country->id ?>" <?php if ($website->country_id == $country->id) echo 'selected="selected"'; ?>><?php echo $country->name; ?></option>

        <?php endforeach; ?>
    </select>
    <br/><?php echo form_error('country_id'); ?>
</div>

<div class="column left">
    <label>*Template: </label>
    <select name="template" id="template" class="txt">
        <?php foreach (template_fetch_available() as $tpl => $lb): ?>
            <option value="<?php echo $tpl ?>" <?php if ($website->template == $tpl) echo 'selected="selected"'; ?>><?php echo $lb ?></option>

        <?php endforeach; ?>
    </select>
    <br/><?php echo form_error('template'); ?>
</div>

<div class="column left">
    <label>*Language: </label>
    <select name="lang_id" id="lang_id" class="txt">
        <?php foreach ($languages as $language): ?>
            <option value="<?php echo $language->id ?>" <?php if ($website->lang_id == $language->id) echo 'selected="selected"'; ?>><?php echo strtoupper($language->lang_code); ?></option>
        <?php endforeach; ?>
    </select>
    <br/><?php echo form_error('country_id'); ?>
</div>
<div class="column left">
    <label>*Alt. Domain (other language): </label>
    <div class="ui-widget">
        <select name="alt_website_id[]" id="alt_website_id" multiple="multiple">
            <?php foreach ($all_websites as $alt_website): ?>
                <option value="<?php echo $alt_website->id ?>" <?php if ($this->Websites_Model->alt_selected($website->id, $alt_website->id)) echo 'selected="selected"'; ?>><?php echo $alt_website->url; ?></option>
            <?php endforeach; ?>
        </select>
		<script>
			$(function(){
				$("#alt_website_id").multiselect();
			});
		</script>
    </div>
    <br/><?php echo form_error('country_id'); ?>
</div>
<div class="column left">
    <label>Parent site: </label>
    <select name="parent_site_id" id="parent_site_id" class="txt">
      <option value="">None</option>
        <?php foreach ($all_websites as $site): ?>
            <option value="<?php echo $site->id ?>" <?php if ($website->parent_site_id == $site->id) echo 'selected="selected"'; ?>><?php echo strtoupper($site->shortname); ?></option>
        <?php endforeach; ?>
    </select>
    <br/><?php echo form_error('country_id'); ?>
</div>
<div class="column  left">
    <label>*URL:</label>
    <input type="text" name="url" id="url" class="txt "
           value="<?php echo set_value('url') ? set_value('url') : $website->url ?>"/>
    <br/><?php echo form_error('url'); ?>
</div>
<div class="column  left">
    <label>s3 Endpoint:</label>
    <input type="text" name="" id="" class="txt "
           value="<?php echo $website->s3_bucket_name . '.s3-website-us-west-2.amazonaws.com' ?>" disabled />
    <br/>
</div>
<div class="column  left">
    <label>CloudFront URL:</label>
    <input type="text" name="cloudfront_url" id="cloudfront_url" class="txt "
           value="<?php echo set_value('cloudfront_url') ? set_value('cloudfront_url') : $website->cloudfront_url ?>"/>
    <br/><?php echo form_error('cloudfront_url'); ?>
</div>

<div class="column left">
    <label>*Name:</label>
    <input type="text" name="name" id="name" class="txt"
           value="<?php echo set_value('name') ? set_value('name') : $website->name; ?>"/>
    <br/><?php echo form_error('name'); ?>
</div>
<div class="column left">
    <label>Max Cars shown in sidebar</label>
    <input type="text" name="sidebar_max_cars" class="txt med"
           value="<?php echo set_value('sidebar_max_cars') ? set_value('sidebar_max_cars') : $website->sidebar_max_cars; ?>"/><br/>
    <br/><?php echo form_error('sidebar_max_cars'); ?>
</div>

<div class="column left">
    <label>City Short Name:</label>
    <input type="text" name="shortcode" id="shortcode" class="txt"
           value="<?php echo set_value('shortcode') ? set_value('shortcode') : $website->shortcode; ?>"/>
    <br/><?php echo form_error('shortcode'); ?>
</div>



<div class="clear"></div>


<div class="column left">
    <label>Offer Meet & Greet?</label>

    <select name="meet_n_greet" id="meet_n_greet" class="txt">
        <option value="0" <?php if (!$website->meet_n_greet) echo 'selected="selected"'; ?>>No</option>
        <option value="1" <?php if ($website->meet_n_greet) echo 'selected="selected"'; ?>>Yes</option>
    </select>

    <br/><?php echo form_error('meet_n_greet'); ?>
</div>

<div class="column left">
    <label>Offer Private Guided Tour?</label>

    <select name="private_guided_tour" id="private_guided_tour" class="txt">
        <option value="0" <?php if (!$website->private_guided_tour) echo 'selected="selected"'; ?>>No</option>
        <option value="1" <?php if ($website->private_guided_tour) echo 'selected="selected"'; ?>>Yes</option>
    </select>

    <br/><?php echo form_error('private_guided_tour'); ?>
</div>


<div class="clear"></div>

<?php if ($this->Users->is_admin()): ?>

    <div class="column med left">
        <label>AWS Key: </label>
        <input type="text" name="aws_key" id="aws_key" class="txt med"
               value="<?php echo set_value('aws_key') ? set_value('aws_key') : $website->aws_key; ?>"/>
        <br/><?php echo form_error('aws_key'); ?>
    </div>
    <div class="column med left">
        <label>AWS Secret:</label>
        <input type="text" name="aws_secret" id="aws_secret" class="txt med"
               value="<?php echo set_value('aws_secret') ? set_value('aws_secret') : $website->aws_secret; ?>"/>
        <br/><?php echo form_error('aws_secret'); ?>
    </div>
    <div class="column med left">
        <label>s3 Bucket:</label>
        <input type="text" name="s3_bucket_name" id="s3_bucket_name" class="txt med"
               value="<?php echo $website->s3_bucket_name; ?>" disabled />
        <br/>
    </div>
    <div class="column med left">
        <label>CloudFront Dist. ID:</label>
        <input type="text" name="cloudfront_distribution" id="cloudfront_distribution" class="txt med"
               value="<?php echo set_value('cloudfront_distribution') ? set_value('cloudfront_distribution') : $website->cloudfront_distribution; ?>"/>
        <br/><?php echo form_error('cloudfront_distribution'); ?>
    </div>

    <div class="clear"></div>
    <div class="column med left">
        <label>*FTP HOST: </label>
        <input type="text" name="ftp_host" id="ftp_host" class="txt med"
               value="<?php echo set_value('ftp_host') ? set_value('ftp_host') : $website->ftp_host; ?>"/>
        <br/><?php echo form_error('ftp_host'); ?>
    </div>
    <div class="column med left">
        <label>*FTP Username:</label>
        <input type="text" name="ftp_user" id="ftp_user" class="txt med"
               value="<?php echo set_value('ftp_user') ? set_value('ftp_user') : $website->ftp_user; ?>"/>
        <br/><?php echo form_error('ftp_user'); ?>
    </div>
    <div class="column med left">
        <label>*FTP Password:</label>
        <input type="text" name="ftp_password" id="ftp_password" class="txt med"
               value="<?php echo set_value('ftp_password') ? set_value('ftp_password') : $website->ftp_password; ?>"/>
        <br/><?php echo form_error('ftp_password'); ?>
    </div>
    <div class="column med left">
        <label>*FTP Publish Dir:</label>
        <input type="text" name="ftp_target_dir" id="ftp_target_dir" class="txt med"
               value="<?php echo isset($website->ftp_target_dir) ? $website->ftp_target_dir : '/httpdocs'; ?>"/>
        <br/><?php echo form_error('ftp_target_dir'); ?>
    </div>

    <div class="clear"></div>

    <div class="column med left">
        <label>*Test FTP HOST: </label>
        <input type="text" name="test_ftp_host" id="test_ftp_host" class="txt med"
               value="<?php echo set_value('test_ftp_host') ? set_value('test_ftp_host') : $website->test_ftp_host; ?>"/>
        <br/><?php echo form_error('test_ftp_host'); ?>
    </div>
    <div class="column med left">
        <label>*Test FTP Username:</label>
        <input type="text" name="test_ftp_user" id="test_ftp_user" class="txt med"
               value="<?php echo set_value('test_ftp_user') ? set_value('test_ftp_user') : $website->test_ftp_user; ?>"/>
        <br/><?php echo form_error('test_ftp_user'); ?>
    </div>
    <div class="column med left">
        <label>*Test FTP Password:</label>
        <input type="text" name="test_ftp_password" id="test_ftp_password" class="txt med"
               value="<?php echo set_value('test_ftp_password') ? set_value('test_ftp_password') : $website->test_ftp_password; ?>"/>
        <br/><?php echo form_error('test_ftp_password'); ?>
    </div>
    <div class="column med left">
        <label>*Test FTP Publish Dir:</label>
        <input type="text" name="test_ftp_target_dir" id="test_ftp_target_dir" class="txt med"
               value="<?php echo isset($website->test_ftp_target_dir) ? $website->test_ftp_target_dir : '/httpdocs'; ?>"/>
        <br/><?php echo form_error('test_ftp_target_dir'); ?>
    </div>
<?php endif; ?>
<div class="clear"></div>
<div class="column  left">
    <label>Meta Description:</label>
    <textarea rows="2" cols="60" name="meta_desc" id="meta_desc"
              class="txt"><?php echo set_value('meta_desc') ? set_value('meta_desc') : $website->meta_desc ?></textarea>
    <br/><?php echo form_error('meta_desc'); ?>
</div>
<div class="column left">
    <label>Meta Keywords:</label>
    <textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords"
              class="txt"><?php echo set_value('meta_keywords') ? set_value('meta_keywords') : $website->meta_keywords ?></textarea>
    <br/><?php echo form_error('meta_keywords'); ?>
</div>

<!--
		<div class="column left">
			<label>Online Book URL:</label>
			<input type="text" name="online_book_url" id="online_book_url" class="txt" value="<?php echo set_value('online_book_url') ?  set_value('online_book_url') : $website->online_book_url;?>" />
			<br /><?php echo form_error('online_book_url');?>
		</div>
		<div class="column left">
			<label>Online Quote URL:</label>
			<input type="text" name="online_quote_url" id="online_quote_url" class="txt" value="<?php echo set_value('online_quote_url') ?  set_value('online_quote_url') : $website->online_quote_url;?>" />
			<br /><?php echo form_error('online_quote_url');?>
		</div>
		-->

<div class="column  left">
    <label>Contact Form Email:</label>
    <input type="text" name="contact_form_email" id="contact_form_email" class="txt"
           value="<?php echo set_value('contact_form_email') ? set_value('contact_form_email') : $website->contact_form_email; ?>"/>
    <br/><?php echo form_error('contact_form_email'); ?>
</div>

<div class="column  left">
    <label>Contact Form CC Email:</label>
    <input type="text" name="contact_form_cc_email" id="contact_form_cc_email" class="txt"
           value="<?php echo set_value('contact_form_cc_email') ? set_value('contact_form_cc_email') : $website->contact_form_cc_email; ?>"/>
    <br/><?php echo form_error('contact_form_cc_email'); ?>
</div>

<div class="column med  left">
    <label>Phone:</label>
    <input type="text" name="phone" id="phone" class="txt med"
           value="<?php echo set_value('phone') ? set_value('phone') : $website->phone; ?>"/>
    <br/><?php echo form_error('phone'); ?>
</div>

<div class="column med left">
    <label>Opening Hours:</label>
    <input type="text" name="opening_hours" id="opening_hours" class="txt med"
           value="<?php echo set_value('opening_hours') ? set_value('opening_hours') : $website->opening_hours; ?>"/>
    <br/><?php echo form_error('opening_hours'); ?>
</div>

<div class="column med left">
    <label>Phone After Hours:</label>
    <input type="text" name="phone_after_hours" id="phone_after_hours" class="txt med"
           value="<?php echo set_value('phone_after_hours') ? set_value('phone_after_hours') : $website->phone_after_hours; ?>"/>
    <br/><?php echo form_error('phone_after_hours'); ?>
</div>


<div class="column med  left">
    <label>Timezone:</label>
    <input type="text" name="timezone" id="timezone" class="txt med"
           value="<?php echo set_value('timezone') ? set_value('timezone') : $website->timezone; ?>"/>
    <br/><?php echo form_error('timezone'); ?>
</div>

<div class="column med  left">
    <label>Fax:</label>
    <input type="text" name="fax" id="fax" class="txt med"
           value="<?php echo set_value('fax') ? set_value('fax') : $website->fax; ?>"/>
    <br/><?php echo form_error('fax'); ?>
</div>

<div class="column med left">
    <label>Skype:</label>
    <input type="text" name="skype" id="skype" class="txt med"
           value="<?php echo set_value('skype') ? set_value('skype') : $website->skype; ?>"/>
    <br/><?php echo form_error('skype'); ?>
</div>

<div class="row inner-spacer">
    <label>livechat(between head tags) :</label>
    <textarea rows="6" cols="50" id="livechat" name="livechat" class="txt large"
              style="width:720px;"><?php echo set_value('livechat') ? set_value('livechat') : $website->livechat; ?></textarea>
</div>
<div class="row inner-spacer">
    <label>Google Tag Manager:</label>
    <textarea rows="5" cols="50" id="ga" name="g_analytics" class="txt large"
              style="width:720px;"><?php echo set_value('g_analytics') ? set_value('g_analytics') : $website->g_analytics; ?></textarea>
</div>

<div class="row inner-spacer">
    <label>Footer Content:</label>
    <textarea rows="5" cols="50" id="footer_content" name="footer_content"
              class="txt large"><?php echo set_value('footer_content') ? set_value('footer_content') : $website->footer_content; ?></textarea>
</div>


<div class="row">
    <div class="column left">
        <label>Logo (309 X 64)</label>
        <input type="file" name="logo"/><br/>
        <span>(keep blank if you don't want to change it)</span>
    </div>
    <div class="column left">
        <label>Current Logo</label>
        <?php if (strlen($website->logo)): ?>
            <div style="padding:10px;background:#460700;width:320px;">
                <img src="<?php echo site_logo_src($website->logo) ?>"/>
            </div>
        <?php else: ?>
            Not set.
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="column left">
        <label>Booking Form Logo (309 X 64)</label>
        <input type="file" name="booking_form_logo"/><br/>
        <span>(keep blank if you don't want to change it)</span>
    </div>
    <div class="column left">
        <label>Current Booking Form Logo</label>
        <?php if (strlen($website->booking_form_logo)): ?>
            <div style="padding:10px;background:#460700;width:320px;">
                <img src="<?php echo site_logo_src($website->booking_form_logo) ?>"/>
            </div>
        <?php else: ?>
            Not set.
        <?php endif; ?>
    </div>
</div>
<div class="row"></div>
<div class="clear column">
    <input type="submit" name="submit" class="form_btn" value="Save"/>
</div>
</form>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>js/editorconfig.js" ></script>
<script type="text/javascript">
    <!--
    $(document).ready(function () {
        $('#footer_content, #contact_info').ckeditor();
    });
    //-->
</script>
