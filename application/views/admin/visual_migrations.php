<!doctype html>
<html>
<head>
	<style>
		body {
			background: #dfdfdf;
		}
		ol {
			list-style-position: inside;
		}
		ul {
			list-style: none;
		}
		.migrations li {
			margin: 3px;
			padding: 0 10px;
			border: 1px solid #a4a4a4;
		}
		dl {
			margin: 5px;
		}
		dt {
			padding: 3px;
		}
		dt a, dt .name {
			font-weight: bold;
		}
		dd {
			margin: 10px 30px;
		}
		dd div{
			border: 1px solid #b3b3b3;
			background-color: #fff;
			padding: 5px;
		}
		.result {
			padding: 0 15px;
			margin-left: 20px;
		}
		.result.ok {
			background-color: #94dd96;
		}
		.result.error{
			background-color: #db3547;
		}
		small {
			font-size: 80%;
		}
	</style>
</head>

<body>
	<script src="/javascript/jquery-1.6.2.min.js"></script>
	<script>
		var tm;
		function apply_migration(name, el){
			$(".result").detach();
			window.clearTimeout(tm);
			var url = '<?php echo site_url("migrate/apply/");?>/' + name;
			$.get(url, function(res){
				$('<span class="result ' + (res.status ? 'ok' : 'error') + '">' + res.message + '</span>').insertAfter(el);
				if (res.status){
					tm = window.setTimeout(function(){$('.result').detach()}, 5000);
				}
			});
		}

		$().ready(function(){
			$('.show_hide').live('click', function(el){
				$(el.target).next().toggle();
			});
		})
	</script>
	<div id="body">
		<h1>Migrations list</h1>
		<p class="intro">
			<ol>
				<li>All migrations must be stored in {project_root}/migrations/</li>
				<li>
					Migration must contain only valid raw-sql code.
				</li>
			</ol>
		</p>
		<ul class="migrations">
			<?php foreach ($items as $name=>$data): $sql = $data['sql']; $created = $data['created']?>
				<li>
					<dl>
						<dt>
							<span class="name"><?php echo $name; ?></span>
							<a href="javascript://" onclick="apply_migration('<?php echo $name?>', this);">
								Apply
							</a>
						</dt>
						<dd>
							<a href="javascript:void(0);" class="show_hide">show/hide sql</a>
							<div style="display: none"><pre><?php echo $sql; ?></pre></div>
						</dd>
					</dl>
					<small>Last changed: <?php echo strftime("%Y-%m-%d %H:%M:%S", $created); ?></small>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</body>
</html>