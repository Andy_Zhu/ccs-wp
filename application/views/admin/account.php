<div class="inner-spacer box">
<h1 class="title big">Welcome, <?php echo $identity->firstname . ' ' . $identity->lastname;?> </h1>
<p class="row">
If your are not <?php echo $identity->firstname . ' ' . $identity->lastname;?>, click  
<a href="<?php echo site_url('admin/logout');?>">
Here
</a>
</p>

<div class="column left" id="account-form-container" style="width:350px;margin-right:10px;">
<h2 class="grid-title row">Your Account Info:</h2>
<?php
if(isset($form_error_message)) {
	echo '<div class="error row">', $form_error_message, '</div>';
}
?>
<form action="<?php echo site_url('admin/account')?>" method="post">
	<dl class="ci_form">
		<dt><label for="firstname">Firstname:</label></dt>
		<dd>
			<input type="text" class="txt" id="firstname" name="firstname" value="<?php echo (set_value('firstname') ? set_value('firstname'): $identity->firstname);?>" />
			<span class="error_txt"><?php echo form_error('firstname');?></span>
		</dd>
		<dt><label for="lastname">Lastname:</label></dt>
		<dd>
			<input type="text" class="txt" id="lastname" name="lastname" value="<?php echo (set_value('lastname')? set_value('lastname') : $identity->lastname);?>" />
			<span class="error_txt"><?php echo form_error('lastname');?></span>
		</dd>
		<dt><label for="email">Email:</label></dt>
		<dd>
			<input type="text" class="txt" id="email" name="email" value="<?php echo (set_value('email') ? set_value('email') : $identity->email);?>" />
			<span class="error_txt"><?php echo form_error('email');?></span>
		</dd>
		<dd><input type="submit" name="profile" class="form_btn" value="Save"/></dd>
	</dl>
</form>
</div>

<div class="column left" id="change-password-container" style="width:350px;">
<h2 class="grid-title row">Change Password:</h2>
<?php

if(isset($change_password_error)) {
	echo '<div class="error row">', $change_password_error, '</div>';
}

?>
<form action="<?php echo site_url('admin/account')?>" method="post">
	<dl class="ci_form">
		<dt><label for="current_password">Current Password:</label></dt>
		<dd>
			<input type="password" class="txt" id="current_password" name="current_password" value="<?php echo set_value('current_password');?>" />
			<span class="error_txt"><?php echo form_error('current_password');?></span>
		</dd>
		<dt><label for="new_password">New Password:</label></dt>
		<dd>
			<input type="password" class="txt" id="new_password" name="new_password" value="<?php echo set_value('new_password');?>" />
			<span class="error_txt"><?php echo form_error('new_password');?></span>
		</dd>
		<dt><label for="new_password2">Confirm New Password:</label></dt>
		<dd>
			<input type="password" class="txt" id="new_password2" name="new_password2" value="<?php echo set_value('new_password2');?>" />
			<span class="error_txt"><?php echo form_error('new_password2');?></span>	
		</dd>
		<dd><input type="submit" name="change_password_btn" class="form_btn" value="Change password"/></dd>
	</dl>
</form>
</div>
</div>

<div class="row" style="width:740px;">
<h2 class="grid-title row">Your Access Privilege:</h2>
<?php echo strtoupper($identity->role);?>
</div>

</div>
