<?php
$filterUrl = site_url('admin/websites');

?>

<div class="clear">
	<form id="search-form" class="search-form" method="get" action="<?php echo $filterUrl;?>">
	<div class="clear">
	<input type="hidden" name="page" value="1" />
	<input type="hidden" name="cid" value="<?php echo $country_id;?>" />
	</div>
	<div class="row clear">
		<div class=" med  left">
		<label for="ordField">Order By</label>
		<select name="ord_field" id="ordField" class="txt med">
			<?php foreach ($order_by_fields as $field => $label):?>
				<option value="<?php echo $field;?>" <?php if($field == $ord_field) echo 'selected="selected"'?>><?php echo $label;?></option>
			<?php endforeach;?>
		</select>
		</div>
		<div class=" med  left">
		<label for="premium">Direction</label>
		<select name="ord_dir" id="dir" class="txt med">
			<option value="desc" <?php if($ord_dir=='desc') echo 'selected="selected"'?>>Descending</option>
			<option value="asc" <?php if($ord_dir=='asc') echo 'selected="selected"'?>>Ascending</option>
		</select>
		</div>
		
		<div class="med left">
			<label for="">&nbsp;</label>
			<input type="submit" value="Order" class="form_btn" />
		</div>
		
	</div>	
</form>
</div>
<div style="background:#333; color:#ccc; padding:20px;width:300px;position:absolute;top:75px;left:520px;">
<h3>Display websites in:</h3>
<h1><?php echo $country->name;?></h1>
</div>
<div class="row"></div>

<?php if($this->Users->is_admin()):?>
<input type="button" id="add-website-btn" value="+ New Website" class="form_btn clear"/>

<div class="clear" id="add-website-container" style="padding:10px; background:#f0f0f0;border:1px solid #ccc;width:830px;">
	<form method="post" action="<?php echo site_url('admin/websites')?>">
	<?php 
		if(isset($create_user_message)) echo '<div class="error">', $create_user_message, '</div>';
	?>
		<div class="column med left">
			<label>*Country: </label>
				<select name="country_id" id="country_id" class="txt med">
					<option value="1">Other</option>
					<?php unset($countries[1]);//others?>
					<?php foreach ($countries as $country):?>
						<option value="<?php echo $country->id?>" <?php if($country_id  == $country->id) echo 'selected="selected"';?>><?php echo $country->name;?></option>
					<?php endforeach;?>
				</select>
			<br /><?php echo form_error('country_id');?>
		</div>
		<div class="column left" style="width:200px;">
		    <label>*Template: </label>
		    <select style="width:200px;" name="template" id="template" class="txt">
		        <?php foreach (template_fetch_available() as $tpl => $lb): ?>
		            <option value="<?php echo $tpl ?>" ><?php echo $lb ?></option>
		           
		        <?php endforeach; ?>
		    </select>
		    <br/><?php echo form_error('template'); ?>
		</div>
		<div class="column left" style="width:200px;">
			<label>*URL: </label>
			<input style="width:200px;" type="text" name="url" id="url" class="txt" value="<?php echo set_value('url')?>" />
			<br /><?php echo form_error('url');?>
		</div>
		<div class="column  left" style="width:200px;">
			<label>*Name:</label>
			<input style="width:200px;" type="text" name="name" id="name" class="txt" value="<?php echo set_value('name')?>" />
			<br /><?php echo form_error('name');?>
		</div>
		
		<div class="clear"></div>
		<div class="column med left">
			<label>*FTP HOST: </label>
			<input type="text" name="ftp_host" id="ftp_host" class="txt med" value="<?php echo set_value('ftp_host')?>" />
			<br /><?php echo form_error('ftp_host');?>
		</div>
		<div class="column med left">
			<label>*FTP Username:</label>
			<input type="text" name="ftp_user" id="ftp_user" class="txt med" value="<?php echo set_value('ftp_user')?>" />
			<br /><?php echo form_error('ftp_host');?>
		</div>
		<div class="column med left">
			<label>*FTP Password: </label>
			<input type="text" name="ftp_password" id="ftp_password" class="txt med" value="<?php echo set_value('ftp_password')?>" />
			<br /><?php echo form_error('ftp_password');?>
		</div>
		<div class="clear"></div>
		<div class="column  left">
			<label>Meta Description: </label>
			<textarea rows="2" cols="60"  name="meta_desc" id="meta_desc" class="txt"><?php echo set_value('meta_desc')?></textarea>
			<br /><?php echo form_error('meta_desc');?>
		</div>
		<div class="column left">
			<label>Meta Keywords: </label>
			<textarea rows="2" cols="60" name="meta_keywords" id="meta_keywords" class="txt"><?php echo set_value('meta_keywords')?></textarea>
			<br /><?php echo form_error('meta_keywords');?>
		</div>
		<div class="clear">
			<input type="submit" name="add_website_btn" class="form_btn" value="Create" />
		</div>
	</form>
</div>
<?php endif;?>

<?php if($paginator['totalCount'] > 0) :?>
<table cellpadding="0" cellspacing="0"  style="width:853px;">
	<thead>
		<tr class="head">
			<th class="id">ID</th>
			<th class="review">Name</th>
			<th class="review">URL / Test URL</th>
			<th>Last Update</th>
			<th>Last Publish</th>
			<th class="rowActions"># Actions
				<img class="global-ajax-info" src="<?php echo SITE_ROOT?>images/admin/ajax-progress.gif" />
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">
				<div>
	             
		             <div class="paginator" style="padding-left:10px;">
		             
		             <?php 
		             	$url = site_url('admin/websites') . "?ord_field=$ord_field&ord_dir=$ord_dir&cid=$country_id";
		             	echo pagination($paginator, $url, true, true);
		             ?>
		             </div>
		             <div class="digg-info">Showing <?=$paginator['from']?> to <?=$paginator['to']?> / <?=$paginator['totalCount']?> items.</div>
	       	 </div> 
			</td>
			<td>
				<div class="paginator">
				<img class="global-ajax-info" src="<?php echo SITE_ROOT?>images/admin/ajax-progress.gif" />
				</div>
			</td>
		</tr>
	</tfoot>
	
	<tbody>
		<?php $counter = 0;?>
		<?php foreach ($paginator['data'] as $website):?>
			<?php $counter++;?>
			<tr class="<?php echo ($counter%2 == 0 ? 'odd' : 'even')?>" id="website-<?php echo $website->id; ?>">
			<td><?php echo $website->id;?></td>
			<td class=""><?php echo $website->name;?></td>
			<td class="">
			
			<a href="<?php echo $website->url;?>" target="_blank"><?php echo $website->url;?></a><br />
			<?php if($website->test_url):?>
				<a href="<?php echo $website->test_url;?>" target="_blank"><?php echo $website->test_url;?></a>
			<?php else:?>
				NA
			<?php endif;?>
			<br />
			<?php if($website->old_url):?>
				<a href="<?php echo $website->old_url;?>" target="_blank"><?php echo $website->old_url;?></a>
			<?php endif;?>
			<br />
			</td>
			<td><?php echo $website->last_update_date ? $website->last_update_date : 'NA';?></td>
			<td><?php echo $website->last_publish_date ? $website->last_publish_date : 'NA';?></td>
			<td class="rowActions website-actions">
				
				<a class=""  href="<?php echo site_url('admin/edit_website?id='. $website->id . "&section=basic");?>">Update</a>
				
				<a href="<?php echo site_url('admin/preview') . "?id=$website->id&amp;section=home";?>" target="_blank" >Preview</a>
				<?php if($this->Users->is_admin()):?>
				<a class="ajax confirm delete"  href="<?php echo site_url('admin/ajax/website/delete/'. $website->id);?>">Delete</a>
				<?php endif;?>
			</td>
			</tr>
		<?php endforeach;?>

	</tbody>
	
</table>

<?php else:?>

<div class="error-msg error" style="width:830px;margin:0;">
	<h1 class="error-msg">Oops...</h1>
	<p>
	Sorry, nothing found. Please try again.
	</p>
</div>
	
<?php endif;?>
