<div id="login-form-container">
<?php 

	echo '<div> <h1>Login <h1></div><br />';
?>
<div>
	<form method="post" action="<?php echo site_url('admin/login')?>">
		<?php 
			if (isset($login_error))
			echo '<div class="error">', $login_error, '</div>';
		?>
		<dl class="ci_form">
			<dt><label for="email">Email:</label></dt>
			<dd>
				<input type="text" name="email" id="email" class="txt" value="<?php echo set_value('email');?>" />
				<span style="color:red;"><?php echo form_error('email');?></span>
			</dd>
			<dt><label for="password">Password</label></dt>
			<dd>
				<input type="password" name="password" id="password" class="txt" />
				<span style="color:red;"><?php echo form_error('password');?></span>
			</dd>
			<dd><input type="submit" value="Login" name="admin_login_btn" class="form_btn" /></dd>
		</dl>
	</form>
</div>

</div>