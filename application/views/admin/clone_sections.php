<div class="clone-wizard" id="wizard">

<form id="clone-form" method="post" action="<?php echo site_url('admin/clone_sections');?>">
	<div class="screens"> 
	
		<div class="screen first" id="sites">
			<h1 class="title">Select a source site</h1>
			<ul class="list">
				<?php foreach ($sites as $site):?>
					<li><a href="#" id="s-<?php echo $site->id;?>"><?php echo $site->name;?></a></li>
				<?php endforeach;?>
			</ul>
		</div>
		
		<div class="screen" id="pages">
			<h1 class="title">Select a source page</h1>
			<ul class="list">
				<?php foreach ($pages as $key => $name):?>
					<li><a href="#<?php echo $key;?>"><?php echo $name;?></a></li>
				<?php endforeach;?>
			</ul>
		</div>
		
		<div class="screen" id="sections">
			<h1 class="title"><input type="checkbox" class="select-all" /> Select section(s) to clone</h1>
			<div class="sections" id="page-sections"></div>
		</div>
		
		<div class="screen" id="targets">
			<h1 class="title"><input type="checkbox" class="select-all" />  Select target(s) site(s)</h1>
			
			<ul class="list">
				<?php foreach ($sites as $site):?>
					<li><a href="#"><input type="checkbox" name="targets[]" value="<?php echo $site->id;?>" /> <?php echo $site->name;?></a></li>
				<?php endforeach;?>
			</ul>
			
		</div>
		
	</div>
	<div class="wizard-navi">
		<input type="button" id="back-btn" value="&larr; Back" class="wizard_btn" />
		<input type="button" id="next-btn" value="Next &rarr;" class="wizard_btn" />
		<input type="submit" id="clone-btn" value="Clone" class="wizard_btn clone_btn" name="clone" />
	</div>
	<div>
		<div>
		<input type="hidden" name="site_id" id="site-id-input"/>
		<input type="hidden" name="page" id="page-input" />
		<input type="hidden" id="load-page-sections-url" value="<?php echo site_url("admin/ajax/clone/load-sections/");?>" />
		</div>
		<div id="message" class="info"></div>
	</div>
	</form>
</div>

<script type="text/javascript">
<!--
var messageIntervalId = null;
var showMessage = function(message){
	$('#message').text(message);
	if(!$('#message').is(':visible')) $('#message').slideDown();
	if(messageIntervalId) clearInterval(messageIntervalId);
	messageIntervalId = setInterval(function(){
		$('#message').slideUp();
	}, 2500);
};

$(document).ready(function(){

	var maxHeight = 0;
	var maxWidth = 0;
	
	$('.screens .screen').each(function(){
		maxHeight = Math.max(maxHeight, $(this).height());
		maxWidth = Math.max(maxWidth, $(this).width());
	});

	maxHeight +=  100;
	maxWidth += 50;
	
	$('.screens').css({
		height:maxHeight + 25,
		width:maxWidth,
		position:'relative',
		overflow: 'hidden'
	});
	
	$('#message').hide();
	
	$('.screens .screen').each(function(){
		$(this).css({
			height:maxHeight,
			position:'absolute',
			top:'0px'
		});
		if(!$(this).hasClass('first')) $(this).css('left', ($(this).width() + 50));
	});

	var startX = - maxWidth - 25;
	var endX = 0;
	
	$('#sites .list li a').click(function(evt){
		evt.preventDefault();
		var siteId = $(this).attr('id').split('-')[1];
		$('#site-id-input').val(siteId);
		$('#sites .list li a.current').removeClass('current');
		$(this).addClass('current');
		//remove any previously selected page
		$('#pages a.current').removeClass('current');
		$('#page-input').val('');
		$('#page-sections').html('');
		//auto move to next screen
		$('#next-btn').trigger('click');
	});

	$('#pages .list li a').click(function(evt){
		evt.preventDefault(); 
		var section = $(this).attr('href').replace('#', '');
		$('#page-input').val(section);
		$('#pages .list li a.current').removeClass('current');
		$(this).addClass('current');
		$('#next-btn').trigger('click');
		$('#sections .select-all').removeAttr('checked');
		
		$.ajax({
			url:$('#load-page-sections-url').val() +'/'+ $('#page-input').val() + '?site_id=' + $('#site-id-input').val(),
			type:'GET',
			beforeSend:function(){
				$('#page-sections').html('').addClass('ajax-loader');
			},
			success:function(data){
				if(data.success) {
					$('#page-sections').html(data.html);
				}else{
					$('#page-sections').html('<div class="info">Request failed, try again.</div>');
				}
			},	
			complete:function(){
				$('#page-sections').removeClass('ajax-loader');
			},
			error:function(){
				$('#page-sections').html('<div class="info">Request failed, try again.</div>');
			}
		});
			
	
	});

	
	$('#targets .list li a').click(function(evt){
		//evt.stopPropagation();
		//if($(this).is('a')) evt.preventDefault();
		$(this).toggleClass('current');
		if($(this).hasClass('current')) {
			$(this).find('input[type=checkbox]').attr('checked', 'checked');
		}else{
			$(this).find('input[type=checkbox]').removeAttr('checked');
			
		}

		if($('#targets .current').length > 0) {
			$('#clone-btn').removeAttr('disabled').removeClass('disabled-btn');
		}else{
			$('#clone-btn').attr('disabled', 'disabled').addClass('disabled-btn');
		}
	});

	$('#page-sections input[type=checkbox]').live('change', function(){
		
		if($(this).is(':checked')) {
			$(this).parent().parent().addClass('current');
		}else{
			$(this).parent().parent().removeClass('current');
		}
	});

	//disabling the submit btn
	$('#clone-btn').attr('disabled', 'disabled').addClass('disabled-btn');
	
	var wizardScreens = new Array();
	var currentScreenIndex = 0;
	$('#wizard .screen').each(function(){
		wizardScreens.push($(this));
	});

	$('#next-btn').click(function(evt){
		evt.preventDefault();
		if($(this).hasClass('disabled')) return;
		var nextIndex = currentScreenIndex + 1;
		if(nextIndex >= (wizardScreens.length)) {
			nextIndex = 0;
			$(this).addClass('disabled-d');
			return;
		}
		if(nextIndex == currentScreenIndex) return;
		if(wizardScreens[nextIndex].attr('id') == 'pages' && $('#sites a.current').length == 0) {
			showMessage('Please select a source site, first');
			return;
		}

		if(wizardScreens[nextIndex].attr('id') == 'sections' && $('#pages a.current').length == 0) {
			showMessage('Please select a source page, first');
			return;
		}

		if(wizardScreens[nextIndex].attr('id') == 'targets' && $('#sections #page-sections input[type=checkbox]:checked').length == 0) {
			showMessage('Please select section(s) to clone, first');
			return;
		}
		
		wizardScreens[currentScreenIndex].animate({left:startX});
		wizardScreens[nextIndex].animate({left:endX}, function(){
			wizardScreens[currentScreenIndex].css('left', Math.abs(startX));
			currentScreenIndex = nextIndex;
			if(currentScreenIndex == (wizardScreens.length - 1 )) {
				if($('#targets .current').length > 0) {
					$('#clone-btn').removeAttr('disabled').removeClass('disabled-btn');
				}else{
					$('#clone-btn').attr('disabled', 'disabled').addClass('disabled-btn');
				}
			}
			
		});
		
	});

	$('#back-btn').click(function(evt){
		evt.preventDefault();
		if($(this).hasClass('disabled')) return;
		var nextIndex = currentScreenIndex - 1;
		if(nextIndex < 0) {
			nextIndex = 0;
			return;
		}
		if(nextIndex == currentScreenIndex) return;

		wizardScreens[currentScreenIndex].animate({left:Math.abs(startX)});
		wizardScreens[nextIndex].css('left', startX);
		wizardScreens[nextIndex].animate({left:endX}, function(){
			wizardScreens[currentScreenIndex].css('left', Math.abs(startX));
			currentScreenIndex = nextIndex;
			//disable submit btn
			$('#clone-btn').attr('disabled', 'disabled').addClass('disabled-btn');
		});
		
	});

	$('.select-all').click(function(evt){
		
		//if($(this).is(':checked')) {
			$(this).parent().parent().find('.list a, .box input[type=checkbox], .image input[type=checkbox]').trigger('click');
		/*}else{
			$(this).parent().parent().find('#page-sections input[type=checkbox]').trigger('click');
		}*/
	});
	

});
//-->
</script>