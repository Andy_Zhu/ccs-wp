<?php
$filterUrl = site_url('admin/logs');

?>

<div class="clear">
	<form id="search-form" class="search-form" method="get" action="<?php echo $filterUrl;?>">
	<div class="clear">
	<input type="hidden" name="page" value="1" />
	</div>
	<div class="row clear">
		<div class="column column-start med  left">
		<label for="site_id">Website</label>
		<select name="site_id" id="site_id" class="txt med">
			<option value="">-- All --</option>
			<?php foreach ($websites as $site):?>
				<option value="<?php echo $site->id;?>" <?php if($site->id == $site_id) echo 'selected="selected"'?>><?php echo $site->name;?></option>
			<?php endforeach;?>
		</select>
		</div>
		<div class="column med  left">
		<label for="site_id">User</label>
		<select name="uid" id="uid" class="txt med">
			<option value="">-- All --</option>
			<?php foreach ($users as $user):?>
				<option value="<?php echo $user->id;?>" <?php if($user->id == $user_id) echo 'selected="selected"'?>><?php echo $user->firstname . ' ' . $user->lastname;?></option>
			<?php endforeach;?>
		</select>
		</div>
		
		<div class="column med  left">
		<label for="section">Page/Action</label>
		<select name="section" id="section" class="txt med">
			<option value="">-- All --</option>
			<?php foreach ($sections as $key => $label):?>
				<option value="<?php echo $key;?>" <?php if($key == $section) echo 'selected="selected"'?>><?php echo $label;?></option>
			<?php endforeach;?>
		</select>
		</div>
		
		<div class="column med left">
			<label for="">&nbsp;</label>
			<input type="submit" value="Filter" class="form_btn" />
		</div>
		
	</div>	
</form>
</div>
<div class="row"></div>


<?php if($paginator['totalCount'] > 0) :?>
<table cellpadding="0" cellspacing="0"  style="width:853px;">
	<thead>
		<tr class="head">
			<th class="id">ID</th>
			<th class="review">Website</th>
			<th class="review">Page/Action</th>
			<th class="review">User</th>
			<th>Message</th>
			<th>DateTime</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">
				<div>
	             
		             <div class="paginator" style="padding-left:10px;">
		             
		             <?php 
		             	$url = site_url('admin/logs') . "?site_id=$site_id&section=$section&uid=$user_id";
		             	echo pagination($paginator, $url, true, true);
		             ?>
		             </div>
		             <div class="digg-info">Showing <?=$paginator['from']?> to <?=$paginator['to']?> / <?=$paginator['totalCount']?> items.</div>
	       	 </div> 
			</td>
			<td>
				<div class="paginator">
				<img class="global-ajax-info" src="<?php echo SITE_ROOT?>images/admin/ajax-progress.gif" />
				</div>
			</td>
		</tr>
	</tfoot>
	
	<tbody>
		<?php $counter = 0;?>
		<?php foreach ($paginator['data'] as $log):?>
			<?php 
				$counter++;
				$website = array_key_exists($log->website_id, $websites) ? $websites[$log->website_id] : null;
				$user = array_key_exists($log->user_id, $users) ? $users[$log->user_id] : null;
			?>
			<tr class="<?php echo ($counter%2 == 0 ? 'odd' : 'even')?>" id="log-<?php echo $log->id; ?>">
			<td><?php echo $log->id;?></td>
			<td class="">
				<?php if($website):?>
				<a href="<?php echo $website->url;?>" target="_blank"><?php echo $website->name;?></a>
				<?php else:?>
				NA
				<?php endif;?>
			</td>
			<td><?php echo ucwords($log->page_uri == 'basic' ? 'Settings' : $log->page_uri);?></td>
			<td><?php echo $user ? ( $user->firstname . ' ' . $user->lastname) : 'NA';?></td>
			<td><?php echo $log->content;?></td>
			<td class=""><?php echo $log->ts_date_created;?></td>
			</tr>
		<?php endforeach;?>
		
	</tbody>
	
</table>

<?php else:?>

<div class="error-msg error" style="width:742px;margin:0;">
	<h1 class="error-msg">Oops...</h1>
	<p>
	Sorry, nothing found. Please try again.
	</p>
</div>
	
<?php endif;?>
