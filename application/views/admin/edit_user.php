<div style="width:700px;">
<div class="row">
	<a href="<?php echo site_url('admin/users');?>">Users</a> &raquo; <a href="<?php echo site_url('admin/edit_user') . "/$id"?>">Updating <?php echo $user->firstname . ' ' . $user->lastname?> </a>
</div>
<h1 class="grid-title row">Updating <?php echo $user->firstname . ' ' . $user->lastname?>'s Account</h1>
<form method="post" action="<?php echo site_url('admin/edit_user') . "/$id"?>" style="background:#ccc;padding:10px 0px 20px;">
	<?php 
		if(isset($create_user_message)) echo '<div class="error">', $create_user_message, '</div>';
	?>
		<div class="column left">
			<label>Firstname:</label>
			<input type="text" name="firstname" id="firstname" class="txt" value="<?php echo set_value('firstname') ? set_value('firstname') : $user->firstname?>" />
			<br /><?php echo form_error('firstname');?>
		</div>
		<div class="column left">
			<label>Lastname: </label>
			<input type="text" name="lastname" id="lastname" class="txt" value="<?php echo set_value('lastname') ? set_value('lastname') : $user->lastname;?>" />
			<br /><?php echo form_error('lastname');?>
		</div>
		<div class="clear"></div>
		<div class="column left">
			<label>Email: </label>
			<input type="text" name="email" id="email" class="txt" value="<?php echo set_value('email') ? set_value('email') : $user->email;?>" />
			<br /><?php echo form_error('email');?>
		</div>
		<div class="column left">
			<label>Role: </label>
			<select name="role" class="txt med">
				<option <?php if(set_value('role') == 'admin' || $user->role == 'admin') echo 'selected="selected" ';?> value="admin">Admin</option>
				<option <?php if(set_value('role') == 'editor' || $user->role == 'editor') echo 'selected="selected" ';?>  value="editor">Editor</option>
			</select>
			<br /><?php echo form_error('role');?>
		</div>
		<div class="clear"></div>
		<div class="column left">
			<label>Password: </label>
			<input type="password" name="password" id="password" class="txt" value="<?php echo set_value('password')?>" />
			<br />
			<span>*leave blank if you don't want to change it</span>
			<br /><?php echo form_error('password');?>
		</div>
		<div class="column left">
			<label>Status</label>
			<select name="status" class="txt med">
				<option value="A">Active</option>
				<option value="B">Blocked</option>
			</select>
			<br /><?php echo form_error('role');?>
		</div>
		<div class="clear" style="padding-left:10px; padding-top:20px;">
			<input type="submit" name="update_user_btn" class="form_btn" value="Update user" />
		</div>
	</form>
	
</div>