<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>CCS Admin</title>
	<?php
//	define('RELATIVE_PATH', str_replace('index.php', '', $_SERVER["SCRIPT_NAME"]));
	add_head_css('admin/adminp.css');
	add_head_css('admin/forms.css');
	echo get_head_css();
	add_head_js('jquery-1.6.2.min.js');
	add_head_js('jquery.form.min.js');
	add_head_js('admin/admin.js');

	if ($tab == 'users') add_head_js('admin/users.js');

	echo get_head_js();
	?>
	<script src="/js/jquery-ui.js"></script>
	<script src="/javascript/admin/jquery.multiselect.min.js"></script>
	<script src="/javascript/admin/jquery.multiselect.filter.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/admin/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="/css/admin/jquery.multiselect.css">
	<link rel="stylesheet" type="text/css" href="/css/admin/jquery.multiselect.filter.css">
</head>
<body>
<?php
if(!isset($isPopup) || !$isPopup) {
?>
<div id="adminp-container">


	<div id="navi-container">

	<?php
			//$homeCss = ($this->tab == 'home') ? 'selected' : '';
			$websitesCSS = ($tab == 'websites') ? 'selected' : '';
			$vehiclesCss = ($tab == 'vehicles') ? 'selected' : '';
			$usersCss = ($tab == 'users') ? 'selected' : '';
      $translationsCss = ($tab == 'translations') ? 'selected' : '';
			$logsCss = ($tab == 'logs') ? 'selected' : '';
			$cloneCss = ($tab == 'clone') ? 'selected' : '';
			$accountCss = ($tab == 'account') ? 'selected' : 'account';
			$templatesCss = ($tab == 'emailtemplates') ? 'selected' : '';
			$bookingCss = ($tab == 'bookingtemplates') ? 'selected' : '';
			$logoutCss = 'logout';
		?>
		<ul id="navi">
			<li><a class="<?php echo $websitesCSS?>" href="<?php echo site_url('admin/websites');?>">Websites</a></li>
			<li><a class="<?php echo $vehiclesCss?>" href="<?php echo site_url('admin/vehicles');?>">Vehicles</a></li>
			<?php if($this->Users->is_admin()) {?><li><a class="<?php echo $cloneCss?>" href="<?php echo site_url('admin/clone_sections');?>">Clone Section(s)</a></li><?php }?>
            <?php if($this->Users->is_admin()) {?><li><a class="<?php echo $translationsCss?>" href="<?php echo site_url('admin/translations');?>">Translations</a></li><?php }?>
            <?php if($this->Users->is_admin()) {?><li><a class="<?php echo $usersCss?>" href="<?php echo site_url('admin/users');?>">Users</a></li><?php }?>
			<?php if($this->Users->is_admin()) {?><li><a class="<?php echo $templatesCss?>" href="<?php echo site_url('admin/email_templates');?>">Email templates</a></li><?php }?>
			<?php if($this->Users->is_admin()) {?><li><a class="<?php echo $logsCss?>" href="<?php echo site_url('admin/logs');?>">Logs</a></li><?php }?>
			<li><a class="<?php echo $accountCss?>" href="<?php echo site_url('admin/account');?>">Account</a></li>
			<li><a class="<?php echo $logoutCss?>" href="<?php echo site_url('admin/logout')?>">Logout</a></li>
			<li><input type="text" class="txt" name="autosuggest_city"
					   id="autosuggest_city" placeholder="Enter city name or website url" /></li>
		</ul>

	</div>

	<br />


<div id="section-container">
<?php }?>

		<?php
		if($this->session->flashdata('info')) {
			if(isset($flashMessages) && is_array($flashMessages)) {
				if(!is_array($this->session->flashdata('info'))) {
					array_push($flashMessages, $this->session->flashdata('info'));
				}else{
					array_merge($flashMessages, $this->session->flashdata('info'));
				}
			}else{
				$flashMessages = $this->session->flashdata('info');
				$flashMessages = is_array($flashMessages) ? $flashMessages : array($flashMessages);
			}
		}

		if($this->session->flashdata('revisions')) {
			if(isset($flashMessages) && is_array($flashMessages)) {
				if(!is_array($this->session->flashdata('revisions'))) {
					array_push($flashMessages, $this->session->flashdata('revisions'));
				}else{
					array_merge($flashMessages, $this->session->flashdata('revisions'));
				}
			}else{
				$flashMessages = $this->session->flashdata('revisions');
				$flashMessages = is_array($flashMessages) ? $flashMessages : array($flashMessages);
			}
		}

		if(isset($flashMessages)) {
			if(count($flashMessages) > 0) {
        		echo '<div id="info-bar">';
        		foreach($flashMessages as $flashMessage) {
        			echo '<span class="info">' . $flashMessage . '</span><br />';
        		}
        		echo '</div>';
        		echo '<script type="text/javascript">setTimeout(function(){$("#info-bar").slideUp();}, 5000);</script>';
        	}
		}
		?>

		<?php
			if(isset($view_page))
			$this->load->view($view_page);

		?>

<?php
if(!isset($isPopup) || !$isPopup) {
?>
	</div>

</div>
<?php }?>
	<script type="text/javascript">
		$(document).ready(function(){
			$.ajax("<?php echo site_url('admin/websites_list'); ?>", {
				success: function(data) {
					$('#autosuggest_city').autocomplete({
						source: data,
						delay: 100,
						minLength: 3,
						focus: function(event, ui){
							event.preventDefault();
							$('#autosuggest_city').val(ui.item.label);
						},
						select: function( event, ui ) {
							event.preventDefault();
							var newEvent = $.Event('keypress', {
								keyCode: event.keyCode
							});
							if (newEvent.keyCode == $.ui.keyCode.TAB) {
								event.preventBubble();
							}
							$('#autosuggest_city').val(ui.item.label);
							var url = '<?php echo site_url('admin/edit_website') ?>?id=' + ui.item.value + '&section=basic';
							window.open(url ,'_blank');
						}
					});
				},
				error: function (error) {
					alert ('Error when loading websites list');
				}
			});
		});
		$(document).delegate("#autosuggest_city", "keydown.autocomplete",
			function (event) {
				var newEvent = $.Event('keypress', {
					keyCode: event.keyCode
				});
				if (newEvent.keyCode == $.ui.keyCode.TAB) {
					event.preventDefault();
					newEvent.keyCode = $.ui.keyCode.DOWN;
					$(this).trigger(newEvent);
				}
			}
		);
	</script>
</body>
</html>
