<h1 class="grid-title row" style="width:760px;">Updating <?php echo $vehicle->short_name?></h1>
<div class="clear" style="padding:10px; background:#f0f0f0;border:1px solid #ccc;width:740px;">
    <form method="post" action="<?php echo site_url('admin/edit_vehicle') . '?id=' . $vehicle->id?>" enctype="multipart/form-data">
    <?php
    if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
    ?>
    <fieldset>
	<legend>Vehicle Info:</legend>
        <div class="edit_vehicle_tabs_container">
        <ul class="edit_vehicle_tabs">
            <li>
                <input type="radio" checked name="tabs" id="tab_enu" value="enu">
                <label for="tab_enu">ENU</label>
            </li>
            <li>
                <input type="radio" name="tabs" id="tab_chn" value="chn">
                <label for="tab_chn">CHN</label>
            </li>
            <li>
                <input type="radio" name="tabs" id="tab_por" value="por">
                <label for="tab_por">POR</label>
            </li>
        </ul>
        </div>
        <div class="clear"></div>
        <div id="tab_enu">
            <div class="column med left">
                <label>*Class: </label>
                <select name="class_id" id="class_id" class="txt med" value="<?php echo set_value('class_id')?>">
                    <option value="">-- Choose One -- </option>
                    <?php foreach ($classes as $class):?>
                        <option <?php if($class->id == $vehicle->class_id) echo 'selected="selected";'?> value="<?php echo $class->id?>"><?php echo $class->name;?></option>
                    <?php endforeach;?>
                </select>
                <br /><?php echo form_error('class_id');?>
            </div>
            <div class="column med left">
                <label>*Short name:</label>
                <input type="text" maxlength="25" name="short_name" id="short_name" class="txt med" value="<?php echo set_value('short_name') ? set_value('short_name') : $vehicle->short_name;?>" />
                <br /><?php echo form_error('short_name');?>
            </div>

            <div class="column med left">
                <label>Internal Name:</label>
                <input type="text" name="internal_name" id="internal_name" class="txt med" value="<?php echo set_value('internal_name') ? set_value('internal_name') : $vehicle->internal_name;?>" />
                <br /><?php echo form_error('internal_name');?>
            </div>

            <div class="column left">
                <label>*Long name:</label>
                <input type="text" maxlength="50" name="long_name" id="long_name" class="txt" value="<?php echo set_value('long_name') ? set_value('long_name') : $vehicle->long_name;?>" />
                <br /><?php echo form_error('long_name');?>
            </div>

            <div class="column med left">
                <label>*Passengers:</label>
                <input type="text" name="passengers" id="passengers" class="txt med" value="<?php echo set_value('passengers') ? set_value('passengers') : $vehicle->passengers;?>" />
                <br /><?php echo form_error('passengers');?>
            </div>

            <div class="column med left">
                <label>*Luggage:</label>
                <input type="text" name="luggage" id="luggage" class="txt med" value="<?php echo set_value('luggage') ? set_value('luggage') : $vehicle->luggage;?>" />
                <br /><?php echo form_error('luggage');?>
            </div>
            <div class="large left inner-spacer">
                <label>*Description:</label>
                <textarea rows="5" cols="60" name="description" id="description" class="txt large"><?php echo set_value('description') ? set_value('description') : $vehicle->description;?></textarea>
                <br /><?php echo form_error('description');?>
            </div>
        <div class="clear"></div>
        </div>

        <div id="tab_por">
            <div class="column med left">
                <label>*Short name:</label>
                <input type="text" maxlength="25" name="short_name_por" id="short_name_por" class="txt med" value="<?php echo set_value('short_name_por') ? set_value('short_name_por') : $vehicle->short_name_por;?>" />
                <br /><?php echo form_error('short_name_por');?>
            </div>

            <div class="column med left">
                <label>Internal Name:</label>
                <input type="text" name="internal_name_por" id="internal_name_por" class="txt med" value="<?php echo set_value('internal_name_por') ? set_value('internal_name_por') : $vehicle->internal_name_por;?>" />
                <br /><?php echo form_error('internal_name_por');?>
            </div>

            <div class="column left">
                <label>*Long name:</label>
                <input type="text" maxlength="50" name="long_name_por" id="long_name_por" class="txt" value="<?php echo set_value('long_name_por') ? set_value('long_name_por') : $vehicle->long_name_por;?>" />
                <br /><?php echo form_error('long_name_por');?>
            </div>
            <div class="large left inner-spacer">
                <label>*Description:</label>
                <textarea rows="5" cols="60" name="description_por" id="description_por" class="txt large"><?php echo set_value('description_por') ? set_value('description_por') : $vehicle->description_por;?></textarea>
                <br /><?php echo form_error('description_por');?>
            </div>
            <div class="clear"></div>
        </div>


        <div id="tab_chn">
            <div class="column med left">
                <label>*Short name:</label>
                <input type="text" maxlength="25" name="short_name_chn" id="short_name_chn" class="txt med" value="<?php echo set_value('short_name_chn') ? set_value('short_name_chn') : $vehicle->short_name_chn;?>" />
                <br /><?php echo form_error('short_name_chn');?>
            </div>

            <div class="column med left">
                <label>Internal Name:</label>
                <input type="text" name="internal_name_chn" id="internal_name_chn" class="txt med" value="<?php echo set_value('internal_name_chn') ? set_value('internal_name_chn') : $vehicle->internal_name_chn;?>" />
                <br /><?php echo form_error('internal_name_chn');?>
            </div>

            <div class="column left">
                <label>*Long name:</label>
                <input type="text" maxlength="50" name="long_name_chn" id="long_name_chn" class="txt" value="<?php echo set_value('long_name_chn') ? set_value('long_name_chn') : $vehicle->long_name_chn;?>" />
                <br /><?php echo form_error('long_name_chn');?>
            </div>
            <div class="large left inner-spacer">
                <label>*Description:</label>
                <textarea rows="5" cols="60" name="description_chn" id="description_chn" class="txt large"><?php echo set_value('description_chn') ? set_value('description_chn') : $vehicle->description_chn;?></textarea>
                <br /><?php echo form_error('description_chn');?>
            </div>
            <div class="clear"></div>
        </div>

		<div class="column left inner-spacer">
			<label>*Thumbnail (300 x 215)</label>
			<input type="file" name="thumb" /><br />
			<span>Current:</span><br />
			<img src="<?php echo vehicle_image_single_thumb_src($vehicle->thumb);?>" />
		</div>
		<div class="column left inner-spacer">
			<label>*Sidebar Icon</label>
			<input type="file" name="icon" />
			<span class="clear">70x45 PNG file</span><br />
			<span>Current:</span><br />
			<img src="<?php echo vehicle_image_single_thumb_src($vehicle->icon);?>" />
		</div>
		</fieldset>
		<fieldset id="gallery">
			<legend>Vehicle Images (600 x 400):</legend>

				<?php for($i=1; $i<5; $i++):?>
					<div class="image-form">

						<?php $image = array_key_exists($i, $images) ? $images[$i] : null;?>

						<div class="img-preview">
							<?php if($image):?>
								<a id="<?php echo $i?>" href="<?php echo vehicle_image_src($image->filename);?>" class="lage-image" rel="gallery">
									<img src="<?php echo vehicle_image_thumb_src($image->filename);?>" class="thumb" />
								</a>
								<div class="hint">
								Click on thumbnail to view fullsize image
								</div>
							<?php else:?>
								<span class="not-set">NOT SET</span>
							<?php endif;?>
						</div>
						<div class="input">
							#<?php echo $i;?> <input type="file" name="image<?php echo $i?>" />
						</div>

					</div>
				<?php endfor;?>

		</fieldset>

		<fieldset>
			<input type="submit" name="edit_vehicle_btn" class="form_btn" value="Save" />
		</fieldset>
	</form>

</div>
<style type="text/css">
<!--
@import url("<?php echo CSS_PATH?>jquery.lightbox-0.5.css");
-->
</style>
<script type="text/javascript" src="<?php echo JS_PATH?>jquery.lightbox-0.5.min.js"></script>
<script type="text/javascript">
<!--

$(document).ready(function(){

	$(function() {
		$('#gallery a').lightBox({fixedNavigation:true});
	});


});
//-->
</script>