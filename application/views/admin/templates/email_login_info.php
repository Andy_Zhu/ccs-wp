<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CCS Account Info</title>
</head>

<body>
<p>Dear <?php echo $firstname , ' ', $lastname;?></p>
<p>Find bellow your CCS login information</p>
<p>
	Username:<b> <?php echo $email;?></b><br /> 
	Password: <b><?php echo $password;?></b><br />
	Login URL: <a href="<?php echo site_url('admin/login')?>">  <?php echo site_url('admin/login');?></a>
</p>
<p>For security measures, please change your password once you login</p>
<p>CCS Team</p>
</body>
</html>
