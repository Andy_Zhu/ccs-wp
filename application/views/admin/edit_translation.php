<div style="width:700px;">
<div class="row">
	<a href="<?php echo site_url('admin/translations');?>">Translations</a> &raquo; <a href="<?php echo site_url('admin/edit_translation') . "/$id"?>">Updating <?php echo $translation->label . ' ' . $translation->language?> </a>
</div>
<h1 class="grid-title row">Updating <?php echo $translation->label . ' for ' . $translation->language?> - Translation</h1>
<form method="post" action="<?php echo site_url('admin/edit_translation') . "/$id"?>" style="background:#ccc;padding:10px 0px 20px;">
	<?php 
		if(isset($create_translation_message)) echo '<div class="error">', $create_translation_message, '</div>';
	?>
		<div class="column left">
			<?php $label_value = set_value('label') ? set_value('label') : $translation->label; ?>
			<label>Label:</label>
			<input type="text" name="label" id="label" class="txt" value="<?php echo $label_value ?>" />
			<br /><?php echo form_error('label');?>
		</div>
		<div class="column left">
			<label>Language: </label>
            <select name="language" id="language" class="txt">
                <?php foreach($languages as $lang): ?>
                    <option value="<?php echo $lang->id; ?>" <?php echo ($lang->id == $translation->lang_id ? 'selected' : '') ?>><?php echo $lang->language; ?></option>
                <?php endforeach; ?>
            </select>
			<br /><?php echo form_error('language');?>
		</div>
		<div class="clear"></div>
		<div class="column left">
			<label>Translation: </label>
			<input type="text" name="translation" id="translation" class="txt" value="<?php echo set_value('translation') ? set_value('translation') : $translation->translation;?>" />
			<br /><?php echo form_error('email');?>
		</div>
		<div class="clear" style="padding-left:10px; padding-top:20px;">
			<input type="submit" name="update_translation_btn" class="form_btn" value="Update translation" />
		</div>
	</form>
	
</div>
