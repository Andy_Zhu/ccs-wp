<?php
$filterUrl = site_url('admin/vehicles');

?>

<div class="clear">
	<form id="search-form" class="search-form" method="get" action="<?php echo $filterUrl;?>">
	<div class="clear">
	<input type="hidden" name="page" value="1" />
	</div>
	<div class="row clear">
		<div class=" med  left">
		<label for="ordField">Order By</label>
		<select name="ord_field" id="ordField" class="txt med">
			<?php foreach ($order_by_fields as $field => $label):?>
				<option value="<?php echo $field;?>" <?php if($field == $ord_field) echo 'selected="selected"'?>><?php echo $label;?></option>
			<?php endforeach;?>
		</select>
		</div>
		<div class=" med  left">
		<label for="premium">Direction</label>
		<select name="ord_dir" id="dir" class="txt med">
			<option value="desc" <?php if($ord_dir=='desc') echo 'selected="selected"'?>>Descending</option>
			<option value="asc" <?php if($ord_dir=='asc') echo 'selected="selected"'?>>Ascending</option>
		</select>
		</div>
		
		<div class="med left">
			<label for="">&nbsp;</label>
			<input type="submit" value="Order" class="form_btn" />
		</div>
		
	</div>	
</form>
</div>
<div class="row"></div>
<input type="button" id="add-vehicle-btn" value="+ New Vehicle" class="form_btn clear"/>
<div class="clear" id="add-vehicle-container" style="padding:10px; background:#f0f0f0;border:1px solid #ccc;width:740px;">
	<form method="post" action="<?php echo site_url('admin/vehicles')?>" enctype="multipart/form-data">
	<?php 
		if(isset($form_msg)) echo '<div class="error">', $form_msg, '</div>';
	?>
		<div class="column med left">
			<label>*Class:</label>
			<select name="class_id" id="class_id" class="txt med" value="<?php echo set_value('class_id')?>">
				<option value="">-- Choose One -- </option>
				<?php foreach ($classes as $class):?>
					<option value="<?php echo $class->id?>"><?php echo $class->name;?></option>
				<?php endforeach;?>
			</select>
			<br /><?php echo form_error('class_id');?>
		</div>
		<div class="column med left">
			<label>*Short name:</label>
			<input maxlength="20" type="text" name="short_name" id="short_name" class="txt med" value="<?php echo set_value('short_name')?>" />
			<br /><?php echo form_error('short_name');?>
		</div>
		<div class="column med left">
			<label>Internal Name:</label>
			<input type="text" name="internal_name" id="internal_name" class="txt med" value="<?php echo set_value('internal_name') ;?>" />
			<br /><?php echo form_error('internal_name');?>
		</div>
		<div class="clear"></div>
		<div class="column  left">
			<label>*Long name:</label>
			<input maxlength="30" type="text" name="long_name" id="long_name" class="txt" value="<?php echo set_value('long_name');?>" />
			<br /><?php echo form_error('long_name');?>
		</div>
		<div class="column med left">
			<label>*Passengers:</label>
			<input type="text" name="passengers" id="passengers" class="txt med" value="<?php echo set_value('passengers')?>" />
			<br /><?php echo form_error('passengers');?>
		</div>
		<div class="column med left">
			<label>*Luggage:</label>
			<input type="text" name="luggage" id="luggage" class="txt med" value="<?php echo set_value('luggage')?>" />
			<br /><?php echo form_error('luggage');?>
		</div>
		<div class="large left inner-spacer">
			<label>*Description:</label>
			<textarea rows="5" cols="60" name="description" id="description" class="txt large"><?php echo set_value('description')?></textarea>
			<br /><?php echo form_error('description');?>
		</div>
		<div class="column left inner-spacer">
			<label>*Thumbnail (300 x 215)</label>
			<input type="file" name="thumb" />
		</div>
		<div class="column left inner-spacer">
			<label>*Sidebar Icon</label>
			<input type="file" name="icon" />
			<span class="clear">70x45 PNG file</span>
		</div>
		<div class="clear">
			<input type="submit" name="add_vehicle_btn" class="form_btn" value="Add" />
		</div>
	</form>
</div>

<?php if($paginator['totalCount'] > 0) :?>
<table cellpadding="0" cellspacing="0" class="business-tb" id="business-tb" style="width:763px;">
	<thead>
		<tr class="head">
			<th class="id">ID</th>
			<th class="">Thumb</th>
			<th>Icon</th>
			<th class="">Name</th>
			<th>Internal Name</th>
			<th class="rowActions"># Actions
				<img class="global-ajax-info" src="<?php echo SITE_ROOT?>images/admin/ajax-progress.gif" />
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="6">
				<div>
	             
		             <div class="paginator" style="padding-left:10px;">
		             
		             <?php 
		             	$url = site_url('admin/vehicles') . "?ord_field=$ord_field&ord_dir=$ord_dir"; 
		             	echo pagination($paginator, $url, true, true);
		             ?>
		             </div>
		             <div class="digg-info">Showing <?=$paginator['from']?> to <?=$paginator['to']?> / <?=$paginator['totalCount']?> items.</div>
	       	 </div> 
			</td>
			<td>
				<div class="paginator">
				<img class="global-ajax-info" src="<?php echo SITE_ROOT?>images/admin/ajax-progress.gif" />
				</div>
			</td>
		</tr>
	</tfoot>
	
	<tbody>
		<?php $counter = 0;?>
		<?php foreach ($paginator['data'] as $vehicle):?>
			<?php $counter++;?>
			<tr class="<?php echo ($counter%2 == 0 ? 'odd' : 'even')?>" id="website-<?php echo $vehicle->id; ?>">
			<td><?php echo $vehicle->id;?></td>
			<td class="img"><img style="width:175px; height:125px;" src="<?php echo vehicle_image_single_thumb_src($vehicle->thumb);?>" /></td>
			<td class="img"><img src="<?php echo vehicle_image_single_thumb_src($vehicle->icon);?>" /></td>
			<td><?php echo $vehicle->short_name;?></td>
			<td><?php echo $vehicle->internal_name;?></td>
			<td class="rowActions website-actions">
				
				<a class=""  href="<?php echo site_url('admin/edit_vehicle?id='. $vehicle->id);?>">Edit</a>
				<?php if($this->Users->is_admin()):?>
				<a class="ajax confirm delete"  href="<?php echo site_url('admin/ajax/vehicle/delete/'. $vehicle->id);?>">Delete</a>
				<?php endif;?>
			</td>
			</tr>
		<?php endforeach;?>
		
	</tbody>
	
</table>

<?php else:?>

<div class="error-msg error" style="width:742px;margin:0;">
	<h1 class="error-msg">Oops...</h1>
	<p>
	Sorry, nothing found. Please try again.
	</p>
</div>
	
<?php endif;?>