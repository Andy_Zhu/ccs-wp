<div class="countries-page">
	<div class="top-bar">
		<h1>Please select a market.</h1>
		<input title="Create a new market" type="button" id="add-country-btn" class="orange-btn" value="+ New Market" />
		<form id="country-form" action="<?php echo site_url('admin/countries');?>" method="post">
			<div>
				<label>Country Name:</label>
				<div>
				<input type="text" name="name" value="" class="txt" />
				<input type="submit" name="add_country" value="Add" class="orange-btn" />
				<img id="ajax-anim" src="<?php echo SITE_ROOT;?>images/admin/ajax-progress.gif" />
				<br /><span id="status"></span>
				</div>
			</div>
		</form>
	</div>
	<p class="row"></p>
	<p class="row"></p>

	<ul class="countries-list" id="countries">
		<?php unset($countries[1]);?>
		<?php foreach ($countries as $country):?>
			<?php $this->load->view('admin/partials/country_row', array('country'=>$country));?>
		<?php endforeach;?>

		<li><a class="cname" href="<?php echo site_url('admin/websites') . '?cid=1';?>">&raquo; Other</a></li>
	</ul>

</div>

<script type="text/javascript">
<!--
	$(document).ready(function(){

		$('#country-form,#country-form #ajax-anim, #countries .edit-form, #countries .edit-form .ajax-progress').hide();
		
		$('#countries a').live('click', function(evt){
			if($(this).hasClass('cname')) {
				return true;
			}
			
			evt.preventDefault();

			if($(this).hasClass('edit')) {

				var countryId = $(this).attr('href').split('-')[1];

				$('#edit-form-' + countryId).slideToggle();
			}


			if($(this).hasClass('delete')) {

				if(confirm('Are you sure you want to delete this country? all websites will be moved to "Other"')) {
					$.get($(this).attr('href'), function(data){
						if(data.success) {
							$('#country-' + data.id).slideUp(function(){$(this).remove();});
						}
					});
				}
			}
			
		});

		//update forms
		$('#countries .edit-form form').live('submit', function(evt){
			evt.preventDefault();
			
			var countryId = $(this).find('input[name="cid"]').val();
			$('#status-' + countryId).text('').hide();
			if($('#cname-' + countryId + ' span').text().trim().toLowerCase() == $(this).find('input[name="name"]').val().trim().toLowerCase()) {
				return ; //no change just return;
			}
			var action = $(this).attr('action');
			var method = $(this).attr('method');
			var params = $(this).serialize() + '&edit_country=true';
			
			$.ajax({
				url:action,
				type:method,
				data:params,
				beforeSend:function(){
					$('#status-' + countryId).text('').hide();
					$('#ajax-anim-' + countryId).fadeIn('fast');
				},
				success:function(data){
					if(data.success) {
						$('#cname-' + countryId + ' span').text(data.name);
						$('#edit-form-' + countryId).slideUp();
					}else{
						$('#status-' + countryId).text('Failed, try again.').show();
					}
				},
				error:function(){
					$('#status-' + countryId).text('Failed, try again.').show();
				},
				complete:function(){
					$('#ajax-anim-' + countryId).fadeOut('fast');
				}
		
			});
		});

		
		$('#add-country-btn').click(function(evt){
			evt.preventDefault();
			$('#country-form').slideToggle();
		});

		$('#country-form').submit(function(evt){
			
			evt.preventDefault();
			if($('#country-form input[name="name"]').val() == '') {
				return false;
			}

			$.ajax({
				url: $('#country-form').attr('action'),
				type: $('#country-form').attr('method'),
				data:$('#country-form').serialize() + '&add_country=true',
				beforeSend:function(){
					$('#country-form #status').text('').hide();
					$('#country-form #ajax-anim').fadeIn('fast');
				},
				success:function(data){
					if(data.success) {
						var country = $(data.html).hide().insertBefore($('#countries li:first'));
						country.find('.edit-form').hide();
						country.find('.ajax-progress').hide();
						country.slideDown();
						$('#country-form input[name="name"]').val('');
					}else{
						//alert(data.message);
						$('#country-form #status').text(data.message).show();
					}
				},
				error:function(){
					$('#country-form #status').text('failed, try again.').show();
				},
				complete:function() {
					$('#country-form #ajax-anim').fadeOut('fast');
				}
			});
		});

		$('#autosuggest_city').focus();
	});
//-->
</script>

<!--<a href="/index.php/admin/get_csv">Get markets list in csv</a>-->