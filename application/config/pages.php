<?php
$config['page_uris'] =  array(
	'home' => 'Home', 
	'about-us' => 'About us', 
	'fleet'=> 'Fleet', 
	'services' => 'Services', 
	'rates' => 'Rates', 
	'online-booking' => 'Online booking', 
	'contact-us' => 'Contact us', 
	'policies' => 'Policies',
	'faqs' => 'Faqs', 
	'tours' => 'Tours'
);