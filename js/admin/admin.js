CCS = {};

CCS.showPublishAnim = function(message){
	
	if(!message) message = "Publishing site...Please wait...dont leave the page...";
	
	if($('#dialog-overlay').length == 0) {
		$('<div id="dialog-overlay"></div>').addClass('dialog-overlay').appendTo('body:first');
	}
	
	$('#dialog-overlay').css({
		'width':'100%',
		'height':$(document).height(),
		'opacity':0.8
	}).show();
	

	$('<div id="ftp-anim">' + message + '</div>').css({
		'position':'absolute',
		'width':400,
		'height':40,
		'background':'#c30',
		'border':'2px solid #000',
		'zIndex':300,
		'left':(($(document).width() - 400) / 2),
		'top':(($(window).height() - 50) / 2),
		'padding':'20px',
		'color':'#fff',
		'fontWeight':'bold',
		'textAlgin':'center',
		'fontSize':'14px'
	}).appendTo('body:first');
};

$(document).ready(function() {

    $('div#tab_chn').hide();
    $('div#tab_por').hide();

    $("*[name='tabs']").change(function(){

        $("[id^=tab]").hide();

        var id_name = "div#"+$(this).attr('id');

        $(id_name).show();
    });


	//no errors hide the form
	if($('#add-vehicle-container .input_error').length == 0) {
		$('#add-vehicle-container').hide();
	}
	
	if($('#add-website-container .input_error').length == 0) {
		$('#add-website-container').hide();
	}

    if($('#add-translation-container .input_error').length == 0) {
        $('#add-translation-container').hide();
    }

	$('#add-vehicle-btn').click(function() {
		$('#add-vehicle-container').slideToggle();
	});
	

	$('#add-website-btn').click(function() {
		$('#add-website-container').slideToggle();
	});

    $('#add-translation-btn').click(function() {
        $('#add-translation-container').slideToggle();
    });

	
	$('.website-actions .delete').live('click', function(evt) {
		evt.preventDefault();
		$_this = $(this);
		if(confirm('Are you sure you want to delete this item?')) {
			
			$.get($(this).attr('href'), function(data){
				if(data.success) {
					$_this.parent().parent().fadeOut('fast', function(){$(this).remove();});
				}
			}, 'json');
			
		}
	});
	
	$('#publish-btn, #test-publish-btn').click(function(evt){
	
		//alert('Publishing function temporarily disabled on LIVE. Contact Leon to re-anable.');
		//return false;
	
		if($(this).attr('id') == 'test-publish-btn') { // no need to confirmm
			CCS.showPublishAnim();
			return true;
		}
		
		if(confirm('Are you sure you want to continue, this will overwrite existing files?')) {
			
			CCS.showPublishAnim();
			
		}else{
			evt.preventDefault();
		}
	});
	//global ajax progress
	$('.global-ajax-info').ajaxStart(function() {
		$(this).fadeIn('fast');
	}).ajaxStop(function(){
		$(this).fadeOut('fast');
	});
});
