$(document).ready(function() {
	
	//business manager ajax function
	$('.restaurant-actions a.ajax').live('click', function(e) {
		
		$_this = $(this);
		//show the progress bar
		if($(this).hasClass('confirm')) {
			var message = 'Are you sure you want to delete this restaurant?, All data including images will be deleted as well.';
			if(confirm(message)) {
				
				$.get($(this).attr('href'), function(data){
					if(data.status == 'ok') {
						$_this.parent().parent().fadeOut('fast', function(){$(this).remove();});
					}
				}, 'json');
			}
		}else {
			
			$.get($(this).attr('href'), function(data){
				
				if(data.status == 'ok') {
					
					if($_this.hasClass('enable')) {//enable btn
						
						$('#restaurant-' + data.id + ' span.status')
						.removeClass('red-txt')
						.addClass('green-txt')
						.text(data.update);
						
						$_this.removeClass('enable').addClass('disable').text('Disable');
						
						$_this.attr('href', data.url);
						
					}else if($_this.hasClass('disable')) {//disable btn
						
						$('#restaurant-' + data.id + ' span.status')
						.addClass('red-txt')
						.removeClass('green-txt')
						.text(data.update);
						
						$_this.removeClass('disbale').addClass('enable').text('Enable');
						$_this.attr('href', data.url);
					}
				}
			}, 'json');
		}
		e.preventDefault();
	});
	
});