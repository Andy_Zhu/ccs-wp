function CKeditor_OnComplete(editor) {

}

CKEDITOR.config.height = 250;
CKEDITOR.config.extraPlugins = 'toolbar,maximize,toolbarswitch';
CKEDITOR.config.toolbar_Basic = [
		[ 'Bold','Italic','Underline','RemoveFormat','Link', 'Unlink','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','NumberedList','BulletedList','Image','Table','Toolbarswitch'],
];
CKEDITOR.config.toolbar_Full = [
		[ 'Source', 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
		[ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ],
		[ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ],
		[ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
		[ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','- ','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ],
		[ 'Link','Unlink','Anchor' ],
		[ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ],
		[ 'Styles','Format','Font','FontSize' ],
		[ 'TextColor','BGColor' ],
		[ 'Toolbarswitch', 'ShowBlocks', '-', 'About' ],
];
CKEDITOR.config.toolbar = 'Basic';
CKEDITOR.config.smallToolbar = 'Basic';
CKEDITOR.config.maximizedToolbar = 'Full';
CKEDITOR.config.entities = false;