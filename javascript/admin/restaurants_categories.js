$(document).ready(function() {
	
	$('.rest-cats-actions a.ajax').live('click',function(evt){
		evt.preventDefault();
		var id = $(this).attr('id').split('-')[1];
		var submitUrl = $('#update-' + id).attr('action');
		if($(this).hasClass('save')) {
			$('#action-' + id).val('update');
		}else if($(this).hasClass('delete')) {
			$('#action-' + id).val('delete');
		}
		$('#update-' + id + ' input').each(function(){
			$(this).val($(this).val());
		});
		
		var queryString = $('#update-' + id).serialize();
		
		if($(this).hasClass('confirm') && !confirm('Are you sure you want to delete this category?')) {
			
			return false;
		}
		
		$.ajax({
			type:'POST',
			url:submitUrl,
			data:queryString,
			success:function(data){
				
				if(data.status == 'ok' && data.action == 'delete') {
					$('#cat-' + data.id).fadeOut(function(){$('#cat-' + data.id).remove()})
				}
			}
		});
		
	});
	
	$('#save-all-btn').click(function(evt){
		evt.preventDefault();
		
		$('.rest-cats-actions a.save').each(function(){
			//simply trigger a click event
			$(this).click();
		});
	});
	
	//add new button
	$('#new-cat-form-container').hide();
	$('#cat-add-btn').click(function(evt){
		evt.preventDefault();
		if($('#new-cat-form-container').is(':visible')) {
			$('#new-cat-form-container').slideUp('fast');
		}else{
			$('#new-cat-form-container').slideDown('fast');
		}
	});
	
	/*$('#new-cat-form-container form input[type=submit]').click(function(evt){
		evt.preventDefault();
		var submitUrl = $('#new-cat-form-container form:first').attr('action');
		var queryString = $('#new-cat-form-container form:first').serialize();
		$.ajax({
			type:'POST',
			url:submitUrl,
			data:queryString,
			success:function(data){
				if(data.status == 'ok') {
					$(data.row).insertBefore('#rest-cats-tb tbody tr:first').addClass('new-insert');
				}
			}
		});
	});*/
});
