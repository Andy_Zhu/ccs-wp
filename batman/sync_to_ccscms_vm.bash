#!/bin/bash

[ "$1" = "DESCRIPTION" ] && cat <<EOD && exit

Syncs local working files to dev VM

EOD


rsync -avz ~/dev/carservicecms/ root@ccscms.local:/var/www/carmageddon/
